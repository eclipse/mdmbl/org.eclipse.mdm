== Integration of OpenId Connect authentication

It is possible to configure an OIDC authentication into openMDM.

Because Glassfish has no embedded OIDC integration an OIDC servlet filter adapter from pac4j is used.

You can find the whole documentation to adapter and configuration under: +
https://www.pac4j.org/4.5.x/docs/index.html

All you have to do is to set up some OIDC specific configuration.

=== Configuration Files

**Configuration file - web.xml:** + 
`org.eclipse.mdm\nucleus\application\src\main\webconfig\web.xml` +

Add OidcFilter before MDMRequestFilter (f.e.):

	<filter>
		<filter-name>OidcFilter</filter-name>
		<filter-class>org.eclipse.mdm.application.oidc.OidcFilter</filter-class>
		<init-param>
			<param-name>configFactory</param-name>
			<param-value>org.eclipse.mdm.application.oidc.OpenMDMConfigFactory</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>OidcFilter</filter-name>
		<url-pattern>/*</url-pattern>
		<dispatcher>REQUEST</dispatcher>
	</filter-mapping>

To have only OIDC as authentication servlet filter, remove or comment out the sections "securityConstraints" and "loginConfig".

**Configuration file - global.properties:** + 
`$GLASSFISH_ROOT/glassfish/domains/domain1/config/global.properties` +

To configure logout redirect uri set the parameter: +
    
	application.logoutRedirect=https://<host>:<port>/org.eclipse.mdm.nucleus/oidc/logout
    
**Configuration of connection parameter to OIDC Identity Provider ** +

Parameters that are needed to connect to Identity Provider must be placed as system properties in Glassfish WebServer: +

	org.eclipse.mdm.oidc.clients
	
Comma seperated list of authentication clients to be enabled. The following clients are available:

* `oidcClient` (mandatory) interactive authentication against Identity Provider
* `apiTokenClient` allows authentication with api tokens provided as Basic Authentication Header
* `m2mClient` allows authentication with access_tokens provided as Bearer Authentication Header

Default: `oidcClient,m2mClient` 

NOTE: To enable api token authentication use `oidcClient,apiTokenClient,m2mClient`

	org.eclipse.mdm.oidc.discoveryUri

Uri to meta data page of Identity Provider. Usually consisting of: https://<AuthServerUrl>/realms/<realmName>/.well-known/openid-configuration

	org.eclipse.mdm.oidc.contextRoot
	
Context root of MDM application. Usually: https://<host>/<port>/org.eclipse.mdm.nucleus

	org.eclipse.mdm.oidc.clientId

Id of the OpenID-Client

	org.eclipse.mdm.oidc.clientSecret
	
Secret of the OpenId-Client

	org.eclipse.mdm.oidc.rolesClaim
	
Claim from which the roles are retrieved. Must be multi-valued. Default is "groups".

	org.eclipse.mdm.oidc.role-mappings

Path to role mappings file. If empty, the role mappings are loaded from the war file /WEB-INF/role-mappings.properties.

	org.eclipse.mdm.oidc.scopes
	
Possible scopes that should be requested. F.e. "openid profile groups"

	org.eclipse.mdm.oidc.expectedAudience
	
Audience that should be present in the checked token. Multiple audiences can be specified as comma seperated list. The one of audiences in the token must at least match one audience listed in this parameter. If the parameter is empty, the audience of the token is not validated.

	org.eclipse.mdm.oidc.m2m.discoveryUri

Uri to meta data page of Identity Provider for machine-to-machine authentication. Usually consisting of: https://<AuthServerUrl>/realms/<realmName>/.well-known/openid-configuration. Defaults to `org.eclipse.mdm.oidc.discoveryUri`.

	org.eclipse.mdm.oidc.m2m.clientId

Id of the OpenID-Client for machine-to-machine authentication. Defaults to `org.eclipse.mdm.oidc.clientId`.

	org.eclipse.mdm.oidc.m2m.clientSecret
	
Secret of the OpenId-Client for machine-to-machine authentication. Defaults to `org.eclipse.mdm.oidc.clientSecret`.

	org.eclipse.mdm.oidc.m2m.useTokenInfoEndpoint
	
Can be set to `true` if token info endpoint should be used instead of introspection endpoint to validate access tokens. Default: `false`.

	org.eclipse.mdm.oidc.m2m.tokeninfoUrl
	
Url of the token info endpoint that should be used when `org.eclipse.mdm.oidc.m2m.useTokenInfoEndpoint` is `true`.