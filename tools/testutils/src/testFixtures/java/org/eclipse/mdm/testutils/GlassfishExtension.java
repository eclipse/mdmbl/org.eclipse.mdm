package org.eclipse.mdm.testutils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.glassfish.embeddable.CommandResult;
import org.glassfish.embeddable.GlassFish;
import org.glassfish.embeddable.GlassFishException;
import org.glassfish.embeddable.GlassFishProperties;
import org.glassfish.embeddable.GlassFishRuntime;
import org.glassfish.embeddable.archive.ScatteredArchive;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ConditionEvaluationResult;
import org.junit.jupiter.api.extension.ExecutionCondition;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.testcontainers.DockerClientFactory;

public class GlassfishExtension implements TestRule, BeforeAllCallback, AfterAllCallback, BeforeEachCallback,
		AfterEachCallback, ExecutionCondition, ParameterResolver {

	private static final int httpPort = 8081;
	private final String contextRoot = "org.eclipse.mdm.nucleus";

	private OdsServerContainer odsServer;
	private Path projectRoot;
	private GlassFish glassfish;
	private String appName;
	private Client client;
	private static GlassFishRuntime glassfishRuntime;
	private NewCookie sessionCookie;

	static {
		try {
			glassfishRuntime = GlassFishRuntime.bootstrap();
		} catch (GlassFishException e) {
			throw new RuntimeException("Cannot bootstrap embedded Glassfish!", e);
		}
	}

	public GlassfishExtension(OdsServerContainer odsServer) {
		this(odsServer, Paths.get("../../"));
	}

	public GlassfishExtension(OdsServerContainer odsServer, Path projectRoot) {
		this.odsServer = odsServer;
		this.projectRoot = projectRoot;
		this.client = createClient();
		bootstrapSystemVariables();
	}

	@Override
	public ConditionEvaluationResult evaluateExecutionCondition(ExtensionContext context) {
		if (odsServer.getDockerImageName().contains("busybox")) {
			return ConditionEvaluationResult.disabled("ODS Server container not available.");
		} else {
			if (DockerClientFactory.instance().isDockerAvailable()) {
				return ConditionEvaluationResult.enabled("ODS Server container created.");
			} else {
				return ConditionEvaluationResult.disabled("Docker is not available");
			}
		}
	}

	@Override
	public void beforeAll(ExtensionContext context) throws Exception {
		startGlassfish();
	}

	@Override
	public void afterAll(ExtensionContext context) throws Exception {
		stopGlassfish();
	}

	@Override
	public void beforeEach(ExtensionContext context) throws Exception {
		login();
	}

	@Override
	public void afterEach(ExtensionContext context) throws Exception {
		logout();
	}

	@Override
	public Statement apply(Statement base, Description description) {
		return new Statement() {

			@Override
			public void evaluate() throws Throwable {
				startGlassfish();
				base.evaluate();
				stopGlassfish();
			}
		};
	}

	private void startGlassfish() throws Exception {
		if (odsServer.isCreated()) {
			Path configRoot = Paths.get("../../tools/testutils/src/main/resources/config-root/").toAbsolutePath()
					.normalize();

			System.setProperty("java.security.auth.login.config",
					configRoot.resolve("login.conf").toAbsolutePath().toString());
			System.setProperty("odsserver.url", odsServer.getConnectionParameters().get("url"));

			Logger.getLogger("").getHandlers()[0].setLevel(Level.INFO);
			// Logger.getLogger("javax.enterprise.system.tools.deployment").setLevel(Level.FINEST);
			// Logger.getLogger("javax.enterprise.system").setLevel(Level.FINEST);

			GlassFishProperties glassfishProperties = new GlassFishProperties();
			glassfishProperties.setPort("http-listener", httpPort);
			glassfish = glassfishRuntime.newGlassFish(glassfishProperties);
			glassfish.start();
			Path keyfile = configRoot.resolve("mdm-keyfile");

			CommandResult r = glassfish.getCommandRunner().run("create-auth-realm",
					"--classname=com.sun.enterprise.security.auth.realm.file.FileRealm",
					"--property=file=" + keyfile.toAbsolutePath().toString().replace("\\", "/").replace(":", "\\:")
							+ ":jaas-context=MDMRealm:assign-groups=MDM",
					"MDMRealm");
			appName = glassfish.getDeployer().deploy(createDeployment().toURI(), "--contextroot=" + contextRoot);
		}
	}

	private void stopGlassfish() throws Exception {
		if (glassfish != null && appName != null) {
			glassfish.getDeployer().undeploy(appName);
			glassfish.stop();
		}
	}

	private void bootstrapSystemVariables() {
		try {
			System.setProperty("org.eclipse.mdm.configPath",
					projectRoot.resolve("tools/testutils/src/main/resources").toRealPath().toString());
		} catch (IOException e) {
			throw new RuntimeException("Cannot resolve org.eclipse.mdm.configPath.", e);
		}
	}

	private ScatteredArchive createDeployment() throws IOException {
		ScatteredArchive archive = new ScatteredArchive(contextRoot, ScatteredArchive.Type.WAR,
				projectRoot.resolve("nucleus/application/src/main/webapp").toFile());

//		archive.addMetadata(projectRoot.resolve("tools/testutils/src/main/resources/web.xml").toFile());
		archive.addMetadata(projectRoot.resolve("nucleus/application/src/main/webconfig/web.xml").toFile());
		archive.addMetadata(projectRoot.resolve("nucleus/application/src/main/webconfig/glassfish-web.xml").toFile());

		archive.addClassPath(projectRoot.resolve("nucleus/application/build/classes/java/main").toFile());
		archive.addClassPath(projectRoot.resolve("nucleus/businessobjects/build/classes/java/main").toFile());
		archive.addClassPath(projectRoot.resolve("nucleus/connector/build/classes/java/main").toFile());
		archive.addClassPath(projectRoot.resolve("nucleus/property/build/classes/java/main").toFile());
		archive.addClassPath(projectRoot.resolve("nucleus/preferences/build/classes/java/main").toFile());

//		archive.addClassPath(projectRoot.resolve("api/base/build/classes/java/main").toFile());
//		archive.addClassPath(projectRoot.resolve("api/odsadapter/build/classes/java/main").toFile());
//		archive.addClassPath(projectRoot.resolve("nucleus/webclient/build/dist").toFile());
		return archive;
	}

	public String getUri() {
		return "http://localhost:" + httpPort + "/" + contextRoot + "/";
	}

	private Client createClient() {
		ClientConfig config = new ClientConfig();
		config.connectorProvider(new ApacheConnectorProvider());
		config.property(ApacheClientProperties.DISABLE_COOKIES, false);
		config.property(ClientProperties.FOLLOW_REDIRECTS, false);
		config.register(JacksonFeature.class);
		config.register(MultiPartFeature.class);
		return ClientBuilder.newClient(config);
	}

	public WebTarget getRoot() {
		return client.target(getUri());
	}

	public NewCookie login() {
		Form form = new Form();
		form.param("j_username", "sa");
		form.param("j_password", "sa");

		Response loginResponse = getRoot().path("j_security_check").request().post(Entity.form(form));
		String body = loginResponse.readEntity(String.class);
		if (loginResponse.getStatus() != 302) {
			throw new RuntimeException("Login failed: " + body);
		} else {
			sessionCookie = loginResponse.getCookies().get("JSESSIONID");
			loginResponse.close();
			return sessionCookie;
		}
	}

	public void logout() {
		Response r = getRoot().path("mdm/logout").request().get();
		String body = r.readEntity(String.class);
		if (r.getStatus() != 302) {
			throw new RuntimeException("Logout failed: " + body);
		}

		r.close();
	}

	@Override
	public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
			throws ParameterResolutionException {
		return parameterContext.getParameter().getType().equals(NewCookie.class);
	}

	@Override
	public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
			throws ParameterResolutionException {
		return sessionCookie;
	}
}
