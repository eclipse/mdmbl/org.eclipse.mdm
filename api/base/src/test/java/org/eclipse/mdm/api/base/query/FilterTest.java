/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Iterator;

import org.eclipse.mdm.api.base.AttributeImpl;
import org.eclipse.mdm.api.base.EntityTypeImpl;
import org.eclipse.mdm.api.base.RelationImpl;
import org.eclipse.mdm.api.base.RelationImpl.AttributeType;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.model.EnumRegistry;
import org.eclipse.mdm.api.base.search.ContextState;
import org.junit.Before;
import org.junit.Test;

/**
 * Test the Filter class
 * 
 * @author Alexander Nehmer
 *
 */
public class FilterTest {

	static String[] values = new String[] { "1", "2", "3" };

	@Before
	public void init() {
		EnumRegistry.getInstance();
	}

	/**
	 * Tests ids(EntityType entityType, Collection<String> ids)
	 */
	@Test
	public void testIdsEntityType() {
		Filter filter = Filter.and();
		assertEquals(filter.stream().toArray().length, 0);

		filter = Filter.and().ids(new EntityTypeImpl(), Arrays.asList(values));

		String[] filterCondition = filter.iterator().next().getCondition().getValue().extract();

		Iterator<String> valuesIterator = Arrays.asList(values).iterator();
		Iterator<String> filterConditionIterator = Arrays.stream(filterCondition).iterator();

		while (valuesIterator.hasNext() && filterConditionIterator.hasNext()) {
			assertEquals(valuesIterator.next(), filterConditionIterator.next());
		}
	}

	/**
	 * Tests ids(Relation relation, Collection<String> ids)
	 */
	@Test
	public void testIdsRelation() {
		Filter filter = Filter.and();
		assertEquals(filter.stream().toArray().length, 0);

		filter = Filter.and().ids(new RelationImpl(AttributeType.ID), Arrays.asList(values));

		String[] filterCondition = filter.iterator().next().getCondition().getValue().extract();

		Iterator<String> valuesIterator = Arrays.asList(values).iterator();
		Iterator<String> filterConditionIterator = Arrays.stream(filterCondition).iterator();

		while (valuesIterator.hasNext() && filterConditionIterator.hasNext()) {
			assertEquals(valuesIterator.next(), filterConditionIterator.next());
		}
	}

	/**
	 * Tests id(EntityType entityType, String id)
	 */
	@Test
	public void testIdEntity() {
		Filter filter = Filter.and();
		assertEquals(filter.stream().toArray().length, 0);

		filter = Filter.and().id(new EntityTypeImpl(), "1");

		String filterCondition = filter.iterator().next().getCondition().getValue().extract();

		assertEquals(filterCondition, "1");
	}

	/**
	 * Tests id(Relation relation, String id)
	 */
	@Test
	public void testIdRelation() {
		Filter filter = Filter.and();
		assertEquals(filter.stream().toArray().length, 0);

		filter = Filter.and().id(new RelationImpl(AttributeType.ID), "1");

		String filterCondition = filter.iterator().next().getCondition().getValue().extract();

		assertEquals(filterCondition, "1");
	}

	@Test
	public void testContext() {
		Attribute first = new AttributeImpl(AttributeImpl.Type.ID);
		Filter expected = Filter.and()
				.add(ComparisonOperator.EQUAL.create(ContextState.MEASURED, first, "foo"));

		assertNotNull(expected);
		assertTrue(expected.hasContext());
		assertEquals(ContextState.MEASURED, expected.getContext());
	}

	@Test
	public void testValidMultipleContext() {
		Attribute first = new AttributeImpl(AttributeImpl.Type.ID);
		Attribute second = new AttributeImpl(AttributeImpl.Type.ID);
		Filter expected = Filter.and()
				.add(ComparisonOperator.EQUAL.create(ContextState.MEASURED, first, "foo"))
				.add(ComparisonOperator.EQUAL.create(ContextState.MEASURED, second, "bar"));

		assertNotNull(expected);
		assertTrue(expected.hasContext());
		assertEquals(ContextState.MEASURED, expected.getContext());
	}

	@Test
	public void testValidMultipleContextMerge() {
		Attribute first = new AttributeImpl(AttributeImpl.Type.ID);
		Attribute second = new AttributeImpl(AttributeImpl.Type.ID);
		Filter firstFilter = Filter.and()
				.add(ComparisonOperator.EQUAL.create(ContextState.MEASURED, first, "foo"));
		Filter secondFilter = Filter.and()
				.add(ComparisonOperator.EQUAL.create(ContextState.MEASURED, second, "foo"));

		Filter expected = Filter.and().merge(firstFilter, secondFilter);

		assertNotNull(expected);
		assertTrue(expected.hasContext());
		assertEquals(ContextState.MEASURED, expected.getContext());
	}

	@Test(expected = IllegalStateException.class)
	public void testInvalidMultipleContext() {
		Attribute first = new AttributeImpl(AttributeImpl.Type.ID);
		Attribute second = new AttributeImpl(AttributeImpl.Type.ID);
		Filter.and()
				.add(ComparisonOperator.EQUAL.create(ContextState.MEASURED, first, "foo"))
				.add(ComparisonOperator.EQUAL.create(ContextState.ORDERED, second, "bar"));

	}

	@Test(expected = IllegalStateException.class)
	public void testInvalidMultipleContextMerge() {
		Attribute first = new AttributeImpl(AttributeImpl.Type.ID);
		Attribute second = new AttributeImpl(AttributeImpl.Type.ID);
		Filter firstFilter = Filter.and()
				.add(ComparisonOperator.EQUAL.create(ContextState.MEASURED, first, "foo"));
		Filter secondFilter = Filter.and()
				.add(ComparisonOperator.EQUAL.create(ContextState.ORDERED, second, "foo"));

		Filter.and().merge(firstFilter, secondFilter);
	}
}
