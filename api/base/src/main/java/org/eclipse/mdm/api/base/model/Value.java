/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.model;

import static java.util.stream.IntStream.range;

import java.lang.reflect.Array;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.mdm.api.base.search.ContextState;

/**
 * Generic value container. This value container is tightly coupled with its
 * {@link ValueType}. It allows only to store assignment compatible values.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 */
public class Value {

	// ======================================================================
	// Class variables
	// ======================================================================

	public static final DateTimeFormatter LOCAL_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

	// ======================================================================
	// Instances variables
	// ======================================================================

	private final ValueType<?> valueType;
	private final String name;
	private final String unit;

	private boolean initialValidMeasured;
	private Object initialValueMeasured;
	private boolean initialValidOrdered;
	private Object initialValueOrdered;

	private boolean validMeasured;
	private boolean validOrdered;
	private Object measured;
	private Object ordered;

	private final Class<?> valueClass;
	private final String enumerationName;
	private final Object defaultValueMeasured;

	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param valueType            The associated {@link ValueType}.
	 * @param name                 The name of this container.
	 * @param unit                 The name of the unit.
	 * @param validMeasured        The initial validity flag for the measured value
	 * @param validOrdered         The initial validity flag for the ordered value
	 * @param valueMeasured        The initial measured value.
	 * @param valueOrdered         The initial ordered value.
	 * @param valueClass           Used for type checking upon assignment.
	 * @param defaultValueMeasured Used as null replacement.
	 * @param enumerationName      Name of the enumeration if the value is an
	 *                             EnumerationValue
	 */
	Value(ValueType<?> valueType, String name, String unit, boolean validMeasured, boolean validOrdered,
			Object valueMeasured, Object valueOrdered, Class<?> valueClass, Object defaultValueMeasured,
			String enumerationName) {
		this.valueType = valueType;
		this.enumerationName = enumerationName;
		this.name = name;
		this.unit = unit == null ? "" : unit;

		this.valueClass = valueClass;
		this.defaultValueMeasured = defaultValueMeasured;

		// set initial value
		set(ContextState.MEASURED, valueMeasured);
		set(ContextState.ORDERED, valueOrdered);
		// overwrite initial validity flag
		setValid(ContextState.MEASURED, validMeasured);
		setValid(ContextState.ORDERED, validOrdered);

		// preserve initial values
		initialValidMeasured = isValid(ContextState.MEASURED);
		initialValueMeasured = copy(extract(ContextState.MEASURED));

		initialValidOrdered = isValid(ContextState.ORDERED);
		initialValueOrdered = copy(extract(ContextState.ORDERED));
	}

	Value(ValueType<?> valueType, String name, String unit, boolean valid, Object value, Class<?> valueClass,
			Object defaultValueMeasured, String enumerationName) {
		this(valueType, name, unit, valid, false, value, null, valueClass, defaultValueMeasured, enumerationName);
	}

	/**
	 * Private constructor which will be mainly used for the merge.
	 *
	 * @param origin        the value on which the merge was initiated
	 * @param inputMeasured merged value of the measured part
	 * @param validMeasured validity flag for the measured part
	 * @param inputOrdered  merged value of the ordered part
	 * @param validOrdered  validity flag for the ordered part
	 */
	private Value(Value origin, Object inputMeasured, boolean validMeasured, Object inputOrdered,
			boolean validOrdered) {
		this(origin.valueType, origin.name, origin.unit, validMeasured, validOrdered, inputMeasured, inputOrdered,
				origin.valueClass, origin.defaultValueMeasured, origin.enumerationName);
	}

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Returns the name of this value container.
	 *
	 * @return This value container's name is returned.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the {@link ValueType} this value container is associated with.
	 *
	 * @return The associated {@code ValueType} is returned.
	 */
	public ValueType<?> getValueType() {
		return valueType;
	}

	/**
	 * Returns the unit name of this value container.
	 *
	 * @return The unit name of this value container is returned.
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Returns the validity flag of this value container. Will use measured context.
	 *
	 * @return True, if the stored value is marked to be valid.
	 */
	public boolean isValid() {
		return isValid(ContextState.MEASURED);
	}

	/**
	 * Returns the validity flag of this value container.
	 *
	 * @param contextState the related context state.
	 *
	 * @return True, if the stored value is marked to be valid.
	 */
	public boolean isValid(ContextState contextState) {
		Object v = extract(contextState);

		if (v != null) {
			if (v.getClass().isArray()) {
				return getValid(contextState) && Array.getLength(v) > 0;
			} else if (v instanceof String) {
				return getValid(contextState) && !((String) v).isEmpty();
			}
		}

		return getValid(contextState) && v != null;
	}

	/**
	 * Overwrites validity flag with given flag. Will use measured context.
	 *
	 * @param valid The new validity flag.
	 */
	public void setValid(boolean valid) {
		setValid(ContextState.MEASURED, valid);
	}

	/**
	 * Overwrites validity flag with given flag.
	 *
	 * @param contextState the related context state.
	 * @param valid        The new validity flag.
	 */
	public void setValid(ContextState contextState, boolean valid) {
		if (ContextState.ORDERED.equals(contextState)) {
			this.validOrdered = valid;
		} else {
			this.validMeasured = valid;
		}
	}

	/**
	 * Returns currently stored value of this value container. Will use measured
	 * context.
	 *
	 * @param <T>  The expected value type.
	 * @param type The {@link ValueType}.
	 * @return Currently stored value is returned.
	 */
	@SuppressWarnings("unchecked")
	public <T> T extract(ValueType<T> type) {
		return (T) measured;
	}

	/**
	 * Returns currently stored value of this value container. Will use measured
	 * context.
	 *
	 * @param <T> The expected value type.
	 * @return Currently stored value is returned.
	 */
	public <T> T extract() {
		return extract(ContextState.MEASURED);
	}

	/**
	 * Returns currently stored value of this value container.
	 *
	 * @param contextState the related context state.
	 * @return Currently stored value is returned.
	 */
	@SuppressWarnings("unchecked")
	public <T> T extract(ContextState contextState) {
		if (ContextState.ORDERED.equals(contextState)) {
			return (T) ordered;
		}
		return (T) measured;
	}

	/**
	 * Returns currently stored value of this value container, or, if value is
	 * invalid, defaultValue. Will use measured context.
	 *
	 * @param <T>          The expected value type.
	 * @param defaultValue The value to return if the currently stored value is
	 *                     invalid
	 * @return Currently stored value is returned.
	 */
	public <T> T extractWithDefault(T defaultValue) {
		return extractWithDefault(ContextState.MEASURED, defaultValue);
	}

	/**
	 * Returns currently stored value of this value container, or, if value is
	 * invalid, defaultValue.
	 *
	 * @param <T>          The expected value type.
	 * @param contextState the related context state.
	 * @param defaultValue The value to return if the currently stored value is
	 *                     invalid
	 * @return Currently stored value is returned.
	 */
	public <T> T extractWithDefault(ContextState contextState, T defaultValue) {
		return (isValid(contextState) ? extract(contextState) : defaultValue);
	}

	/**
	 * Replaces currently stored value with the given one. If {@code null} is given,
	 * then a well defined default value is used instead and the validity flag is
	 * automatically set to {@code false}. Will use measured context.
	 *
	 * @param input The new value must be an instance of the type defined in
	 *              {@link ValueType#type} or in case of an enumeration type an
	 *              appropriate enumeration constant or array thereof.
	 * @throws IllegalArgumentException Thrown if an incompatible value is given.
	 */
	public void set(Object input) {
		set(ContextState.MEASURED, input);
	}

	/**
	 * Replaces currently stored value with the given one. If {@code null} is given,
	 * then a well defined default value is used instead and the validity flag is
	 * automatically set to {@code false}.
	 *
	 * @param contextState the related context state.
	 * @param input        The new value must be an instance of the type defined in
	 *                     {@link ValueType#type} or in case of an enumeration type
	 *                     an appropriate enumeration constant or array thereof.
	 * @throws IllegalArgumentException Thrown if an incompatible value is given.
	 */
	public void set(ContextState contextState, Object input) {
		if (input == null) {
			if (ContextState.ORDERED.equals(contextState)) {
				ordered = null;
				setValid(ContextState.ORDERED, false);
			} else {
				measured = defaultValueMeasured;
				setValid(ContextState.MEASURED, false);
			}
		} else if (valueClass.isInstance(input)) {
			if (ContextState.ORDERED.equals(contextState)) {
				ordered = input;
				setValid(ContextState.ORDERED, true);
			} else {
				measured = input;
				setValid(ContextState.MEASURED, true);
			}
		} else if (input instanceof EnumerationValue && valueType.isEnumeration()) {
			setForEnumerationValue(contextState, (EnumerationValue) input);
		} else if (input instanceof EnumerationValue[] && valueType.isEnumerationSequence()) {
			setForEnumerationValues(contextState, (EnumerationValue[]) input);
		} else {
			throw new IllegalArgumentException("Incompatible value type '" + input.getClass().getSimpleName()
					+ "' passed, expected '" + valueClass.getSimpleName() + "'.");
		}
	}

	/**
	 * Will swap internal values for the measured and ordered context.
	 */
	public void swapContext() {
		Object obj = measured;
		boolean valid = validMeasured;

		measured = ordered;
		validMeasured = validOrdered;
		ordered = obj;
		validOrdered = valid;
	}

	/**
	 * Merges given value container with this instance. To be able to do so, the
	 * given value container must be compatible with this one. Value containers are
	 * compatible if the their name, unit and {@link ValueType} is equal. If the
	 * stored values or the validity flags do not match, then both values are
	 * discarded and {@code null} is taken as the initial value.
	 *
	 * @param value The value container that will be merged with this instance.
	 * @return A new value container with merged value is returned.
	 * @throws IllegalArgumentException Thrown if given value container is not
	 *                                  compatible.
	 */
	public Value merge(Value value) {
		if (!isValidMerge(value)) {
			throw new IllegalArgumentException("Unable to merge, incompatible value passed.");
		}

		Merged measuredValue = getMergedValue(value, ContextState.MEASURED);
		Merged orderedValue = getMergedValue(value, ContextState.ORDERED);

		return new Value(this, measuredValue.getValue(), measuredValue.isValid(), orderedValue.getValue(),
				orderedValue.isValid());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if (!(other instanceof Value)) {
			return false;
		}

		Value val = (Value) other;

		return Objects.equals(this.valueType, val.valueType) && Objects.equals(this.name, val.name)
				&& Objects.equals(this.unit, val.unit)
				&& Objects.equals(this.initialValidMeasured, val.initialValidMeasured)
				&& Objects.deepEquals(this.validMeasured, val.validMeasured)
				&& Objects.equals(this.validOrdered, val.validOrdered)
				&& Objects.deepEquals(this.measured, val.measured) && Objects.deepEquals(this.ordered, val.ordered)
				&& Objects.equals(this.valueClass, val.valueClass)
				&& Objects.equals(this.initialValueMeasured, val.initialValueMeasured)
				&& Objects.equals(this.initialValidOrdered, val.initialValidOrdered)
				&& Objects.equals(this.initialValueOrdered, val.initialValueOrdered)
				&& Objects.equals(this.enumerationName, val.enumerationName)
				&& Objects.equals(this.defaultValueMeasured, val.defaultValueMeasured);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(valueType, name, unit, initialValidMeasured, initialValidOrdered, initialValueMeasured,
				initialValueOrdered, validMeasured, validOrdered, measured, ordered, valueClass, enumerationName,
				defaultValueMeasured);
	}

	/**
	 * Returns a human readable {@code String} representation of this value. In case
	 * of a sequence value container with up to 10 values the complete sequence will
	 * be printed. Otherwise only the 5 first and last values will be printed. If
	 * the contained value is marked as not valid, then the contained value is
	 * omitted in the representation.
	 *
	 * @return The {@code String} representation of this value.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		boolean measuredValid = isValid();
		boolean orderedValid = isValid(ContextState.ORDERED);

		if (measuredValid && !orderedValid) {
			Object v = extract();
			sb.append(name).append(" = ");
			sb.append(valueToString(v));
		} else if (measuredValid && orderedValid) {
			Object v = extract();
			sb.append(name).append("(measured)").append(" = ");
			sb.append(valueToString(v));
		}

		if (measuredValid && orderedValid) {
			sb.append(", ");
		}

		if (orderedValid) {
			sb.append(name).append("(ordered)").append(" = ");
			Object v = extract(ContextState.ORDERED);
			sb.append(valueToString(v));
		}

		if (!getUnit().isEmpty()) {
			sb.append(" [").append(getUnit()).append(']');
		}

		return sb.toString();
	}

	/**
	 * Checks whether either the validity flag or the value have been modified since
	 * initialization.
	 *
	 * @return Returns {@code true} either if the flag or the value has been
	 *         modified.
	 */
	public boolean isModified() {
		return isModified(ContextState.MEASURED) || isModified(ContextState.ORDERED);
	}

	/**
	 * Checks whether either the validity flag or the value have been modified since
	 * initialization for the given ContextState.
	 *
	 * @param contextState {@link ContextState}
	 * @return Returns {@code true} either if the flag or the value has been
	 *         modified.
	 */
	public boolean isModified(ContextState contextState) {
		return wasValid(contextState) != isValid(contextState)
				|| !Objects.deepEquals(extractInitial(contextState), extract(contextState));
	}

	/**
	 * Returns the initial validity flag.
	 *
	 * @return Returns {@code true} if the value was initially marked as valid.
	 */
	public boolean wasValid() {
		return wasValid(ContextState.MEASURED);
	}

	/**
	 * Returns the initial validity flag.
	 *
	 * @return Returns {@code true} if the value was initially marked as valid.
	 */
	public boolean wasValid(ContextState contextState) {
		if (ContextState.ORDERED.equals(contextState)) {
			return initialValidOrdered;
		} else {
			return initialValidMeasured;
		}
	}

	/**
	 * Returns the initial value.
	 *
	 * @return The initial value is returned.
	 */
	public Object extractInitial() {
		return extractInitial(ContextState.MEASURED);
	}

	/**
	 * Returns the initial value.
	 *
	 * @return The initial value is returned.
	 */
	public Object extractInitial(ContextState contextState) {
		if (ContextState.ORDERED.equals(contextState)) {
			return initialValueOrdered;
		} else {
			return initialValueMeasured;
		}
	}

	/**
	 * Overwrites the initial validity flag and value with the current ones.
	 */
	public void apply() {
		initialValidMeasured = isValid(ContextState.MEASURED);
		initialValueMeasured = copy(extract(ContextState.MEASURED));
		initialValidOrdered = isValid(ContextState.ORDERED);
		initialValueOrdered = copy(extract(ContextState.ORDERED));
	}

	// ======================================================================
	// Package methods
	// ======================================================================

	/**
	 * Returns the {@code String} value from given array at given position.
	 *
	 * @param array The array {@code Object}.
	 * @param index The index of the required value.
	 * @return The {@code String} value of the requested value is returned.
	 */
	static String readAt(Object array, int index) {
		Object value = Array.get(array, index);
		if (value != null && byte[].class.isInstance(value)) {
			return Arrays.toString((byte[]) value);
		}

		return value == null ? "" : String.valueOf(value);
	}

	// ======================================================================
	// Private methods
	// ======================================================================

	/**
	 * Creates a string representation of the current value object.
	 *
	 * @param v the value object
	 * @return string representation
	 */
	private String valueToString(Object v) {
		StringBuilder sb = new StringBuilder();
		if (v != null && v.getClass().isArray()) {
			int length = Array.getLength(v);
			sb.append('[');

			if (length > 10) {
				sb.append(range(0, 5).mapToObj(i -> readAt(v, i)).collect(Collectors.joining(", ")));
				sb.append(", ..., ");
				sb.append(range(length - 5, length).mapToObj(i -> readAt(v, i)).collect(Collectors.joining(", ")));
			} else {
				sb.append(range(0, length).mapToObj(i -> readAt(v, i)).collect(Collectors.joining(", ")));
			}
			sb.append(']');
		} else {
			sb.append(v);
		}

		return sb.toString();
	}

	/**
	 * Returns the validity flag for the given context.
	 *
	 * @param contextState the context.
	 * @return <code>true</code> if the valid for context, <code>false</code>
	 *         otherwise.
	 */
	private boolean getValid(ContextState contextState) {
		if (ContextState.ORDERED.equals(contextState)) {
			return validOrdered;
		} else {
			return validMeasured;
		}
	}

	/**
	 * Returns a copy of given {@code Object}, so modifications in one do not affect
	 * to other.
	 *
	 * @param value The object which will be copied.
	 * @return The copy is returned.
	 */
	private static Object copy(Object value) {
		if (value == null) {
			return null;
		}

		Class<?> valueClass = value.getClass();
		if (valueClass.isArray() && Array.getLength(value) > 0) {
			return createDeepCopy(value, valueClass);
		} else if (value instanceof FileLink) {
			return new FileLink((FileLink) value);
		}

		// simple immutable value
		return value;
	}

	public String getEnumName() {
		return enumerationName;
	}

	/**
	 * Replaces currently stored value with the given one.
	 *
	 * @param input The new value must be an instance of the enumeration type an
	 *              appropriate enumeration constant.
	 * @throws IllegalArgumentException Thrown if an incompatible value is given.
	 */
	private void setForEnumerationValue(ContextState contextState, EnumerationValue input) {
		String inpvalueTypeDescr = input.getOwner().getName();
		if (inpvalueTypeDescr == null) {
			throw new IllegalArgumentException(
					"EnumerationValue value description of input value not correctly initialized");
		}
		if (enumerationName == null) {
			throw new IllegalArgumentException(
					"EnumerationValue value description not correctly initialized got null, '" + "' expected '"
							+ valueClass.getSimpleName() + "'.");
		}

		if (enumerationName.equals(inpvalueTypeDescr)) {
			if (ContextState.ORDERED.equals(contextState)) {
				ordered = input;
				setValid(ContextState.ORDERED, true);
			} else {
				measured = input;
				setValid(ContextState.MEASURED, true);
			}
		} else {
			throw new IllegalArgumentException("Incompatible value type description'" + inpvalueTypeDescr
					+ "' passed, expected '" + enumerationName + "'.");
		}
	}

	/**
	 * Replaces currently stored value with the given one.
	 *
	 * @param input The new value must be an instance of the enumeration type an
	 *              appropriate enumeration constant.
	 * @throws IllegalArgumentException Thrown if an incompatible value is given.
	 */
	private void setForEnumerationValues(ContextState contextState, EnumerationValue[] input) {
		if (input.length == 0) {
			if (ContextState.ORDERED.equals(contextState)) {
				ordered = null;
				setValid(ContextState.ORDERED, false);
			} else {
				measured = null;
				setValid(ContextState.MEASURED, false);
			}
		} else {
			List<String> l = Stream.of(input).map(i -> i.getOwner().getName()).distinct().collect(Collectors.toList());
			if (l.isEmpty()) {
				throw new IllegalArgumentException(
						"EnumerationValue value description of input value not correctly initialized");
			}
			if (l.size() > 1) {
				throw new IllegalArgumentException("EnumerationValues have to be from one Enumeration, but got: " + l);
			}

			String inpvalueTypeDescr = l.get(0);
			if (inpvalueTypeDescr == null) {
				throw new IllegalArgumentException(
						"EnumerationValue value description of input value not correctly initialized");
			}
			if (enumerationName == null) {
				throw new IllegalArgumentException(
						"EnumerationValue value description not correctly initialized got null, '" + "' expected '"
								+ valueClass.getSimpleName() + "'.");
			}

			if (enumerationName.equals(inpvalueTypeDescr)) {
				if (ContextState.ORDERED.equals(contextState)) {
					ordered = input;
					setValid(ContextState.ORDERED, true);
				} else {
					measured = input;
					setValid(ContextState.MEASURED, true);
				}
			} else {
				throw new IllegalArgumentException("Incompatible value type description'" + inpvalueTypeDescr
						+ "' passed, expected '" + enumerationName + "'.");
			}
		}
	}

	/**
	 * Check wether the given value can be merged with the current one or not. Name,
	 * unit and type must match.
	 *
	 * @param value the value which should be checked.
	 * @return <code>true</code> if name, unit and type match, <code>false</code>
	 *         otherwise.
	 */
	private boolean isValidMerge(Value value) {
		boolean nameMatch = getName().equals(value.getName());
		boolean unitMatch = getUnit().equals(value.getUnit());
		boolean typeMatch = getValueType().equals(value.getValueType());

		return nameMatch && unitMatch && typeMatch;
	}

	/**
	 * Returns the {@link Merged} representation for a given value and the context.
	 *
	 * @param value        value which should be used.
	 * @param contextState the context.
	 * @return the merged representation
	 */
	private Merged getMergedValue(Value value, ContextState contextState) {
		Merged result = new Merged(null, false);

		boolean equalValues = Objects.deepEquals(extract(contextState), value.extract(contextState));

		boolean bothValid = isValid(contextState) && value.isValid(contextState);
		boolean sourceValid = isValid(contextState) && !value.isValid(contextState);
		boolean targetValid = !isValid(contextState) && value.isValid(contextState);

		if ((equalValues && bothValid) || sourceValid) {
			Object obj = extract(contextState);
			boolean valid = isValid(contextState);
			result = new Merged(obj, valid);
		} else if (targetValid) {
			Object obj = value.extract(contextState);
			boolean valid = value.isValid(contextState);
			result = new Merged(obj, valid);
		}

		return result;
	}

	/**
	 * Returns a deep copy of given {@code Object}, so modifications in one do not
	 * affect to other.
	 *
	 * @param value      The object which will be copied.
	 * @param valueClass The class of the value object.
	 * @return The copy is returned.
	 */
	private static Object createDeepCopy(Object value, Class<?> valueClass) {
		int length = Array.getLength(value);

		if (valueClass.getComponentType().isPrimitive()) {
			Object copy = Array.newInstance(valueClass.getComponentType(), length);
			System.arraycopy(value, 0, copy, 0, length);
			return copy;
		} else {
			if (value instanceof byte[][]) {
				return Arrays.stream((byte[][]) value).map(byte[]::clone).toArray(byte[][]::new);
			} else if (value instanceof FileLink[]) {
				return Arrays.stream((FileLink[]) value).map(FileLink::new).toArray(FileLink[]::new);
			} else {
				return Arrays.copyOf((Object[]) value, length);
			}
		}
	}

	/**
	 * Internal class which allows to transport a value and validity flag during
	 * {@link Value#merge(Value)}.
	 */
	private static class Merged {
		Object value;
		boolean valid;

		public Merged(final Object value, final boolean valid) {
			this.value = value;
			this.valid = valid;
		}

		public Object getValue() {
			return value;
		}

		public boolean isValid() {
			return valid;
		}
	}
}
