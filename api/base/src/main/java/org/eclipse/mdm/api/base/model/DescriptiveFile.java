/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.model;

import org.eclipse.mdm.api.base.adapter.Core;

public class DescriptiveFile extends MDMFile {
	public static String TYPE_NAME = "DescriptiveFile";

	public static String ATTR_TEMPLATE_ATTRIBUTE = "TemplateAttribute";

	protected DescriptiveFile(Core core) {
		super(core);
	}
}
