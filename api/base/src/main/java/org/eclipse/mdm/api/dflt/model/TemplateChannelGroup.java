/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.dflt.model;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Sortable;

/**
 * Entity TemplateChannelGroup
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
public class TemplateChannelGroup extends BaseEntity implements Deletable, Sortable {

	public static final String ATTR_MINNOROWS = "MinNoRows";
	public static final String ATTR_MAXNOROWS = "MaxNoRows";
	public static final String ATTR_TPLMEARESULT = "TplMeaResult";

	protected TemplateChannelGroup(Core core) {
		super(core);
	}

	public Short getMinNoRows() {
		return getValue(ATTR_MINNOROWS).extract();
	}

	public void setMinNoRows(Short minNoRows) {
		getValue(ATTR_MINNOROWS).set(minNoRows);
	}

	public Short getMaxNoRows() {
		return getValue(ATTR_MAXNOROWS).extract();
	}

	public void setMaxNoRows(Short maxNoRows) {
		getValue(ATTR_MAXNOROWS).set(maxNoRows);
	}

	public Integer getTplMeaResult() {
		return getValue(ATTR_TPLMEARESULT).extract();
	}

	public void setTplMeaResult(Integer tplMeaResult) {
		getValue(ATTR_TPLMEARESULT).set(tplMeaResult);
	}

	public void addItem(TemplateMeasurementQuantity tplMeaQuantiy) {
		getCore().getNtoMStore().add("TplMeaQuantity", tplMeaQuantiy);
	}

	public void removeItem(TemplateMeasurementQuantity tplMeaQuantiy) {
		getCore().getNtoMStore().remove("TplMeaQuantity", tplMeaQuantiy);
	}
}
