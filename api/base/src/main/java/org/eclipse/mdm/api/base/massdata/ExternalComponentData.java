/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.massdata;

import java.util.Objects;

import org.eclipse.mdm.api.base.model.ExtCompFile;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.TypeSpecification;

/**
 * Configuration used for storing measured values in externally referenced
 * files.
 * 
 * @author Matthias Koller, Peak Solution GmbH
 * @author Martin Fleischer, Peak Solution GmbH
 *
 */
public final class ExternalComponentData {

	private TypeSpecification typeSpec;
	private int length;
	private Long startOffset;
	private Integer blocksize;
	private Integer valuesPerBlock;
	private Integer valueOffset;
	private FileLink fileLink;
	private FileLink flagsFileLink;
	private String valuesExtCompFileId;
	private String flagsExtCompFileId;
	private ExtCompFile valuesExtCompFile;
	private ExtCompFile flagsExtCompFile;
	private Long flagsStartOffset;
	private Short bitCount;
	private Short bitOffset;

	/**
	 * Constructor.
	 */
	public ExternalComponentData() {
	}

	/**
	 * Constructor
	 * 
	 * @param typeSpec         The {@link TypeSpecification} for this instance.
	 * @param length           The length for this instance.
	 * @param startOffset      The start offset for this instance.
	 * @param blocksize        The block size for this instance.
	 * @param valuesPerBlock   The values per block for this instance.
	 * @param valueOffset      The value offset for this instance.
	 * @param fileLink         The values {@link FileLink} for this instance.
	 * @param flagsFileLink    The flags {@link FileLink} for this instance.
	 * @param valuesExtCompFileId The ID of the values {@link ExtCompFile} attached to this instance.
	 * @param flagsExtCompFileId The ID of the flags {@link ExtCompFile} attached to this instance.
	 * @param flagsFileLink    The flags {@link FileLink} for this instance.
	 * @param flagsStartOffset The flags start offset for this instance.
	 * @param bitCount         The bit count for this instance.
	 * @param bitOffset        The bit offset for this instance.
	 */
	public ExternalComponentData(TypeSpecification typeSpec, int length, Object startOffset, Integer blocksize,
			Integer valuesPerBlock, Integer valueOffset, FileLink fileLink, FileLink flagsFileLink, 
			String valuesExtCompFileId, String flagsExtCompFileId, Object flagsStartOffset, Short bitCount, 
			Short bitOffset) {
		this.typeSpec = typeSpec;
		this.length = length;
		this.startOffset = toLong(startOffset);
		this.blocksize = blocksize;
		this.valuesPerBlock = valuesPerBlock;
		this.valueOffset = valueOffset;
		this.fileLink = fileLink;
		this.flagsFileLink = flagsFileLink;
		this.valuesExtCompFileId = valuesExtCompFileId;
		this.flagsExtCompFileId = flagsExtCompFileId;
		this.flagsStartOffset = toLong(flagsStartOffset);
		this.bitCount = bitCount;
		this.bitOffset = bitOffset;
	}

	/**
	 * Sets the {@link TypeSpecification} for this instance.
	 * 
	 * @param typeSpec The {@link TypeSpecification} to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setTypeSpecification(TypeSpecification typeSpec) {
		this.typeSpec = typeSpec;
		return this;
	}

	/**
	 * Sets the length for this instance.
	 * 
	 * @param length The length to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setLength(int length) {
		this.length = length;
		return this;
	}

	/**
	 * Sets the start offset for this instance.
	 * 
	 * @param startOffset The start offset to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setStartOffset(Long startOffset) {
		this.startOffset = startOffset;
		return this;
	}

	/**
	 * Sets the block size for this instance.
	 * 
	 * @param blocksize The block size to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setBlocksize(Integer blocksize) {
		this.blocksize = blocksize;
		return this;
	}

	/**
	 * Sets the values per block for this instance.
	 * 
	 * @param valuesPerBlock The values per block to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setValuesPerBlock(Integer valuesPerBlock) {
		this.valuesPerBlock = valuesPerBlock;
		return this;
	}

	/**
	 * Sets the value offset for this instance.
	 * 
	 * @param valueOffset The value offset to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setValueOffset(Integer valueOffset) {
		this.valueOffset = valueOffset;
		return this;
	}

	/**
	 * Sets the values {@link FileLink} for this instance.
	 * 
	 * @param fileLink The values {@FileLink} to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setFileLink(FileLink fileLink) {
		this.fileLink = fileLink;
		return this;
	}

	/**
	 * Sets the flags {@link FileLink} for this instance.
	 * 
	 * @param flagsFileLink The flags {@link FileLink} to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setFlagsFileLink(FileLink flagsFileLink) {
		this.flagsFileLink = flagsFileLink;
		return this;
	}
	
	/**
	 * Sets the ID of the values {@link ExtCompFile} attached to this instance.
	 * 
	 * @param valuesExtCompFileId The ID of the values {ExtCompFile} to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setValuesExtCompFileId(String valuesExtCompFileId) {
		this.valuesExtCompFileId = valuesExtCompFileId;
		return this;
	}

	/**
	 * Sets the ID of the flags {@link ExtCompFile} attached to this instance.
	 * 
	 * @param flagsExtCompFileId The ID of the flags {ExtCompFile} to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setFlagsExtCompFileId(String flagsExtCompFileId) {
		this.flagsExtCompFileId = flagsExtCompFileId;
		return this;
	}
	
	/**
	 * Sets the values {@link ExtCompFile} attached to this instance.
	 * 
	 * @param valuesExtCompFile The values {ExtCompFile} to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setValuesExtCompFile(ExtCompFile valuesExtCompFile) {
		this.valuesExtCompFile = valuesExtCompFile;
		return this;
	}

	/**
	 * Sets the flags {@link ExtCompFile} attached to this instance.
	 * 
	 * @param flagsExtCompFile The flags {ExtCompFile} to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setFlagsExtCompFile(ExtCompFile flagsExtCompFile) {
		this.flagsExtCompFile = flagsExtCompFile;
		return this;
	}

	/**
	 * Sets the flags start offset for this instance.
	 * 
	 * @param flagsStartOffset The flags start offset to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setFlagsStartOffset(Long flagsStartOffset) {
		this.flagsStartOffset = flagsStartOffset;
		return this;
	}

	/**
	 * Sets the bit count for this instance.
	 * 
	 * @param bitCount The bit count to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setBitCount(Short bitCount) {
		this.bitCount = bitCount;
		return this;
	}

	/**
	 * Sets the bit offset for this instance.
	 * 
	 * @param bitOffset The bitOffset to set.
	 * @return A reference to this ExternalComponent instance.
	 */
	public ExternalComponentData setBitOffset(Short bitOffset) {
		this.bitOffset = bitOffset;
		return this;
	}

	/**
	 * Gets the {@link TypeSpecification} of this instance.
	 * 
	 * @return The {@link TypeSpecification} of this instance.
	 */
	public TypeSpecification getTypeSpecification() {
		return typeSpec;
	}

	/**
	 * Gets the length of this instance.
	 * 
	 * @return The length of this instance.
	 */
	public int getLength() {
		return length;
	}

	/**
	 * Gets the start offset of this instance.
	 * 
	 * @return The start offset of this instance.
	 */
	public Long getStartOffset() {
		return startOffset;
	}

	/**
	 * Gets the block size of this instance.
	 * 
	 * @return The block size of this instance.
	 */
	public Integer getBlocksize() {
		return blocksize;
	}

	/**
	 * Gets the values per block of this instance.
	 * 
	 * @return The values per block of this instance.
	 */
	public Integer getValuesPerBlock() {
		return valuesPerBlock;
	}

	/**
	 * Gets the value offset of this instance.
	 * 
	 * @return The value offset of this instance.
	 */
	public Integer getValueOffset() {
		return valueOffset;
	}

	/**
	 * Gets the values {@link FileLink} of this instance.
	 * 
	 * @return The values {@link FileLink} of this instance.
	 */
	public FileLink getFileLink() {
		return fileLink;
	}

	/**
	 * Gets the flags {@link FileLink} of this instance.
	 * 
	 * @return The flags {@link FileLink} of this instance.
	 */
	public FileLink getFlagsFileLink() {
		return flagsFileLink;
	}
	
	/**
	 * Gets the ID of the values {@link ExtCompFile} attached to this instance.
	 * 
	 * @return The ID of the values {@link ExtCompFile} attached to this instance.
	 */
	public String getValuesExtCompFileId() {
		return valuesExtCompFileId;
	}
	
	/**
	 * Gets the ID of the flags {@link ExtCompFile} attached to this instance.
	 * 
	 * @return The ID of the flags {@link ExtCompFile} attached to this instance.
	 */
	public String getFlagsExtCompFileId() {
		return flagsExtCompFileId;
	}
	
	/**
	 * Gets the values {@link ExtCompFile} attached to this instance.
	 * 
	 * @return The values {@link ExtCompFile} attached to this instance.
	 */
	public ExtCompFile getValuesExtCompFile() {
		return valuesExtCompFile;
	}
	
	/**
	 * Gets the flags {@link ExtCompFile} attached to this instance.
	 * 
	 * @return The flags {@link ExtCompFile} attached to this instance.
	 */
	public ExtCompFile getFlagsExtCompFile() {
		return flagsExtCompFile;
	}

	/**
	 * Gets the flags start offset of this instance.
	 * 
	 * @return The flags start offset of this instance.
	 */
	public Long getFlagsStartOffset() {
		return flagsStartOffset;
	}

	/**
	 * Gets the bit offset of this instance.
	 * 
	 * @return The bit offset of this instance.
	 */
	public Short getBitOffset() {
		return bitOffset;
	}

	/**
	 * Gets the bit count of this instance.
	 * 
	 * @return The bit count of this instance.
	 */
	public Short getBitCount() {
		return bitCount;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof ExternalComponentData))
			return false;
		ExternalComponentData other = (ExternalComponentData) o;

		return Objects.equals(typeSpec, other.getTypeSpecification()) 
				&& Objects.equals(length, other.getLength())
				&& Objects.equals(startOffset, other.getStartOffset())
				&& Objects.equals(blocksize, other.getBlocksize())
				&& Objects.equals(valuesPerBlock, other.getValuesPerBlock())
				&& Objects.equals(valueOffset, other.getValueOffset())
				&& Objects.equals(fileLink, other.getFileLink())
				&& Objects.equals(flagsFileLink, other.getFlagsFileLink())
				&& Objects.equals(valuesExtCompFileId, other.getValuesExtCompFileId())
				&& Objects.equals(flagsExtCompFileId, other.getFlagsExtCompFileId())
				&& Objects.equals(valuesExtCompFile, other.getValuesExtCompFile())
				&& Objects.equals(flagsExtCompFile, other.getFlagsExtCompFile())
				&& Objects.equals(flagsStartOffset, other.getFlagsStartOffset())
				&& Objects.equals(bitCount, other.getBitCount()) 
				&& Objects.equals(bitOffset, other.getBitOffset());
	}

	@Override
	public int hashCode() {
		return Objects.hash(typeSpec, length, startOffset, blocksize, valuesPerBlock, valueOffset,
				fileLink, flagsFileLink, valuesExtCompFileId, flagsExtCompFileId, flagsStartOffset, bitCount, bitOffset);
	}
	
	private static Long toLong(Object obj) {
		if (obj instanceof Long) {
			return (Long) obj;
		} else if (obj instanceof Integer) {
			return ((Integer) obj).longValue();
		} else if (obj instanceof Short) {
			return ((Short) obj).longValue();
		}
		
		return null;
	}
}
