/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.query;

import java.util.Arrays;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.search.ContextState;

/**
 * The selection operations define query instructions like "equal", "greater",
 * etc. Such operations are case sensitive. If an insensitive instruction is
 * required, then its counterpart starting with the "CI_" prefix has to be used.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 * @see Condition
 * @see Filter
 */
public enum ComparisonOperator {

	// ======================================================================
	// Enumerations
	// ======================================================================

	/**
	 * Exact match
	 */
	EQUAL,

	/**
	 * Not equal (!=)
	 */
	NOT_EQUAL,

	/**
	 * Less than (&lt;)
	 */
	LESS_THAN,

	/**
	 * Less than equal (&lt;=)
	 */
	LESS_THAN_OR_EQUAL,

	/**
	 * Greater than (&gt;)
	 */
	GREATER_THAN,

	/**
	 * Greater than equal (&gt;=)
	 */
	GREATER_THAN_OR_EQUAL,

	/**
	 * In set, value can be a sequence.
	 */
	IN_SET,

	/**
	 * Not in set, value can be a sequence.
	 */
	NOT_IN_SET,

	/**
	 * like, use pattern matching, see Pattern for the wildcard definitions.
	 */
	LIKE,

	/**
	 * Not LIKE
	 */
	NOT_LIKE,

	/**
	 * Equal. case insensitive for {@link ValueType#STRING}
	 */
	CASE_INSENSITIVE_EQUAL,

	/**
	 * Not equal. case insensitive for {@link ValueType#STRING}
	 */
	CASE_INSENSITIVE_NOT_EQUAL,

	/**
	 * Less than. case insensitive for {@link ValueType#STRING}
	 */
	CASE_INSENSITIVE_LESS_THAN,

	/**
	 * Less than equal. case insensitive for {@link ValueType#STRING}
	 */
	CASE_INSENSITIVE_LESS_THAN_OR_EQUAL,

	/**
	 * Greater than. case insensitive for {@link ValueType#STRING}
	 */
	CASE_INSENSITIVE_GREATER_THAN,

	/**
	 * Greater than equal. case insensitive for {@link ValueType#STRING}
	 */
	CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL,

	/**
	 * In set, value can be a sequence. case insensitive for
	 * {@link ValueType#STRING}
	 */
	CASE_INSENSITIVE_IN_SET,

	/**
	 * Not in set, value can be a sequence. case insensitive for
	 * {@link ValueType#STRING}
	 */
	CASE_INSENSITIVE_NOT_IN_SET,

	/**
	 * like, use pattern matching, see Pattern for the wildcard definitions. case
	 * insensitive for DT_STRING.
	 */
	CASE_INSENSITIVE_LIKE,

	/**
	 * Not LIKE, case insensitive for DT_STRING.
	 */
	CASE_INSENSITIVE_NOT_LIKE,

	/**
	 * Value is NULL
	 */
	IS_NULL,

	/**
	 * Value is not NULL
	 */
	IS_NOT_NULL,

	/**
	 * Between; selects all instances where the value of 'attr' lies between the
	 * first two values given in 'value' (with 'attr', 'value' being elements of the
	 * SelValue structure; 'value' must be of data type S_*).
	 */
	BETWEEN;

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Creates a new {@link Condition} for given {@link Attribute} and value.
	 *
	 * @param attribute The {@code Attribute} the condition will be applied to.
	 * @param value     The value.
	 * @return The created {@code Condition} is returned.
	 */
	public Condition create(Attribute attribute, Object value) {
		return new Condition(attribute, this, "", value);
	}

	/**
	 * Creates a new {@link Condition} for given {@link Attribute} and value.
	 *
	 * <p>
	 * Be aware: Right now filter only support one {@link ContextState} per filter.
	 * {@link Filter#merge} and {@link Filter#add} will throw an exception when you
	 * try to use different {@link ContextState} within a filter.
	 * </p>
	 *
	 * @param contextState The {@link ContextState} which will be applied to the
	 *                     condition.
	 * @param attribute    The {@code Attribute} the condition will be applied to.
	 * @param value        The value.
	 * @return The created {@code Condition} is returned.
	 */
	public Condition create(ContextState contextState, Attribute attribute, Object value) {
		return new Condition(contextState, attribute, this, "", value);
	}

	// ======================================================================
	// Package methods
	// ======================================================================

	/**
	 * Checks whether this operation requires a sequence value container. This is
	 * the case for the following types:
	 *
	 * <ul>
	 * <li>{@link ComparisonOperator#IN_SET}</li>
	 * <li>{@link ComparisonOperator#NOT_IN_SET}</li>
	 * <li>{@link ComparisonOperator#CASE_INSENSITIVE_IN_SET}</li>
	 * <li>{@link ComparisonOperator#CASE_INSENSITIVE_NOT_IN_SET}</li>
	 * <li>{@link ComparisonOperator#BETWEEN}</li>
	 * </ul>
	 *
	 * @return True if this operation is one of those listed above.
	 */
	public boolean requiresSequence() {
		return Arrays.asList(IN_SET, NOT_IN_SET, CASE_INSENSITIVE_IN_SET, CASE_INSENSITIVE_NOT_IN_SET, BETWEEN)
				.contains(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	public static ComparisonOperator fromString(String operator) {
		switch (operator.toLowerCase()) {
		case "bw":
			return BETWEEN;
		case "ci_eq":
			return CASE_INSENSITIVE_EQUAL;
		case "ci_gt":
			return CASE_INSENSITIVE_GREATER_THAN;
		case "ci_ge":
			return CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL;
		case "ci_in":
			return CASE_INSENSITIVE_IN_SET;
		case "ci_lt":
			return CASE_INSENSITIVE_LESS_THAN;
		case "ci_le":
			return CASE_INSENSITIVE_LESS_THAN_OR_EQUAL;
		case "ci_lk":
			return CASE_INSENSITIVE_LIKE;
		case "ci_ne":
			return CASE_INSENSITIVE_NOT_EQUAL;
		case "ci_not_in":
			return CASE_INSENSITIVE_NOT_IN_SET;
		case "ci_not_lk":
			return CASE_INSENSITIVE_NOT_LIKE;
		case "eq":
			return EQUAL;
		case "gt":
			return GREATER_THAN;
		case "ge":
			return GREATER_THAN_OR_EQUAL;
		case "in":
			return IN_SET;
		case "is_not_null":
			return IS_NOT_NULL;
		case "is_null":
			return IS_NULL;
		case "lt":
			return LESS_THAN;
		case "le":
			return LESS_THAN_OR_EQUAL;
		case "lk":
			return LIKE;
		case "ne":
			return NOT_EQUAL;
		case "not_in":
			return NOT_IN_SET;
		case "not_lk":
			return NOT_LIKE;
		default:
			throw new IllegalArgumentException(
					"The value '" + operator + "' does not map to a valid comparison operator.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Enum#toString()
	 */
	@Override
	public String toString() {
		switch (this) {
		case BETWEEN:
			return "bw";
		case CASE_INSENSITIVE_EQUAL:
			return "ci_eq";
		case CASE_INSENSITIVE_GREATER_THAN:
			return "ci_gt";
		case CASE_INSENSITIVE_GREATER_THAN_OR_EQUAL:
			return "ci_ge";
		case CASE_INSENSITIVE_IN_SET:
			return "ci_in";
		case CASE_INSENSITIVE_LESS_THAN:
			return "ci_lt";
		case CASE_INSENSITIVE_LESS_THAN_OR_EQUAL:
			return "ci_le";
		case CASE_INSENSITIVE_LIKE:
			return "ci_lk";
		case CASE_INSENSITIVE_NOT_EQUAL:
			return "ci_ne";
		case CASE_INSENSITIVE_NOT_IN_SET:
			return "ci_not_in";
		case CASE_INSENSITIVE_NOT_LIKE:
			return "ci_not_lk";
		case EQUAL:
			return "eq";
		case GREATER_THAN:
			return "gt";
		case GREATER_THAN_OR_EQUAL:
			return "ge";
		case IN_SET:
			return "in";
		case IS_NOT_NULL:
			return "is_not_null";
		case IS_NULL:
			return "is_null";
		case LESS_THAN:
			return "lt";
		case LESS_THAN_OR_EQUAL:
			return "le";
		case LIKE:
			return "lk";
		case NOT_EQUAL:
			return "ne";
		case NOT_IN_SET:
			return "not_in";
		case NOT_LIKE:
			return "not_lk";
		default:
			return this.toString();
		}
	}

}
