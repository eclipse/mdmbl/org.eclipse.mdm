/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.massdata;

import java.util.Arrays;
import java.util.List;

import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;

/**
 * This builder adds {@link ExternalComponentData}s to the {@link WriteRequest} . It
 * is possible to add a sequence where all of its values are valid.
 *
 */
public class ExternalComponentBuilder extends BaseValuesBuilder {

	/**
	 * Constructor.
	 *
	 * @param writeRequest The {@link WriteRequest} given {@link ExternalComponentData}s
	 *                     will be added to.
	 */
	ExternalComponentBuilder(WriteRequest writeRequest) {
		super(writeRequest);
	}

	public UnitIndependentBuilder externalComponent(ScalarType scalarType, ExternalComponentData externalComponent) {
		return externalComponents(scalarType, null,
				externalComponent == null ? null : Arrays.asList(externalComponent));
	}

	public UnitIndependentBuilder externalComponents(ScalarType scalarType, Short globalFlag,
			ExternalComponentData externalComponent) {
		return externalComponents(scalarType, globalFlag,
				externalComponent == null ? null : Arrays.asList(externalComponent));
	}

	public UnitIndependentBuilder externalComponents(ScalarType scalarType,
			List<ExternalComponentData> externalComponents) {
		return externalComponents(scalarType, null, externalComponents);
	}

	public UnitIndependentBuilder externalComponents(ScalarType scalarType, Short globalFlag,
			List<ExternalComponentData> externalComponents) {
		SequenceRepresentation seqRep = getWriteRequest().getSequenceRepresentation();
		if (SequenceRepresentation.EXPLICIT.equals(seqRep)) {
			seqRep = SequenceRepresentation.EXPLICIT_EXTERNAL;
		} else if (SequenceRepresentation.RAW_LINEAR.equals(seqRep)) {
			seqRep = SequenceRepresentation.RAW_LINEAR_EXTERNAL;
		} else if (SequenceRepresentation.RAW_LINEAR_CALIBRATED.equals(seqRep)) {
			seqRep = SequenceRepresentation.RAW_LINEAR_CALIBRATED_EXTERNAL;
		} else if (SequenceRepresentation.RAW_POLYNOMIAL.equals(seqRep)) {
			seqRep = SequenceRepresentation.RAW_POLYNOMIAL_EXTERNAL;
		} else if (null == seqRep || !seqRep.isExternal()) {
			seqRep = SequenceRepresentation.EXPLICIT_EXTERNAL;
		}

		WriteRequest writeRequest = getWriteRequest();
		writeRequest.setSequenceRepresentation(seqRep);
		writeRequest.setRawScalarType(scalarType);
		writeRequest.setGlobalFlag(globalFlag);
		if (externalComponents != null) {
			for (ExternalComponentData externalComponent : externalComponents) {
				writeRequest.addExternalComponent(externalComponent);
			}
		}

		return new UnitIndependentBuilder(writeRequest);
	}
}
