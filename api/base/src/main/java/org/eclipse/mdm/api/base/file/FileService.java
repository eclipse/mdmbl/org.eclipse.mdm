/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.file;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Collection;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.FileLink;

/**
 * Provides stream and download operations to access (externally) linked files.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public interface FileService {

	public enum FileServiceType {
		EXTREF, AOFILE
	}

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Sequential download of given {@link FileLink}s into given target {@code
	 * Path}. Remote paths linked multiple times are downloaded only once.
	 *
	 * @param entity      Used for security checks.
	 * @param target      Must be a directory.
	 * @param fileLinks   Collection of {@code FileLink}s to download.
	 * @param transaction The {@link Transaction} to use.
	 * @throws IOException Thrown if unable to download files.
	 */
	default void downloadSequential(Entity entity, Path target, Collection<FileLink> fileLinks, Transaction transaction)
			throws IOException {
		downloadSequential(entity, target, fileLinks, transaction, null);
	}

	/**
	 * Sequential download of given {@link FileLink}s into given target {@code
	 * Path}. Remote paths linked multiple times are downloaded only once. The
	 * download progress may be traced with a progress listener.
	 *
	 * @param entity           Used for security checks.
	 * @param target           Must be a directory.
	 * @param fileLinks        Collection of {@code FileLink}s to download.
	 * @param transaction      The {@link Transaction} to use.
	 * @param progressListener The progress listener.
	 * @throws IOException Thrown if unable to download files.
	 */
	void downloadSequential(Entity entity, Path target, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException;

	/**
	 * Parallel download of given {@link FileLink}s into given target {@code
	 * Path}. Remote paths linked multiple times are downloaded only once.
	 *
	 * @param entity      Used for security checks.
	 * @param target      Must be a directory.
	 * @param fileLinks   Collection of {@code FileLink}s to download.
	 * @param transaction The {@link Transaction} to use.
	 * @throws IOException Thrown if unable to download files.
	 */
	default void downloadParallel(Entity entity, Path target, Collection<FileLink> fileLinks, Transaction transaction)
			throws IOException {
		downloadParallel(entity, target, fileLinks, transaction, null);
	}

	/**
	 * Parallel download of given {@link FileLink}s into given target {@code
	 * Path}. Remote paths linked multiple times are downloaded only once. The
	 * download progress may be traced with a progress listener.
	 *
	 * @param entity           Used for security checks.
	 * @param target           Must be a directory.
	 * @param fileLinks        Collection of {@code FileLink}s to download.
	 * @param transaction      The {@link Transaction} to use.
	 * @param progressListener The progress listener.
	 * @throws IOException Thrown if unable to download files.
	 */
	void downloadParallel(Entity entity, Path target, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException;

	/**
	 * Downloads given {@link FileLink} into given target {@code Path}.
	 *
	 * @param entity      Used for security checks.
	 * @param target      Must be a directory.
	 * @param fileLink    The {@code FileLink} to download.
	 * @param transaction The {@link Transaction} to use.
	 * @throws IOException Thrown if unable to download file.
	 */
	default void download(Entity entity, Path target, FileLink fileLink, Transaction transaction) throws IOException {
		download(entity, target, fileLink, transaction, null);
	}

	/**
	 * Downloads given {@link FileLink} into given target {@code Path}. The download
	 * progress may be traced with a progress listener.
	 *
	 * @param entity           Used for security checks.
	 * @param target           Must be a directory.
	 * @param fileLink         The {@code FileLink} to download.
	 * @param transaction      The {@link Transaction} to use.
	 * @param progressListener The progress listener.
	 * @throws IOException Thrown if unable to download file.
	 */
	void download(Entity entity, Path target, FileLink fileLink, Transaction transaction,
			ProgressListener progressListener) throws IOException;

	/**
	 * Opens an {@code InputStream} for given {@link FileLink}. The returned stream
	 * should be consumed with a try-with-resources statement to ensure the stream
	 * is closed properly, e.g:
	 *
	 * <pre>
	 * try (InputStream is = openStream(entity, fileLink)) {
	 * 	// do something useful
	 * }
	 * </pre>
	 *
	 * @param entity      Used for security checks.
	 * @param fileLink    The {@code FileLink} to be provided as a stream.
	 * @param transaction The {@link Transaction} to use.
	 * @return A consumable {@code InputStream} is returned.
	 * @throws IOException Thrown if unable to provide as stream.
	 */
	default InputStream openStream(Entity entity, FileLink fileLink, Transaction transaction) throws IOException {
		return openStream(entity, fileLink, transaction, null);
	}

	/**
	 * Opens an {@code InputStream} for given {@link FileLink}. The progress of the
	 * stream consumption may be traced with a progress listener. The returned
	 * stream should be consumed with a try-with-resources statement to ensure the
	 * stream is closed properly, e.g:
	 *
	 * <pre>
	 * try (InputStream is = openStream(entity, fileLink)) {
	 * 	// do something useful
	 * }
	 * </pre>
	 *
	 * @param entity           Used for security checks.
	 * @param fileLink         The {@code FileLink} to be provided as a stream.
	 * @param transaction      The {@link Transaction} to use.
	 * @param progressListener The progress listener.
	 * @return A consumable {@code InputStream} is returned.
	 * @throws IOException Thrown if unable to provide as stream.
	 */
	InputStream openStream(Entity entity, FileLink fileLink, Transaction transaction, ProgressListener progressListener)
			throws IOException;

	/**
	 * Loads the file size for given {@link FileLink}. The file size is stored in
	 * the given {@code FileLink}.
	 *
	 * @param entity      Used for security checks.
	 * @param fileLink    The {@code FileLink} whose file size is requested.
	 * @param transaction The {@link Transaction} to use.
	 * @throws IOException Thrown if unable to load file size.
	 */
	void loadSize(Entity entity, FileLink fileLink, Transaction transaction) throws IOException;

	// ======================================================================
	// Inner classes
	// ======================================================================

	/**
	 * A {@link FileService} consumer may implement this interface to get notified
	 * about stream or download progress.
	 */
	@FunctionalInterface
	interface ProgressListener {

		/**
		 * Progress notification.
		 *
		 * @param bytes   Number of transferred bytes since last notification.
		 * @param percent The overall percentage, where percent p &isin; [0.0, 1.0].
		 */
		void progress(int bytes, float percent);

	}

	/**
	 * Sequential upload of given {@link FileLink}s. Local {@link Path}s linked
	 * multiple times are uploaded only once. The upload progress may be traced with
	 * a progress listener.
	 *
	 * @param entity           Used for security checks.
	 * @param fileLinks        Collection of {@code FileLink}s to upload.
	 * @param transaction      The {@link Transaction} to use.
	 * @param progressListener The progress listener.
	 * @throws IOException Thrown if unable to upload files.
	 */
	public void uploadSequential(Entity entity, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException;

	/**
	 * Parallel upload of given {@link FileLink}s. Local {@link Path}s linked
	 * multiple times are uploaded only once. The upload progress may be traced with
	 * a progress listener.
	 *
	 * @param entity           Used for security checks.
	 * @param fileLinks        Collection of {@code FileLink}s to upload.
	 * @param transaction      The {@link Transaction} to use.
	 * @param progressListener The progress listener.
	 * @throws IOException Thrown if unable to upload files.
	 */
	public void uploadParallel(Entity entity, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException;

	/**
	 * Deletes given {@link FileLink}s form the remote storage.
	 *
	 * @param entity      Used for security checks.
	 * @param fileLinks   Collection of {@code FileLink}s to delete.
	 * @param transaction The {@link Transaction} to use.
	 */
	public void delete(Entity entity, Collection<FileLink> fileLinks, Transaction transaction);

	/**
	 * Deletes given {@link FileLink} form the remote storage.
	 *
	 * @param entity      Used for security checks.
	 * @param fileLink    The {@code FileLink}s to delete.
	 * @param transaction The {@link Transaction} to use.
	 */
	public void delete(Entity entity, FileLink fileLink, Transaction transaction);

}
