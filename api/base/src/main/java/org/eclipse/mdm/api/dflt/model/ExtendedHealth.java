/********************************************************************************
 * Copyright (c) 2025 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.api.dflt.model;

/**
 * 
 */
public class ExtendedHealth extends Health{
	
	
	private String odsServerUptime;
	
	private long odsLatency;
	
	private int activeODSSessions;
	
	private int maxODSSessions;
	
	
	/**
	 * 
	 */
	public ExtendedHealth() {
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * @return the odsLatency
	 */
	public long getOdsLatency() {
		return odsLatency;
	}


	/**
	 * @return the activeODSSessions
	 */
	public int getActiveODSSessions() {
		return activeODSSessions;
	}

	/**
	 * @param activeODSSessions the activeODSSessions to set
	 */
	public void setActiveODSSessions(int activeODSSessions) {
		this.activeODSSessions = activeODSSessions;
	}

	/**
	 * @return the maxODSSessions
	 */
	public int getMaxODSSessions() {
		return maxODSSessions;
	}

	/**
	 * @param maxODSSessions the maxODSSessions to set
	 */
	public void setMaxODSSessions(int maxODSSessions) {
		this.maxODSSessions = maxODSSessions;
	}


	/**
	 * @return the odsServerUptime
	 */
	public String getOdsServerUptime() {
		return odsServerUptime;
	}

	/**
	 * @param odsServerUptime the odsServerUptime to set
	 */
	public void setOdsServerUptime(String odsServerUptime) {
		this.odsServerUptime = odsServerUptime;
	}

	/**
	 * @param odsLatency the odsLatency to set
	 */
	public void setOdsLatency(long odsLatency) {
		this.odsLatency = odsLatency;
	}
	
	

}
