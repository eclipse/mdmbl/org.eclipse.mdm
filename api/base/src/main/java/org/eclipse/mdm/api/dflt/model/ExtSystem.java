/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.dflt.model;

import java.util.List;

import org.eclipse.mdm.api.base.adapter.Core;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.Describable;

/**
 * Implementation of an external system entity type. An external system
 * attribute contains several mdm attributes.
 *
 * @author Juergen Kleck, Peak Solution GmbH
 *
 */
public class ExtSystem extends BaseEntity implements Describable, Deletable {

	/**
	 * Constructor.
	 *
	 * @param core The {@link Core}.
	 */
	protected ExtSystem(Core core) {
		super(core);
	}

	public List<ExtSystemAttribute> getAttributes() {
		return getExtSystemAttributes();
	}

	/**
	 * Returns all available {@link ExtSystemAttribute}s related to this external
	 * system.
	 *
	 * @return The returned {@code List} is unmodifiable.
	 */
	public List<ExtSystemAttribute> getExtSystemAttributes() {
		return getCore().getChildrenStore().get(ExtSystemAttribute.class);
	}

}
