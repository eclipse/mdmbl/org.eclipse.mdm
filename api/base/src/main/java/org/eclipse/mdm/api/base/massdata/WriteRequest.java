/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.base.massdata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.query.DataAccessException;

/**
 * Holds required data to write mass data.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 */
public final class WriteRequest {

	// ======================================================================
	// Instance variables
	// ======================================================================

	private final List<ExternalComponentData> listExternalComponentData = new ArrayList<>();
	private final ChannelGroup channelGroup;
	private final Channel channel;
	private final AxisType axisType;

	private SequenceRepresentation sequenceRepresentation;
	private double[] generationParameters;
	private Unit sourceUnit;
	private boolean independent;

	private ScalarType rawScalarType;
	private Object values;
	private boolean allValid;
	private short[] flags;

	private Short globalFlag;
	private String mimeType = "application/x-asam.aolocalcolumn";

	// ======================================================================
	// Constructors
	// ======================================================================

	/**
	 * Constructor.
	 *
	 * @param channelGroup The {@link ChannelGroup} for this request.
	 * @param channel      The {@link Channel} specified mass data will be dedicated
	 *                     to.
	 * @param axisType     The {@link AxisType} of the written mass data.
	 */
	private WriteRequest(ChannelGroup channelGroup, Channel channel, AxisType axisType) {
		this.channelGroup = channelGroup;
		this.channel = channel;
		this.axisType = axisType;
	}

	// ======================================================================
	// Public methods
	// ======================================================================

	/**
	 * Starts a new {@link WriteRequest} by returning a {@link WriteRequestBuilder}.
	 *
	 * @param channelGroup The {@link ChannelGroup} for this request.
	 * @param channel      The {@link Channel} specified mass data will be dedicated
	 *                     to.
	 * @param axisType     The {@link AxisType} of the written mass data.
	 * @return A {@code WriteRequestBuilder} is returned.
	 */
	public static WriteRequestBuilder create(ChannelGroup channelGroup, Channel channel, AxisType axisType) {
		return new WriteRequestBuilder(new WriteRequest(channelGroup, channel, axisType));
	}

	/**
	 * Returns the {@link ChannelGroup} of this request.
	 *
	 * @return The {@code ChannelGroup} is returned.
	 */
	public ChannelGroup getChannelGroup() {
		return channelGroup;
	}

	/**
	 * Returns the {@link Channel} of this request.
	 *
	 * @return The {@code Channel} is returned.
	 */
	public Channel getChannel() {
		return channel;
	}

	/**
	 * Returns the {@link AxisType} of this request.
	 *
	 * @return The {@code AxisType} is returned.
	 */
	public AxisType getAxisType() {
		return axisType;
	}

	/**
	 * Returns the {@link SequenceRepresentation} of this request.
	 *
	 * @return The {@code SequenceRepresentation} is returned.
	 */
	public SequenceRepresentation getSequenceRepresentation() {
		return sequenceRepresentation;
	}

	/**
	 * Returns the generation parameters of this request. The length of the returned
	 * array depends on the {@link SequenceRepresentation} of this request.
	 *
	 * <p>
	 * <b>NOTE:</b> In case of an implicit sequence representation this method will
	 * always return an empty array since these generation parameters are correctly
	 * typed and stored as measured values.
	 *
	 * @return If none available, then an empty array is returned.
	 */
	public double[] getGenerationParameters() {
		return generationParameters == null ? new double[0] : generationParameters.clone();
	}

	/**
	 * Returns the independent flag of this request.
	 *
	 * @return Returns {@code true} if the measured values do not depend on those of
	 *         other {@link Channel}s within their common {@link ChannelGroup}.
	 */
	public boolean isIndependent() {
		return independent;
	}

	/**
	 * Returns the source Unit of this request.
	 *
	 * @return The unit in which the values are given is returned
	 */
	public Unit getSourceUnit() {
		return sourceUnit;
	}

	/**
	 * Checks whether this request has measured values.
	 *
	 * @return Returns {@code true} if {@link SequenceRepresentation#isExternal()}
	 *         returns {@code false}.
	 * @see #hasExternalComponents()
	 */
	public boolean hasValues() {
		return !getSequenceRepresentation().isExternal();
	}

	/**
	 * Returns the stored measured values.
	 *
	 * @return The measured values are returned.
	 * @throws IllegalStateException Thrown if values are not available.
	 */
	public Object getValues() {
		return this.getValues(channel.getUnit());
	}

	/**
	 * Returns the stored measured values in the requested unit.
	 *
	 * @param targetUnit Unit in which the values should be returned
	 * @return The measured values are returned.
	 * @throws IllegalStateException Thrown if values are not available or values
	 *                               cannot be converted into requested unit
	 */
	public Object getValues(Unit targetUnit) {
		if (hasValues()) {
			if (sourceUnit == null || targetUnit.getID().equals(sourceUnit.getID())) {
				return values;
			} else if (targetUnit.getPhysicalDimension().getID().equals(sourceUnit.getPhysicalDimension().getID())) {
				return convert(values, sourceUnit, targetUnit);
			} else {
				throw new IllegalArgumentException("Values in Unit " + sourceUnit.getName()
						+ " cannot be converted into Unit " + targetUnit.getName()
						+ ", because the units do not belong to the same PhysicalDimension ("
						+ sourceUnit.getPhysicalDimension().getName() + " <-> "
						+ targetUnit.getPhysicalDimension().getName() + ").");
			}
		}

		throw new IllegalStateException("Values are not available.");
	}

	/**
	 * Checks whether this request has measured values, stored in externally linked
	 * files.
	 *
	 * @return Returns {@code true} if {@link SequenceRepresentation#isExternal()}
	 *         returns {@code true}.
	 * @see #hasValues()
	 */
	public boolean hasExternalComponents() {
		return getSequenceRepresentation().isExternal();
	}

	/**
	 * Returns the configurations for measured values stored in externally
	 * referenced files.
	 * 
	 * @return Returned {@code List} is unmodifiable.
	 * @throws IllegalStateException Thrown if configurations are not available.
	 */
	public List<ExternalComponentData> getExternalComponents() {
		if (!hasExternalComponents()) {
			throw new IllegalStateException("External components are not available.");
		}

		return Collections.unmodifiableList(listExternalComponentData);
	}

	/**
	 * Returns the {@link ScalarType} of the stored measured value sequence.
	 *
	 * @return The raw {@code ScalarType} is returned.
	 */
	public ScalarType getRawScalarType() {
		return rawScalarType;
	}

	/**
	 * Returns the calculated {@link ScalarType} which reflects the final
	 * {@code ScalarType} of the generated measured value sequence. If
	 * {@link #getGenerationParameters()} returns a not empty array and
	 * {@link #getRawScalarType()} is an integer type (byte, short, int, long), then
	 * the returned {@code ScalarType} is bumped to the next suitable floating point
	 * type as listed below:
	 *
	 * <ul>
	 * <li>{@link ScalarType#BYTE} -&gt; {@link ScalarType#FLOAT}</li>
	 * <li>{@link ScalarType#SHORT} -&gt; {@link ScalarType#FLOAT}</li>
	 * <li>{@link ScalarType#INTEGER} -&gt; {@link ScalarType#FLOAT}</li>
	 * <li>{@link ScalarType#LONG} -&gt; {@link ScalarType#DOUBLE}</li>
	 * </ul>
	 *
	 * @return The calculated {@code ScalarType} is returned.
	 */
	public ScalarType getCalculatedScalarType() {
		if (getGenerationParameters().length > 0 && getRawScalarType().isIntegerType()) {
			return getRawScalarType().isLong() ? ScalarType.DOUBLE : ScalarType.FLOAT;
		}

		return getRawScalarType();
	}

	/**
	 * Checks whether all measured values within the whole sequence are valid. If
	 * this method returns {@code true}, then {@link #getFlags()} will return an
	 * empty array.
	 *
	 * @return Returns {@code true} if all measured values are valid.
	 * @see #getFlags()
	 */
	public boolean areAllValid() {
		return allValid;
	}

	/**
	 * Returns the validity flags sequence for the stored measured values. If
	 * {@link #areAllValid()} returns {@code false} this method will return an array
	 * with the same length as the measured values sequence, where each flag
	 * indicates whether the corresponding measured value is valid or not.
	 *
	 * @return The validity flags sequence is returned.
	 * @see #allValid
	 */
	public short[] getFlags() {
		if (flags == null) {
			return flags;
		} else {
			return flags.clone();
		}
	}

	/**
	 * Gets the GlobalFlag. For use with external components only.
	 * 
	 * @return The GlobalFlag.
	 */
	public Short getGlobalFlag() {
		return globalFlag;
	}

	/**
	 * Gets the MimeType.
	 * 
	 * @return The MimeType.
	 */
	public String getMimeType() {
		return mimeType;
	}

	// ======================================================================
	// Package methods
	// ======================================================================

	/**
	 * Sets the {@link SequenceRepresentation} for this request.
	 *
	 * @param sequenceRepresentation The {@link SequenceRepresentation}.
	 */
	void setSequenceRepresentation(SequenceRepresentation sequenceRepresentation) {
		this.sequenceRepresentation = sequenceRepresentation;
	}

	/**
	 * Sets generation parameters for this request.
	 *
	 * @param generationParameters The generation parameters.
	 */
	void setGenerationParameters(double[] generationParameters) {
		this.generationParameters = generationParameters.clone();
	}

	/**
	 * Sets the independent flag for this request.
	 */
	void setIndependent() {
		independent = true;
	}

	/**
	 * Sets the independent flag for this request to the value specified.
	 * 
	 * @param independent The independent flag.
	 */
	void setIndependent(boolean independent) {
		this.independent = independent;
	}

	/**
	 * Triggers an adjustment of the generation parameters and modification of the
	 * {@link SequenceRepresentation} of this request to match the {@link Channel}s
	 * default {@link Unit}.
	 *
	 * @param sourceUnit The {@link Unit} of the measured values.
	 */
	void setSourceUnit(Unit sourceUnit) {
		this.sourceUnit = sourceUnit;
	}

	/**
	 * Sets the raw {@link ScalarType} for this request.
	 *
	 * @param rawScalarType The {@link ScalarType}.
	 */
	void setRawScalarType(ScalarType rawScalarType) {
		this.rawScalarType = rawScalarType;
	}

	/**
	 * Sets the measured values sequence where each value in the sequence is
	 * expected to be valid.
	 *
	 * @param values The measured value sequence.
	 * @throws IllegalStateException Thrown if {@link #hasValues()} returns
	 *                               {@code false}.
	 */
	void setValues(Object values) {
		if (hasValues()) {
			this.values = values;
			allValid = true;
		} else {
			throw new IllegalStateException("Measured values stored in externally linked files expected.");
		}
	}

	/**
	 * Sets the measured values sequence where each value's validity is specified in
	 * the given flags array.
	 *
	 * @param values The measured value sequence.
	 * @param flags  The validity flag sequence.
	 * @throws IllegalStateException Thrown if {@link #hasValues()} returns
	 *                               {@code false}.
	 */
	void setValues(Object values, short[] flags) {
		if (hasValues()) {
			this.values = values;
			this.flags = flags.clone();
		} else {
			throw new IllegalStateException("Measured values stored in externally linked files expected.");
		}
	}

	/**
	 * Adds a configuration for measured values stored in externally linked files.
	 *
	 * @param externalComponentData The new configuration, which will be appended to
	 *                              the end of the list of
	 *                              {@link ExternalComponentData}s of this
	 *                              WriteRequest.
	 * @throws IllegalStateException Thrown if {@link #hasExternalComponents()}
	 *                               returns {@code false}.
	 */
	void addExternalComponent(ExternalComponentData externalComponentData) {
		if (hasExternalComponents()) {
			listExternalComponentData.add(externalComponentData);
		} else {
			throw new IllegalStateException("Measured values expected");
		}
	}

	/**
	 * Sets the GlobalFlag. For use with external components only.
	 * 
	 * @param globalFlag The GlobalFlag to set.
	 */
	void setGlobalFlag(Short globalFlag) {
		this.globalFlag = globalFlag;
	}

	/**
	 * Sets the MimeType.
	 * 
	 * @param mimeType The MimeType to set.
	 */
	void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * Converts the values given in sourceUnit into the target unit and returns the
	 * converted values.
	 * 
	 * @param values     values given in sourceUnit
	 * @param sourceUnit the source unit the values are provided
	 * @param targetUnit the target unit the values should be converted to
	 * @return the values converted into the target unit or null, if the given
	 *         values were null.
	 * @throws DataAccessException if source or target unit is null
	 */
	private Object convert(Object values, Unit sourceUnit, Unit targetUnit) {
		if (sourceUnit == null) {
			throw new DataAccessException("Cannot convert values if source unit is null!");
		}
		if (targetUnit == null) {
			throw new DataAccessException("Cannot convert values if target unit is null!");
		}

		if (values instanceof byte[]) {
			byte[] bytes = (byte[]) values;
			byte[] convertedBytes = new byte[bytes.length];
			for (int i = 0; i < bytes.length; i++) {
				double siValue = ((bytes[i] * sourceUnit.getFactor()) + sourceUnit.getOffset());
				convertedBytes[i] = (byte) ((siValue - targetUnit.getOffset()) / targetUnit.getFactor());
			}
			return convertedBytes;
		} else if (values instanceof short[]) {
			short[] shorts = (short[]) values;
			short[] convertedShorts = new short[shorts.length];
			for (int i = 0; i < shorts.length; i++) {
				double siValue = ((shorts[i] * sourceUnit.getFactor()) + sourceUnit.getOffset());
				convertedShorts[i] = (short) ((siValue - targetUnit.getOffset()) / targetUnit.getFactor());
			}
			return convertedShorts;
		} else if (values instanceof int[]) {
			int[] ints = (int[]) values;
			int[] convertedInts = new int[ints.length];
			for (int i = 0; i < ints.length; i++) {
				double siValue = ((ints[i] * sourceUnit.getFactor()) + sourceUnit.getOffset());
				convertedInts[i] = (int) ((siValue - targetUnit.getOffset()) / targetUnit.getFactor());
			}
			return convertedInts;
		} else if (values instanceof long[]) {
			long[] longs = (long[]) values;
			long[] convertedLongs = new long[longs.length];
			for (int i = 0; i < longs.length; i++) {
				double siValue = ((longs[i] * sourceUnit.getFactor()) + sourceUnit.getOffset());
				convertedLongs[i] = (long) ((siValue - targetUnit.getOffset()) / targetUnit.getFactor());
			}
			return convertedLongs;
		} else if (values instanceof float[]) {
			float[] floats = (float[]) values;
			float[] convertedFloats = new float[floats.length];
			for (int i = 0; i < floats.length; i++) {
				double siValue = ((floats[i] * sourceUnit.getFactor()) + sourceUnit.getOffset());
				convertedFloats[i] = (float) ((siValue - targetUnit.getOffset()) / targetUnit.getFactor());
			}
			return convertedFloats;
		} else if (values instanceof double[]) {
			double[] doubles = (double[]) values;
			double[] convertedDoubles = new double[doubles.length];
			for (int i = 0; i < doubles.length; i++) {
				double siValue = ((doubles[i] * sourceUnit.getFactor()) + sourceUnit.getOffset());
				convertedDoubles[i] = (double) ((siValue - targetUnit.getOffset()) / targetUnit.getFactor());
			}
			return convertedDoubles;
		}

		return values;
	}

}
