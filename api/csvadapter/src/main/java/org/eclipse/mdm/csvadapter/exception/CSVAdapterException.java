/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.csvadapter.exception;

/**
 * @author akn
 *
 */
public class CSVAdapterException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1326790058953991435L;

	/**
	 * @param message
	 * @param cause
	 */
	public CSVAdapterException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public CSVAdapterException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public CSVAdapterException(Throwable cause) {
		super(cause);
	}

}
