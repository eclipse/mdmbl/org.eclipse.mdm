/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.api.atfxadapter;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.rules.TemporaryFolder;

import com.google.common.collect.ImmutableMap;
import com.google.common.io.Files;

public class AtfxAdapterWriteTest {

	public static final String ATFX_FILE = "src/test/resources/mdm_complete_application_model_500.xml";

	public static ApplicationContext context;

	@ClassRule
	public static TemporaryFolder tmpFolder = new TemporaryFolder();

	@BeforeClass
	public static void init() throws ConnectionException, IOException {

		File f = tmpFolder.newFile("open.atfx");
		Files.copy(new File(ATFX_FILE), f);

		Map<String, String> map = ImmutableMap.of("atfxfile", f.getAbsolutePath(), "freetext.active", "false",
				"includeCatalog", "true");

		context = new ATFXContextFactory().connect("ATFX", map);
	}

	@AfterClass
	public static void teardown() throws ConnectionException {
		context.close();
	}

	@org.junit.Test
	public void test() {
		CatalogComponent catComp = context.getEntityFactory().get().createCatalogComponent(ContextType.UNITUNDERTEST,
				"MyComp");

		Transaction t = context.getEntityManager().get().startTransaction();
		t.create(Arrays.asList(catComp));
		t.commit();
	}
}
