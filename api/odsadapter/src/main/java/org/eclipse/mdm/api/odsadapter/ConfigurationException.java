package org.eclipse.mdm.api.odsadapter;

/**
 * Thrown to indicate configuration errors.
 *
 * @since 1.0.0
 * @author Viktor Stoehr, Gigatronik Ingolstadt GmbH
 */
public class ConfigurationException extends RuntimeException {

	private static final long serialVersionUID = 8274169747397284193L;

	/**
	 * Constructor.
	 *
	 * @param message The error message.
	 */
	public ConfigurationException(String message) {
		super(message);
	}

	/**
	 * Constructor.
	 *
	 * @param message   The error message.
	 * @param throwable The origin cause.
	 */
	public ConfigurationException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
