/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.notification;

import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.notification.NotificationException;
import org.eclipse.mdm.api.base.notification.NotificationService;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.odsadapter.ODSContext;
import org.eclipse.mdm.api.odsadapter.ODSContextFactory;
import org.eclipse.mdm.api.odsadapter.ODSHttpContext;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.api.odsadapter.notification.avalon.AvalonNotificationManager;
import org.eclipse.mdm.api.odsadapter.notification.ods.OdsPollingNotificationManager;
import org.eclipse.mdm.api.odsadapter.notification.peak.PeakNotificationManager;
import org.eclipse.mdm.api.odsadapter.notification.peak.PeakPollingNotificationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;

/**
 * Factory for creating a notification service.
 * 
 * @since 1.0.0
 * @author Matthias Koller, Peak Solution GmbH
 *
 */
@SuppressWarnings("deprecation")
public class ODSNotificationServiceFactory {
	private static final Logger LOGGER = LoggerFactory.getLogger(ODSNotificationServiceFactory.class);

	public static final String PARAM_SERVER_TYPE = "freetext.notificationType";
	public static final String PARAM_URL = "freetext.notificationUrl";
	public static final String PARAM_POLLING_INTERVAL = "freetext.pollingInterval";

	@Deprecated
	public static final String SERVER_TYPE_PEAK = "peak";
	@Deprecated
	public static final String SERVER_TYPE_PEAK_POLLING = "peak_polling";
	@Deprecated
	public static final String SERVER_TYPE_AVALON = "avalon";
	public static final String SERVER_TYPE_ODS_POLLING = "ods_polling";
	public static final String SERVER_TYPE_NONE = "none";

	public static final String PARAM_NAMESERVICE_URL = "nameserviceURL";

	public Optional<NotificationService> create(ApplicationContext context, Map<String, String> parameters)
			throws ConnectionException {
		String type = parameters.getOrDefault(PARAM_SERVER_TYPE, SERVER_TYPE_ODS_POLLING);

		QueryService queryService = context.getQueryService()
				.orElseThrow(() -> new ServiceNotProvidedException(QueryService.class));

		if (!ODSContext.class.isInstance(context)) {
			throw new ConnectionException("ApplicationContext is not a ODSContext!");
		}

		if (SERVER_TYPE_NONE.equalsIgnoreCase(type)) {
			LOGGER.info("No notification server configured, notifications are disabled.");

			return Optional.empty();
		} else if (SERVER_TYPE_PEAK.equalsIgnoreCase(type)) {
			LOGGER.warn("Parameter value '{}' of 'freetext.notificationType' is deprecated use '{}' instead.",
					SERVER_TYPE_PEAK, SERVER_TYPE_ODS_POLLING);

			String url = getParameter(parameters, PARAM_URL);

			LOGGER.info("Connecting to Peak Notification Server ...");
			LOGGER.info("URL: {}", url);

			try {
				return Optional.of(new PeakNotificationManager((ODSContext) context, queryService, url, true));
			} catch (NotificationException e) {
				throw new ConnectionException("Could not connect to notification service!", e);
			}
		} else if (SERVER_TYPE_PEAK_POLLING.equalsIgnoreCase(type)) {
			LOGGER.warn("Parameter value '{}' of 'freetext.notificationType' is deprecated use '{}' instead.",
					SERVER_TYPE_PEAK_POLLING, SERVER_TYPE_ODS_POLLING);
			String url = getParameter(parameters, PARAM_URL);

			LOGGER.info("Connecting to Peak Notification Server ...");
			LOGGER.info("URL: {}", url);

			long pollingInterval = 10_000L;
			try {
				pollingInterval = Long.parseLong(parameters.getOrDefault(PARAM_POLLING_INTERVAL, "10000"));
			} catch (NumberFormatException e) {
				LOGGER.warn("Could not parse parse parameter pollingInterval. Using default value: " + pollingInterval,
						e);
			}

			try {
				return Optional.of(new PeakPollingNotificationManager((ODSContext) context, queryService, url, true,
						pollingInterval));
			} catch (NotificationException e) {
				throw new ConnectionException("Could not connect to notification service!", e);
			}
		} else if (SERVER_TYPE_ODS_POLLING.equalsIgnoreCase(type)) {
			String url = parameters.get(PARAM_URL);
			if ((url == null || url.isEmpty()) && context instanceof ODSHttpContext) {
				url = getParameter(parameters, ODSHttpContextFactory.PARAM_URL);
			}
			LOGGER.info("Connecting to ODS Notification Server ...");
			LOGGER.info("URL: {}", url);

			long pollingInterval = 10_000L;
			try {
				pollingInterval = Long.parseLong(parameters.getOrDefault(PARAM_POLLING_INTERVAL, "10000"));
			} catch (NumberFormatException e) {
				LOGGER.warn("Could not parse parse parameter pollingInterval. Using default value: " + pollingInterval,
						e);
			}

			try {
				return Optional.of(new OdsPollingNotificationManager((ODSContext) context, queryService, url, true,
						pollingInterval));
			} catch (NotificationException e) {
				throw new ConnectionException("Could not connect to notification service!", e);
			}
		} else if (SERVER_TYPE_AVALON.equalsIgnoreCase(type)) {
			LOGGER.warn("Parameter value '{}' of 'freetext.notificationType' is deprecated use '{}' instead.",
					SERVER_TYPE_AVALON, SERVER_TYPE_ODS_POLLING);

			String serviceName = getParameter(parameters, ODSContextFactory.PARAM_SERVICENAME);
			serviceName = serviceName.replace(".ASAM-ODS", "");
			String nameServiceURL = getParameter(parameters, ODSContextFactory.PARAM_NAMESERVICE);

			LOGGER.info("Connecting to Avalon Notification Server ...");
			LOGGER.info("Name service URL: {}", nameServiceURL);
			LOGGER.info("Service name: {}", serviceName);

			long pollingInterval = 500L;
			try {
				pollingInterval = Long.parseLong(getParameter(parameters, PARAM_POLLING_INTERVAL));
			} catch (NumberFormatException | ConnectionException e) {
				LOGGER.warn("Could not parse parse parameter pollingInterval. Using default value: " + pollingInterval,
						e);
			}

			return Optional.of(new AvalonNotificationManager((ODSContext) context, queryService, serviceName,
					nameServiceURL, true, pollingInterval));
		} else {
			throw new ConnectionException("Invalid server type. Expected on of: "
					+ Joiner.on(", ").join(SERVER_TYPE_PEAK_POLLING, SERVER_TYPE_PEAK, SERVER_TYPE_AVALON));
		}

	}

	private String getParameter(Map<String, String> parameters, String name) throws ConnectionException {
		String value = parameters.get(name);
		if (value == null || value.isEmpty()) {
			throw new ConnectionException("Connection parameter with name '" + name + "' is either missing or empty.");
		}

		return value;
	}
}
