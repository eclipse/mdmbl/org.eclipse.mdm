/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.filetransfer;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Collection;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.query.DataAccessException;

/**
 * This class is used to indicate that External Reference files are not
 * supported in ODS HTTP API. Instead AoFiles ({@link HttpAoFileService}) should
 * be used. All methods in this class will throw a {@link DataAccessException}
 * with a corresponding message.
 * 
 */
public class HttpExtRefFileService implements FileService {

	private static final String ERROR_MESSAGE = "ODS Adapter in HTTP mode does not allow to work with External Reference files on the server. Please consider migrating External References to AoFiles.";

	@Override
	public void downloadSequential(Entity entity, Path target, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException {
		throw new DataAccessException(ERROR_MESSAGE);
	}

	@Override
	public void downloadParallel(Entity entity, Path target, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException {
		throw new DataAccessException(ERROR_MESSAGE);
	}

	@Override
	public void download(Entity entity, Path target, FileLink fileLink, Transaction transaction,
			ProgressListener progressListener) throws IOException {
		throw new DataAccessException(ERROR_MESSAGE);
	}

	@Override
	public InputStream openStream(Entity entity, FileLink fileLink, Transaction transaction,
			ProgressListener progressListener) throws IOException {
		throw new DataAccessException(ERROR_MESSAGE);
	}

	@Override
	public void loadSize(Entity entity, FileLink fileLink, Transaction transaction) throws IOException {
		fileLink.setFileSize(0);
	}

	@Override
	public void uploadSequential(Entity entity, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException {
		throw new DataAccessException(ERROR_MESSAGE);
	}

	@Override
	public void uploadParallel(Entity entity, Collection<FileLink> fileLinks, Transaction transaction,
			ProgressListener progressListener) throws IOException {
		throw new DataAccessException(ERROR_MESSAGE);
	}

	@Override
	public void delete(Entity entity, Collection<FileLink> fileLinks, Transaction transaction) {
		throw new DataAccessException(ERROR_MESSAGE);
	}

	@Override
	public void delete(Entity entity, FileLink fileLink, Transaction transaction) {
		throw new DataAccessException(ERROR_MESSAGE);
	}

}
