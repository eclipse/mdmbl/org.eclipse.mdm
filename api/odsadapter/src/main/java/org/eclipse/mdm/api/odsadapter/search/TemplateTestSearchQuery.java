package org.eclipse.mdm.api.odsadapter.search;

import java.util.Collections;
import java.util.List;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;

public class TemplateTestSearchQuery extends BaseEntitySearchQuery {

	private ODSModelManager modelManager;

	/**
	 * Constructor.
	 *
	 * @param modelManager Used to load {@link EntityType}s.
	 * @param contextState The {@link ContextState}.
	 */
	TemplateTestSearchQuery(ODSModelManager modelManager, QueryService queryService, ContextState contextState) {
		super(modelManager, queryService, TemplateTest.class, TemplateTest.class);
		this.modelManager = modelManager;

	}

	@Override
	public List<EntityType> listEntityTypes() {
		return Collections.singletonList(this.modelManager.getEntityType(TemplateTest.class));
	}
}
