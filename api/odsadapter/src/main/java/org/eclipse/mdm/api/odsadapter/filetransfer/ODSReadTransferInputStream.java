package org.eclipse.mdm.api.odsadapter.filetransfer;

import java.io.IOException;
import java.io.InputStream;

import org.asam.ods.AoException;
import org.asam.ods.ODSReadTransfer;

public final class ODSReadTransferInputStream extends InputStream
{
	private ODSReadTransfer readTransfer;
	int bufferSize = 8192;

	byte[] buffer;
	int position = -1;

	public ODSReadTransferInputStream(ODSReadTransfer readTransfer) throws IOException {
		try {
			this.readTransfer = readTransfer;
			buffer = readTransfer.getOctetSeq(bufferSize);
		} catch (AoException exc) {
			throw new IOException("Unable to read file due to: " + exc.getMessage(), exc);
		}
	}

	@Override
	public int read() throws IOException {
		try {
			position++;
			if (position >= buffer.length) {
				buffer = readTransfer.getOctetSeq(bufferSize);
				if (buffer.length == 0) {
					return -1;
				}
				position = 0;
			}

			return buffer[position] & 0xff;
		}
		catch (AoException exc) {
			throw new IOException("Unable to read file due to: " + exc.getMessage(), exc);
		}
	}
}
