/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.search;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.groups.Tuple;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.massdata.ChannelValues;
import org.eclipse.mdm.api.base.massdata.ValuesFilter;
import org.eclipse.mdm.api.base.massdata.ValuesFilter.ValuesCondition;
import org.eclipse.mdm.api.base.massdata.ValuesFilter.ValuesConjunction;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.BooleanOperator;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.FilterItem;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.base.search.SearchService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.mockito.Mockito;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.google.common.collect.ImmutableMap;

@Testcontainers(disabledWithoutDocker = true)
class ODSSearchServiceIT {

	@Container
	static OdsServerContainer odsServer = OdsServerContainer.create();

	private static ApplicationContext context;
	private static ModelManager modelManager;
	private static SearchService searchService;

	@BeforeAll
	static void setUpBeforeClass() throws ConnectionException {

		context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		modelManager = context.getModelManager().orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));
		searchService = context.getSearchService()
				.orElseThrow(() -> new ServiceNotProvidedException(SearchService.class));
	}

	@AfterAll
	static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	private Function<FilterItem, Tuple> filterExtractors = new Function<FilterItem, Tuple>() {

		@Override
		public Tuple apply(FilterItem f) {
			return tuple(f.isBooleanOperator() ? f.getBooleanOperator() : null,
					f.isCondition() ? f.getCondition().getAttribute().getName() : null,
					f.isCondition() ? f.getCondition().getComparisonOperator() : null,
					f.isCondition() ? f.getCondition().getValue().extract() : null,
					f.isBracketOperator() ? f.getBracketOperator() : null);
		}
	};

	@org.junit.jupiter.api.Test
	void testChildMultiplicityDoesNotEffectParentResultCount() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Project project = ef.createProject("ChildMultiplicityDoesNotEffectParentResultCount");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool);
		for (int t = 1; t <= 25; t++) {
			final int x = t;
			TestStep testStep = ef.createTestStep("MyTestStep" + x, test);
			Stream.iterate(0, i -> i + 1).limit(50)
					.map(i -> ef.createMeasurement("MyMeasurement" + x + "." + i, testStep))
					.forEach(m -> m.setDescription("Measurement for TestStep " + x));
		}

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}

		EntityType etProject = modelManager.getEntityType(Project.class);
		EntityType etTestStep = modelManager.getEntityType(TestStep.class);
		EntityType etMeaResult = modelManager.getEntityType(Measurement.class);

		// select TestStep.Name and MeaResult.Description; each MeaResult per TestStep
		// has the same description
		List<Result> result = searchService.fetchResults(TestStep.class,
				Arrays.asList(etTestStep.getNameAttribute(), etMeaResult.getAttribute(Measurement.ATTR_DESCRIPTION)),
				Filter.nameOnly(etProject, "ChildMultiplicityDoesNotEffectParentResultCount")
						.add(ComparisonOperator.GREATER_THAN.create(etMeaResult.getIDAttribute(), "0")),
				"", 0, 1001);

		assertThat(result).hasSize(25)
				.extracting(r -> r.getValue(etTestStep.getNameAttribute()).extract(ValueType.STRING),
						r -> r.getValue(etMeaResult.getAttribute(Measurement.ATTR_DESCRIPTION))
								.extract(ValueType.STRING))
				.contains(tuple("MyTestStep1", "Measurement for TestStep 1"),
						tuple("MyTestStep25", "Measurement for TestStep 25"));
	}

	@org.junit.jupiter.api.Test
	void testChildMultiplicityDoesNotEffectParentResultCountDifferentChildValues() {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Project project = ef.createProject("CMDNEParentResultCountDifferentChildValues");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool);
		for (int t = 1; t <= 25; t++) {
			final int x = t;
			TestStep testStep = ef.createTestStep("MyTestStep" + x, test);
			Stream.iterate(0, i -> i + 1).limit(50)
					.map(i -> ef.createMeasurement("MyMeasurement" + x + "." + i, testStep))
					.forEach(m -> m.setDescription("Measurement for TestStep " + x));
		}

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(project));
			t.commit();
		}

		EntityType etProject = modelManager.getEntityType(Project.class);
		EntityType etTestStep = modelManager.getEntityType(TestStep.class);
		EntityType etMeaResult = modelManager.getEntityType(Measurement.class);

		// select TestStep.Name and MeaResult.Name; each MeaResult per TestStep has a
		// different Name
		List<Result> result = searchService
				.fetchResults(TestStep.class,
						Arrays.asList(etTestStep.getNameAttribute(), etMeaResult.getNameAttribute()),
						Filter.nameOnly(etProject, "CMDNEParentResultCountDifferentChildValues")
								.add(ComparisonOperator.GREATER_THAN.create(etMeaResult.getIDAttribute(), "0")),
						"", 0, 1001);

		// the result set from the ODS will contain all MeaResults, e.g. 1001. The
		// Results will be reduced to 21 TestSteps, the MeaResult.Name will not be
		// filled, because there would be 50 different values, except for the last one.
		assertThat(result).hasSize(21)
				.extracting(r -> r.getValue(etTestStep.getNameAttribute()).extract(ValueType.STRING),
						r -> r.getValue(etMeaResult.getNameAttribute()).extract(ValueType.STRING))
				.contains(tuple("MyTestStep1", ""), tuple("MyTestStep20", ""),
						tuple("MyTestStep21", "MyMeasurement21.0"));
	}

	@org.junit.jupiter.api.Test
	void testGetMergedFilter() throws Exception {

		ODSSearchService service = Mockito.spy((ODSSearchService) searchService);

		Mockito.doReturn(ImmutableMap.of(TestStep.class, Arrays.asList("10", "12"))).when(service)
				.fetchIds(Mockito.anyString());

		EntityType testStep = modelManager.getEntityType(TestStep.class);

		assertThat(service.getMergedFilter(Filter.idOnly(testStep, "11"), "query")).hasSize(3)
				.extracting(filterExtractors).containsExactly(tuple(null, "Id", ComparisonOperator.EQUAL, "11", null),
						tuple(BooleanOperator.AND, null, null, null, null),
						tuple(null, "Id", ComparisonOperator.IN_SET, new String[] { "10", "12" }, null));
	}

	@org.junit.jupiter.api.Test
	void testGetMergedFilterNoAttributeFilter() throws Exception {
		ODSSearchService service = Mockito.spy((ODSSearchService) searchService);

		Mockito.doReturn(ImmutableMap.of(TestStep.class, Arrays.asList("10"))).when(service)
				.fetchIds(Mockito.anyString());

		assertThat(service.getMergedFilter(Filter.and(), "query")).extracting(filterExtractors)
				.containsExactly(tuple(null, "Id", ComparisonOperator.IN_SET, new String[] { "10" }, null));
	}

	@org.junit.jupiter.api.Test
	void testGetMergedFilterNoFreetextResult() throws Exception {
		ODSSearchService service = Mockito.spy((ODSSearchService) searchService);

		Mockito.doReturn(Collections.emptyMap()).when(service).fetchIds(Mockito.anyString());

		EntityType testStep = modelManager.getEntityType(TestStep.class);

		assertThat(service.getMergedFilter(Filter.idOnly(testStep, "11"), "")).extracting(filterExtractors)
				.containsExactly(tuple(null, "Id", ComparisonOperator.EQUAL, "11", null));
	}

	@org.junit.jupiter.api.Test
	void testGetMergedFilterNoAttributeFilterAndNoFreetextResult() throws Exception {
		ODSSearchService service = Mockito.spy((ODSSearchService) searchService);

		Mockito.doReturn(Collections.emptyMap()).when(service).fetchIds(Mockito.anyString());

		assertThat(service.getMergedFilter(Filter.and(), null)).isEmpty();

		assertThat(service.getMergedFilter(Filter.and(), "")).isEmpty();
	}

	@org.junit.jupiter.api.Test
	void testGetChannelValues() throws Exception {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Quantity quantity = ef.createQuantity("MyQuantity");

		Project project = ef.createProject("testGetChannelValues");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool);
		TestStep testStep = ef.createTestStep("MyTestStep", test);
		Measurement measurement1 = ef.createMeasurement("MyMeasurement1", testStep);
		Measurement measurement2 = ef.createMeasurement("MyMeasurement2", testStep);
		ChannelGroup channelGroup1 = ef.createChannelGroup("MyChannelGroup1", 3, measurement1);

		Channel channel1_1 = ef.createChannel("MyChannel1", measurement1, quantity);
		Channel channel1_2 = ef.createChannel("MyChannel2", measurement1, quantity);
		Channel channel1_3 = ef.createChannel("MyChannel3", measurement1, quantity);

		// Write initial values
		WriteRequest values1_1 = WriteRequest.create(channelGroup1, channel1_1, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 3.3f, 0.001f, 0.123f }, new boolean[] { true, true, true }).build();
		WriteRequest values1_2 = WriteRequest.create(channelGroup1, channel1_2, AxisType.Y_AXIS).explicit()
				.integerValues(new int[] { 3, 2, 3 }, new boolean[] { true, true, true }).build();
		WriteRequest values1_3 = WriteRequest.create(channelGroup1, channel1_3, AxisType.Y_AXIS).explicit()
				.stringValues(new String[] { "eins", "zwei", "drei" }, new boolean[] { true, true, true }).build();

		ChannelGroup channelGroup2 = ef.createChannelGroup("MyChannelGroup2", 3, measurement2);
		Channel channel2_1 = ef.createChannel("MyChannel1", measurement2, quantity);
		Channel channel2_2 = ef.createChannel("MyChannel2", measurement2, quantity);

		// Write initial values
		WriteRequest values2_1 = WriteRequest.create(channelGroup2, channel2_1, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 4.2f, 0.003f, 0.987f }, new boolean[] { true, true, true }).build();
		WriteRequest values2_2 = WriteRequest.create(channelGroup2, channel2_2, AxisType.Y_AXIS).explicit()
				.integerValues(new int[] { 3, 4, 5 }, new boolean[] { true, true, true }).build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(quantity));
			t.create(Arrays.asList(project));
			t.writeMeasuredValues(Arrays.asList(values1_1, values1_2, values1_3, values2_1, values2_2));
			t.commit();
		}

		EntityType etProject = modelManager.getEntityType(Project.class);
		Filter filter = Filter.idOnly(etProject, project.getID());

		ValuesFilter valuesFilter1 = new ValuesCondition("MyChannel2", ComparisonOperator.EQUAL, "3");

		Map<String, Collection<ChannelValues>> result1 = searchService.fetchValueResults(filter, "", valuesFilter1, 0,
				1001);

		assertThat(result1.values().stream().flatMap(c -> c.stream()).collect(Collectors.toList()))
				.usingRecursiveFieldByFieldElementComparatorIgnoringFields("values.initialValueMeasured", "flags")
				.contains(
						new ChannelValues(channel1_1.getID(), channel1_1.getName(), channelGroup1.getID(),
								ValueType.UNKNOWN.create("Values", new float[] { 3.3f, 0.123f })),
						new ChannelValues(channel1_2.getID(), channel1_2.getName(), channelGroup1.getID(),
								ValueType.UNKNOWN.create("Values", new int[] { 3, 3 })),
						new ChannelValues(channel1_3.getID(), channel1_3.getName(), channelGroup1.getID(),
								ValueType.UNKNOWN.create("Values", new String[] { "eins", "drei" })),
						new ChannelValues(channel2_1.getID(), channel2_1.getName(), channelGroup2.getID(),
								ValueType.UNKNOWN.create("Values", new float[] { 4.2f })),
						new ChannelValues(channel2_2.getID(), channel2_2.getName(), channelGroup2.getID(),
								ValueType.UNKNOWN.create("Values", new int[] { 3 })));

		// MyChannel2 <= 3 and (MyChannel1 > 4.1 or MyChannel3 = "drei")
		ValuesFilter valuesFilter2 = ValuesConjunction.and(
				new ValuesCondition("MyChannel2", ComparisonOperator.LESS_THAN_OR_EQUAL, "3"),
				ValuesConjunction.or(new ValuesCondition("MyChannel1", ComparisonOperator.GREATER_THAN, "4.1"),
						new ValuesCondition("MyChannel3", ComparisonOperator.EQUAL, "drei")));

		Map<String, Collection<ChannelValues>> result2 = searchService.fetchValueResults(filter, "", valuesFilter2, 0,
				1001);

		assertThat(result2.values().stream().flatMap(c -> c.stream()).collect(Collectors.toList()))
				.usingRecursiveFieldByFieldElementComparatorIgnoringFields("values.initialValueMeasured", "flags")
				.contains(
						new ChannelValues(channel1_1.getID(), channel1_1.getName(), channelGroup1.getID(),
								ValueType.UNKNOWN.create("Values", new float[] { 0.123f })),
						new ChannelValues(channel1_2.getID(), channel1_2.getName(), channelGroup1.getID(),
								ValueType.UNKNOWN.create("Values", new int[] { 3 })),
						new ChannelValues(channel1_3.getID(), channel1_3.getName(), channelGroup1.getID(),
								ValueType.UNKNOWN.create("Values", new String[] { "drei" })),
						new ChannelValues(channel2_1.getID(), channel2_1.getName(), channelGroup2.getID(),
								ValueType.UNKNOWN.create("Values", new float[] { 4.2f })),
						new ChannelValues(channel2_2.getID(), channel2_2.getName(), channelGroup2.getID(),
								ValueType.UNKNOWN.create("Values", new int[] { 3 })));
	}

	@org.junit.jupiter.api.Test
	void testFilterOnAllDataTypes() throws Exception {
		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		Quantity quantity = ef.createQuantity("MyQuantityAllDatatypes");

		Project project = ef.createProject("testGetChannelValues");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool);
		TestStep testStep = ef.createTestStep("MyTestStep", test);
		Measurement measurement = ef.createMeasurement("MyMeasurement", testStep);
		ChannelGroup channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		Channel channel1 = ef.createChannel("IntChannel", measurement, quantity);
		Channel channel2 = ef.createChannel("LongChannel", measurement, quantity);
		Channel channel3 = ef.createChannel("FloatChannel", measurement, quantity);
		Channel channel4 = ef.createChannel("DoubleChannel", measurement, quantity);
		Channel channel5 = ef.createChannel("StringChannel", measurement, quantity);
		Channel channel6 = ef.createChannel("InstantChannel", measurement, quantity);
		Channel channel7 = ef.createChannel("ShortChannel", measurement, quantity);
		Channel channel8 = ef.createChannel("ByteChannel", measurement, quantity);
		Channel channel9 = ef.createChannel("BooleanChannel", measurement, quantity);
		Channel channel10 = ef.createChannel("ByteArrayChannel", measurement, quantity);

		// Write initial values
		WriteRequest values1 = WriteRequest.create(channelGroup, channel1, AxisType.Y_AXIS).explicit()
				.integerValues(new int[] { 3, 1, 3 }, new boolean[] { true, true, true }).build();
		WriteRequest values2 = WriteRequest.create(channelGroup, channel2, AxisType.Y_AXIS).explicit()
				.longValues(new long[] { 3L, 2L, 3L }, new boolean[] { true, true, true }).build();
		WriteRequest values3 = WriteRequest.create(channelGroup, channel3, AxisType.Y_AXIS).explicit()
				.floatValues(new float[] { 3.3f, 0.001f, 0.123f }, new boolean[] { true, true, true }).build();
		WriteRequest values4 = WriteRequest.create(channelGroup, channel4, AxisType.Y_AXIS).explicit()
				.doubleValues(new double[] { 3.3, 0.001, 0.123 }, new boolean[] { true, true, true }).build();
		WriteRequest values5 = WriteRequest.create(channelGroup, channel5, AxisType.Y_AXIS).explicit()
				.stringValues(new String[] { "eins", "zwei", "drei" }, new boolean[] { true, true, true }).build();
		WriteRequest values6 = WriteRequest.create(channelGroup, channel6, AxisType.Y_AXIS).explicit()
				.dateValues(new Instant[] { Instant.parse("2024-05-12T12:13:15Z"),
						Instant.parse("2024-06-17T12:13:15Z"), Instant.parse("2024-12-06T12:13:15Z") },
						new boolean[] { true, true, true })
				.build();
		WriteRequest values7 = WriteRequest.create(channelGroup, channel7, AxisType.Y_AXIS).explicit().shortValues(
				new short[] { (short) -12345, (short) 42, (short) 32_123 }, new boolean[] { true, true, true }).build();
		WriteRequest values8 = WriteRequest.create(channelGroup, channel8, AxisType.Y_AXIS).explicit()
				.byteValues(new byte[] { -45, 0, 127 }, new boolean[] { true, true, true }).build();
		WriteRequest values9 = WriteRequest.create(channelGroup, channel9, AxisType.Y_AXIS).explicit()
				.booleanValues(new boolean[] { true, false, true }, new boolean[] { true, true, true }).build();
		WriteRequest values10 = WriteRequest.create(channelGroup, channel10, AxisType.Y_AXIS).explicit()
				.byteStreamValues(new byte[][] { new byte[] { 4 }, "test".getBytes(), "abc".getBytes() },
						new boolean[] { true, true, true })
				.build();

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(quantity));
			t.create(Arrays.asList(project));
			t.writeMeasuredValues(Arrays.asList(values1, values2, values3, values4, values5, values6, values7, values8,
					values9, values10));
			t.commit();
		}

		EntityType etProject = modelManager.getEntityType(Project.class);
		Filter filter = Filter.idOnly(etProject, project.getID());

		ValuesFilter valuesFilter1 = new ValuesCondition("IntChannel", ComparisonOperator.EQUAL, "3");

		Map<String, Collection<ChannelValues>> result1 = searchService.fetchValueResults(filter, "", valuesFilter1, 0,
				1001);

		Value expectedFlags = ValueType.SHORT_SEQUENCE.create("Flags", new short[] { (short) 15, (short) 15 });

		assertThat(result1.values().stream().flatMap(c -> c.stream()).collect(Collectors.toList()))
				.usingRecursiveFieldByFieldElementComparatorIgnoringFields("values.initialValueMeasured").contains(
						new ChannelValues(channel1.getID(), channel1.getName(), channelGroup.getID(),
								ValueType.UNKNOWN.create("Values", new int[] { 3, 3 }), expectedFlags, null),
						new ChannelValues(channel2.getID(), channel2.getName(), channelGroup.getID(),
								ValueType.UNKNOWN.create("Values", new long[] { 3, 3 }), expectedFlags, null),
						new ChannelValues(channel3.getID(), channel3.getName(), channelGroup.getID(),
								ValueType.UNKNOWN.create("Values", new float[] { 3.3f, 0.123f }), expectedFlags, null),
						new ChannelValues(channel4.getID(), channel4.getName(), channelGroup.getID(),
								ValueType.UNKNOWN.create("Values", new double[] { 3.3, 0.123 }), expectedFlags, null),
						new ChannelValues(channel5.getID(), channel5.getName(), channelGroup.getID(),
								ValueType.UNKNOWN.create("Values", new String[] { "eins", "drei" }), expectedFlags,
								null),
						new ChannelValues(channel6.getID(), channel6.getName(), channelGroup.getID(),
								ValueType.UNKNOWN.create("Values",
										new Instant[] { Instant.parse("2024-05-12T12:13:15Z"),
												Instant.parse("2024-12-06T12:13:15Z") }),
								expectedFlags, null),
						new ChannelValues(channel7.getID(), channel7.getName(), channelGroup.getID(),
								ValueType.UNKNOWN.create("Values", new short[] { (short) -12345, (short) 32_123 }),
								expectedFlags, null),
						new ChannelValues(channel8.getID(), channel8.getName(), channelGroup.getID(),
								ValueType.UNKNOWN.create("Values", new byte[] { -45, 127 }), expectedFlags, null),
						new ChannelValues(channel9.getID(), channel9.getName(), channelGroup.getID(),
								ValueType.UNKNOWN.create("Values", new boolean[] { true, true }), expectedFlags, null),
						new ChannelValues(channel10.getID(), channel10.getName(), channelGroup.getID(),
								ValueType.UNKNOWN.create("Values", new byte[][] { new byte[] { 4 }, "abc".getBytes() }),
								expectedFlags, null));
	}
}
