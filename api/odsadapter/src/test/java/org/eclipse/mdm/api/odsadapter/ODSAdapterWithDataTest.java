/********************************************************************************
 * Copyright (c) 2015-2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter;

import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_NAMESERVICE;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_PASSWORD;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_SERVICENAME;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_USER;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.file.DescriptiveFileManager;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequestBuilder;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.BaseEntity;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextDescribable;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Deletable;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.EnumRegistry;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.PhysicalDimension;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.model.VersionState;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.search.SearchService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.CatalogComponent;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.TemplateComponent;
import org.eclipse.mdm.api.dflt.model.TemplateRoot;
import org.eclipse.mdm.api.dflt.model.TemplateTest;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.api.odsadapter.model.TestContextData;
import org.eclipse.mdm.api.odsadapter.model.TestData;
import org.eclipse.mdm.api.odsadapter.model.TestStepData;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

// FIXME 10.7.2017: this test needs a running ODS Server, that is not suitable for continous build in Jenkins.
// Comment this in for local tests only.
@Ignore
public class ODSAdapterWithDataTest {

	/*
	 * ATTENTION: ==========
	 *
	 * To run this test make sure the target service is running a MDM default model
	 * and any database constraint which enforces a relation of Test to a parent
	 * entity is deactivated!
	 */

	private static final Logger LOGGER = LoggerFactory.getLogger(ODSAdapterTest.class);

	private static final String NAME_SERVICE = "corbaloc::1.2@%s:%s/NameService";

	private static final String USER = "sa";
	private static final String PASSWORD = "sa";

	private static ApplicationContext context;
	private static EntityManager entityManager;
	private static EntityFactory entityFactory;
	private static ModelManager modelManager;
	private static DescriptiveFileManager descriptiveFileManager;

	private List<CatalogComponent> catalogComponents;
	private List<TemplateRoot> templateRoots;
	private List<TemplateTestStep> templateTestSteps;
	private TemplateTest templateTest;
	private PhysicalDimension physicalDimension;
	private Unit unit;
	private Quantity quantity;
	List<Project> projects;

	private static final TestContextData[] testContextData = {
			new TestContextData("value1", Instant.parse("2023-04-05T14:05:00Z"), 2L,
					Arrays.asList(new String[][] { { "C:\\temp", "112-test.pdf" } }),
					new String[] { "C:\\temp", "1-Test.pdf" },
					Arrays.asList(
							new String[][] { { "https://www.heise.de", null }, { "C:\\temp", "AVXLAG_REF0148.txt" } })),
			new TestContextData("value2", Instant.parse("2023-04-05T14:15:00Z"), 2L, new ArrayList<>(),
					new String[] { "C:\\temp", "Aggregate_1_0.wsdl" }, new ArrayList<>()),
			new TestContextData("value_M1", Instant.parse("2023-04-08T14:15:00Z"), 2L, new ArrayList<>(),
					new String[] { "C:\\temp", "anlegenVersuchsmotorRequest.txt" }, new ArrayList<>()) };

	private static final TestStepData[] testStepDataDiff = {
			new TestStepData("TestStep1", testContextData[0], testContextData[2]),
			new TestStepData("TestStep2", testContextData[1], testContextData[2]) };
	private static final TestStepData[] testStepDataIdent = {
			new TestStepData("TestStep1", testContextData[0], testContextData[0]),
			new TestStepData("TestStep2", testContextData[1], testContextData[1]) };
	private static final TestStepData[] testStepDataTS = { new TestStepData("TestStep1", testContextData[0], null),
			new TestStepData("TestStep2", testContextData[1], null) };
	private static final TestStepData[] testStepDataMeaResult = {
			new TestStepData("TestStep1", null, testContextData[0]) };

	private static final TestData[] testData = { new TestData("Test - gleiche Daten", testStepDataIdent),
			new TestData("Test - unterschiedlieche Daten", testStepDataDiff),
			new TestData("Test - Kontext an TestStep", testStepDataTS),
			new TestData("Test - Kontext an Messung", testStepDataMeaResult) };

	@BeforeClass
	public static void setUpBeforeClass() throws ConnectionException {
		String nameServiceHost = System.getProperty("host");
		String nameServicePort = System.getProperty("port");
		String serviceName = System.getProperty("service");

		if (nameServiceHost == null || nameServiceHost.isEmpty()) {
			throw new IllegalArgumentException("name service host is unknown: define system property 'host'");
		}

		nameServicePort = nameServicePort == null || nameServicePort.isEmpty() ? String.valueOf(2809) : nameServicePort;
		if (nameServicePort == null || nameServicePort.isEmpty()) {
			throw new IllegalArgumentException("name service port is unknown: define system property 'port'");
		}

		if (serviceName == null || serviceName.isEmpty()) {
			throw new IllegalArgumentException("service name is unknown: define system property 'service'");
		}

		Map<String, String> connectionParameters = new HashMap<>();
		connectionParameters.put(PARAM_NAMESERVICE, String.format(NAME_SERVICE, nameServiceHost, nameServicePort));
		connectionParameters.put(PARAM_SERVICENAME, serviceName + ".ASAM-ODS");
		connectionParameters.put(PARAM_USER, USER);
		connectionParameters.put(PARAM_PASSWORD, PASSWORD);
		connectionParameters.put("freetext.notificationType", System.getProperty("notificationType"));
		connectionParameters.put("freetext.notificationUrl", System.getProperty("notificationUrl"));

		context = new ODSContextFactory().connect(serviceName, connectionParameters);
		entityManager = context.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
		entityFactory = context.getEntityFactory()
				.orElseThrow(() -> new IllegalStateException("Entity manager factory not available."));
		modelManager = context.getModelManager()
				.orElseThrow(() -> new IllegalStateException("Model manager not available."));
		descriptiveFileManager = new DescriptiveFileManager(modelManager);
	}

	@AfterClass
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	/*
	 * FIXME this test requires that there is a teststep with id 2, that has a
	 * unitundertest component called "filetest", that has an empty filelink
	 * attribute "myextref" and a string attribute "attr1". remove the comment at
	 * org.junit.Test if you fulfill these requirements
	 */
	// @org.junit.Test
	public void changeFile() throws Exception {
		String idteststep = "2";
		SearchService searchService = context.getSearchService().get();

		EntityType etteststep = modelManager.getEntityType(TestStep.class);
		Transaction transaction;

		transaction = entityManager.startTransaction();

		try {
			List<TestStep> mealist;
			mealist = searchService.fetch(TestStep.class, Filter.idOnly(etteststep, idteststep));
			assertEquals(1, mealist.size());
			TestStep ts = mealist.get(0);
			Map<ContextType, ContextRoot> loadContexts = ts.loadContexts(entityManager, ContextType.UNITUNDERTEST);
			ContextRoot contextRoot = loadContexts.get(ContextType.UNITUNDERTEST);
			Optional<ContextComponent> contextComponent = contextRoot.getContextComponent("filetest");
			Value value = contextComponent.get().getValue("myextref");
			contextComponent.get().getValue("attr1").set("val4711");
			FileLink fl = FileLink.newRemote("", new MimeType(""), "", -1L, null, FileServiceType.EXTREF);
			FileLink fl2 = (FileLink) value.extract();
			assertEquals(fl2, fl);
			List<BaseEntity> toUpdate = new ArrayList<>();
			toUpdate.add(contextComponent.get());
			transaction.update(toUpdate);
			transaction.commit();
		} catch (RuntimeException e) {
			transaction.abort();
			throw e;
		}
	}

	@org.junit.Test
	public void runTestScript() throws DataAccessException {
		List<Test> listTests = entityManager.loadAll(Test.class);
		List<TestStep> listTestSteps = entityManager.loadAll(TestStep.class);
		List<Measurement> listMeasurements = entityManager.loadAll(Measurement.class);

		listTests.size();

	}

	@org.junit.Test
	public void runtTestScript() {
		this.prepareTestData();
		this.removeTestData();
	}

	public void prepareTestData() throws DataAccessException {
		catalogComponents = createCatalogComponents();
		templateRoots = createTemplateRoots(catalogComponents);
		templateTestSteps = createTemplateTestSteps(templateRoots);
		templateTest = createTemplateTest(templateTestSteps);
		physicalDimension = entityFactory.createPhysicalDimension("any_physical_dimension");
		unit = entityFactory.createUnit("any_unit", physicalDimension);
		quantity = entityFactory.createQuantity("any_quantity", unit);

		Transaction transaction = entityManager.startTransaction();
		try {
			create(transaction, "catalog components", catalogComponents);
			create(transaction, "template roots", templateRoots);
			create(transaction, "template test steps", templateTestSteps);
			create(transaction, "template test", Collections.singletonList(templateTest));
			create(transaction, "physical dimension", Collections.singletonList(physicalDimension));
			create(transaction, "unit", Collections.singletonList(unit));
			create(transaction, "quantity", Collections.singletonList(quantity));

			transaction.commit();
		} catch (RuntimeException e) {
			transaction.abort();
			e.printStackTrace();
			fail("Unable to create test data due to: " + e.getMessage());
		}

		projects = Collections.emptyList();
		try {
			projects = createTestData(templateTest, quantity);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}

	public void removeTestData() throws DataAccessException {
		this.deleteFiles(projects);

		Transaction transaction = entityManager.startTransaction();
		try {
			// delete in reverse order!
			if (!projects.isEmpty()) {
				delete(transaction, "projects and their children", projects);
			}

			transaction.commit();
		} catch (RuntimeException e) {
			transaction.abort();
			fail("Unable to delete test data due to: " + e.getMessage());
		}

		transaction = entityManager.startTransaction();
		try {
			delete(transaction, "quantity", Collections.singletonList(quantity));
			delete(transaction, "unit", Collections.singletonList(unit));
			delete(transaction, "physical dimension", Collections.singletonList(physicalDimension));
			delete(transaction, "template test", Collections.singletonList(templateTest));
			delete(transaction, "template test steps", templateTestSteps);
			delete(transaction, "template roots", templateRoots);
			delete(transaction, "catalog components", catalogComponents);

			transaction.commit();
		} catch (RuntimeException e) {
			transaction.abort();
			fail("Unable to delete test data due to: " + e.getMessage());
		}

		if (projects.isEmpty()) {
			fail("Was unable to create test data.");
		}
	}

	private List<Project> createTestData(TemplateTest templateTest, Quantity quantity) throws DataAccessException {
		List<Project> projects = Collections.emptyList();
		Project project = entityFactory.createProject("simple_project");
		Pool pool = entityFactory.createPool("simple_pool", project);

		List<Test> tests = createTests(2, project, pool, templateTest);

//		tests.get(0).getCommissionedTestSteps().get(0)

		// create measurement test data
		List<WriteRequest> writeRequests = new ArrayList<>();
		for (Test test : tests) {
			for (TestStep testStep : test.getCommissionedTestSteps()) {
				Optional<TemplateTestStep> templateTestStep = TemplateTestStep.of(testStep);
				List<ContextRoot> contextRoots = new ArrayList<>();
				if (templateTestStep.isPresent()) {
					contextRoots = templateTestStep.get().getTemplateRoots().stream()
							.map(templateRoot -> entityFactory.createContextRoot(templateRoot))
							.collect(Collectors.toList());
				}

				for (int i = 1; i < 3; i++) {
					Measurement measurement = entityFactory.createMeasurement("measurement_" + i, testStep,
							contextRoots);
					Optional<ContextRoot> contextRootUUT = contextRoots.stream()
							.filter(cr -> cr.getContextType().equals(ContextType.UNITUNDERTEST)).findFirst();
					if (contextRootUUT.isPresent() && !contextRootUUT.get().getContextComponents().isEmpty()) {
						contextRootUUT.get().getContextComponents().forEach(cc -> {
							TestContextData tcd = this.getMeaContextData(test.getName(), testStep.getName());
							if (tcd != null) {
								cc.getValue("date").set(tcd.getDateVal());
								cc.getValue("long").set(tcd.getLongVal());
								if (cc.getName().endsWith("child")) {
									cc.getValue("string").set(tcd.getStrVal() + " child");
								} else {
									cc.getValue("string").set(tcd.getStrVal());
									try {
										FileLink fl = this.getFileLink(tcd.getDirAndFile()[0], tcd.getDirAndFile()[1],
												FileServiceType.EXTREF);
										cc.getValue("file_link").set(fl);
									} catch (Exception e) {
										fail("Unable to create filelink due to: " + e.getMessage());
									}
									try {
										List<FileLink> liste = this.getFileLinkArray(tcd.getArrayDirAndFile(),
												FileServiceType.EXTREF, null, null);
										if (!liste.isEmpty()) {
											cc.getValue("file_link_array").set(liste.toArray(new FileLink[0]));
										}
									} catch (Exception e) {
										fail("Unable to create filelinksequence due to: " + e.getMessage());
									}
								}
							}
						});
					}

					// create channels
					List<Channel> channels = new ArrayList<>();
					for (int j = 0; j < 9; j++) {
						channels.add(entityFactory.createChannel("channel_ " + j, measurement, quantity));
					}

					// create channel group
					ChannelGroup channelGroup = entityFactory.createChannelGroup("group", 10, measurement);
					writeRequests.addAll(createMeasurementData(measurement, channelGroup, channels));
				}
			}
		}

		Transaction transaction = entityManager.startTransaction();
		try {
			update(transaction, "test", tests);
			transaction.writeMeasuredValues(writeRequests);
			transaction.commit();
			projects = Collections.singletonList(project);
		} catch (DataAccessException e) {
			e.printStackTrace();
			transaction.abort();
		}

		this.createMeasurementAoFile(projects);
		return projects;
	}

	private List<WriteRequest> createMeasurementData(Measurement measurement, ChannelGroup channelGroup,
			List<Channel> channels) {
		// set length of the channel value sequence
		List<WriteRequest> writeRequests = new ArrayList<>();

		// populate channel value write requests - one per channel
		Collections.sort(channels, (c1, c2) -> c1.getName().compareTo(c2.getName()));

		WriteRequestBuilder wrb = WriteRequest.create(channelGroup, channels.get(0), AxisType.X_AXIS);
		writeRequests.add(wrb.implicitLinear(ScalarType.FLOAT, 0, 1).independent().build());

		wrb = WriteRequest.create(channelGroup, channels.get(1), AxisType.Y_AXIS);
		writeRequests.add(wrb.explicit()
				.booleanValues(new boolean[] { true, true, false, true, true, false, true, false, false, false })
				.build());

		wrb = WriteRequest.create(channelGroup, channels.get(2), AxisType.Y_AXIS);
		writeRequests.add(wrb.explicit().byteValues(new byte[] { 5, 32, 42, 9, 17, 65, 13, 8, 15, 21 }).build());

		wrb = WriteRequest.create(channelGroup, channels.get(3), AxisType.Y_AXIS);
		writeRequests.add(
				wrb.explicit().integerValues(new int[] { 423, 645, 221, 111, 675, 353, 781, 582, 755, 231 }).build());

		wrb = WriteRequest.create(channelGroup, channels.get(4), AxisType.Y_AXIS);
		writeRequests.add(wrb.explicit()
				.stringValues(new String[] { "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10" }).build());

		Instant now = Instant.now();
		wrb = WriteRequest.create(channelGroup, channels.get(5), AxisType.Y_AXIS);
		writeRequests.add(wrb.explicit()
				.dateValues(new Instant[] { now, now.plus(1, ChronoUnit.DAYS), now.plus(2, ChronoUnit.DAYS),
						now.plus(3, ChronoUnit.DAYS), now.plus(4, ChronoUnit.DAYS), now.plus(5, ChronoUnit.DAYS),
						now.plus(6, ChronoUnit.DAYS), now.plus(7, ChronoUnit.DAYS), now.plus(8, ChronoUnit.DAYS),
						now.plus(9, ChronoUnit.DAYS) })
				.build());

		wrb = WriteRequest.create(channelGroup, channels.get(6), AxisType.Y_AXIS);
		writeRequests.add(wrb.explicit().byteStreamValues(new byte[][] { { 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, { 9, 10 },
				{ 11 }, { 12, 13, 14 }, { 15, 16 }, { 17, 18, 19, 20 }, { 21, 22 }, { 23 } }).build());

		wrb = WriteRequest.create(channelGroup, channels.get(7), AxisType.Y_AXIS);
		writeRequests.add(wrb.implicitConstant(ScalarType.SHORT, Short.MAX_VALUE).build());

		wrb = WriteRequest.create(channelGroup, channels.get(8), AxisType.Y_AXIS);
		writeRequests.add(wrb.implicitSaw(ScalarType.FLOAT, 0, 1, 4).build());

		return writeRequests;
	}

	private void createMeasurementAoFile(List<Project> projects) {
		List<ContextComponent> ccs = new ArrayList<>();
		projects.forEach(p -> p.getChildren(Pool.class)
				.forEach(po -> po.getChildren(Test.class).forEach(t -> t.getChildren(TestStep.class).forEach(ts -> {
					List<Measurement> meaResults = ts.getChildren(Measurement.class);
					if (meaResults != null && !meaResults.isEmpty()) {
						Measurement mea = meaResults.get(0);
						Map<ContextType, ContextRoot> loadMeaContexts = mea.loadContexts(entityManager,
								ContextType.TESTEQUIPMENT, ContextType.TESTSEQUENCE, ContextType.UNITUNDERTEST);
						ContextRoot contextMeaRootUUT = loadMeaContexts.get(ContextType.UNITUNDERTEST);
						if (contextMeaRootUUT != null && !contextMeaRootUUT.getContextComponents().isEmpty()) {
							contextMeaRootUUT.getContextComponents().forEach(cc -> {
								if (!cc.getName().endsWith("child")) {
									try {
										TestContextData tcd = this.getMeaContextData(t.getName(), ts.getName());
										if (tcd != null && tcd.getDirAndAoFile() != null) {
											this.getDescriptiveFile(this.getFileLinkArray(tcd.getDirAndAoFile(),
													FileServiceType.AOFILE, null, null), cc, "file_relation");
										}
									} catch (Exception e) {
										fail("Unable to create file relation due to: " + e.getMessage());
									}
									if (!ccs.contains(cc)) {
										ccs.add(cc);
									}
								}
							});
						}
					}
				}))));
		final Transaction finalTransaction = entityManager.startTransaction();
		try {
			if (!ccs.isEmpty()) {
				update(finalTransaction, "ContextComponents", ccs);
			}
			finalTransaction.commit();
		} catch (RuntimeException e) {
			finalTransaction.abort();
			e.printStackTrace();
			fail("Unable to create test data due to: " + e.getMessage());
		}
	}

	private static void delete(Transaction transaction, String key, Collection<? extends Deletable> entities)
			throws DataAccessException {
		LOGGER.info(">>>>>>>>>>>>>>>>> deleting " + key + "...");
		long start = System.currentTimeMillis();
		transaction.delete(entities);
		LOGGER.info(">>>>>>>>>>>>>>>>> " + key + " deleted in " + (System.currentTimeMillis() - start) + " ms");
	}

	private static void create(Transaction transaction, String key, Collection<? extends Entity> entities)
			throws DataAccessException {
		LOGGER.info(">>>>>>>>>>>>>>>>> creating " + key + "...");
		long start = System.currentTimeMillis();
		transaction.create(entities);
		LOGGER.info(">>>>>>>>>>>>>>>>> " + key + " written in " + (System.currentTimeMillis() - start) + " ms");
	}

	private static void update(Transaction transaction, String key, Collection<? extends Entity> entities)
			throws DataAccessException {
		LOGGER.info(">>>>>>>>>>>>>>>>> update " + key + "...");
		long start = System.currentTimeMillis();
		transaction.update(entities);
		LOGGER.info(">>>>>>>>>>>>>>>>> " + key + " written in " + (System.currentTimeMillis() - start) + " ms");
	}

	private List<Test> createTests(int count, Project project, Pool pool, TemplateTest templateTest) {
		List<Test> collect = IntStream.range(0, testData.length)
				.mapToObj(i -> entityFactory.createTest(testData[i].getName(), pool, templateTest))
				.collect(Collectors.toList());

		Transaction transaction = entityManager.startTransaction();
		try {
			create(transaction, "project and pool with tests based on teamplates with measurements and mass data",
					Collections.singleton(project));
			transaction.commit();
		} catch (RuntimeException e) {
			transaction.abort();
			e.printStackTrace();
			fail("Unable to create test data due to: " + e.getMessage());
		}

		List<ContextComponent> ccs = new ArrayList<>();
		Map<Entity, Set<FileLink>> mapEntityFilelinks = new HashMap<>();
		for (Test test : collect) {
			for (TestStep ts : test.getCommissionedTestSteps()) {
				Map<ContextType, ContextRoot> loadContexts = entityManager.loadContexts(ts, ContextType.TESTEQUIPMENT,
						ContextType.TESTSEQUENCE, ContextType.UNITUNDERTEST);

				ContextRoot contextRootUUT = loadContexts.get(ContextType.UNITUNDERTEST);
				if (contextRootUUT != null && !contextRootUUT.getContextComponents().isEmpty()) {
					contextRootUUT.getContextComponents().forEach(cc -> {
						TestContextData tcd = this.getContextData(test.getName(), ts.getName());
						if (tcd != null) {
							cc.getValue("date").set(tcd.getDateVal());
							cc.getValue("long").set(tcd.getLongVal());
							if (cc.getName().endsWith("child")) {
								cc.getValue("string").set(tcd.getStrVal() + " child");
							} else {
								cc.getValue("string").set(tcd.getStrVal());
								try {
									List<FileLink> liste = this.getFileLinkArray(tcd.getDirAndAoFile(),
											FileServiceType.AOFILE, ts, mapEntityFilelinks);
									this.getDescriptiveFile(liste, cc, "file_relation");
								} catch (Exception e) {
									fail("Unable to create file relation due to: " + e.getMessage());
								}
								if (tcd.getDirAndFile()[0] != null) {
									try {
										FileLink fl = this.getFileLink(tcd.getDirAndFile()[0], tcd.getDirAndFile()[1],
												FileServiceType.EXTREF);
										cc.getValue("file_link").set(fl);
										if (!mapEntityFilelinks.containsKey(ts)) {
											mapEntityFilelinks.put(ts, new HashSet<>());
										}
										mapEntityFilelinks.get(ts).add(fl);
									} catch (Exception e) {
										fail("Unable to create filelink due to: " + e.getMessage());
									}
								}
								try {
									List<FileLink> liste = this.getFileLinkArray(tcd.getArrayDirAndFile(),
											FileServiceType.EXTREF, ts, mapEntityFilelinks);
									if (!liste.isEmpty()) {
										cc.getValue("file_link_array").set(liste.toArray(new FileLink[0]));
									}
								} catch (Exception e) {
									fail("Unable to create filelinksequence due to: " + e.getMessage());
								}
							}
							ccs.add(cc);
						}
					});
				}
			}
		}

		final Transaction finalTransaction = entityManager.startTransaction();
		try {
			update(finalTransaction, "test", collect);
			update(finalTransaction, "ContextComponents", ccs);
			FileService fileService = context.getFileService(FileServiceType.EXTREF).get();
			mapEntityFilelinks.forEach((key, value) -> {
				try {
					fileService.uploadSequential(key, value, finalTransaction, null);
				} catch (IOException e) {
					fail("Unable to upload file due to: " + e.getMessage());
				}
			});
			finalTransaction.commit();
		} catch (RuntimeException e) {
			finalTransaction.abort();
			e.printStackTrace();
			fail("Unable to create test data due to: " + e.getMessage());
		}

		return collect;
	}

	private FileLink getFileLink(String valPath, String valFile, FileServiceType fileServiceType) throws IOException {
		Path path = FileSystems.getDefault().getPath(valPath, valFile);
		File file = path.toAbsolutePath().toFile();
		FileInputStream fis = new FileInputStream(file);
		String mimeType = Files.probeContentType(path);
		MimeType mimeT = mimeType == null ? null : new MimeType(mimeType);
		return FileLink.newLocal(fis, file.getName(), -1L, mimeT, "", null, fileServiceType);
	}

	private List<FileLink> getFileLinkArray(List<String[]> valListe, FileServiceType fileServiceType, TestStep ts,
			Map<Entity, Set<FileLink>> mapEntityFilelinks) throws IOException {
		List<FileLink> liste = new ArrayList<>();
		for (String[] values : valListe) {
			FileLink fl;
			if (values[1] == null) {
				fl = FileLink.newLocal(null, values[0], -1L, new MimeType("application/x-openmdm.link"), values[0],
						null, fileServiceType);
				fl.setRemotePath(values[0]);
			} else {
				fl = this.getFileLink(values[0], values[1], fileServiceType);
				if (mapEntityFilelinks != null) {
					if (!mapEntityFilelinks.containsKey(ts)) {
						mapEntityFilelinks.put(ts, new HashSet<>());
					}
					mapEntityFilelinks.get(ts).add(fl);
				}
			}
			liste.add(fl);
		}
		return liste;
	}

	private void getDescriptiveFile(List<FileLink> listeFileLink, ContextComponent contextComponent,
			String attributeName) {
		if (listeFileLink.isEmpty()) {
			return;
		}
		Relation relDescriptiveFile = null;
		if (descriptiveFileManager.getDescriptiveFileRelationNames(contextComponent).contains(attributeName)) {
			relDescriptiveFile = descriptiveFileManager.getDescriptiveFileRelation(contextComponent, attributeName);
		}
		for (FileLink fileLink : listeFileLink) {
			DescriptiveFile descriptiveFile = entityFactory.createDescriptiveFile(contextComponent, relDescriptiveFile);
			descriptiveFile.setName(Strings.isNullOrEmpty(fileLink.getFileName()) ? descriptiveFile.getTypeName()
					: fileLink.getFileName());
			descriptiveFile.setDescription(fileLink.getDescription());
			descriptiveFile.setFileMimeType(fileLink.getMimeType().toString());
			descriptiveFile.setOriginalFileName(fileLink.getFileName());
			try (Transaction t = entityManager.startTransaction()) {
				t.create(Collections.singleton(descriptiveFile));
				t.commit();
			}
			descriptiveFile.setFileLink(fileLink);
		}
		try (Transaction t = entityManager.startTransaction()) {
			FileService fileService = context.getFileService(FileServiceType.AOFILE).get();
			try {
				fileService.uploadSequential(contextComponent, listeFileLink, t, null);
			} catch (IOException e) {
				fail("Unable to upload file due to: " + e.getMessage());
			}
			t.commit();
		}
	}

	private TestContextData getContextData(String testName, String testStepName) {
		for (int i = 0; i < testData.length; i++) {
			if (testData[i].getName().equals(testName)) {
				TestStepData[] tsData = testData[i].getTestSteps();
				if (tsData != null) {
					for (int j = 0; j < tsData.length; j++) {
						if (tsData[j].getName().equals(testStepName)) {
							return tsData[j].getCcData();
						}
					}
				}
			}
		}
		return null;
	}

	private TestContextData getMeaContextData(String testName, String testStepName) {
		for (int i = 0; i < testData.length; i++) {
			if (testData[i].getName().equals(testName)) {
				TestStepData[] tsData = testData[i].getTestSteps();
				if (tsData != null) {
					for (int j = 0; j < tsData.length; j++) {
						if (tsData[j].getName().equals(testStepName)) {
							return tsData[j].getMeaCcData();
						}
					}
				}
			}
		}
		return null;
	}

	private TemplateTest createTemplateTest(List<TemplateTestStep> templateTestSteps) {
		TemplateTest templateTest = entityFactory.createTemplateTest("tpl_test");
		templateTestSteps.forEach(tts -> {
			entityFactory.createTemplateTestStepUsage(UUID.randomUUID().toString(), templateTest, tts);
		});
		templateTest.setVersionState(VersionState.VALID);
		return templateTest;
	}

	private List<TemplateTestStep> createTemplateTestSteps(List<TemplateRoot> templateRoots) {
		// make sure each context type is given only once
		templateRoots.stream().collect(Collectors.toMap(TemplateRoot::getContextType, Function.identity()));

		List<TemplateTestStep> templateTestSteps = new ArrayList<>();
		for (TestStepData ttd : testStepDataIdent) {
			TemplateTestStep templateTestStep1 = entityFactory.createTemplateTestStep(ttd.getName());
			templateRoots.forEach(tr -> templateTestStep1.setTemplateRoot(tr));
			templateTestStep1.setVersionState(VersionState.VALID);
			templateTestSteps.add(templateTestStep1);
		}

		return templateTestSteps;
	}

	private List<TemplateRoot> createTemplateRoots(List<CatalogComponent> catalogComponents) {
		Map<ContextType, List<CatalogComponent>> groups = catalogComponents.stream()
				.collect(Collectors.groupingBy(CatalogComponent::getContextType));

		List<TemplateRoot> templateRoots = new ArrayList<>();
		groups.forEach((contextType, catalogComps) -> {
			TemplateRoot templateRoot = entityFactory.createTemplateRoot(contextType,
					"tpl_" + toLower(contextType.name()) + "_root");

			// create child template components for template root
			catalogComps.forEach(catalogComp -> {
				TemplateComponent templateComponent = entityFactory
						.createTemplateComponent("tpl_" + catalogComp.getName() + "_parent", templateRoot, catalogComp);
				entityFactory.createTemplateComponent("tpl_" + catalogComp.getName() + "_child", templateComponent,
						catalogComp);
			});

			templateRoot.setVersionState(VersionState.VALID);
			templateRoots.add(templateRoot);
		});

		return templateRoots;
	}

	private List<CatalogComponent> createCatalogComponents() {
		List<CatalogComponent> catalogComponents = new ArrayList<>();
		catalogComponents.add(createCatalogComponent(ContextType.UNITUNDERTEST, "Testunit"));
		catalogComponents.add(createCatalogComponent(ContextType.TESTSEQUENCE, "Testseq"));
		catalogComponents.add(createCatalogComponent(ContextType.TESTEQUIPMENT, "Testequip"));
		return catalogComponents;
	}

	private CatalogComponent createCatalogComponent(ContextType contextType, String name) {
		CatalogComponent catalogComponent = entityFactory.createCatalogComponent(contextType, name);

		entityFactory.createCatalogAttribute("string", ValueType.STRING, catalogComponent);
		entityFactory.createCatalogAttribute("date", ValueType.DATE, catalogComponent);
		entityFactory.createCatalogAttribute("long", ValueType.LONG, catalogComponent);
		entityFactory.createCatalogAttribute("file_relation", ValueType.FILE_RELATION, catalogComponent);
		entityFactory.createCatalogAttribute("file_link", ValueType.FILE_LINK, catalogComponent);
		entityFactory.createCatalogAttribute("file_link_array", ValueType.FILE_LINK_SEQUENCE, catalogComponent);
		EnumRegistry er = EnumRegistry.getInstance();
		entityFactory.createCatalogAttribute("scalar_type", ValueType.ENUMERATION,
				er.get(context.getSourceName(), EnumRegistry.SCALAR_TYPE), catalogComponent);

		return catalogComponent;
	}

	private static String toLower(String name) {
		return name.toLowerCase(Locale.ROOT);
	}

	private void deleteFiles(List<Project> projects) {
		Transaction transaction = entityManager.startTransaction();
		try {
			// delete in reverse order!
			if (!projects.isEmpty()) {
				List<Test> tests = new ArrayList<>();
				List<TestStep> testSteps = new ArrayList<>();
				List<Measurement> listeMeaResult = new ArrayList<>();
				List<ContextRoot> listeRC = new ArrayList<>();
				List<ContextRoot> listeMeaCC = new ArrayList<>();
				List<ContextComponent> listeCC = new ArrayList<>();
				Map<ContextDescribable, List<FileLink>> mapFL = new HashMap<>();
				List<DescriptiveFile> listeDF = new ArrayList<>();
				projects.forEach(p -> p.getChildren(Pool.class).forEach(
						po -> po.getChildren(Test.class).forEach(t -> t.getChildren(TestStep.class).forEach(ts -> {
							Map<ContextType, ContextRoot> loadContexts = entityManager.loadContexts(ts,
									ContextType.TESTEQUIPMENT, ContextType.TESTSEQUENCE, ContextType.UNITUNDERTEST);
							ContextRoot contextRootUUT = loadContexts.get(ContextType.UNITUNDERTEST);
							if (contextRootUUT != null && !contextRootUUT.getContextComponents().isEmpty()) {
								contextRootUUT.getContextComponents().forEach(cc -> {
									cc.getValue("string").set(null);
									cc.getValue("date").set(null);
									cc.getValue("long").set(null);
									listeDF.addAll(this.getAoFiles4Deletion(cc));
									mapFL.put(ts, this.getExtRefFiles4Deletion(cc, ts));
									cc.getValue("file_link").set(null);
									cc.getValue("file_link_array").set(null);
									listeCC.add(cc);
								});
								if (!listeRC.contains(contextRootUUT)) {
									listeRC.add(contextRootUUT);
								}
							}
							contextRootUUT = loadContexts.get(ContextType.TESTSEQUENCE);
							if (contextRootUUT != null && !contextRootUUT.getContextComponents().isEmpty()) {
								contextRootUUT.getContextComponents().forEach(cc -> listeCC.add(cc));
								if (!listeRC.contains(contextRootUUT)) {
									listeRC.add(contextRootUUT);
								}
							}
							contextRootUUT = loadContexts.get(ContextType.TESTEQUIPMENT);
							if (contextRootUUT != null && !contextRootUUT.getContextComponents().isEmpty()) {
								contextRootUUT.getContextComponents().forEach(cc -> listeCC.add(cc));
								if (!listeRC.contains(contextRootUUT)) {
									listeRC.add(contextRootUUT);
								}
							}
							List<Measurement> meaResults = ts.getChildren(Measurement.class);
							if (meaResults != null && !meaResults.isEmpty()) {
								Measurement mea = meaResults.get(0);
								Map<ContextType, ContextRoot> loadMeaContexts = mea.loadContexts(entityManager,
										ContextType.TESTEQUIPMENT, ContextType.TESTSEQUENCE, ContextType.UNITUNDERTEST);
//								Map<ContextType, ContextRoot> loadMeaContexts = entityManager.loadContexts(mea,
//										ContextType.TESTEQUIPMENT, ContextType.TESTSEQUENCE, ContextType.UNITUNDERTEST);
								ContextRoot contextMeaRootUUT = loadMeaContexts.get(ContextType.UNITUNDERTEST);
								if (contextMeaRootUUT != null && !contextMeaRootUUT.getContextComponents().isEmpty()) {
									contextMeaRootUUT.getContextComponents().forEach(cc -> {
										cc.getValue("string").set(null);
										cc.getValue("date").set(null);
										cc.getValue("long").set(null);
//										listeDF.addAll(this.getAoFiles4Deletion(cc));
										mapFL.put(mea, this.getExtRefFiles4Deletion(cc, mea));
										cc.getValue("file_link").set(null);
										cc.getValue("file_link_array").set(null);
										listeCC.add(cc);
									});
									if (!listeMeaCC.contains(contextMeaRootUUT)) {
										listeMeaCC.add(contextMeaRootUUT);
									}
								}
								if (!listeMeaResult.contains(mea)) {
									listeMeaResult.add(mea);
								}
								contextRootUUT = loadMeaContexts.get(ContextType.TESTSEQUENCE);
								if (contextRootUUT != null && !contextRootUUT.getContextComponents().isEmpty()) {
									contextRootUUT.getContextComponents().forEach(cc -> listeCC.add(cc));
									if (!listeMeaCC.contains(contextMeaRootUUT)) {
										listeMeaCC.add(contextMeaRootUUT);
									}
								}
								contextRootUUT = loadMeaContexts.get(ContextType.TESTEQUIPMENT);
								if (contextRootUUT != null && !contextRootUUT.getContextComponents().isEmpty()) {
									contextRootUUT.getContextComponents().forEach(cc -> listeCC.add(cc));
									if (!listeMeaCC.contains(contextMeaRootUUT)) {
										listeMeaCC.add(contextMeaRootUUT);
									}
								}
							}
							if (!testSteps.contains(ts)) {
								testSteps.add(ts);
							}
							if (!tests.contains(t)) {
								tests.add(t);
							}
						}))));
				if (!listeDF.isEmpty()) {
					delete(transaction, "descriptivefiles", listeDF);
				}
				if (!mapFL.isEmpty()) {
					FileService fileService = context.getFileService(FileServiceType.EXTREF).get();
					mapFL.forEach((key, value) -> {
						fileService.delete(key, value, transaction);
					});
				}
				if (!listeCC.isEmpty()) {
					delete(transaction, "Context", listeCC);
				}
				if (!listeRC.isEmpty()) {
					delete(transaction, "FileRelation", listeRC);
				}
				if (!listeMeaCC.isEmpty()) {
					delete(transaction, "FileRelation", listeMeaCC);
				}
				if (!listeMeaResult.isEmpty()) {
					delete(transaction, "Measurement", listeMeaResult);
				}
				if (!testSteps.isEmpty()) {
					delete(transaction, "TestSteps", testSteps);
				}
				if (!tests.isEmpty()) {
					delete(transaction, "Tests", tests);
				}
			}
			transaction.commit();
		} catch (RuntimeException e) {
			transaction.abort();
			fail("Unable to delete test data due to: " + e.getMessage());
		}
	}

	private List<DescriptiveFile> getAoFiles4Deletion(ContextComponent cc) {
		List<DescriptiveFile> listeDF = new ArrayList<>();
		if (descriptiveFileManager.getDescriptiveFileRelationNames(cc).contains("file_relation")) {
			Relation relDescriptiveFile = descriptiveFileManager.getDescriptiveFileRelation(cc, "file_relation");
			cc.getDescriptiveFiles(relDescriptiveFile, entityManager).forEach(df -> {
				boolean hasAdditionalRelatedInstances = descriptiveFileManager
						.hasAdditionalRelatedInstances(entityManager, df, relDescriptiveFile);
				cc.removeDescriptiveFile(relDescriptiveFile, df);
				if (!hasAdditionalRelatedInstances) {
					listeDF.add(df);
				}
			});
		}
		return listeDF;
	}

	private List<FileLink> getExtRefFiles4Deletion(ContextComponent cc, ContextDescribable ts) {
		List<FileLink> liste = new ArrayList<>();
		Value val = cc.getValue("file_link");
		if (val.extractInitial() instanceof FileLink) {
			liste.add((FileLink) val.extractInitial());
		}
		val = cc.getValue("file_link_array");
		if (val.extractInitial() instanceof FileLink[]) {
			FileLink[] fls = (FileLink[]) val.extractInitial();
			for (int i = 0; i < fls.length; i++) {
				if (!fls[i].isLink()) {
					liste.add(fls[i]);
				}
			}
		}
		return liste;
	}
}
