package org.eclipse.mdm.api.odsadapter.model;

public class TestStepData {

	private String name;
	private TestContextData ccData;
	private TestContextData meaCcData;

	public TestStepData(String name, TestContextData ccData, TestContextData meaCcData) {
		this.name = name;
		this.ccData = ccData;
		this.meaCcData = meaCcData;
	}

	public String getName() {
		return name;
	}

	public TestContextData getCcData() {
		return ccData;
	}

	public TestContextData getMeaCcData() {
		return meaCcData;
	}
}
