/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.api.odsadapter.search;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_NAMESERVICE;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_PASSWORD;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_SERVICENAME;
import static org.eclipse.mdm.api.odsadapter.ODSContextFactory.PARAM_USER;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.ParameterSet;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.FilterItem;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.search.ContextState;
import org.eclipse.mdm.api.base.search.SearchService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.ODSContextFactory;
import org.eclipse.mdm.api.odsadapter.query.ODSModelManager;
import org.eclipse.mdm.api.odsadapter.search.JoinTree.JoinConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.mockito.Mockito;

import com.google.common.collect.ImmutableBiMap;

@Ignore
//FIXME 10.7.2017: this test needs a running ODS Server, that is not suitable for continous build in Jenkins.
//Comment this in for local tests only.
public class ODSSearchServiceStatusTest {

	/*
	 * ATTENTION: ==========
	 *
	 * To run this test make sure the target service is running a MDM default model
	 * and any database constraint which enforces a relation of Test to a parent
	 * entity is deactivated!
	 */

	private static final String NAME_SERVICE = "corbaloc::1.2@%s:%s/NameService";

	private static final String USER = "sa";
	private static final String PASSWORD = "sa";

	private static ApplicationContext context;
	private static ModelManager modelManager;
	private static SearchService searchService;

	@BeforeClass
	public static void setUpBeforeClass() throws ConnectionException {
		String nameServiceHost = System.getProperty("host");
		String nameServicePort = System.getProperty("port");
		String serviceName = System.getProperty("service");

		if (nameServiceHost == null || nameServiceHost.isEmpty()) {
			throw new IllegalArgumentException("name service host is unknown: define system property 'host'");
		}

		nameServicePort = nameServicePort == null || nameServicePort.isEmpty() ? String.valueOf(2809) : nameServicePort;
		if (nameServicePort == null || nameServicePort.isEmpty()) {
			throw new IllegalArgumentException("name service port is unknown: define system property 'port'");
		}

		if (serviceName == null || serviceName.isEmpty()) {
			throw new IllegalArgumentException("service name is unknown: define system property 'service'");
		}

		Map<String, String> connectionParameters = new HashMap<>();
		connectionParameters.put(PARAM_NAMESERVICE, String.format(NAME_SERVICE, nameServiceHost, nameServicePort));
		connectionParameters.put(PARAM_SERVICENAME, serviceName + ".ASAM-ODS");
		connectionParameters.put(PARAM_USER, USER);
		connectionParameters.put(PARAM_PASSWORD, PASSWORD);

		context = new ODSContextFactory().connect(serviceName, connectionParameters);
		modelManager = context.getModelManager().orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));
		searchService = context.getSearchService()
				.orElseThrow(() -> new IllegalStateException("Search service not available."));
	}

	@AfterClass
	public static void tearDownAfterClass() throws ConnectionException {
		if (context != null) {
			context.close();
		}
	}

	final class MeasurementSearchQuery extends BaseEntitySearchQuery {

		/**
		 * Constructor.
		 *
		 * @param modelManager Used to load {@link EntityType}s.
		 * @param contextState The {@link ContextState}.
		 */
		MeasurementSearchQuery(ODSModelManager modelManager, QueryService queryService, ContextState contextState) {
			super(modelManager, queryService, ParameterSet.class, Project.class);

			// layers
			addJoinConfig(JoinConfig.up(Pool.class, Project.class));
			addJoinConfig(JoinConfig.up(Test.class, Pool.class));
			addJoinConfig(JoinConfig.up(TestStep.class, Test.class));

			// context
			addJoinConfig(contextState);
		}
	};

	final static class ServiceUtils {

		public static ImmutableBiMap<String, String> entityType2entityName = new ImmutableBiMap.Builder<String, String>()
				.put("StructureLevel", "Pool").put("MeaResult", "Measurement").put("SubMatrix", "ChannelGroup")
				.put("MeaQuantity", "Channel").build();

		private ServiceUtils() {
		}

		public static String workaroundForTypeMapping(EntityType entityType) {
			return workaroundForTypeMapping(entityType.getName());
		}

		/**
		 * Simple workaround for naming mismatch between Adapter and Business object
		 * names.
		 *
		 * @param entityType entity type
		 * @return MDM business object name
		 */
		public static String workaroundForTypeMapping(String entityType) {
			return entityType2entityName.getOrDefault(entityType, entityType);
		}
	};

	@org.junit.Test
	public void tesGetProjec4TestStep() throws Exception {

		ODSSearchService service = Mockito.spy((ODSSearchService) searchService);

		EntityType test = modelManager.getEntityType(Test.class);

		Filter filter = Filter.and().add(ComparisonOperator.EQUAL.create(test.getAttribute("Status"), "Active"));
		List<Attribute> attributesList = getAttributeListFromFilter(filter);

		List<Project> projects = service.fetch(Project.class, attributesList, filter);

		assertThat(projects.get(0).getID()).isEqualTo("2001");
	}

	@org.junit.Test
	public void tesGetContextComponent() throws Exception {
		EntityType test = modelManager.getEntityType(TestStep.class);
		Class<? extends Entity> resultType = TestStep.class;

		List<EntityType> searchableTypes = searchService.listEntityTypes(resultType);
		List<String> columns = Arrays.asList("Test.Name", "TestStep.Name", "vehicle.id", "vehicle.vehicle_type",
				"vehicle.vehicle_category", "tyre.axle", "tyre.manufacturer", "tyre.Name", "tyre.side", "tyre.size");
		List<Attribute> attributes = columns.stream().map(c -> getAttribute(searchableTypes, c))
				.filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());

		Filter filter = Filter.and()
				.add(ComparisonOperator.EQUAL.create(test.getAttribute("Name"), "EngineNoise - 100% load"));

		EntityType resultContextCompType = null;
		for (EntityType type : searchableTypes) {
			if ("tyre".equalsIgnoreCase(type.getName())) {
				resultContextCompType = type;
				break;
			}
		}
		List<?> result = searchService.fetchResults(resultType, attributes, filter, "", 0, 0, resultContextCompType);
		int anz = result.size();

		assertThat(result.size()).isEqualTo(8);
	}

	private Optional<Attribute> getAttribute(List<EntityType> searchableTypes, String c) {
		String[] parts = c.split("\\.");

		if (parts.length != 2) {
			throw new IllegalArgumentException("Cannot parse attribute " + c + "!");
		}

		String type = parts[0];
		String attributeName = parts[1];

		Optional<EntityType> entityType = searchableTypes.stream()
				.filter(e -> ServiceUtils.workaroundForTypeMapping(e).equalsIgnoreCase(type)).findFirst();

		if (entityType.isPresent()) {
			return entityType.get().getAttributes().stream().filter(a -> a.getName().equalsIgnoreCase(attributeName))
					.findFirst();
		} else {
			return Optional.empty();
		}
	}

	private List<Attribute> getAttributeListFromFilter(Filter filter) {
		List<Attribute> attributeList = new ArrayList<>();
		Iterator<FilterItem> fIterator = filter.iterator();
		while (fIterator.hasNext()) {
			FilterItem filterItem = fIterator.next();
			if (filterItem.isCondition()) {
				attributeList.add(filterItem.getCondition().getAttribute());
			}
		}

		return attributeList;
	}
}
