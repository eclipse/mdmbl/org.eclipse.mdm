/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { DatePipe } from "@angular/common";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { HttpErrorHandler } from "@core/http-error-handler";
import { MDMItem } from "@core/mdm-item";
import { PropertyService } from "@core/property.service";
import { Parameter, ParameterSet } from "@details/model/details.model";
import { DataType } from "@details/model/parameterset/parameter";
import { Node } from "@navigator/node";
import { catchError, map } from "rxjs/operators";

@Injectable()
export class ParameterSetService {
  private prefEndpoint: string;

  private httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json",
    }),
  };

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandler,
    private _prop: PropertyService,
    private datePipe: DatePipe,
  ) {
    this.prefEndpoint = _prop.getUrl("mdm/environments/");
  }

  createParameterSet(node: Node | MDMItem, parameterSet: ParameterSet) {
    const requestData = new Object();
    requestData["Name"] = parameterSet.name;
    if (node != null) {
      requestData[node.type] = node.id;
    }

    return this.http.post(this.prefEndpoint + node["sourceName"] + "/parametersets", JSON.stringify(requestData), this.httpOptions);
  }

  removeParameter(parameter: Parameter) {
    return this.http.delete(this.prefEndpoint + parameter.sourceName + "/parameters/" + parameter.id, this.httpOptions);
  }

  removeParameterSet(parameterSet: ParameterSet) {
    return this.http.delete(this.prefEndpoint + parameterSet.sourceName + "/parametersets/" + parameterSet.id, this.httpOptions);
  }

  updateParameterSet(parameterSet: ParameterSet) {
    const requestData = new Object();
    requestData["Name"] = parameterSet.name;
    return this.http.put(
      this.prefEndpoint + parameterSet.sourceName + "/parametersets/" + parameterSet.id,
      JSON.stringify(requestData),
      this.httpOptions,
    );
  }

  createParameterForParameterSet(parameterSet: ParameterSet, parameter: Parameter) {
    const requestData = new Object();
    requestData["Name"] = parameter.name;
    requestData["ParameterSet"] = parameterSet.id;
    requestData["Unit"] = parameter.unitId;

    switch (DataType[parameter.dataType]) {
      case DataType.string:
        requestData["stringValue"] = parameter.stringValue;
        break;
      case DataType.integer:
        requestData["integerValue"] = parameter.integerValue;
        break;
      case DataType.double:
        requestData["doubleValue"] = parameter.doubleValue;
        break;
      case DataType.boolean:
        requestData["booleanValue"] = parameter.booleanValue;
        break;
      case DataType.date:
        requestData["dateValue"] = this.datePipe.transform(parameter.dateValue, "yyyyMMddHHmmss");
        break;
    }

    return this.http.post(this.prefEndpoint + parameterSet.sourceName + "/parameters", JSON.stringify(requestData), this.httpOptions);
  }

  getParameterSets(node: Node | MDMItem) {
    if (node.type === "Measurement" || node.type === "Channel") {
      return this.getParameterSetsByRoot(node["sourceName"], node.type, node.id);
    }
  }

  getParameterSetsByRoot(sourceName: string, type: string, id: string) {
    let url = this.prefEndpoint + sourceName;
    url = url + "/parametersets?filter=" + type + '.Id eq "' + id + '"';
    return this.http.get<any>(url).pipe(
      map((response) => (<Node[]>response.data) as any),
      catchError((e) => addErrorDescription(e, "Could not request resultparameter element!")),
    );
  }

  getParameterSetBId(sourceName: string, id: string) {
    const url = this.prefEndpoint + sourceName + "/parametersets" + id;
    return this.http.get<any>(url).pipe(
      map((response) => (<Node[]>response.data) as any),
      catchError((e) => addErrorDescription(e, "Could not request resultparameter element!")),
    );
  }

  getParameters(sourceName: string, parametersetId: string) {
    let url = this.prefEndpoint + sourceName;
    url = url + '/parameters?filter=ResultParameterSet.Id eq "' + parametersetId + '"';
    return this.http.get<any>(url).pipe(
      map((response) => (<Node[]>response.data) as any),
      catchError((e) => addErrorDescription(e, "Could not request resultparameter element!")),
    );
  }

  getUnit(sourceName: string, unitId: string) {
    const url = this.prefEndpoint + sourceName + "/units/" + unitId;
    return this.http.get<any>(url).pipe(
      map((response) => (<Node[]>response.data) as any),
      catchError((e) => addErrorDescription(e, "Could not request resultparameter element!")),
    );
  }

  getUnits(sourceName: string) {
    const url = this.prefEndpoint + sourceName + "/units/";
    return this.http.get<any>(url).pipe(
      map((response) => (<Node[]>response.data) as any),
      catchError((e) => addErrorDescription(e, "Could not request resultparameter element!")),
    );
  }

  saveParameter(parameter: Parameter) {
    const url = this.prefEndpoint + parameter.sourceName + "/parameters/" + parameter.id;

    const requestData = new Object();
    switch (parameter.dataType) {
      case DataType.boolean:
        requestData["Value"] = parameter.value.toString();
        break;
      case DataType.date:
        requestData["Value"] = this.datePipe.transform(parameter.dateValue, "yyyyMMddHHmmss");
        break;
      default:
        requestData["Value"] = parameter.value;
    }

    requestData["Unit"] = parameter.unitId;

    return this.http.put<any>(url, JSON.stringify(requestData), this.httpOptions).pipe(
      map((response) => (<Node[]>response.data) as any),
      catchError((e) => addErrorDescription(e, "Could not request resultparameter element!")),
    );
  }
}
