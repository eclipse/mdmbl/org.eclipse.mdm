/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { MDMItem } from "@core/mdm-item";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { NavigatorService } from "@navigator/navigator.service";
import { Node } from "@navigator/node";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "mdm-detail-view",
  templateUrl: "mdm-detail-view.component.html",
  styleUrls: ["mdm-detail-view.component.css"],
})
export class MDMDetailViewComponent implements OnInit, OnDestroy {
  selectedNode: Node;
  unit: Node;
  quantity: Node;
  subscription: any;
  shoppable = false;

  private NO_NODE_STATUS = "file-explorer.file-explorer-nav-card.status-no-node-selected";
  public VIRTUAL_NODE_STATUS = "file-explorer.file-explorer-nav-card.status-virtual-node-selected";
  public status = this.NO_NODE_STATUS;

  constructor(
    private navigatorService: NavigatorService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private nodeService: NodeService,
  ) {}

  ngOnInit() {
    this.subscription = this.navigatorService.onSelectionChanged().subscribe(
      (node) => this.handleNavigatorSelectionChanged(node),
      (error) => this.notificationService.notifyError(this.translateService.instant("details.mdm-detail-view.cannot-update-node"), error),
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private handleNavigatorSelectionChanged(object: Node | MDMItem) {
    this.resetState();
    if (object instanceof Node) {
      this.handleSelectedNodeChanged(object);
    } else if (object instanceof MDMItem) {
      this.status = this.VIRTUAL_NODE_STATUS;
    } else {
      this.status = this.NO_NODE_STATUS;
    }
  }

  private resetState() {
    this.selectedNode = undefined;
    this.quantity = undefined;
    this.unit = undefined;
  }

  private handleSelectedNodeChanged(node: Node) {
    this.selectedNode = node;
    if (node !== undefined) {
      if (node.type.toLowerCase() === "channel") {
        this.loadQuantity(node);
        this.loadUnit(node);
      } else {
        this.quantity = undefined;
        this.unit = undefined;
      }
      this.shoppable = this.isShoppable();
    }
  }

  private loadUnit(node: Node) {
    if (node.relations != undefined) {
      node.relations
        .filter((rel) => rel.entityType.toLowerCase() === "unit")
        .filter((rel) => rel.ids.length > 0)
        .forEach((rel) =>
          this.nodeService.getNodeByRelation(node.sourceName, rel.entityType, rel.ids[0]).subscribe((res) => (this.unit = res[0])),
        );
    }
  }

  private loadQuantity(node: Node) {
    if (node.relations != undefined) {
      node.relations
        .filter((rel) => rel.entityType.toLowerCase() === "quantity")
        .filter((rel) => rel.ids.length > 0)
        .forEach((rel) =>
          this.nodeService.getNodeByRelation(node.sourceName, rel.entityType, rel.ids[0]).subscribe((res) => (this.quantity = res[0])),
        );
    }
  }

  isShoppable() {
    if (
      this.selectedNode &&
      this.selectedNode.name != undefined &&
      this.selectedNode.type !== "Environment" &&
      this.selectedNode.type !== "Unit" &&
      this.selectedNode.type !== "Channel"
    ) {
      return false;
    }
    return true;
  }

  isReleasable() {
    if (this.selectedNode && this.selectedNode.sourceType === "TestStep") {
      return false;
    }
    return true;
  }
}
