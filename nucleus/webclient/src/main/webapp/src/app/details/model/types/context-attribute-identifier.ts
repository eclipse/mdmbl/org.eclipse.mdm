/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Attribute, ContextGroup, Node } from "@navigator/node";

export class ContextAttributeIdentifier {
  contextDescribable: Node;
  contextComponent: Node;
  sourceName?: string;
  attribute: Attribute;
  contextGroup?: ContextGroup;
  contextType?: string;
  constructor(contextDescribable: Node, contextComponent: Node, attribute: Attribute, contextGroup?: ContextGroup, contextType?: string) {
    this.contextDescribable = contextDescribable;
    this.contextComponent = contextComponent;
    this.attribute = attribute;
    this.contextGroup = contextGroup;
    this.contextType = contextType;
  }
}
