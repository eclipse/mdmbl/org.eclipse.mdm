/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { Node } from "@navigator/node";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";

@Injectable()
export class SystemParameterService {
  private endpointEnv: string;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this.endpointEnv = _prop.getUrl("mdm/environments/");
  }

  findSystemParameter(environment: string, parameter: string): Observable<Node[]> {
    return this.http.get<any>(this.endpointEnv + environment + '/systemparameters?filter=SystemParameter.Name eq "' + parameter + '"').pipe(
      map((resp) => (<Node[]>resp.data) as any),
      catchError((e) => addErrorDescription(e, "Could not request SystemParameter " + parameter)),
    );
  }
}
