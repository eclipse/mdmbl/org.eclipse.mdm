/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import { NgModel } from "@angular/forms";
import { PreferenceService } from "@core/preference.service";
import { toMdmRestApiString } from "@core/utils/date-time-utils";
import { AttributeValidatorService } from "@details/services/attribute-validator.service";
import { EnumerationService } from "@details/services/enumeration.service";
import { AttributeValuePipe } from "@navigator/attribute-value.pipe";
import { MdmEnumValue } from "@navigator/node";
import { DateTime } from "luxon";
import { map, Observable, startWith, take, tap } from "rxjs";
import { ValueListService } from "src/app/administration/value-list/valuelist.service";
import { QueryService } from "src/app/tableview/query.service";
import { TimezoneService } from "src/app/timezone/timezone-service";
import { ContextAttributeIdentifier } from "../../model/details.model";

@Component({
  selector: "mdm-attribute-editor",
  templateUrl: "./attribute-editor.component.html",
  styleUrls: ["./attribute-editor.component.css"],
  providers: [AttributeValuePipe],
})
export class AttributeEditorComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  @Input() contextAttribute: ContextAttributeIdentifier;
  suggestions: string[] = [];
  dateTime: DateTime;
  editDialogVisible = true;
  currentSequenceValue = "";
  enumerations: { [name: string]: MdmEnumValue[] } = {};
  @Input() width: string;
  @Input() validationPattern: Observable<string>;

  @Output() formReady = new EventEmitter<{ controlName: string; control: NgModel }>();
  @ViewChild("inputField", { static: false }) inputField: NgModel;
  controlName: string;

  private readonly PREF_ATTR_REX_EXP = "context_attribute.validators";

  public valueListValues: Observable<string[]>;

  constructor(
    private queryService: QueryService,
    private valueListService: ValueListService,
    private attributeValuePipe: AttributeValuePipe,
    private enumerationService: EnumerationService,
    private timezoneService: TimezoneService,
    private preferenceService: PreferenceService,
    private attributeValidatorService: AttributeValidatorService,
  ) {}

  ngOnInit(): void {
    this.controlName = this.contextAttribute?.attribute?.name;
    this.getSequenceAttributeValue();

    if (this.contextAttribute?.contextDescribable) {
      this.queryService
        .suggestValues([this.getSourceName()], this.contextAttribute?.contextDescribable.name, this.contextAttribute?.attribute.name)
        .subscribe((data) => {
          this.suggestions = data;
        });
    }

    const valueListId = this.contextAttribute.attribute.valueList;
    if (valueListId) {
      this.valueListValues = this.valueListService
        .readValueListValues(this.getSourceName(), valueListId)
        .pipe(map((vlvs) => vlvs?.map((vlv) => vlv.value).sort((vlv1, vlv2) => vlv1.localeCompare(vlv2))));
    }

    this.validationPattern = this.attributeValidatorService
      .getAttributeValidatorPattern(this.contextAttribute)
      .pipe(startWith("Loading..."));
  }

  ngAfterViewInit(): void {
    this.formReady.emit({ controlName: this.controlName, control: this.inputField });
  }

  private getSourceName(): string {
    return this.contextAttribute?.sourceName ?? this.contextAttribute?.contextComponent?.sourceName;
  }

  private getSequenceAttributeValue(): void {
    this.currentSequenceValue = this.attributeValuePipe.transform(this.contextAttribute?.attribute, this.contextAttribute.contextGroup);
  }

  public addNewSequenceEntry(): void {
    if (this.contextAttribute?.attribute.value[this.contextAttribute?.contextGroup] == "") {
      this.contextAttribute.attribute.value[this.contextAttribute?.contextGroup] = [];
    }
    this.contextAttribute?.attribute.value[this.contextAttribute?.contextGroup].push("");
  }

  public deleteSequenceEntry(index: number): void {
    this.contextAttribute?.attribute.value[this.contextAttribute?.contextGroup].splice(index, 1);
  }

  trackByFn(index: any): number {
    return index;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (Object.prototype.hasOwnProperty.call(changes, "contextAttribute")) {
      if (
        this.contextAttribute.attribute.dataType === "ENUMERATION" ||
        this.contextAttribute.attribute.dataType === "ENUMERATION_SEQUENCE"
      ) {
        const sourceName = this.contextAttribute.sourceName ?? this.contextAttribute.contextComponent?.sourceName;
        this.enumerationService.getAllEnumerations(sourceName).subscribe(
          (e) =>
            (this.enumerations = e.reduce((p, c) => {
              p[c.name] = c.values;
              return p;
            }, {} as { [name: string]: MdmEnumValue[] })),
        );
      }
      if (this.contextAttribute.attribute.dataType === "DATE") {
        const backendDate = this.contextAttribute.attribute.value[this.contextAttribute?.contextGroup];
        this.timezoneService
          .getTimeZone()
          .pipe(
            take(1),
            map((zone) => this.toDateTime(backendDate, zone)),
            tap((dateTime) => (this.dateTime = dateTime)),
          )
          .subscribe();
      }
    }
  }

  ngOnDestroy() {
    this.onDateChanged();
  }

  onDateChanged() {
    if (this.contextAttribute.attribute.dataType === "DATE") {
      this.contextAttribute.attribute.value[this.contextAttribute?.contextGroup] = toMdmRestApiString(this.dateTime);
    }
  }

  clear() {
    this.contextAttribute.attribute.value[this.contextAttribute.contextGroup] = "";
  }

  private toDateTime(backendDate: string, zone: string) {
    return backendDate == null || backendDate === "" ? null : DateTime.fromISO(backendDate, { zone });
  }
}
