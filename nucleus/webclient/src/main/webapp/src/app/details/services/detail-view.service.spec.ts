/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClientTestingModule } from "@angular/common/http/testing";
import { async, inject, TestBed } from "@angular/core/testing";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { PropertyService } from "@core/property.service";
import { NodeService } from "@navigator/node.service";
import { TranslateModule } from "@ngx-translate/core";
import { Observable, of as observableOf } from "rxjs";
import { MDMTagService } from "src/app/mdmtag/mdmtag.service";
import { DetailViewService } from "./detail-view.service";

class TestPreferenceService {
  getPreference(key?: string): Observable<Preference[]> {
    return observableOf([
      {
        id: 1,
        key: "ignoredAttributes",
        scope: Scope.USER,
        source: null,
        user: "testUser",
        value: '["*.MimeType", "TestStep.Sortindex"]',
      },
      {
        id: 2,
        key: "ignoredAttributes",
        scope: Scope.SYSTEM,
        source: null,
        user: null,
        value: '["Project.*"]',
      },
      {
        id: 3,
        key: "ignoredAttributes",
        scope: Scope.SOURCE,
        source: "MDMTEST",
        user: null,
        value: '["*.Id"]',
      },
      {
        id: 4,
        key: "ignoredAttributes",
        scope: Scope.SOURCE,
        source: "MDM_OTHER",
        user: null,
        value: '["Pool.*"]',
      },
    ]);
  }
}

class TestMDMTagService {
  getAllMDMTags(scope: string): Observable<Node[]> {
    const nodes: Node[] = [];
    return observableOf(nodes);
  }

  getMDMTagOfTagable(scope: string, tagable: Node): Observable<Node[]> {
    const nodes: Node[] = [];
    return observableOf(nodes);
  }

  tagEntity(scope: string, mdmTag: Node, entity: Node) {}

  removeTagEntity(scope: string, mdmTag: Node, entity: Node) {}
}

describe("DetailViewService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [
        {
          provide: PreferenceService,
          useClass: TestPreferenceService,
        },
        {
          provide: NodeService,
        },
        {
          provide: PropertyService,
        },
        {
          provide: MDMTagService,
          useClass: TestMDMTagService,
        },
        DetailViewService,
        MDMNotificationService,
      ],
    });
  });

  describe("getFilters()", () => {
    it("should return filtered attributes", async(
      inject([DetailViewService], (detailViewService) => {
        expect(detailViewService.getFilters("MDMTEST")).toEqual(["Project.*", "*.Id", "*.MimeType", "TestStep.Sortindex"]);
      }),
    ));
  });
});
