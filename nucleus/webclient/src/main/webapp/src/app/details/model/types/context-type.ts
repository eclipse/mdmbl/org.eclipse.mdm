export type ContextType = "UNITUNDERTEST" | "TESTEQUIPMENT" | "TESTSEQUENCE";

const contextTypeAbbrivations = ["UUT", "TE", "TS"] as const;
export type ContextTypeAbbr = (typeof contextTypeAbbrivations)[number];

export function getContextTypeAbbr(context: ContextType): ContextTypeAbbr {
  switch (context.toUpperCase()) {
    case "UNITUNDERTEST":
      return "UUT";
    case "TESTEQUIPMENT":
      return "TE";
    case "TESTSEQUENCE":
      return "TS";
  }
}
export function getContextType(contextType: ContextTypeAbbr): ContextType {
  switch (contextType.toUpperCase()) {
    case "UUT":
      return "UNITUNDERTEST";
    case "TE":
      return "TESTEQUIPMENT";
    case "TS":
      return "TESTSEQUENCE";
  }
}

export function isContextTypeAbbr(input: string): input is ContextTypeAbbr {
  return contextTypeAbbrivations.some((abbr) => abbr === input);
}
