/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { MDMEnum } from "@navigator/node";
import { catchError, map, shareReplay } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class EnumerationService {
  private _contextUrl: string;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this._contextUrl = _prop.getUrl("mdm/environments");
  }

  getAllEnumerations(sourceName: string) {
    const url = this._contextUrl + "/" + sourceName + "/enums";

    return this.http.get<MDMEnum[]>(url).pipe(
      catchError((e) => addErrorDescription(e, "Could not request enumerations!")),
      shareReplay(),
    );
  }

  getEnumeration(sourceName: string, enumerationName: string) {
    return this.getAllEnumerations(sourceName).pipe(map((enums) => enums.find((e) => e.name === enumerationName)));
  }
}
