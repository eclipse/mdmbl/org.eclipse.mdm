import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LastModifiedComponent } from "./components/last-modified/last-modified.component";
import { NewlyCreatedComponent } from "./components/newly-created/newly-created.component";
import { RecentChangesNavCardComponent } from "./components/recent-changes-nav-card/recent-changes-nav-card.component";

const userPrefRoutes: Routes = [
  {
    path: "",
    component: RecentChangesNavCardComponent,
    children: [
      { path: "newly-created", component: NewlyCreatedComponent },
      { path: "last-modified", component: LastModifiedComponent },
      { path: "", redirectTo: "newly-created", pathMatch: "full" },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(userPrefRoutes)],
  exports: [RouterModule],
})
export class RecentChangesRoutingModule {}
