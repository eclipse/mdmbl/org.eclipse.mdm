/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from "@angular/core";
import { MDMIdentifier } from "@core/mdm-identifier";
import { MDMItem } from "@core/mdm-item";
import { Preference, PreferenceConstants, PreferenceService, Scope } from "@core/preference.service";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";
import {
  ConfirmationService,
  FilterMatchMode,
  FilterService as PrimeNgFilterService,
  SelectItem,
  SelectItemGroup,
  SortEvent,
} from "primeng/api";
import { Table } from "primeng/table";
import { forkJoin, of, Subscription } from "rxjs";
import { delay, flatMap, map, shareReplay, take, withLatestFrom } from "rxjs/operators";
import { MDMNotificationService } from "src/app/core/mdm-notification.service";
import { ArrayUtilService } from "src/app/core/services/array-util.service";
import {
  FormulaPreference,
  TYPE_VIRTUAL_CHANNEL,
  UserFormulaService,
  VirtualChannelUnitUI,
} from "src/app/user-prefs/services/user-formula.service";
import { ScopedUnitPreference, UnitFilter, UserUnitPreferenceService } from "src/app/user-prefs/services/user-unit-mapping.service";
import { ChannelFilterPreference, UnitPreference, UserChannelFilterPreferenceService } from "src/app/user-prefs/user-prefs.module";
import { PhysDimService } from "../../../administration/units/physicaldimension.service";
import { SourceUnits, UnitService, UnitsPerSource } from "../../../administration/units/unit.service";
import { Node } from "../../../navigator/node";
import { Condition, FilterService, Operator, SearchFilter } from "../../../search/filter.service";
import { SearchService } from "../../../search/search.service";
import { EditViewComponent } from "../../../tableview/editview.component";
import { QueryService, Row, SearchResult } from "../../../tableview/query.service";
import { View, ViewColumn } from "../../../tableview/tableview.service";
import { ChannelSelectionRow, ChannelUnitUI } from "../../model/chartviewer.model";
import { TYPE_CHANNEL, TYPE_CHANNELGROUP, TYPE_MEASUREMENT } from "../../model/constants";
import { Channel } from "../../model/types/channel.class";
import { ChannelGroup } from "../../model/types/channelgroup.class";
import { Measurement } from "../../model/types/measurement.class";
import { QueryConfig } from "../../model/types/query-config.class";
import { ChartViewerDataService } from "../../services/chart-viewer-data.service";
import { ChartViewerService } from "../../services/chart-viewer.service";

/**
 * Internal class only which is used for the user preference of the y-channel table configuration
 */
export class SimpleColumn {
  type: string;
  name: string;

  constructor(type: string, name: string) {
    this.type = type;
    this.name = name;
  }
}
export class DataSelectionEvent {
  rows: ChannelSelectionRow[];
  groups: ChannelGroup[];
}

type WithUnit = {
  unitName: string;
};

@Component({
  selector: "mdm-xy-chart-data-selection-panel",
  templateUrl: "./xy-chart-data-selection-panel.component.html",
  styleUrls: ["../../chart-viewer.style.css", "./xy-chart-data-selection-panel.component.css"],
})
export class XyChartDataSelectionPanelComponent implements OnInit, OnDestroy, OnChanges, IStatefulComponent {
  @Input()
  public measurements: Measurement[] = [];

  @Input()
  public xyMode: boolean;

  @Output()
  public selectedMeasurementsChange = new EventEmitter<Measurement[]>();

  @Output()
  public selectedChannelGroupsChange = new EventEmitter<ChannelGroup[]>();

  @Input()
  public selectedChannelRows: ChannelSelectionRow[];
  @Output()
  public selectedChannelRowsChange = new EventEmitter<ChannelSelectionRow[]>();

  @ViewChild(EditViewComponent)
  editViewComponent: EditViewComponent;

  @ViewChild("yChannelTable")
  yChannelTable: Table;

  readonly preferencePrefix = "xy-chart-y-channels";
  readonly maxYChannelRows = 100;

  private isCaseSensitive = false;

  // internal search for y-channel dynamic display
  private results: SearchResult = new SearchResult();
  public selectedView: View = new View();

  // select options for channels
  public yChannelOptions: Channel[] = [];
  public xChannelOptions: { [channelGroupId: string]: Channel[] } = {};

  //public yChannelCols: ViewColumn[];
  private readonly channelNameViewColumn = new ViewColumn("Channel", "Name");
  private readonly measurementNameViewColumn = new ViewColumn("Measurement", "Name");
  private readonly unitViewColumn = new ViewColumn("Unit", "Name");

  private readonly defaultViewColumns = [this.channelNameViewColumn, this.measurementNameViewColumn, this.unitViewColumn];

  public sortViewColumn = this.channelNameViewColumn;
  public sortOrder = 1;

  // used by table header checkbox
  public channelGroupOptions: ChannelGroup[] = [];
  public loadingYChannels = false;

  // channel(group) selection for chart
  // !! change via setter to update cache for workarround !!

  public selectedMeasurements: Measurement[] = [];
  public selectedChannelGroups: ChannelGroup[] = [];
  public selectedYChannels: Channel[] = [];
  public selectedTableChannels: Channel[] = [];

  private selectionChanged = false;

  // listbox workarround to determine toggled item(s)
  private lastChannelGroupSelection: ChannelGroup[] = [];
  private lastYChannelSelection: Channel[] = [];
  private tmpLastHandledSelectedNode: MDMIdentifier;

  // Filterstrings
  public yChannelFilter = "";
  public yChannelPanelCollapsed = false;
  public xChannelPanelCollapsed = true;

  // Toggler for Dialog
  public isYChannelQueryConfigDialogVisible = false;
  public resultLimitModeOptions: SelectItem[] = [
    { label: "chartviewer.xy-chart-data-selection-panel.default-result-limit", value: "DEFAULT" },
    { label: "chartviewer.xy-chart-data-selection-panel.all-results", value: "ALL" },
    { label: "chartviewer.xy-chart-data-selection-panel.manual-result-limit", value: "MANUAL" },
  ];
  public dialogResultLimitMode: "DEFAULT" | "ALL" | "MANUAL" = "DEFAULT";
  public dialogResultLimit: string;
  public dialogResultOffset: string;
  private queryConfig: QueryConfig = { resultLimit: undefined, resultOffset: 0 };

  // for x-channel default value
  private independentChannels = new Map<string, Channel>();

  /********** subscriptions */
  private chartViewerSub: Subscription;
  private chartViewerAppendSub: Subscription;
  private selectChannelSub: Subscription;
  private appendMDMItemSub: Subscription;

  private allFormulaPreferences: FormulaPreference[] = [];
  private loadFormulaSubcription: Subscription;

  private rootNodes$ = this.nodeService.getRootNodes().pipe(
    //tap(r => console.trace("rootNodes loaded", r)),
    shareReplay(),
    //tap(r => console.trace("rootNodes replayed", r))
  );

  public matchModeOptions = [
    { label: "RegEx (Case sensitive)", value: "regex_s" },
    { label: "RegEx (Case insensitive)", value: "regex_i" },
    { label: "Starts With", value: FilterMatchMode.STARTS_WITH },
    { label: "Contains", value: FilterMatchMode.CONTAINS },
  ];

  private unitMap$ = this.getUnitMap().pipe(
    //tap(r => console.trace("unit map loaded", r)),
    shareReplay(),
    //tap(r => console.trace("unit map replayed", r))
  );

  // physDim Name -> physDim Id
  private physDimMap$ = this.getPhysDimMap().pipe(
    //tap(r => console.trace("physdim map loaded", r)),
    shareReplay(),
    //tap(r => console.trace("physdim map replayed", r))
  );

  private allSearchAttributes$ = this.rootNodes$.pipe(
    take(1),
    map((envs) => envs.map((e) => e.sourceName)),
    flatMap((sourceNames) => this.searchService.loadSearchAttributesStructured(sourceNames)),
    shareReplay(),
  );

  constructor(
    private chartViewerService: ChartViewerService,
    private chartViewerDataService: ChartViewerDataService,
    private arrayUtil: ArrayUtilService,
    private confirmationService: ConfirmationService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private queryService: QueryService,
    private searchService: SearchService,
    private filterService: FilterService,
    private nodeService: NodeService,
    private preferenceService: PreferenceService,
    private physDimService: PhysDimService,
    private unitService: UnitService,
    private userFormulaService: UserFormulaService,
    private userUnitPrefService: UserUnitPreferenceService,
    private userChnlFltrPrefService: UserChannelFilterPreferenceService,
    private primeNgFilterService: PrimeNgFilterService,
    private stateService: StateService,
  ) {
    this.stateService.register(this, "xy-chart-data-selection-panel");
    if (this.isCaseSensitive) {
      this.primeNgFilterService.register("regex_s", this.primeNgFilterCaseSensitive);
    } else {
      this.primeNgFilterService.register("regex_i", this.primeNgFilterCaseInsensitive);
    }
  }

  getState(): unknown {
    return {
      yChannelFilter: this.getYChannelFilter(),
      yChannelPanelCollapsed: this.yChannelPanelCollapsed,
      xChannelPanelCollapsed: this.xChannelPanelCollapsed,
      selectedNodes: this.selectedChannelRows.map((n) => ({
        channelGroup: { source: n.channelGroup.source, type: n.channelGroup.type, id: n.channelGroup.id },
        yChannel: { source: n.yChannel.source, type: n.yChannel.type, id: n.yChannel.id, unitName: n.yChannel.unitName },
        xChannel: { source: n.xChannel.source, type: n.xChannel.type, id: n.xChannel.id, unitName: n.xChannel.unitName },
      })),
    };
  }

  nodesToSelectFromState: { channelGroup: MDMIdentifier; yChannel: MDMIdentifier & WithUnit; xChannel: MDMIdentifier }[] = [];

  applyState(state: unknown) {
    this.yChannelFilter = state["yChannelFilter"] || "";
    this.yChannelPanelCollapsed = state["yChannelPanelCollapsed"];
    this.xChannelPanelCollapsed = state["xChannelPanelCollapsed"];
    this.nodesToSelectFromState = state["selectedNodes"] || [];
  }

  /****************  Angular lifecycle-hooks ****************************/

  ngOnInit() {
    this.chartViewerSub = this.chartViewerService.onNodeMetaChange().subscribe((node) => this.handleNodeSelectionInNavTree(node));
    this.chartViewerAppendSub = this.chartViewerService.onAppendNodeMetaChange().subscribe((node) => this.handleNodeAppending(node));
    this.selectChannelSub = this.chartViewerService.onSelectChannelChange().subscribe((channel) => this.onSelectChannelFromBasket(channel));
    this.appendMDMItemSub = this.chartViewerService
      .onAppendMDMItemChange()
      .pipe(withLatestFrom(this.unitMap$))
      .subscribe(
        ([node, unitMap]) => this.appendMDMItem(node, unitMap),
        (error) => this.notificationService.notifyError("details.mdm-detail-view.cannot-update-node", error),
      );

    this.preferenceService.getPreferenceForScope(Scope.SYSTEM, PreferenceConstants.SEARCH_CASE_SENSITIVE).subscribe((preferences) => {
      if (preferences.length > 0) {
        this.isCaseSensitive = "true" === preferences[0].value;
      }
    });

    this.initFormulaPrefs();

    // apply the default table columns
    this.selectedView.columns.push(...this.defaultViewColumns);

    // load the user preferences for table columns
    this.preferenceService
      .getPreference(this.preferencePrefix)
      .pipe(
        map((preferences) => preferences.map((p) => JSON.parse(p.value) as SimpleColumn)),
        take(1),
      )
      .subscribe((data) => {
        if (data.length > 0) {
          const dataArray = data[0] as unknown as SimpleColumn[];
          this.selectedView.columns = [];
          for (const col of dataArray) {
            this.selectedView.columns.push(new ViewColumn(col.type, col.name));
          }

          if (!this.selectedView.columns.some((c) => c.type === this.unitViewColumn.type && c.name === this.unitViewColumn.name)) {
            this.selectedView.columns.push(this.unitViewColumn);
          }
        }
      });

    this.userUnitPrefService.getAllUnitPreferences().subscribe((up) => this.initUserUnitPrefs(up));
    this.userChnlFltrPrefService.getUserChannelFilterPreferences().subscribe((up) => this.initUserChannelFilterPrefs(up));

    setTimeout(() => {
      this.setYChannelFilter(this.yChannelFilter);
    });
  }

  ngOnDestroy() {
    this.stateService.unregister(this);

    this.chartViewerSub.unsubscribe();
    this.chartViewerAppendSub.unsubscribe();
    this.selectChannelSub.unsubscribe();
    this.appendMDMItemSub.unsubscribe();
  }

  private getYChannelFilter() {
    const nameFilter = this.yChannelTable.filters["name"];
    if (nameFilter instanceof Array) {
      return nameFilter[0].value;
    } else {
      return nameFilter.value;
    }
  }

  private setYChannelFilter(filterValue: string) {
    const nameFilter = this.yChannelTable.filters["name"];
    if (nameFilter instanceof Array) {
      nameFilter[0].value = filterValue;
    } else {
      nameFilter.value = filterValue;
    }
  }

  private getUnitMap() {
    return this.rootNodes$.pipe(
      map((envs) => envs.map((e) => e.sourceName)),
      flatMap((sourceNames) => this.unitService.getUnitMap(sourceNames)),
      map((x) => x.reduce((prev, current) => Object.assign(prev, current), {})),
    );
  }

  private getPhysDimMap() {
    return this.rootNodes$.pipe(
      map((envs) => envs.map((e) => e.sourceName)),
      flatMap((sourceNames) =>
        forkJoin(
          sourceNames.map((s) =>
            this.physDimService.getPhysDimsForSource(s).pipe(
              map((x) => {
                return { sourceName: s, physDims: x };
              }),
              take(1),
            ),
          ),
        ),
      ),
      map((x) => {
        const s: { [sourceName: string]: Map<string, string> } = {};
        x.forEach((y) => {
          const map = new Map<string, string>();
          y.physDims.forEach((p) => map.set(p.name, p.id));
          s[y.sourceName] = map;
        });
        return s;
      }),
    );
  }

  public onSelectAllYChannels(event: { checked: boolean }) {
    if (event.checked) {
      this.selectedYChannels = this.selectedTableChannels;
    } else {
      this.selectedYChannels = [];
    }
    this.onSelectedYChannelsChanged();
  }

  public onSelectChannelFromBasket(channel: Channel) {
    // select the channel group first
    let allGroups: ChannelGroup[] = [];
    let channelGroup = undefined;
    this.measurements
      .map((m) => m.channelGroups)
      .forEach((cgs) =>
        cgs.forEach((cg) => {
          if (cg.id === channel.channelGroupId) {
            channelGroup = cg;
            allGroups = cgs;
          }
        }),
      );
    if (channelGroup) {
      this.channelGroupOptions = [...this.channelGroupOptions];
      allGroups.forEach((grp) => this.channelGroupOptions.push(grp));
      const treeChannelGroup = undefined;

      /*this.channelGroupsTree.map(tn => tn.children).forEach(chld => {
        chld.forEach(chld1 => {
          if (chld1.data.id === channelGroup.id) {
            treeChannelGroup = chld1;
          }
        });
      });*/
      if (treeChannelGroup) {
        // this.selectedChannelGroupsTree.push(treeChannelGroup);
        this.onYChannelClick({ data: channel });
      }
    }
  }

  public onYChannelClick(event: { data: Channel }) {
    const channel: Channel = event.data;
    if (this.selectedYChannels.some((c) => c.id === channel.id)) {
      this.selectedYChannels = this.selectedYChannels.filter((c) => c.id !== channel.id);
    } else {
      this.selectedYChannels = [...this.selectedYChannels, channel];
    }

    this.onSelectedYChannelsChanged();
  }

  public isYChannelSelected(channel: Channel) {
    return this.selectedYChannels.some((c) => c.id === channel.id);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["xyMode"]) {
      this.reloadAxisSelectOptions();
      if (this.selectionChanged) {
        this.selectedChannelRowsChange.emit(this.selectedChannelRows);
        this.fireSelectionChanged();
        this.selectionChanged = false;
      }
    }

    if (changes["measurements"]) {
      const wasNodeHandled = this.tmpLastHandledSelectedNode !== undefined && !this.arrayUtil.isNotEmpty(this.channelGroupOptions);

      //this.initChannelGroupOptions(changes.measurement.previousValue === undefined);

      if (wasNodeHandled) {
        this.handleNodeSelectionInNavTree(this.tmpLastHandledSelectedNode);
      }

      this.filterAndSelectUserUnitPrefs();
      this.filterAndSelectUserChannelFilterPrefs();
    }
    /*
    if (changes["selectedMeasurements"]) {
      console.log("selectedMeasurements", changes["selectedMeasurements"]);
      //this.selectedMeasurementsChange.emit(this.selectedMeasurements);
    }
    if (changes["selectedChannelGroups"]) {
      console.log("selectedChannelGroups", changes["selectedChannelGroups"]);
      if (this.selectedChannelGroups) {
        this.onSelectedChannelGroupsChanged(this.selectedChannelGroups);
      }
      //this.selectedMeasurementsChange.emit(this.selectedChannelGroups);
    }
    if (changes["selectedChannelRows"]) {
      console.log("selectedChannelRows", changes["selectedChannelRows"]);
    }
    */
  }

  selectMDMIdentifiers(dataItems: { channelGroup: MDMIdentifier; yChannel: MDMIdentifier & WithUnit; xChannel: MDMIdentifier }[]) {
    const channels: Channel[] = [];
    const channelRows: ChannelSelectionRow[] = [];

    for (const yChannel of this.yChannelOptions) {
      for (const dataItem of dataItems) {
        if (
          dataItem.yChannel.id === yChannel.id &&
          dataItem.yChannel.type === yChannel.type &&
          dataItem.yChannel.source === yChannel.source
        ) {
          const channelGroup = this.selectedChannelGroups.find(
            (cg) =>
              dataItem.channelGroup.id == cg.id && dataItem.channelGroup.type === cg.type && dataItem.channelGroup.source === cg.source,
          );
          const xChannel = (this.xChannelOptions[dataItem.channelGroup.id] || []).find(
            (x) => dataItem.xChannel.id == x.id && dataItem.xChannel.type === x.type && dataItem.xChannel.source === x.source,
          );
          if (channelGroup && xChannel) {
            (yChannel as ChannelUnitUI).setSelectedUnitName(dataItem.yChannel.unitName);
            channels.push(yChannel);
            channelRows.push({ channelGroup: channelGroup, yChannel: yChannel, xChannel: xChannel });
          } else {
            console.warn(
              "Could not select channel " +
                dataItem.yChannel.id +
                ", because either ChannelGroup (" +
                dataItem.channelGroup.id +
                ") or X-Channel (" +
                dataItem.xChannel.id +
                ") was not found ",
            );
          }
        }
      }
    }

    if (channels.length > 0) {
      this.selectedTableChannels = channels;
      this.setSelectedYChannels(channels);
      this.selectedChannelRows = channelRows;
      this.onSelectedYChannelsChanged();
    }
  }

  onSelectedMeasurementsChange(selectedMeasurements: Measurement[]) {
    this.selectedMeasurements = selectedMeasurements;
    this.selectedMeasurementsChange.emit(this.selectedMeasurements);
  }

  private readonly virtualUnitPreference = new ScopedUnitPreference(
    Scope.SYSTEM,
    new UnitPreference(this.translateService.instant("user-prefs.unit-prefs.virtual")),
  );
  public selectedUnitPreference = this.virtualUnitPreference;
  public paramUserUnitPrefs: ScopedUnitPreference[];
  public groupedUserUnitPrefs: SelectItemGroup[];

  private readonly emptyChannelFilterPref = new ChannelFilterPreference(
    this.translateService.instant("user-prefs.channel-filter-prefs.nothing-selected"),
  );
  public selectedChannelFilterPreference = this.emptyChannelFilterPref;
  public paramUserChannelFilterPrefs: ChannelFilterPreference[];
  public groupedUserChannelFilterPrefs: SelectItemGroup[];

  private initUserUnitPrefs(userUnitPrefs: ScopedUnitPreference[]): void {
    if (userUnitPrefs !== undefined && userUnitPrefs.length > 0) {
      const hasEmptyEntry = userUnitPrefs.some((uup) => uup.unitFilters === undefined || uup.unitFilters.length === 0);
      if (!hasEmptyEntry) {
        userUnitPrefs.push(this.virtualUnitPreference);
      }
      this.paramUserUnitPrefs = this.userUnitPrefService.sortUnitPreferences(userUnitPrefs);
    } else {
      this.paramUserUnitPrefs = [this.virtualUnitPreference];
    }
    this.filterAndSelectUserUnitPrefs();
  }

  private initUserChannelFilterPrefs(userPrefs: ChannelFilterPreference[]): void {
    if (userPrefs !== undefined && userPrefs.length > 0) {
      const hasEmptyEntry = userPrefs.some((uup) => uup.channelFilters === undefined || uup.channelFilters.length === 0);
      userPrefs = this.userChnlFltrPrefService.sortChannelFilterPreferences(userPrefs);
      this.paramUserChannelFilterPrefs = hasEmptyEntry ? userPrefs : [this.emptyChannelFilterPref].concat(userPrefs);
    } else {
      this.paramUserChannelFilterPrefs = [this.emptyChannelFilterPref];
    }
    this.filterAndSelectUserChannelFilterPrefs();
  }

  onChangedSelectedUnitPreference() {
    forkJoin(this.unitMap$, this.physDimMap$).subscribe(([u, p]) => this.updateSelectedChannels(u, p));
  }

  onChangedSelectedChannelFilterPreference() {
    if (this.arrayUtil.isNotEmpty(this.selectedYChannels)) {
      // deselect all channels
      this.selectedYChannels = [];
      this.selectedTableChannels = [];
      this.onSelectedYChannelsChanged();
    }

    this.selectChannelsByPrefFilters();
  }

  private filterAndSelectUserUnitPrefs() {
    if (this.paramUserUnitPrefs) {
      this.groupedUserUnitPrefs = this.groupByScope(this.paramUserUnitPrefs);
    }
  }

  private filterAndSelectUserChannelFilterPrefs() {
    if (this.paramUserChannelFilterPrefs) {
      this.groupedUserChannelFilterPrefs = this.groupByScope(this.paramUserChannelFilterPrefs);
    }
  }

  private groupByScope<T extends ScopedUnitPreference | ChannelFilterPreference>(prefs: T[]): SelectItemGroup[] {
    const grouped = prefs
      .filter((up) => up.source === "*" || this.measurements.some((m) => up.source === m.source))
      .reduce((prev, current) => {
        let group = <SelectItemGroup>prev[current["scope"] || Scope.USER];
        if (!group) {
          group = <SelectItemGroup>{ label: Scope.toLabel(current["scope"] || Scope.USER), items: [] };
          prev[current["scope"] || Scope.USER] = group;
        }
        group.items.push(this.toSelectItem(current));
        return prev;
      }, <{ [x: string]: SelectItemGroup }>{});

    return Object.entries(grouped).map((g) => g[1]);
  }

  private toSelectItem<T extends ScopedUnitPreference | ChannelFilterPreference>(pref: T): SelectItem {
    return <SelectItem<T>>{
      value: pref,
      label: pref.displName + " (" + (pref.source === "*" ? "Global" : pref.source) + ")",
      scope: pref["scope"] || Scope.USER,
    };
  }

  /**************** Html-template listeners *****************************/

  // x-axis
  public onSelectedXChannelChanged(event: { value: object }) {
    if (event.value === undefined) {
      // work around: if x-axis is deselected for an y-channel, plat over independent channel.
      // should force selection on dropdown instead.
      this.selectedChannelRows
        .filter((row) => row.xChannel === undefined)
        .map((row) => (row.xChannel = this.getDefaultXChannel(row.channelGroup)));
    }
    this.selectedChannelRowsChange.emit(this.selectedChannelRows);
    this.fireSelectionChanged();
  }

  public useChannelForAll(row: ChannelSelectionRow) {
    const rowsToChange = this.selectedChannelRows.filter(
      (selectedChannelRow) =>
        selectedChannelRow.channelGroup.id === row.channelGroup.id && selectedChannelRow.xChannel.id !== row.xChannel.id,
    );

    if (rowsToChange.length > 0) {
      rowsToChange.forEach((rowToChange) => {
        rowToChange.xChannel = row.xChannel;
      });
      this.selectedChannelRowsChange.emit(this.selectedChannelRows);
      this.fireSelectionChanged();
    }
  }

  public onShowSelectedXChannelChanged(rowData: ChannelSelectionRow) {
    if (this.getDefaultXChannel(rowData.channelGroup) === undefined) {
      this.confirmationService.confirm({
        header: this.translateService.instant("chartviewer.xy-chart-data-selection-panel.no-x-channel-dialog-header"),
        message: this.translateService.instant("chartviewer.xy-chart-data-selection-panel.no-x-channel-dialog-message"),
        acceptLabel: "OK",
        rejectVisible: false,
      });
    }
  }

  // y-axis
  public onSelectedYChannelsChanged() {
    let channels: Channel[];

    if (this.selectedYChannels.length < this.lastYChannelSelection.length) {
      // Deselect
      channels = this.lastYChannelSelection.filter((yChannel) => !this.selectedYChannels.some((c) => yChannel.id === c.id));
    } else {
      // Select
      channels = this.selectedYChannels.filter((yChannel) => !this.lastYChannelSelection.some((c) => yChannel.id === c.id));
    }

    this.measurements.forEach((mea) =>
      channels.forEach((c) => {
        const chnlGrp =
          c instanceof VirtualChannelUnitUI
            ? mea.findChannelGroup((<VirtualChannelUnitUI>c).channelGroupId)
            : mea.findChannelGroupByChannel(c);
        this.addOrRemoveRow(chnlGrp, c);
      }),
    );
    this.lastYChannelSelection = this.selectedYChannels;
    this.selectedChannelRowsChange.emit(this.selectedChannelRows);
    this.fireSelectionChanged();
  }

  public onSelectedChannelGroupsChanged(channelGroups: ChannelGroup[]) {
    this.loadingYChannels = true;
    this.selectedChannelGroups = channelGroups;
    // All deselected
    if (this.selectedChannelGroups.length === 0) {
      this.resetChannelData();
      this.selectedChannelRowsChange.emit(this.selectedChannelRows);
      this.fireSelectionChanged();
    } else {
      // Deselect
      if (this.selectedChannelGroups.length < this.lastChannelGroupSelection.length) {
        this.lastChannelGroupSelection
          .filter((group) => group != undefined)
          .filter((group) => !this.selectedChannelGroups.some((g) => group.id === g.id))
          .forEach((group) => this.handleDeselectChannelGroup(group));
        // Select
      } else {
        this.handleSelectChannelGroups(
          this.selectedChannelGroups
            .filter((group) => group != undefined)
            .filter((group) => !this.lastChannelGroupSelection.some((g) => group.id === g.id)),
        );
      }
    }
    this.sendQueryForDynamicColumns();
    this.lastChannelGroupSelection = this.selectedChannelGroups.filter((group) => group != undefined);
    this.loadingYChannels = false;
    this.selectedChannelRowsChange.emit(this.selectedChannelRows);
  }

  /**
   * Dislplays Editor-Dialog for y-channels and initalizes query config params
   * @param event
   */
  public onShowYChannelConfigDialog() {
    switch (this.dialogResultLimitMode) {
      case "DEFAULT":
      case "ALL":
        this.dialogResultLimit = undefined;
        break;
      case "MANUAL":
        this.dialogResultLimit = this.queryConfig.resultLimit != undefined ? this.queryConfig.resultLimit.toString() : undefined;
        break;
    }

    this.dialogResultOffset = this.queryConfig.resultOffset.toString();
    this.isYChannelQueryConfigDialogVisible = true;
  }

  /**
   * Applies Query-Settings from Editor-Dialog for y-channels
   * @param event
   */
  public onApplyYChannelQueryConfig() {
    const lim = +this.dialogResultLimit;
    const off = +this.dialogResultOffset;
    this.queryConfig.resultOffset = isNaN(off) ? 0 : off;

    switch (this.dialogResultLimitMode) {
      case "DEFAULT":
        this.queryConfig.resultLimit = undefined;
        break;
      case "ALL":
        this.queryConfig.resultLimit = 0;
        break;
      case "MANUAL":
        this.queryConfig.resultLimit = isNaN(lim) ? undefined : lim;
        break;
    }
    this.isYChannelQueryConfigDialogVisible = false;
    this.chartViewerDataService.resultLimit = this.queryConfig.resultLimit;
    this.chartViewerDataService.resultOffset = this.queryConfig.resultOffset;
    this.chartViewerService.sendQueryConfig(this.queryConfig);
  }

  /**
   * Hides Query-Editor-Dialog for y-channels
   * @param event
   */
  public onCloseYChannelConfigDialog() {
    this.isYChannelQueryConfigDialogVisible = false;
  }

  /**
   * let the chart viewer show the channel(s) again after unit choice changed
   * @param channelId
   */
  public onUnitChange(channelId: string) {
    if (this.arrayUtil.isNotEmpty(this.selectedYChannels) && this.selectedYChannels.some((c) => c.id === channelId)) {
      this.selectedChannelRowsChange.emit(this.selectedChannelRows);
    }
  }

  /**
   * Handler to open the edit view and processing the new result columns
   *
   * @param e
   */
  public editSelectedView(e: Event) {
    e.stopPropagation();
    this.editViewComponent
      .showDialog(this.selectedView)
      .pipe(take(1))
      .subscribe(
        (v) => {
          const newColumns = [];

          for (const col of v.columns) {
            newColumns.push(new ViewColumn(col.type, col.name));
          }

          // Add unit column at the last column
          if (!newColumns.some((c) => c.type === this.unitViewColumn.type && c.name === this.unitViewColumn.name)) {
            newColumns.push(this.unitViewColumn);
          }

          this.selectedView = v;
          this.selectedView.columns = newColumns;
          this.sendQueryForDynamicColumns();

          // save the preferences
          this.preferenceService.savePreference(this.viewColumnsToPreference()).pipe(take(1)).subscribe();
        },
        (error) => this.notificationService.notifyError(this.translateService.instant("tableview.view.err-cannot-select-view"), error),
      );
  }

  onSort(event: SortEvent) {
    this.sortViewColumn = event.field as unknown as ViewColumn;
    this.sortOrder = event.order;
    event.data.sort(this.channelSorter(this.sortViewColumn, this.sortOrder));
  }

  /**
   * channel sorting algorithm (by display value)
   * @param event
   * @param order
   * @param channels
   */
  private sortChannels(column: ViewColumn, order: number, channels: Channel[]): Channel[] {
    return channels.sort(this.channelSorter(column, order));
  }

  private channelSorter(column: ViewColumn, order: number) {
    const colIndex = this.selectedView.columns.findIndex((viewCol) => viewCol.type === column.type && viewCol.name === column.name);

    return (data1, data2) => {
      const value1 = this.getTranslatedRowValue(data1, column.type, colIndex);
      const value2 = this.getTranslatedRowValue(data2, column.type, colIndex);
      let result = null;

      if (value1 === null && value2 !== null) {
        result = -1;
      } else if (value1 !== null && value2 === null) {
        result = 1;
      } else if (value1 === null && value2 === null) {
        result = 0;
      } else if (typeof value1 === "string" && typeof value2 === "string") {
        result = value1.localeCompare(value2);
      } else {
        result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;
      }

      return order * result;
    };
  }

  private handleDeselectChannelGroup(channelGroup: ChannelGroup) {
    // remove rows
    if (this.arrayUtil.isNotEmpty(this.selectedChannelRows)) {
      this.selectedChannelRows = this.selectedChannelRows.filter((row) => row.channelGroup.id !== channelGroup.id);
    }
    // remove selection
    if (this.arrayUtil.isNotEmpty(this.selectedYChannels)) {
      this.setSelectedYChannels(
        this.selectedYChannels.filter((channel) =>
          this.measurements.find((mea) => mea.hasChannel(channel.id) as boolean).hasChannel(channel.id),
        ),
      );
      this.selectedTableChannels = this.selectedYChannels;
    }
    this.reloadAxisSelectOptions();
    this.selectedChannelGroupsChange.emit(this.selectedChannelGroups);
    this.fireSelectionChanged();
  }

  /**
   * Handle channelGroups selection via multiselect in html template
   * @param channelGroups
   */
  private handleSelectChannelGroups(channelGroups: ChannelGroup[]) {
    this.unitMap$.subscribe((u) => {
      this.setChannelOptionsFromGroups(channelGroups, u);
      this.selectedChannelGroupsChange.emit(this.selectedChannelGroups);
      this.fireSelectionChanged();
    });
  }

  private handleNodeAppending(ident: MDMIdentifier) {
    this.processNodeSelection(ident, true);
    //this.createChannelGroupTree(true);
  }

  private handleNodeSelectionInNavTree(ident: MDMIdentifier) {
    this.channelGroupOptions = [];
    this.processNodeSelection(ident, false);
  }

  private processNodeSelection(ident: MDMIdentifier, append: boolean) {
    if (ident !== undefined) {
      switch (ident.type) {
        case TYPE_MEASUREMENT:
          /** measurement is set via @input and thus new ChannelGroupoption are set set via ngOnChanges */
          if (!append) {
            this.resetChannelData();
          }
          this.selectedChannelRowsChange.emit(this.selectedChannelRows);
          this.fireSelectionChanged();
          /** the channel was processed before the measurement */
          if (this.tmpLastHandledSelectedNode !== undefined) {
            this.handleChangedChannel(this.tmpLastHandledSelectedNode, append, true);
          }
          break;
        case TYPE_CHANNELGROUP: {
          if (!append) {
            this.resetChannelData();
          }
          const mesChGrp = this.measurements.find((mea) => mea !== undefined && mea.findChannelGroup(ident.id) !== undefined);
          const channelGrp = mesChGrp !== undefined ? mesChGrp.findChannelGroup(ident.id) : undefined;
          if (channelGrp === undefined) {
            // node was selected BEFORE measurment was changed by event
            this.tmpLastHandledSelectedNode = ident;
          } else {
            /*if (this.tmpLastHandledSelectedNode !== node) {
              this.initChannelGroupOptions(true);
            }*/
            this.addChannelGroupToSelection(ident.id, channelGrp);
            this.tmpLastHandledSelectedNode = undefined;
          }
          break;
        }
        case TYPE_CHANNEL:
          if (
            this.arrayUtil.isNotEmpty(this.selectedChannelGroups) &&
            this.selectedChannelGroups.some(
              (selected) =>
                selected !== undefined &&
                selected.type === "ChannelGroup" &&
                this.channelGroupOptions.find((mea) => mea.id === selected.id) === undefined,
            )
          ) {
            /** the channel can"t be processed before the measurement */
            this.tmpLastHandledSelectedNode = ident;
          } else {
            this.handleChangedChannel(ident, append, false);
          }
          break;
      }
    }
  }

  /**
   *
   * @param ident
   * @param append if true, then the channel is added to the chart
   * @param handleChannelGroup if a channel is added to the xy-chart from a new measurement, then the channelgroup
   * has to be handeled first
   */
  private handleChangedChannel(ident: MDMIdentifier, append: boolean, handleChannelGroup: boolean) {
    this.unitMap$.subscribe((u) => {
      if (!handleChannelGroup) {
        this.cleanOldState();
      }
      const mesChn = this.measurements.find((mea) => mea !== undefined && mea.findChannel(ident.id) !== undefined);
      const channel = mesChn !== undefined ? mesChn.findChannel(ident.id) : undefined;
      const channelGroup =
        channel === undefined
          ? undefined
          : this.measurements.find((mea) => mea.findChannelGroupByChannel(channel) !== undefined).findChannelGroupByChannel(channel);
      // add related channel group to selection
      if (channel === undefined || channelGroup === undefined) {
        // node was selected BEFORE measurment was changed by event
        if (!append) {
          this.resetChannelData();
        }
        this.tmpLastHandledSelectedNode = ident;
      } else {
        if (handleChannelGroup) {
          this.handleSelectChannelGroups([channelGroup]);
        }
        if (!this.isChannelGroupIn(channelGroup, this.selectedChannelGroups)) {
          this.setSelectedChannelGroups(this.selectedChannelGroups.concat([channelGroup]));
          this.setChannelOptions(channelGroup, u);
          this.addChannelToSelection(channel, channelGroup);
        } else {
          // if channel is in axis type y, set as selected, if not selected already
          this.addChannelToSelection(channel, channelGroup);
        }
        this.sendQueryForDynamicColumns();
        this.tmpLastHandledSelectedNode = undefined;
      }
    });
  }

  /**
   * reset chart data, select options and selection, if selected channelgroups
   * contain a channelGroup which does not belongto this measurement.
   *
   * notice: this works for channels aswell, since a channel cannot be selected without
   * selecting the parent channelGroup aswell.
   */
  private cleanOldState() {
    if (
      this.arrayUtil.isNotEmpty(this.selectedChannelGroups) &&
      this.selectedChannelGroups.some(
        (selected) =>
          selected === undefined || this.measurements.find((mea) => mea.findChannelGroup(selected.id) !== undefined) === undefined,
      )
    ) {
      this.resetChannelData();
    }
  }

  /**
   * Handle channel selection via nav-tree
   * @param channel
   * @param channelGroup
   */
  private addChannelToSelection(channel: Channel, channelGroup: ChannelGroup) {
    // adds channel to selected y-channels if possible and creates row in table
    if (this.isChannelIn(channel, this.yChannelOptions)) {
      const index = this.selectedYChannels.findIndex((c) => c.id === channel.id);
      if (index === -1) {
        this.setSelectedYChannels(this.selectedYChannels.concat([channel]));
        this.addRow(channelGroup, channel);
        this.selectedChannelRowsChange.emit(this.selectedChannelRows);
        this.fireSelectionChanged();
      }
      /**
       *  @TODO -> expected behaviour on selecting x-channel in tree.
       * suggestion: set it as x-channel for all selected y-channels of that channelgroup.
       */
      // if channel is of axis type x, set ... ?
    } else if (this.isChannelIn(channel, this.xChannelOptions[channelGroup.id])) {
      this.notificationService.notifyWarn("Selected X-Channel", "Selected channel is of type x-axis. Change x/y mode.");
    } else {
      this.notificationService.notifyError("Unknown channel option", "Channel " + channel.name + " has no axis.");
    }
  }

  /**
   * Handle channelGroup selection via nav-tree
   * @param id
   * @param channelGroup
   */
  private addChannelGroupToSelection(id: string, channelGroup: ChannelGroup) {
    if (this.selectedChannelGroups === undefined) {
      this.setSelectedChannelGroups([]);
    }
    if (channelGroup != undefined && !this.isChannelGroupIn(channelGroup, this.selectedChannelGroups)) {
      this.setSelectedChannelGroups(this.selectedChannelGroups.concat([channelGroup]));
      this.handleSelectChannelGroups([channelGroup]);
    }
  }

  private setChannelOptionsFromGroups(channelGroups: ChannelGroup[], unitsPerSource: UnitsPerSource) {
    channelGroups.forEach((cg) => this.setChannelOptions(cg, unitsPerSource));

    if (this.selectedChannelRows.length === 0 && this.nodesToSelectFromState.length > 0) {
      this.selectMDMIdentifiers(this.nodesToSelectFromState);
      this.nodesToSelectFromState = [];
    }
  }
  /**
   * Adds options for x- and y-channels depending on given channelGroup
   *
   * @param axisInfos
   * @param channelGroup
   */
  private setChannelOptions(channelGroup: ChannelGroup, unitsPerSource: UnitsPerSource) {
    if (channelGroup !== undefined && "ChannelGroup" === channelGroup.type) {
      // create empty array if property not exists
      this.xChannelOptions[channelGroup.id] = this.xChannelOptions[channelGroup.id] || [];

      // mimic flatMap
      const channels = this.measurements.map((mea) => mea.findChannels(channelGroup)).reduce((acc, x) => acc.concat(x), []);

      if (this.xyMode) {
        channels.forEach((channel) => {
          if (
            channel.axisType === "X_AXIS" ||
            channel.axisType === "XY_AXIS" ||
            (channel.axisType === undefined && channel.isIndependent)
          ) {
            this.xChannelOptions[channelGroup.id].push(channel);
            if (channel.isIndependent) {
              this.independentChannels.set(channelGroup.id, channel);
            }
          } else if (
            !this.yChannelOptions.find((ychnl) => ychnl.id === channel.id) &&
            (channel.axisType === "Y_AXIS" || channel.axisType === "XY_AXIS")
          ) {
            this.yChannelOptions.push(channel);
          }
        });
      } else {
        const channel = this.measurements
          .find((mea) => mea.findChannelGroup(channelGroup.id) !== undefined)
          .getIndependentChannel(channelGroup);
        if (channel) {
          this.independentChannels.set(channelGroup.id, channel);
        }

        this.yChannelOptions = this.yChannelOptions.concat(channels);
        this.xChannelOptions[channelGroup.id] = this.xChannelOptions[channelGroup.id].concat(this.yChannelOptions);
      }

      this.yChannelOptions = this.yChannelOptions.map((c) => this.addDefaultColumnData(c));
      this.yChannelOptions = this.addVirtualChannels(this.yChannelOptions);
      this.yChannelOptions = this.yChannelOptions.map((c) => this.convertToChannelUnitUi(c, unitsPerSource));

      //this.yChannelOptions = this.yChannelOptions.map(c => this.convertOneResultToYChannelOption(c, c.source, this.results.rows));

      // TODO
      this.xChannelOptions[channelGroup.id] = this.sortChannels(this.sortViewColumn, this.sortOrder, this.xChannelOptions[channelGroup.id]);
    }
  }

  /**
   * Resets selections and select options to empty state.
   */
  private resetChannelData() {
    this.setSelectedChannelGroups([]);
    this.selectedChannelRows = [];
    this.setSelectedYChannels([]);
    this.xChannelOptions = {};
    this.yChannelOptions = [];
  }

  /**
   * Reloads the select options for the x- and y-axis dropdowns
   */
  private reloadAxisSelectOptions() {
    this.xChannelOptions = {};
    this.yChannelOptions = [];
    if (this.arrayUtil.isNotEmpty(this.selectedChannelGroups)) {
      this.unitMap$.subscribe((u) => {
        this.selectedChannelGroups.forEach((cg) => {
          this.setChannelOptions(cg, u);
        });
        this.removeSelectionsNotMatchingXYMode();
      });
    }
  }

  private removeSelectionsNotMatchingXYMode() {
    if (this.selectedYChannels.length > 0 && this.selectedChannelRows !== undefined && this.selectedChannelRows.length > 0) {
      const l = this.selectedChannelRows.length;
      this.selectedChannelRows = this.selectedChannelRows.filter((row) => this.isChannelIn(row.yChannel, this.yChannelOptions));
      this.setSelectedYChannels(this.selectedYChannels.filter((channel) => this.isChannelIn(channel, this.yChannelOptions)));
      this.selectionChanged = l !== this.selectedChannelRows.length;
    }
  }

  /**
   * Row control for table with x-channel selection.
   *
   * @param channelGroup
   * @param yChannel
   */
  private addOrRemoveRow(channelGroup: ChannelGroup, yChannel: Channel) {
    if (!channelGroup || !yChannel) {
      return;
    }
    const index = this.selectedChannelRows.findIndex((row) => row.yChannel.id === yChannel.id);
    if (index > -1) {
      this.selectedChannelRows.splice(index, 1);
    } else {
      this.addRow(channelGroup, yChannel);
    }
  }

  /**
   * Add row to selection
   * @param channelGroup
   * @param yChannel
   */
  private addRow(channelGroup: ChannelGroup, yChannel: Channel) {
    const row = new ChannelSelectionRow(channelGroup, yChannel, this.getDefaultXChannel(channelGroup));
    this.selectedChannelRows.push(row);
  }

  /**
   * Get the default channel for x-axis. First tries to find an independent channel,
   * if non is found the first channel with axistype X-Axis or XY-Axis is returned.
   * @param channelGroup
   */
  private getDefaultXChannel(channelGroup: ChannelGroup) {
    this.measurements.find((mea) => mea.findChannelGroup(channelGroup.id) !== undefined).getIndependentChannel(channelGroup);

    let defaultXChannel = this.independentChannels.get(channelGroup.id);
    if (defaultXChannel === undefined && this.xChannelOptions[channelGroup.id].length > 0) {
      defaultXChannel = this.xChannelOptions[channelGroup.id][0];
    }
    return defaultXChannel;
  }

  /**
   * Returns true if node with given id is in the given array.
   *
   * @param array the array
   * @param id the id
   */
  private isChannelIn(node: Channel, array: Channel[]) {
    let response = false;
    if (this.arrayUtil.isNotEmpty(array)) {
      response = array.findIndex((n) => n.id === node.id) > -1;
    }
    return response;
  }

  /**
   * Returns true if node with given id is in the given array.
   *
   * @param array the array
   * @param id the id
   */
  private isChannelGroupIn(node: ChannelGroup, array: ChannelGroup[]) {
    let response = false;
    if (node != undefined && this.arrayUtil.isNotEmpty(array)) {
      response = array.findIndex((n) => n.id === node.id) > -1;
    }
    return response;
  }

  private fireSelectionChanged() {
    this.selectedMeasurementsChange.emit(this.selectedMeasurements);
  }

  private setSelectedYChannels(channels: Channel[]) {
    this.selectedYChannels = channels;
    this.lastYChannelSelection = channels;
  }

  private setSelectedChannelGroups(channelGroups: ChannelGroup[]) {
    this.selectedChannelGroups = channelGroups;
    this.lastChannelGroupSelection = channelGroups;
  }

  private getMatchingVirtualChannels(yChannels: Channel[]): VirtualChannelUnitUI[] {
    const formulaIDs: string[] = yChannels
      .filter((vc) => vc instanceof VirtualChannelUnitUI || (vc.id.startsWith("-") && vc.type === TYPE_VIRTUAL_CHANNEL))
      .map((vc) => vc.id);
    const matchingFormulas = this.userFormulaService.mapFormulasToChannels(
      this.userFormulaService.filterFormulasForChannels(
        this.allFormulaPreferences,
        this.yChannelOptions.filter((c) => !c.id.startsWith("-") || c.type !== TYPE_VIRTUAL_CHANNEL),
        this.isCaseSensitive,
      ),
    );

    return matchingFormulas
      .filter((vc) => !formulaIDs.includes(vc.id))
      .map((vc) => this.addDefaultColumnData(vc))
      .map((vc) => this.convertVirtualChannel(vc, yChannels));
  }

  private convertVirtualChannel<T extends Channel>(channel: T, realChannels: Channel[]) {
    if (channel.type === TYPE_VIRTUAL_CHANNEL && channel instanceof VirtualChannelUnitUI) {
      const realChannel = realChannels.find((c) => c.id === channel.channelIds[0]);

      const index = this.selectedView.columns.findIndex((vc) => vc.type === "Channel" && vc.name === "Name");
      if (index >= 0 && realChannel["columns"]) {
        channel.columns = Array.from(realChannel["columns"]);
        channel.columns[index] = channel.name;
      }
    }
    return channel;
  }

  private addTableColumnData<T extends Channel>(channel: T, resultRows: Row[]) {
    const row = resultRows.find((r) => r.id === channel.id);

    const columns = [];
    if (row) {
      for (const viewCol of this.selectedView.columns) {
        const column = this.findViewColumn(row, viewCol);
        if (column) {
          columns.push(column.value);
        } else {
          columns.push("-");
        }
      }
    }
    channel["columns"] = columns;
    channel["columnsName"] = this.selectedView.columns.map((column: ViewColumn) => `${column.name} (${column.type})`);
    return channel;
  }

  private addDefaultColumnData<T extends Channel>(c: T) {
    if (c["columns"] === undefined) {
      if (c instanceof VirtualChannelUnitUI && c.channels && c.getFirstRequiredChannel()) {
        c["columns"] = this.selectedView.columns.map((x) => this.getDefaultValue(x, c.getFirstRequiredChannel()));
      } else {
        c["columns"] = this.selectedView.columns.map((x) => this.getDefaultValue(x, c));
      }
    }

    if (c["columnsName"] === undefined) {
      c["columnsName"] = this.selectedView.columns.map((column: ViewColumn) => `${column.name} (${column.type})`);
    }
    return c;
  }

  private getDefaultValue(viewColumn: ViewColumn, channel: Channel) {
    if (this.isNameColumn(viewColumn)) {
      return channel.name;
    } else if (this.isUnitColumn(viewColumn)) {
      return channel.unitId;
    } else if (this.isMeasurmentNameColumn(viewColumn)) {
      return channel.measurement;
    } else {
      return "-";
    }
  }

  private findViewColumn(row: Row, viewCol: ViewColumn) {
    return row.columns.find((c) => c.type === viewCol.type && c.attribute === viewCol.name);
  }

  public isUnitColumn(column: ViewColumn) {
    return column.type === this.unitViewColumn.type && column.name === this.unitViewColumn.name;
  }
  public isNameColumn(column: ViewColumn) {
    return column.type === this.channelNameViewColumn.type && column.name === this.channelNameViewColumn.name;
  }
  public isMeasurmentNameColumn(column: ViewColumn) {
    return column.type === this.measurementNameViewColumn.type && column.name === this.measurementNameViewColumn.name;
  }

  private addVirtualChannels(yChannels: Channel[]) {
    return Array.from(yChannels).concat(this.getMatchingVirtualChannels(yChannels));
  }

  private primeNgFilterCaseSensitive(value: string, filterString: string): boolean {
    if (filterString != undefined && filterString !== "") {
      try {
        const regEx = new RegExp(filterString, "");
        return regEx.test(value);
      } catch {
        return value.includes(filterString);
      }
    } else {
      return true;
    }
  }
  private primeNgFilterCaseInsensitive(value: string, filterString: string): boolean {
    if (filterString != undefined && filterString !== "") {
      try {
        const regEx = new RegExp(filterString.toLowerCase(), "i");
        return regEx.test(value);
      } catch {
        return value.toLowerCase().includes(filterString);
      }
    } else {
      return true;
    }
  }

  /**
   * Get a clone of the search filter
   * @param filter
   */
  private getFilterClone(filter: SearchFilter): SearchFilter {
    if (filter === undefined) {
      return undefined;
    }
    return new SearchFilter(
      filter.name,
      filter.environments.slice(),
      filter.resultType,
      filter.fulltextQuery,
      filter.conditions.map((c) => {
        const clone: Condition = new Condition(
          c.type,
          c.attribute,
          c.operator,
          c.value.slice(),
          c.valueType,
          c.isCurrentlyLinked,
          c.isCreatedViaLink,
        );
        clone.sortIndex = c.sortIndex;
        return clone;
      }),
    );
  }

  /**
   * Get an empty filter
   */
  private getEmptyFilter(): SearchFilter {
    const filter = this.getFilterClone(this.filterService.EMPTY_FILTER);
    const sources = this.selectedChannelGroups.map((cg) => cg.source);
    // filter duplicates
    filter.environments = sources.filter((n, i) => sources.indexOf(n) === i);
    return filter;
  }

  /**
   * Send the search query for the current column configuration from the editview dialog
   */
  private sendQueryForDynamicColumns() {
    if (this.selectedChannelGroups && this.selectedChannelGroups.length > 0) {
      const filter = this.getEmptyFilter();

      // use the selected channelgroups as filter condition, the measurement object is only provided if all channelgroups are selected
      const channelGroups = this.selectedChannelGroups;
      if (channelGroups.length > 0) {
        const ids = channelGroups.map((cg) => cg.id);
        filter.conditions.push(new Condition("ChannelGroup", "Id", Operator.IN, ids, "string", false, false));
      } else {
        console.log("no filter");
      }

      this.allSearchAttributes$
        .pipe(
          map((allSearchAttributes) => {
            // build the query on the Channel result type
            const query = this.searchService.convertToQuery(filter, allSearchAttributes, this.selectedView, false, this.isCaseSensitive);
            query.resultType = "Channel";
            // read all Channel
            query.resultLimit = 0;
            return query;
          }),
          flatMap((query) => this.queryService.query(query)),
          withLatestFrom(this.unitMap$, this.physDimMap$),
        )
        .subscribe(
          ([result, units, physDimName2Id]) => {
            console.log("data loaded ", result, units);
            this.results = result;
            this.yChannelOptions = this.convertResultsToYChannelOptions(this.yChannelOptions, this.results.rows, units);
            this.selectPreferredUnits(this.yChannelOptions, physDimName2Id);
            this.selectChannelsByPrefFilters();
            this.yChannelOptions = this.addVirtualChannels(this.yChannelOptions);
          },
          (error) =>
            this.notificationService.notifyError(this.translateService.instant("search.mdm-search.err-cannot-process-search-query"), error),
        );
    }
  }

  private updateSelectedChannels(u: UnitsPerSource, physDimName2Id: { [sourceName: string]: Map<string, string> }) {
    if (this.selectedUnitPreference !== undefined) {
      // selected units might change bec. of user"s unit preference change
      // or when changing the drop-downs selection
      this.yChannelOptions = this.convertResultsToYChannelOptions(this.yChannelOptions, this.results.rows, u);
      this.selectPreferredUnits(this.yChannelOptions, physDimName2Id);
      this.selectChannelsByPrefFilters();

      if (this.arrayUtil.isNotEmpty(this.selectedYChannels)) {
        // to keep the selection order we need to iterate using the Ids
        const chnlIds: string[] = this.selectedYChannels.map((c) => c.id);

        this.selectedYChannels = chnlIds.map((id) => this.yChannelOptions.find((c) => c.id === id)).filter((c) => c !== undefined);

        this.lastYChannelSelection = this.selectedYChannels;

        chnlIds.length = 0;

        const xChannelMap: Map<string, Channel> = new Map();
        const channelGroupMap: Map<string, ChannelGroup> = new Map();
        this.selectedChannelRows.forEach((row) => {
          xChannelMap.set(row.yChannel.id, row.xChannel);
          channelGroupMap.set(row.yChannel.id, row.channelGroup);
        });

        const rows: ChannelSelectionRow[] = [];
        this.selectedYChannels.forEach((c) => rows.push(new ChannelSelectionRow(channelGroupMap.get(c.id), c, xChannelMap.get(c.id))));
        this.selectedChannelRows = rows;
        channelGroupMap.clear();
        xChannelMap.clear();

        this.selectedChannelRowsChange.emit(this.selectedChannelRows);
        this.fireSelectionChanged();
      } else {
        this.selectedYChannels = [];
      }
    }
  }

  private selectPreferredUnits(channel: Channel[], physDimName2Id: { [sourceName: string]: Map<string, string> }) {
    channel.forEach((c) => this.selectPreferredUnit(c, physDimName2Id[c.source]));
  }

  private selectPreferredUnit(channel: Channel, physDimName2Id: Map<string, string>) {
    if (channel instanceof VirtualChannelUnitUI) {
      channel.updateColumnData(this.selectedView.columns.findIndex((c) => this.isUnitColumn(c)));
    } else if (channel instanceof ChannelUnitUI) {
      if (physDimName2Id) {
        const unitFilter = this.selectedUnitPreference.unitFilters.find((uf) =>
          this.channelMatchesUnitFilter(channel, uf, physDimName2Id.get(uf.physDim)),
        );

        if (unitFilter) {
          channel.setSelectedUnitName(unitFilter.unit);
        } else {
          channel.setSelectedUnitName(channel.unitName);
        }
      }
      channel.updateColumnData(this.selectedView.columns.findIndex((c) => this.isUnitColumn(c)));
    }
  }

  private channelMatchesUnitFilter(channel: ChannelUnitUI, unitFilter: UnitFilter, physDimId: string) {
    if (physDimId === channel.unit.physDimId) {
      if (unitFilter.meaQuant !== undefined && unitFilter.meaQuant !== "*" && unitFilter.meaQuant.trim().length > 0) {
        try {
          const regExpr = new RegExp(unitFilter.meaQuant, this.isCaseSensitive ? "" : "i");
          return regExpr.test(channel.name);
        } catch {
          return this.isCaseSensitive
            ? channel.name === unitFilter.meaQuant
            : channel.name.toLowerCase() === unitFilter.meaQuant.toLowerCase();
        }
      } else {
        return true;
      }
    }
    return false;
  }

  private selectChannelsByPrefFilters() {
    if (
      this.selectedChannelFilterPreference !== undefined &&
      this.arrayUtil.isNotEmpty(this.selectedChannelFilterPreference.channelFilters) &&
      this.arrayUtil.isNotEmpty(this.yChannelOptions)
    ) {
      let matchExpressions: RegExp[];
      if (this.isCaseSensitive) {
        matchExpressions = this.selectedChannelFilterPreference.channelFilters.map((expr) => new RegExp(expr));
      } else {
        matchExpressions = this.selectedChannelFilterPreference.channelFilters.map((expr) => new RegExp(expr, "i"));
      }
      const preSelectedYChannels = this.yChannelOptions.filter((c) => matchExpressions.some((re) => re.test(c.name)));

      if (
        (this.selectedYChannels === undefined || this.selectedYChannels.length === 0) &&
        this.arrayUtil.isNotEmpty(preSelectedYChannels)
      ) {
        // select the channels
        const delayedCall = of(preSelectedYChannels).pipe(delay(250));
        delayedCall.subscribe((data) => {
          this.selectedYChannels = data;
          // selectedTableChannels will mark the channels as checked, in the Y-Channel table
          this.selectedTableChannels = [].concat(data);
          this.onSelectedYChannelsChanged();
        });
      }
    }
  }

  private convertResultsToYChannelOptions(channels: Channel[], resultRows: Row[], units: UnitsPerSource) {
    if (this.selectedUnitPreference && resultRows && resultRows.length > 0 && this.yChannelOptions.length > 0) {
      const realChannels = channels.map((c) => this.convertOneResultToYChannelOption(c, resultRows, units));
      const allChannels = realChannels.map((c) => this.convertVirtualChannel(c, realChannels));

      return allChannels;
    }
    return [];
  }

  private convertOneResultToYChannelOption(channel: Channel, resultRows: Row[], units: UnitsPerSource): Channel {
    return this.addTableColumnData(this.convertToChannelUnitUi(channel, units), resultRows);
  }

  private convertToChannelUnitUi(channel: Channel, units: UnitsPerSource) {
    if (channel.type === TYPE_CHANNEL) {
      if (channel instanceof ChannelUnitUI) {
        return channel;
      } else {
        const u: SourceUnits = units[channel.source];
        if (u) {
          return new ChannelUnitUI(channel, u[channel.unitId]);
        } else {
          return channel;
        }
      }
    }
    return channel;
  }

  /**
   * Get the real attribute value from the search result, according to the table configuration
   *
   * @param channel
   * @param field
   * @param header
   */
  private getTranslatedRowValue(channel: Channel, field: string, colIndex: number) {
    if (field === this.unitViewColumn.type && channel instanceof ChannelUnitUI) {
      return (<ChannelUnitUI>channel).unitName;
    } else if (channel.type === TYPE_VIRTUAL_CHANNEL) {
      if ([this.unitViewColumn.type, "Quantity", "LocalColumn"].includes(field)) {
        return "";
      } else if (field === "Channel") {
        return this.getArrayIndex(channel["columns"], colIndex);
      } else {
        const firstChannel: Channel = (<VirtualChannelUnitUI>channel).channels.values().next().value;
        return this.getArrayIndex(firstChannel["columns"], colIndex);
      }
    } else {
      return this.getArrayIndex(channel["columns"], colIndex);
    }
  }

  private getArrayIndex<T>(a: T[], index: number) {
    if (a && a.length >= index) {
      return a[index];
    } else {
      return "-";
    }
  }

  private viewColumnsToPreference() {
    const data = [];
    for (const col of this.selectedView.columns) {
      data.push(new SimpleColumn(col.type, col.name));
    }

    const pref = new Preference();
    pref.value = JSON.stringify(data);
    pref.key = this.preferencePrefix;
    pref.scope = Scope.USER;
    return pref;
  }

  /**
   * Add a mdm item to the chart
   * @param mdmItem the mdm item which is either a channel, channel group or measurement
   */
  private appendMDMItem(mdmItems: MDMItem[], unitsPerSource: UnitsPerSource) {
    // only append the following types
    mdmItems.forEach((mdmItem) => {
      const isChannel = mdmItem.type === "Channel";
      if (isChannel || mdmItem.type === "ChannelGroup" || mdmItem.type === "Measurement") {
        this.chartViewerDataService.loadMeasurementByData(mdmItem.type, mdmItem.source, mdmItem.id, undefined).subscribe((mea) => {
          // convert to node
          const node = new Node();
          node.type = mdmItem.type;
          node.id = mdmItem.id;
          node.idAttribute = mdmItem.idAttribute;
          node.source = mdmItem.source;
          node.filter = mdmItem.filter;
          node.serial = mdmItem.serial;

          if (isChannel) {
            this.addSingleChannel(mea, mdmItem.id, unitsPerSource);
            this.chartViewerService.sendAppendMetaNode(node);
          } else {
            // default handling, existing logic will create the tree
            this.chartViewerService.sendAppendNode(node);
          }
        });
      }
    });
  }

  /**
   * Add a single channel to the chart
   * @param mea the measurement
   * @param channelId the channel id
   */
  private addSingleChannel(mea: Measurement, channelId: string, unitsPerSource: UnitsPerSource) {
    const channel = mea.allChannels().find((tmp) => tmp.id === channelId);

    if (channel !== undefined) {
      // check if the measurement already is added
      let created = false;
      let channelFound = false;
      this.measurements
        .map((mes) => mes.allChannels())
        .map((chn) =>
          chn.forEach((obj) => {
            if (obj.id === channelId) {
              channelFound = true;
            }
          }),
        );

      if (!channelFound) {
        // re-create the array to invoke onChanges event
        const measurements = [];
        this.measurements.forEach((m) => measurements.push(m));
        measurements.push(mea);
        this.measurements = measurements;
        created = true;
      }

      // append the measurement node
      if (created) {
        const nodeMes = new Node();
        nodeMes.type = "Measurement";
        nodeMes.id = mea.id;
        nodeMes.source = mea.source;
        this.chartViewerService.sendAppendNode(nodeMes);
      }

      /*const groups = this.selectedChannelGroupsTree !== undefined ? [... this.selectedChannelGroupsTree] : [];
      // select the measurement if only one channelgroup exists
      if (mea.channelGroups.length === 1 && groups.find(grp => grp.data.type === "Measurement" && grp.data.id === mea.id) === undefined) {
        groups.push({ children: mea.channelGroups, data: mea, expanded: true, partialSelected: false, parent: null });
      }

      // select the channelgroup
      if (groups.find(grp => grp.data.type === "ChannelGroup" && grp.data.id === channel.channelGroupId) === undefined) {
        groups.push({ data: mea.channelGroups.find(cg => cg.id === channel.channelGroupId), parent: mea, partialSelected: false });
        this.selectedChannelGroupsTree = groups;
        this.selectedChannelGroups = this.selectedChannelGroupsTree.map(tn => tn.data);
      }*/

      // populate x-channel dropdown
      if (this.xChannelOptions[channel.channelGroupId] === undefined) {
        this.xChannelOptions[channel.channelGroupId] = [];
      }
      if (this.xChannelOptions[channel.channelGroupId].find((chn) => chn.id === channel.id) === undefined) {
        this.xChannelOptions[channel.channelGroupId].push(channel);
      }

      // special handling for channel pre-selection
      const channels = [...this.selectedTableChannels];
      if (channels.find((chn) => chn.id === channel.id) === undefined) {
        channels.push(channel);
        this.selectedTableChannels = channels;

        if (this.yChannelOptions.find((chn) => chn.id === channel.id) === undefined) {
          this.yChannelOptions.push(channel);
        }
        let pushCount = 0;
        mea.channels.forEach((chn) =>
          chn.forEach((tmp) => {
            if (tmp.channelGroupId === channel.channelGroupId && this.yChannelOptions.find((chn2) => chn2.id === tmp.id) === undefined) {
              // append into y-channel options
              let convertedTmp: Channel;
              if (this.results.rows && this.results.rows.length > 0) {
                convertedTmp = this.convertOneResultToYChannelOption(tmp, this.results.rows, unitsPerSource);
                // TODO
                convertedTmp = this.convertVirtualChannel(convertedTmp, this.yChannelOptions);
              } else {
                convertedTmp = tmp;
              }
              pushCount++;
              this.yChannelOptions.push(convertedTmp);
              const additionalMatchingFormulas = this.getMatchingVirtualChannels(this.yChannelOptions);
              if (additionalMatchingFormulas.length > 0) {
                pushCount += additionalMatchingFormulas.length;
                this.yChannelOptions = this.yChannelOptions.concat(additionalMatchingFormulas);
              }
            }
          }),
        );

        if (pushCount > 0) {
          this.yChannelOptions = this.sortChannels(this.sortViewColumn, this.sortOrder, this.yChannelOptions);
        }
      }
    }
  }

  private initFormulaPrefs() {
    this.allFormulaPreferences = [];
    this.loadFormulaSubcription = this.userFormulaService
      .getUserFormulaPreferences()
      .pipe()
      .subscribe(
        (userPrefs) => this.mapFormulaPrefs(userPrefs),
        (error) =>
          this.notificationService.notifyError(
            this.translateService.instant("user-prefs.formula-prefs.err-cannot-load-formula-prefs"),
            error,
          ),
      );
  }

  private mapFormulaPrefs(formulaPrefs: FormulaPreference[]) {
    if (formulaPrefs.length === 0) {
      this.allFormulaPreferences = [];
    } else {
      this.allFormulaPreferences = this.userFormulaService.sortFormulaPreferences(formulaPrefs);
    }
    this.loadFormulaSubcription.unsubscribe();
  }
}
