/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from "@angular/core";
import { RequestOptions } from "../../model/chartviewer.model";
import { Measurement } from "../../model/types/measurement.class";

@Component({
  selector: "mdm-request-options",
  templateUrl: "./request-options.component.html",
})
export class RequestOptionsComponent implements OnChanges {
  public readonly minIndex = 1;
  private readonly maxRequestedValues = 10000;

  public rangeValues: number[] = [];
  public numberOfRows = 0;
  public step = 1;

  // The component is multi-used by quickviewer and xy-chartviewer which either pass a single entity or an entity array in the latter case
  @Input()
  public measurements: Measurement | Measurement[];

  @Input()
  public requestOptions: RequestOptions;

  @Output()
  public requestOptionsChanged = new EventEmitter<RequestOptions>();

  ngOnChanges(changes: SimpleChanges) {
    if (changes["requestOptions"] && this.requestOptions) {
      this.applyRequestOptionstoRangeValues();
    }
    if (changes["measurements"]) {
      this.reload();
      this.emitRequestOptionsChanged();
    }
  }

  public onChangeValueRange() {
    this.applyRangeValuesToRequestOptions();
  }

  private applyRequestOptionstoRangeValues() {
    if (this.requestOptions.requestSize === 0) {
      this.rangeValues = [NaN, NaN];
    } else {
      this.rangeValues = [this.requestOptions.startIndex + 1, this.requestOptions.startIndex + this.requestOptions.requestSize];
    }
  }

  private applyRangeValuesToRequestOptions() {
    // check if input string is numeric, set min/max otherwise
    if (isNaN(+this.rangeValues[0])) {
      this.rangeValues[0] = this.minIndex;
    }
    if (isNaN(+this.rangeValues[1])) {
      this.rangeValues[1] = this.numberOfRows;
    }
    // reassign for change detection
    this.rangeValues = [].concat(this.rangeValues);
    this.requestOptions.startIndex = this.rangeValues[0] - 1;
    this.requestOptions.requestSize = this.rangeValues[1] - this.rangeValues[0] + 1;
  }

  public onApplySettings() {
    this.emitRequestOptionsChanged();
  }

  public onResetSettings() {
    this.requestOptions = new RequestOptions({} as RequestOptions);

    this.reload();
    this.emitRequestOptionsChanged();
  }

  private emitRequestOptionsChanged() {
    this.requestOptionsChanged.emit(this.requestOptions);
  }

  private reload() {
    if (!this.requestOptions.numberOfChunks) {
      this.requestOptions.numberOfChunks = 600;
    }
    if (this.requestOptions.previewEnabled === undefined) {
      this.requestOptions.previewEnabled = true;
    }

    const channelGroups = this.getChannelGroups();
    if (channelGroups.length > 0) {
      this.numberOfRows = channelGroups.map((cg) => cg.numberOfRows).reduce((prev, curr) => Math.max(prev, curr), 0);

      this.step = Math.min(1000, Math.max(Math.floor(this.numberOfRows / 100), 1));
      /*
      if (this.numberOfRows < this.requestOptions.numberOfChunks) {
        this.requestOptions.previewEnabled = false;
      }
      */
      const requestedValues = Math.min(this.numberOfRows, this.maxRequestedValues);
      this.rangeValues = [Math.max(this.minIndex, this.rangeValues[0]), Math.min(requestedValues, this.rangeValues[1])];
      this.onChangeValueRange();
    }
  }

  private getChannelGroups() {
    const channelGroups = [];
    if (Array.isArray(this.measurements)) {
      this.measurements.forEach((mea) => {
        for (const cg of mea.channelGroups) {
          channelGroups.push(cg);
        }
      });
    } else if (this.measurements) {
      for (const cg of (this.measurements as unknown as Measurement).channelGroups) {
        channelGroups.push(cg);
      }
    }
    return channelGroups;
  }
}
