/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { TYPE_VIRTUAL_CHANNEL, UserFormulaBaseService, VirtualChannelUnitUI } from "../../user-prefs/services/user-formula-base.service";
import { MeasuredValues } from "../model/chartviewer.model";
import { Channel } from "../model/types/channel.class";
import { TYPES_SCALAR_NUMERIC } from "../model/types/measured-values.class";
import { NumberArray } from "../model/types/number-array.class";
import { ChartViewerService } from "./chart-viewer.service";

@Injectable({
  providedIn: "root",
})
export class ChartViewerFormulaService extends UserFormulaBaseService {
  constructor(private chartViewerService: ChartViewerService) {
    super();
  }

  /**
   * Merge list of Channels with MeasuredValues by channel name. Only channels of one ChannelGroup
   * are allowed, because names of Channels within a ChannelGroup are unique.
   * @param channels
   * @param measuredValues
   */
  public mergeMeasuredValuesByName(
    channels: Channel[],
    measuredValues: MeasuredValues[],
  ): { channel: Channel; measuredValues: MeasuredValues }[] {
    const retVal: { channel: Channel; measuredValues: MeasuredValues }[] = channels
      .filter((c) => c.type !== TYPE_VIRTUAL_CHANNEL)
      .map((c) => {
        return { channel: c, measuredValues: measuredValues.find((mv) => mv.name === c.name) };
      });

    channels
      .filter((vc) => vc.type === TYPE_VIRTUAL_CHANNEL)
      .map((vc) => <VirtualChannelUnitUI>vc)
      .map((vc) => {
        const measurements: MeasuredValues[] = [];
        vc.channels.forEach((c) => measurements.push(measuredValues.find((mv) => mv.name === c.name)));
        // measurements might contain undefined / unfound values
        const result: MeasuredValues = this.calculateMeasurementsToNumbers(vc, measurements);
        retVal.push({ channel: vc, measuredValues: result });
      });
    return retVal;
  }

  /**
   * exceptions, while calculating might occur (e.g. division by zero)
   * @param virtChannel
   * @param measurements might contain undefined / unfound values
   * @returns always MeasuredValues, never undefined
   */
  private calculateMeasurementsToNumbers(virtChannel: VirtualChannelUnitUI, measurements: MeasuredValues[]): MeasuredValues {
    let retVal: MeasuredValues;
    const validMeasurements: MeasuredValues[] = measurements.filter((m) => m !== undefined);
    const numericMeasurements: MeasuredValues[] = validMeasurements.filter((m) => TYPES_SCALAR_NUMERIC.includes(m.scalarType));

    if (numericMeasurements.length === measurements.length) {
      // we can calculate
      retVal = this.convertNumbersToMeasuredValues(
        virtChannel.name,
        this.evaluateDataWith(
          virtChannel.mathJsParsableFormula,
          virtChannel.channelAliasList,
          virtChannel.channelIds,
          this.convertMeasurementsToNumbers(numericMeasurements),
        ),
      );
    } else {
      // no way to calculate, return 1st numeric measurement
      if (numericMeasurements.length > 0) {
        retVal = numericMeasurements[0];
        retVal.name = virtChannel.name;
      } else if (validMeasurements.length > 0) {
        retVal = validMeasurements[0];
        retVal.name = virtChannel.name;
      } else {
        retVal = this.convertNumbersToMeasuredValues(virtChannel.name, []);
      }
    }
    return retVal;
  }

  private convertMeasurementsToNumbers(numericMeasurements: MeasuredValues[]): Array<number[]> {
    const retVal: Array<number[]> = new Array<number[]>();
    numericMeasurements.forEach((m) => {
      const result = this.chartViewerService.getDataArrayWithFlagsRespection(m);
      if (result instanceof NumberArray) {
        retVal.push((<NumberArray>result).values);
      } else {
        retVal.push([]);
      }
    });
    return retVal;
  }

  private convertNumbersToMeasuredValues(name: string, rawData: number[]): MeasuredValues {
    const retVal: MeasuredValues = new MeasuredValues();
    const numbers: NumberArray = new NumberArray();
    numbers.values = rawData;
    retVal.name = name;
    retVal.scalarType = TYPES_SCALAR_NUMERIC[TYPES_SCALAR_NUMERIC.length - 1];
    retVal.doubleArray = numbers;
    retVal.flags = new Array<boolean>(rawData.length);
    retVal.flags.fill(true);
    return retVal;
  }
}
