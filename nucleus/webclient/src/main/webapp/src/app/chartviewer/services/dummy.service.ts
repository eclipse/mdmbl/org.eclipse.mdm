/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { of } from "rxjs";
import { Node } from "../../navigator/node";
import { MeasuredValues, MeasuredValuesResponse, NumberArray } from "../model/chartviewer.model";

const timeChannel = new MeasuredValues();
timeChannel.name = "time";
timeChannel.unit = "s";
timeChannel.length = 5;
timeChannel.independent = true;
timeChannel.scalarType = "INTEGER";
timeChannel.integerArray = new NumberArray();
timeChannel.integerArray.values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
timeChannel.flags = [true, true, true, true, true, true, true, true, true, true];

const channel1 = new MeasuredValues();
channel1.name = "channel1";
channel1.unit = "m";
channel1.length = 5;
channel1.independent = true;
channel1.scalarType = "INTEGER";
channel1.integerArray = new NumberArray();
channel1.integerArray.values = [3, 5, 4, 3, 2, 5, 1, 1, 7, 2];
channel1.flags = [true, true, true, true, true, true, true, true, true, true];

const data = new MeasuredValuesResponse();
data.values = [timeChannel, channel1];

@Injectable({
  providedIn: "root",
})
export class DummyChartviewerService {
  constructor() {}

  loadMeasuredValues(channelGroup: Node, channels: Node[], startIndex = 0, requestSize = 0) {
    data.values[0].length = requestSize;
    data.values[0].integerArray.values = Array.from(Array(requestSize).keys()).map((v, i) => startIndex + i);
    data.values[0].flags = data.values[0].integerArray.values.map((v) => true);

    data.values[1].length = requestSize;
    data.values[1].integerArray.values = data.values[0].integerArray.values.map((v) => Math.random() * 10);
    data.values[1].flags = data.values[1].integerArray.values.map((v) => true);

    return of(data.values);
  }

  readMeasuredValuesMetadata(channelGroup: Node, channels: Node[]) {
    return of(data.values);
  }
}
