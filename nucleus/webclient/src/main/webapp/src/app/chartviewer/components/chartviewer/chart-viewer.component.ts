/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { TranslateService } from "@ngx-translate/core";
import { Chart } from "chart.js";
import zoomPlugin from "chartjs-plugin-zoom";
import { MenuItem } from "primeng/api";
import { UIChart } from "primeng/chart";
import { ContextMenu } from "primeng/contextmenu";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { Node } from "../../../navigator/node";
import { ChannelSelectionRow, MeasuredValues, RequestOptions } from "../../model/chartviewer.model";
import { Channel } from "../../model/types/channel.class";
import { ChannelGroup } from "../../model/types/channelgroup.class";
import { Measurement } from "../../model/types/measurement.class";
import { ChartViewerDataService } from "../../services/chart-viewer-data.service";
import { ChartViewerService } from "../../services/chart-viewer.service";
import { ColorService } from "../basic-chart.component";

Chart.register(zoomPlugin);

type ChartViewerDataset = {
  id: string;
  label: string;
  data: unknown[];
  borderColor: string;
};

type ChartViewerData = {
  labels: number[];
  datasets: ChartViewerDataset[];
};

@Component({
  selector: "mdm-chart-viewer",
  templateUrl: "chart-viewer.component.html",
  providers: [ColorService],
})
export class ChartViewerComponent implements OnInit, OnChanges, OnDestroy, IStatefulComponent {
  readonly numberOfMeasurementsToKeepForState = 10;
  public contextMenuItems: MenuItem[];

  @Input()
  measurement: Measurement;
  @Input()
  selectedChannelRows: ChannelSelectionRow[];

  @ViewChild("lineChart")
  chart: UIChart;
  @ViewChild("contextMenu")
  contextMenu: ContextMenu;

  // options for measured value data request
  requestOptions = new RequestOptions({ startIndex: 2 } as RequestOptions);
  requestOptionsPerMeasurement = [] as { id: string; requestOptions: RequestOptions }[];

  chartTimer: any;

  data = this.getEmptyData();
  tmpData = this.getEmptyData();

  options = {
    scales: {
      x: {
        type: "linear",
      },
    },
    elements: {
      point: {
        radius: 0,
      },
      line: {
        tension: 0,
      },
    },
    legend: {
      display: true,
    },
    plugins: {
      zoom: {
        pan: {
          enabled: true,
          mode: "xy",
          onDrag: true,
          treshold: 10,
          mouseButtonPan: "left",
        },
        zoom: {
          wheel: {
            enabled: true,
          },
          pinch: {
            enabled: true,
          },
          mode: "xy",
          scaleMode: "xy",
        },
      },
    },
  };
  hiddenChannels: string[] = [];

  constructor(
    private chartService: ChartViewerDataService,
    private chartViewerService: ChartViewerService,
    private colorService: ColorService,
    private stateService: StateService,
    private translateService: TranslateService,
  ) {
    this.stateService.register(this, "chart-viewer");
  }

  getState() {
    // add new requestOptions if measurement is defined and requestOptions does not have default values
    if (this.measurement && this.measurement.id && !this.requestOptions.isDefault()) {
      this.requestOptionsPerMeasurement = this.requestOptionsPerMeasurement.filter((fr) => fr.id !== this.measurement?.id);
      this.requestOptionsPerMeasurement.push({ id: this.measurement?.id, requestOptions: this.requestOptions });
    }

    // only keep the last n entries
    this.requestOptionsPerMeasurement = this.requestOptionsPerMeasurement.slice(-this.numberOfMeasurementsToKeepForState);

    return {
      requestOptions: this.requestOptionsPerMeasurement,
      //hiddenChannels: this.chart._metasets.map((m) => (m.hidden ? m.label : null)).filter((m) => m === null),
    };
  }

  applyState(state: unknown) {
    if (state["requestOptions"]) {
      this.requestOptionsPerMeasurement = state["requestOptions"] || [];
      if (!Array.isArray(this.requestOptionsPerMeasurement)) {
        // for backwards compatibility, we check that we have an array and no object
        this.requestOptionsPerMeasurement = [];
      }
      this.requestOptions = this.getRequestOptionsForMeasurement(this.measurement);
    }
    if (state["hiddenChannels"]) {
      this.hiddenChannels = state["hiddenChannels"];
    }
  }

  ngOnInit(): void {
    this.createContextMenu();
  }

  // invoked from chart-viewer-nav-card.onSelectedNodeChange().subscribe calls
  ngOnChanges(changes: SimpleChanges) {
    this.data = this.getEmptyData();
    this.tmpData = this.getEmptyData();

    if (this.measurement && this.requestOptionsPerMeasurement && (changes["measurement"] || changes["selectedChannelRows"])) {
      this.onRequestOptionsChanged(this.getRequestOptionsForMeasurement(this.measurement));
    }
    /*for (const propName in changes) {
      if (this.measurement !== undefined && (propName === "measurement" || propName === "selectedChannelRows")) {
        this.measurement.channelGroups.forEach((group) => {
          this.chartChannel(group);
        });

        break;
      }
    }*/
  }

  ngOnDestroy() {
    this.stateService.unregister(this);
  }

  private createContextMenu() {
    this.contextMenuItems = [
      {
        label: this.translateService.instant("chartviewer.reset-scale"),
        icon: "pi pi-fw pi-undo",
        command: () => this.chart.chart.resetZoom(),
      },
    ];
  }

  onRightClick(event: MouseEvent): void {
    event.preventDefault();
    this.contextMenu.show(event);
  }

  private getRequestOptionsForMeasurement(measurement: Measurement) {
    if (measurement && measurement.id) {
      return new RequestOptions(
        this.requestOptionsPerMeasurement.find((e) => e.id === measurement.id)?.requestOptions || ({} as RequestOptions),
      );
    } else {
      return new RequestOptions({} as RequestOptions);
    }
  }

  getEmptyData() {
    return {
      labels: [],
      datasets: [
        {
          id: "-1",
          label: "No data",
          data: [],
          borderColor: "#fff",
        },
      ],
    } as ChartViewerData;
  }

  getColor(name: string) {
    return this.data.datasets.find((dataset) => dataset.label === name).borderColor;
  }

  // Sets new data request options and redraws the graph
  public onRequestOptionsChanged(options: RequestOptions) {
    this.requestOptions = options;
    this.measurement.channelGroups.forEach((group) => {
      this.chartChannel(group);
    });
  }

  chartChannel(channelGroup: ChannelGroup) {
    if (this.requestOptions != undefined && this.selectedChannelRows !== undefined) {
      console.log("Chart channel", channelGroup, this.selectedChannelRows);
      let group: ChannelGroup;
      let channels: Channel[] = [];

      if (
        this.selectedChannelRows.length > 0 &&
        channelGroup !== undefined &&
        this.selectedChannelRows.findIndex((cr) => channelGroup.id === cr.channelGroup.id) > -1
      ) {
        group = channelGroup;
        channels = this.selectedChannelRows.filter((cr) => cr.channelGroup.id === group.id).map((cr) => cr.yChannel);
      } else if (this.selectedChannelRows.length === 0) {
        // either the selected channel or all channels from the map
        // group = this.measurement.getFirstChannelGroup();
        // if (group) {
        //  channels = this.measurement.findChannels(group);
        // }
        this.data = this.getEmptyData();
        this.tmpData = this.getEmptyData();
        group = undefined;
      } else {
        group = this.selectedChannelRows[0].channelGroup;
        channels = this.selectedChannelRows.filter((cr) => cr.channelGroup.id === group.id).map((cr) => cr.yChannel);
      }

      if (group !== undefined) {
        this.chartMeasuredValues(
          this.chartService.loadValues(
            group,
            channels,
            this.requestOptions.startIndex,
            this.requestOptions.requestSize,
            this.requestOptions.getChunks(),
          ),
          channels,
        );
      }
    }
  }

  chartMeasuredValues(measuredValues: Observable<MeasuredValues[]>, channels: Channel[]) {
    measuredValues
      .pipe(
        map(
          (mvl) =>
            ({
              labels: this.getLabels(),
              datasets: mvl.map((mv, i) => ({ measuredValues: mv, channelId: channels[i].id })).map(this.convertToDataset, this),
            } as ChartViewerData),
        ),
        catchError((e) => addErrorDescription(e, "Could chart measured values!")),
      )
      .subscribe((d) => {
        if (this.tmpData.labels.length === 0) {
          this.tmpData = d;
        } else {
          const result = Object.values(
            [].concat(this.tmpData.datasets, d.datasets).reduce((prev, c) => ((prev[c.id] = Object.assign(prev[c.id] || {}, c)), prev), {}),
          ) as [];

          this.tmpData.datasets = result;
        }

        if (this.chartTimer) {
          clearTimeout(this.chartTimer);
        }
        // adequate delay or the chart display will only render the latest group
        this.chartTimer = setTimeout(() => this.processChartUpdate(), 250);
      });
  }

  processChartUpdate() {
    this.data = this.tmpData;
    this.tmpData = this.getEmptyData();
  }

  getLabels() {
    const labels: number[] = [];
    if (this.requestOptions.previewEnabled && this.requestOptions.requestSize > this.requestOptions.numberOfChunks) {
      for (
        let i = this.requestOptions.startIndex;
        i <= this.requestOptions.startIndex + this.requestOptions.requestSize;
        i += this.requestOptions.requestSize / this.requestOptions.numberOfChunks
      ) {
        labels.push(Math.floor(i));
      }
    } else {
      for (let i = this.requestOptions.startIndex; i <= this.requestOptions.startIndex + this.requestOptions.requestSize; i++) {
        labels.push(Math.floor(i));
      }
    }
    return labels;
  }

  getLength(measuredValues: MeasuredValues[]) {
    return measuredValues.map((mv) => mv.length).reduce((p, l) => Math.max(p, l));
  }

  convertToDataset(x: { measuredValues: MeasuredValues; channelId: string }) {
    //measuredValues: MeasuredValues, channelId: string) {
    const data = this.chartViewerService.getDataArrayWithFlagsRespection(x.measuredValues);

    const label = x.measuredValues.name + " [" + x.measuredValues.unit + "]";
    const { color, dash } = this.colorService.getColorAndDash(label + x.measuredValues.optionalLabel);
    return {
      id: x.channelId,
      label: label,
      unit: x.measuredValues.unit,
      data: data ? data.values : [],
      borderColor: color,
      borderDash: dash,
      borderWidth: 1,
      fill: false,
      hidden: this.hiddenChannels.findIndex((c) => c === label) >= 0,
      measuredValues: map,
      channel: new Node(),
    } as ChartViewerDataset;
  }
}
