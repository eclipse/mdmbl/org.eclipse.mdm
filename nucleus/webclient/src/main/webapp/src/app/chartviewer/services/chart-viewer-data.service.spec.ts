/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { PropertyService } from "@core/property.service";
import { QueryService } from "../../tableview/query.service";
import { ChannelUnitUI, MeasuredValues, MeasuredValuesResponse, NumberArray } from "../model/chartviewer.model";
import { Channel } from "../model/types/channel.class";
import { ChannelGroup } from "../model/types/channelgroup.class";
import { ChartViewerDataService } from "./chart-viewer-data.service";

const timeChannel = new MeasuredValues();
timeChannel.name = "time";
timeChannel.unit = "s";
timeChannel.length = 5;
timeChannel.independent = true;
timeChannel.integerArray = new NumberArray();
timeChannel.integerArray.values = [1, 2, 3, 4, 5];
timeChannel.flags = [true, true, true, true, true];

const channel1 = new MeasuredValues();
channel1.name = "channel1";
channel1.unit = "m";
channel1.length = 5;
channel1.independent = true;
channel1.integerArray = new NumberArray();
channel1.integerArray.values = [3, 5, 4, 3, 2];
channel1.flags = [true, true, true, true, true];

export const data = new MeasuredValuesResponse();
data.values = [channel1, timeChannel];

describe("ChartViewerDataService", () => {
  let httpTestingController: HttpTestingController;
  let chartViewerDataService: ChartViewerDataService;

  beforeEach(() => {
    TestBed.resetTestEnvironment();
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ChartViewerDataService, PropertyService, QueryService],
    });

    httpTestingController = TestBed.get(HttpTestingController);
    chartViewerDataService = TestBed.get(ChartViewerDataService);
  });

  it("can read values", () => {
    const channelGroup = new ChannelGroup("MDM", "1", 5);
    const channel = new ChannelUnitUI(new Channel());
    channel.name = channel1.name;

    chartViewerDataService
      .loadMeasuredValues(channelGroup, [channel])
      .subscribe((channelGroups) => expect(channelGroups).toEqual(data.values));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/MDM/values/read");
    expect(req.request.method).toEqual("POST");
    req.flush(data);

    // Finally, assert that there are no outstanding requests.
    httpTestingController.verify();
  });

  it("can filter on data", () => {
    const channelGroup = new ChannelGroup("MDM", "1", 5);
    const channel = new ChannelUnitUI(new Channel());

    chartViewerDataService
      .loadMeasuredValues(channelGroup, [channel])
      .subscribe((channelGroups) => expect(channelGroups.filter((c) => c.name === "time")).toEqual([timeChannel]));

    const req = httpTestingController.expectOne("/org.eclipse.mdm.nucleus/mdm/environments/MDM/values/read");
    expect(req.request.method).toEqual("POST");
    req.flush(data);

    // Finally, assert that there are no outstanding requests.
    httpTestingController.verify();
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });
});
