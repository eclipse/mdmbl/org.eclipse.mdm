/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { TestBed } from "@angular/core/testing";
// PreferenceService injection dependencies for FormulaService
import { PreferenceService } from "@core/preference.service";
import { PropertyService } from "@core/property.service";
// AuthenticationService injection dependencies for FormulaService
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { of } from "rxjs";
import { AuthenticationService } from "../../core/authentication/authentication.service";
import { FormulaValidationResult, UserFormulaService } from "./user-formula.service";

class MockAuthenticationService {
  getCurrentUsername() {
    return of("testuser");
  }
}
describe("userFormulaService", () => {
  let formulaService: UserFormulaService;

  const CHNL1_EXPR = "^chan{2}el [oO]ne$";
  const CHNL2_EXPR = "^channel [^oO][^nN][^eE]$";
  const CHNL1_INPUT = "${ " + CHNL1_EXPR + " }";
  const CHNL2_INPUT = "${" + CHNL2_EXPR + "}";
  const CHNL_INPUT_LIST = [" " + CHNL1_EXPR + " ", CHNL2_EXPR];
  const CHNL_EXPR_LIST = [CHNL1_EXPR, CHNL2_EXPR];
  const CHNL1_OUTPUT = "${" + CHNL1_EXPR + "}";
  const CHNL2_OUTPUT = CHNL2_INPUT;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        UserFormulaService,
        { provide: AuthenticationService, useClass: MockAuthenticationService },
        PropertyService,
        PreferenceService,
      ],
    });

    formulaService = TestBed.get(UserFormulaService);
    formulaService.testMode = true;
  });

  it("should be created", () => {
    expect(formulaService).toBeTruthy();
  });

  it("should CORRECT (clean-up) bogus input formulas", () => {
    expect(formulaService.getCleanedFormula(".5+1...1..23..4.5^^(%#^(#){)+++}12***13*/({25.{0)}")).toEqual("0.5+1.12345^(#%^#)+12*13/25.0");

    expect(formulaService.getCleanedFormula("+.5+1...1..23..4.5^^(%#^(#){)+++}12***13*/({25.{0)}")).toEqual(
      "0.5+1.12345^(#%^#)+12*13/25.0",
    );

    expect(formulaService.getCleanedFormula("-.5+1...1..23..4.5^^(%#^(#){)+++}12***13*/({25.{0)}")).toEqual(
      "-0.5+1.12345^(#%^#)+12*13/25.0",
    );

    expect(formulaService.getCleanedFormula("9(+)(*)(-)(/)(^)")).toEqual("9");
    expect(formulaService.getCleanedFormula("9(+*)(-/^)")).toEqual("9");
    expect(formulaService.getCleanedFormula("9(+*(-/^(")).toEqual("9");
    expect(formulaService.getCleanedFormula("9(!(9(%)")).toEqual("99%");
    expect(formulaService.getCleanedFormula("9(+*)(-/^)9")).toEqual("9+9");
    expect(formulaService.getCleanedFormula("..5")).toEqual("0.5");
    expect(formulaService.getCleanedFormula("5...")).toEqual("5");
    expect(formulaService.getCleanedFormula("..5..")).toEqual("0.5");
    expect(formulaService.getCleanedFormula("  cccoooosss(#)  *  pppii ")).toEqual("cos(#)*pi");
    expect(formulaService.getCleanedFormula("  ssinn(#)  *  PI / 2 ")).toEqual("sin(#)*pi/2");
    // place-holders are illegal (letter + digit)
    expect(formulaService.getCleanedFormula("a0 + b0 * #")).toEqual("#");
    expect(formulaService.getCleanedFormula("a0 + # * e0")).toEqual("#");
    expect(formulaService.getCleanedFormula("3.a0 + #.3 - 4.# * b.0")).toEqual("30+#-#");
    // no function name check is implemented at this point
    // this shall be catched when parsing with mathjs.org
    expect(formulaService.getCleanedFormula("abcd0 + 1.2# * efgh0")).toEqual("abcd+#*efgh");
    // constants are legal
    expect(formulaService.getCleanedFormula("a0 + # * e")).toEqual("#*e");
    expect(formulaService.getCleanedFormula("LOG2E + # * LOG10E")).toEqual("LOG2E+#*LOG10E");
    expect(formulaService.getCleanedFormula("SQRT1_2 + # * SQRT2")).toEqual("SQRT1_2+#*SQRT2");
    expect(formulaService.getCleanedFormula("SQRT1_2 + #0.0 * SQRT2")).toEqual("SQRT1_2+#*SQRT2");
  });

  it("should CONVERT VALID input formulas", () => {
    expect(formulaService.getCleanedFormula("")).toEqual("");
    expect(formulaService.getCleanedFormula("10 + 2 * 6")).toEqual("10+2*6");
    expect(formulaService.getCleanedFormula("100 * 2 + 12")).toEqual("100*2+12");
    expect(formulaService.getCleanedFormula("100 * 2 + -12")).toEqual("100*2-12");
    expect(formulaService.getCleanedFormula("# + 2 * #")).toEqual("#+2*#");
    expect(formulaService.getCleanedFormula("# * 2 + #")).toEqual("#*2+#");
    expect(formulaService.getCleanedFormula("-# * 2 + -#")).toEqual("-#*2-#");
    expect(formulaService.getCleanedFormula("100 * ( 2 + 12 )")).toEqual("100*(2+12)");
    expect(formulaService.getCleanedFormula("(100) * (( 2 ) + (12) )")).toEqual("100*(2+12)");
    expect(formulaService.getCleanedFormula("round(e, 3)")).toEqual("round(e,3)");
    expect(formulaService.getCleanedFormula("atan2(3, -3) / pi")).toEqual("atan2(3,-3)/pi");
    expect(formulaService.getCleanedFormula("log(10000, 10)")).toEqual("log(10000,10)");
    expect(formulaService.getCleanedFormula("sqrt(3^2 + 4^2)")).toEqual("sqrt(3^2+4^2)");
    expect(formulaService.getCleanedFormula("round(sin(-pi),3)")).toEqual("round(sin(-pi),3)");
    expect(formulaService.getCleanedFormula("cos(45 deg)")).toEqual("cos(45deg)");
    expect(formulaService.getCleanedFormula("sin(-0.5*pi)")).toEqual("sin(-0.5*pi)");
    expect(formulaService.getCleanedFormula("sin(0)")).toEqual("sin(0)");
    expect(formulaService.getCleanedFormula("round(#, 3)")).toEqual("round(#,3)");
    expect(formulaService.getCleanedFormula("atan2(#, -3) / pi")).toEqual("atan2(#,-3)/pi");
    expect(formulaService.getCleanedFormula("atan2(3, -#) / pi")).toEqual("atan2(3,-#)/pi");
    expect(formulaService.getCleanedFormula("log(#, 10)")).toEqual("log(#,10)");
    expect(formulaService.getCleanedFormula("sqrt(3^2 + #^2)")).toEqual("sqrt(3^2+#^2)");
    expect(formulaService.getCleanedFormula("round(sin(-#),3)")).toEqual("round(sin(-#),3)");
    expect(formulaService.getCleanedFormula("cos(# deg)")).toEqual("cos(#deg)");
  });

  it("should VALIDATE user input as expected", () => {
    let result: FormulaValidationResult = formulaService.validate(
      ".5+1...1..23..4.5^^(%${{.()0.A1^2b+3c@+-4*5/6}^(${second channel name}){)+++}12***13*/({25.{0)",
    );
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("0.5+1.12345^(a0%^b0)+12*13/25.0");
    expect(result.channelExpressions).toEqual(["{.()0.A1^2b+3c@+-4*5/6}", "second channel name"]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual(
      "Parenthesis ) expected (char 41): `0.5+1.12345^(${{.()0.A1^2b+3c@+-4*5/6}}%^${second channel name})+12*13/25.0`",
    );

    result = formulaService.validate(".5+1...1..23..4.5^^(${{.()0.A1^2b+3c@+-4*5/6][}^(${second channel name}){)+++}12***13*/({25.{0)");
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("0.5+1.12345^(a0^b0)+12*13/25.0");
    expect(result.channelExpressions).toEqual(["{.()0.A1^2b+3c@+-4*5/6][}", "second channel name"]);
    expect(result.validatedChannelExpressions).toEqual([
      "#syntax error# `{.()0.A1^2b+3c@+-4*5/6][}` #syntax error#",
      "second channel name",
    ]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual(
      "0.5 + 1.12345 ^ (${#syntax error# `{.()0.A1^2b+3c@+-4*5/6][}` #syntax error#} ^ ${second channel name}) + 12 * 13 / 25",
    );

    result = formulaService.validate("-.5+1...1..23..4.5^^(${{.()0.A1^2b+3c@+-4*5/6][}^(${second channel name}){)+++}12***13*/({25.{0)");
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("-0.5+1.12345^(a0^b0)+12*13/25.0");
    expect(result.channelExpressions).toEqual(["{.()0.A1^2b+3c@+-4*5/6][}", "second channel name"]);
    expect(result.validatedChannelExpressions).toEqual([
      "#syntax error# `{.()0.A1^2b+3c@+-4*5/6][}` #syntax error#",
      "second channel name",
    ]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual(
      "-0.5 + 1.12345 ^ (${#syntax error# `{.()0.A1^2b+3c@+-4*5/6][}` #syntax error#} ^ ${second channel name}) + 12 * 13 / 25",
    );

    result = formulaService.validate("9(+)(*)(-)(/)(^)");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("9");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("9");

    result = formulaService.validate("9(+*)(-/^)");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("9");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("9");

    result = formulaService.validate("9(+*(-/^(");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("9");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("9");

    result = formulaService.validate("9(+*)(-/^)9");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("9+9");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("9 + 9");

    result = formulaService.validate("10 + 2 * 6");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("10+2*6");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("10 + 2 * 6");

    result = formulaService.validate("100 * 2 + 12");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("100*2+12");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("100 * 2 + 12");

    result = formulaService.validate("100 * 2 - 12");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("100*2-12");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("100 * 2 - 12");

    result = formulaService.validate("100 * 2 + -12");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("100*2-12");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("100 * 2 - 12");

    result = formulaService.validate("100 * (2) + -12");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("100*2-12");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("100 * 2 - 12");

    result = formulaService.validate("-100 * 2 + 12");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("-100*2+12");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("-100 * 2 + 12");

    result = formulaService.validate("100 * 2 ^ 12");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("100*2^12");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("100 * 2 ^ 12");

    result = formulaService.validate("( 100 * 2 ) ^ 12");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("(100*2)^12");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("(100 * 2) ^ 12");

    result = formulaService.validate("( 2 ^ 3 * 2 ) * 4 / 14");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("(2^3*2)*4/14");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("(2 ^ 3 * 2) * 4 / 14");

    result = formulaService.validate("2 ^ ( 3 * 2 ) * 4 / 14");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("2^(3*2)*4/14");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("2 ^ (3 * 2) * 4 / 14");

    result = formulaService.validate("100 * ( 2 + 12 )");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("100*(2+12)");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("100 * (2 + 12)");

    result = formulaService.validate("(100) * (( 2 ) + (12) )");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("100*(2+12)");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("100 * (2 + 12)");

    result = formulaService.validate("100 * ( 2 + 12 ) / 14");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("100*(2+12)/14");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("100 * (2 + 12) / 14");

    result = formulaService.validate("100 * ( 2*3 + 12 ) / 14");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("100*(2*3+12)/14");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("100 * (2 * 3 + 12) / 14");

    result = formulaService.validate("100 * ( 2 + 12*3 ) / 14");
    expect(result.channelAliasList).toEqual([]);
    expect(result.mathJsParsableFormula).toEqual("100*(2+12*3)/14");
    expect(result.channelExpressions).toEqual([]);
    expect(result.validatedChannelExpressions).toEqual([]);
    expect(result.valid).toBe(false);
    expect(result.parsedFormulaString).toEqual("100 * (2 + 12 * 3) / 14");

    result = formulaService.validate("${ ^channel [oO]ne } + 2 * ${^channel [^oO][^nN][^eE]}");
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("a0+2*b0");
    expect(result.channelExpressions).toEqual([" ^channel [oO]ne ", "^channel [^oO][^nN][^eE]"]);
    expect(result.validatedChannelExpressions).toEqual(["^channel [oO]ne", "^channel [^oO][^nN][^eE]"]);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual("${^channel [oO]ne} + 2 * ${^channel [^oO][^nN][^eE]}");

    result = formulaService.validate("${ ^channel [oO]ne } + 2 * ${^channel two} / ${^channel [^tT][^hH][^rR][^eE][^eE]} ");
    expect(result.channelAliasList).toEqual(["a0", "b0", "c0"]);
    expect(result.mathJsParsableFormula).toEqual("a0+2*b0/c0");
    expect(result.channelExpressions).toEqual([" ^channel [oO]ne ", "^channel two", "^channel [^tT][^hH][^rR][^eE][^eE]"]);
    expect(result.validatedChannelExpressions).toEqual(["^channel [oO]ne", "^channel two", "^channel [^tT][^hH][^rR][^eE][^eE]"]);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual("${^channel [oO]ne} + 2 * ${^channel two} / ${^channel [^tT][^hH][^rR][^eE][^eE]}");

    result = formulaService.validate(`${CHNL1_INPUT} * 2 + ${CHNL2_INPUT}`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("a0*2+b0");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`${CHNL1_OUTPUT} * 2 + ${CHNL2_OUTPUT}`);

    result = formulaService.validate(`${CHNL1_INPUT} * 2 + -${CHNL2_INPUT}`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("a0*2-b0");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`${CHNL1_OUTPUT} * 2 - ${CHNL2_OUTPUT}`);

    result = formulaService.validate(`-${CHNL1_INPUT} * 2 + -${CHNL2_INPUT}`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("-a0*2-b0");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`-${CHNL1_OUTPUT} * 2 - ${CHNL2_OUTPUT}`);

    result = formulaService.validate(`${CHNL1_INPUT} * (2) + -${CHNL2_INPUT}`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("a0*2-b0");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`${CHNL1_OUTPUT} * 2 - ${CHNL2_OUTPUT}`);

    result = formulaService.validate(`-${CHNL1_INPUT} * 2 + ${CHNL2_INPUT}`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("-a0*2+b0");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`-${CHNL1_OUTPUT} * 2 + ${CHNL2_OUTPUT}`);

    result = formulaService.validate(`${CHNL1_INPUT} * ${CHNL2_INPUT} ^ 12`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("a0*b0^12");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`${CHNL1_OUTPUT} * ${CHNL2_OUTPUT} ^ 12`);

    result = formulaService.validate(`( ${CHNL1_INPUT} * ${CHNL2_INPUT} ) ^ 12`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("(a0*b0)^12");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`(${CHNL1_OUTPUT} * ${CHNL2_OUTPUT}) ^ 12`);

    result = formulaService.validate(`${CHNL1_INPUT} * ( 2 + ${CHNL2_INPUT} )`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("a0*(2+b0)");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`${CHNL1_OUTPUT} * (2 + ${CHNL2_OUTPUT})`);

    result = formulaService.validate(`(${CHNL1_INPUT}) * (( ${CHNL2_INPUT} ) + 12 )`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("a0*(b0+12)");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`${CHNL1_OUTPUT} * (${CHNL2_OUTPUT} + 12)`);

    result = formulaService.validate(`${CHNL1_INPUT} * ( 2 + 12 ) / ${CHNL2_INPUT}`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("a0*(2+12)/b0");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`${CHNL1_OUTPUT} * (2 + 12) / ${CHNL2_OUTPUT}`);

    result = formulaService.validate(`${CHNL1_INPUT} * ( 2*${CHNL2_INPUT} + 12 ) / 14`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("a0*(2*b0+12)/14");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`${CHNL1_OUTPUT} * (2 * ${CHNL2_OUTPUT} + 12) / 14`);

    result = formulaService.validate(`${CHNL1_INPUT} * ( 2 + 12*${CHNL2_INPUT} ) / 14`);
    expect(result.channelAliasList).toEqual(["a0", "b0"]);
    expect(result.mathJsParsableFormula).toEqual("a0*(2+12*b0)/14");
    expect(result.channelExpressions).toEqual(CHNL_INPUT_LIST);
    expect(result.validatedChannelExpressions).toEqual(CHNL_EXPR_LIST);
    expect(result.valid).toBe(true);
    expect(result.parsedFormulaString).toEqual(`${CHNL1_OUTPUT} * (2 + 12 * ${CHNL2_OUTPUT}) / 14`);
  });

  it("should CALCULATE result correctly, based on user input and numeric mesurement data", () => {
    let result = formulaService.validate("10 + 2 * 6");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(22.0);

    result = formulaService.validate("100 * 2 + 12");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(212.0);

    result = formulaService.validate("100 * 2 - 12");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(188.0);

    result = formulaService.validate("-100 * 2 + 12");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(-188.0);

    result = formulaService.validate("100 * 2 ^ 5");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(3200.0);

    result = formulaService.validate("( 100 * 2 ) ^ 5");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(320000000000.0);

    result = formulaService.validate("( 2 ^ 3 * 2 ) * 4 / 16");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(4.0);

    result = formulaService.validate("2 ^ ( 3 * 2 ) * 4 / 16");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(16.0);

    result = formulaService.validate("100 * ( 2 + 12 )");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(1400.0);

    result = formulaService.validate("100 * ( 2 + 12 ) / 14");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(100.0);

    result = formulaService.validate("100 * ( 2*3 + 12 ) / 15");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(120.0);

    result = formulaService.validate("100 * ( 3 + 13 * 3 ) / 14");
    expect(formulaService.evaluateFormulaUT(result.mathJsParsableFormula)).toBe(300.0);

    result = formulaService.validate("${abcd} + 2 * ${xyz}");
    expect(
      formulaService.evaluateFormulaUT(result.mathJsParsableFormula, formulaService.toScopeDataObject(result.channelAliasList, [10, 70])),
    ).toBe(150.0);

    result = formulaService.validate("${abcd} * 2 + ${xyz}");
    expect(
      formulaService.evaluateFormulaUT(result.mathJsParsableFormula, formulaService.toScopeDataObject(result.channelAliasList, [10, 140])),
    ).toBe(160.0);

    result = formulaService.validate("-${abcd} * 2 - ${xyz}");
    expect(
      formulaService.evaluateFormulaUT(result.mathJsParsableFormula, formulaService.toScopeDataObject(result.channelAliasList, [10, 140])),
    ).toBe(-160.0);

    result = formulaService.validate("${abcd} * 2 - ${xyz}");
    expect(
      formulaService.evaluateFormulaUT(result.mathJsParsableFormula, formulaService.toScopeDataObject(result.channelAliasList, [140, 140])),
    ).toBe(140.0);

    result = formulaService.validate("-${abcd} * 2 + ${xyz}");
    expect(
      formulaService.evaluateFormulaUT(result.mathJsParsableFormula, formulaService.toScopeDataObject(result.channelAliasList, [20, 140])),
    ).toBe(100.0);

    result = formulaService.validate("${abcd} * ${xyz} ^ 12");
    expect(
      formulaService.evaluateFormulaUT(result.mathJsParsableFormula, formulaService.toScopeDataObject(result.channelAliasList, [5, 1.4])),
    ).toBeCloseTo(283.47, 2);

    result = formulaService.validate("( ${abcd} * ${xyz} ) ^ 12");
    expect(
      formulaService.evaluateFormulaUT(result.mathJsParsableFormula, formulaService.toScopeDataObject(result.channelAliasList, [5, 0.4])),
    ).toBe(4096.0);

    result = formulaService.validate("${abcd} * ( 2 + ${xyz} )");
    expect(
      formulaService.evaluateFormulaUT(
        result.mathJsParsableFormula,
        formulaService.toScopeDataObject(result.channelAliasList, [100.0, 12.0]),
      ),
    ).toBe(1400.0);

    result = formulaService.validate("${abcd} * ( 2 + 12 ) / ${xyz}");
    expect(
      formulaService.evaluateFormulaUT(
        result.mathJsParsableFormula,
        formulaService.toScopeDataObject(result.channelAliasList, [100.0, 14.0]),
      ),
    ).toBe(100.0);

    result = formulaService.validate("${abcd} * ( 2*${xyz} + 10 ) / 14");
    expect(
      formulaService.evaluateFormulaUT(
        result.mathJsParsableFormula,
        formulaService.toScopeDataObject(result.channelAliasList, [100.0, 2.0]),
      ),
    ).toBe(100.0);

    result = formulaService.validate("${abcd} * ( 2.0 + 12.0*${xyz} ) / 14.6");
    expect(
      formulaService.evaluateFormulaUT(
        result.mathJsParsableFormula,
        formulaService.toScopeDataObject(result.channelAliasList, [100.0, 12.0]),
      ),
    ).toBe(1000.0);
  });
});
