/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { ConfirmationService } from "primeng/api";
// import { SelectItem } from 'primeng/api';
import { MDMNotificationService } from "@core/mdm-notification.service";
import { Node } from "@navigator/node";
import { NodeService } from "@navigator/node.service";
import { TranslateService } from "@ngx-translate/core";
import { EditableColumn } from "primeng/table";
import { Subscription, take } from "rxjs";
import { FormulaPreference, UserFormulaService } from "../../services/user-formula.service";

const KEYS_IGNORE = [
  "Control",
  "Shift",
  "Alt",
  "ArrowLeft",
  "ArrowRight",
  "ArrowUp",
  "ArrowDown",
  "Home",
  "End",
  "PageUp",
  "PageDown",
  "Insert",
];
const KEYS_FORMULA_IGNORE = [" ", "+", "-", "*", "/", "$", "{"];

@Component({
  selector: "mdm-user-formula",
  templateUrl: "user-formula.component.html",
  styleUrls: ["./user-formula.component.css"],
  providers: [EditableColumn],
})
export class UserFormulaComponent implements OnInit {
  private readonly MSG_PREFIX = "user-prefs.formula-prefs";
  private readonly MSG_ERR_PREFIX = this.MSG_PREFIX + ".err-cannot-";
  private readonly MSG_POSTFIX = "-formula-prefs";

  public formulaPreferences: FormulaPreference[] = [];

  public isUserPrefFormulaOpen = true;

  tmpFormulaPrefDeleteIdx = -1;
  isShowDialogFormulaPrefDelete = false;
  tmpFormulaPrefEditIdx = -1;
  tmpFormulaPrefEdit: FormulaPreference;
  isShowDialogFormulaPrefEdit = false;
  formulaPrefAlreadyExists: string;

  loadSubcription: Subscription;

  environments: Node[];

  constructor(
    private nodeService: NodeService,
    private userFormulaService: UserFormulaService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private confirmationService: ConfirmationService,
  ) {}

  ngOnInit() {
    this.nodeService
      .getRootNodes()
      .pipe(take(1))
      .subscribe({
        next: (envs) => {
          const globalNode = {
            id: "*",
            name: "Global",
            sourceName: "*",
            type: "Environment",
            sourceType: "Environment",
          } as Node;

          this.environments = [globalNode].concat(envs);
          this.reloadFormulaPrefs();
        },
        error: (error) =>
          this.translateService
            .instant("user-prefs.unit-prefs.err-cannot-load-data-sources")
            .subscribe((msg) => this.notificationService.notifyError(msg, error)),
      });
  }

  private reloadFormulaPrefs() {
    this.formulaPreferences = [];
    this.loadSubcription = this.userFormulaService
      .getUserFormulaPreferences()
      .pipe()
      .subscribe(
        (userPrefs) => this.mapFormulaPrefs(userPrefs),
        (error) =>
          this.notificationService.notifyError(this.translateService.instant(this.MSG_ERR_PREFIX + "load" + this.MSG_POSTFIX), error),
      );
  }

  private mapFormulaPrefs(formulaPrefs: FormulaPreference[]) {
    if (formulaPrefs.length === 0) {
      this.formulaPreferences = new Array<FormulaPreference>();
    } else {
      this.formulaPreferences = this.userFormulaService.sortFormulaPreferences(formulaPrefs);
    }

    this.loadSubcription.unsubscribe();
  }

  public onNewFormulaPref(e: any): void {
    e.stopPropagation();
    this.formulaPrefAlreadyExists = undefined;
    this.confirmedNewFormulaPref();
  }

  private confirmedNewFormulaPref(): void {
    this.formulaPrefAlreadyExists = undefined;
    this.tmpFormulaPrefEditIdx = -1;
    this.tmpFormulaPrefEdit = this.getNewFormulaPreference();
    this.isShowDialogFormulaPrefEdit = true;
  }

  public hideDialogFormulaPref(): void {
    if (this.isShowDialogFormulaPrefEdit) {
      this.cancelDialogFormulaPref();
    }
  }

  public cancelDialogFormulaPref(): void {
    this.tmpFormulaPrefEditIdx = -1;
    this.tmpFormulaPrefEdit = undefined;
    this.formulaPrefAlreadyExists = undefined;
    this.isShowDialogFormulaPrefEdit = false;
  }

  private validateFormula(): void {
    if (!this.tmpFormulaPrefEdit.canValidateOnFly()) {
      this.tmpFormulaPrefEdit.mathJsParsableFormula = undefined;
    } else {
      this.tmpFormulaPrefEdit = this.userFormulaService.validateFormula(this.tmpFormulaPrefEdit);
    }
  }

  public confirmDialogFormulaPref(skipValidate = false): void {
    if (!skipValidate) {
      this.validateFormula();
    }
    if (this.tmpFormulaPrefEdit.isValid()) {
      if (this.tmpFormulaPrefEditIdx < 0) {
        this.formulaPreferences.push(this.tmpFormulaPrefEdit);
      } else {
        this.formulaPreferences = this.formulaPreferences.map((pref, i) => {
          return this.tmpFormulaPrefEditIdx === i ? this.tmpFormulaPrefEdit : pref;
        });
      }
      this.userFormulaService.sortFormulaPreferences(this.formulaPreferences);
      this.cancelDialogFormulaPref();
    }
  }

  private displNameCheckUnique(): void {
    let formulaPref = undefined;

    if (this.tmpFormulaPrefEdit !== undefined && this.tmpFormulaPrefEdit.displName.trim().length > 0) {
      formulaPref = this.formulaPreferences.find(
        (pref, i) => pref.displName === this.tmpFormulaPrefEdit.displName && i !== this.tmpFormulaPrefEditIdx,
      );
    }

    if (formulaPref !== undefined) {
      this.formulaPrefAlreadyExists = this.translateService.instant(this.MSG_PREFIX + ".err-already-exists" + this.MSG_POSTFIX, {
        displName: this.tmpFormulaPrefEdit.displName,
      });
    } else {
      this.formulaPrefAlreadyExists = undefined;
    }
  }

  public onKeyUpDialogFormulaPref(e: KeyboardEvent): void {
    e.stopPropagation();
    if (KEYS_IGNORE.includes(e.key)) {
      return;
    }
    if (this.tmpFormulaPrefEdit !== undefined && this.tmpFormulaPrefEdit.displName.trim().length > 0) {
      this.displNameCheckUnique();
      if (
        this.formulaPrefAlreadyExists === undefined &&
        this.tmpFormulaPrefEdit !== undefined &&
        this.tmpFormulaPrefEdit.displName.trim().length > 0 &&
        this.tmpFormulaPrefEdit.hasInput()
      ) {
        if (e.code === "Enter") {
          this.confirmDialogFormulaPref();
        } else {
          this.validateFormula();
        }
      }
    }
  }

  public onKeyUpDialogFormulaPrefInput(e: KeyboardEvent, isDown = false): void {
    e.stopPropagation();
    if (KEYS_IGNORE.includes(e.key)) {
      return;
    }
    if (
      this.tmpFormulaPrefEdit !== undefined &&
      this.tmpFormulaPrefEdit.displName.trim().length > 0 &&
      this.formulaPrefAlreadyExists === undefined
    ) {
      const canValidate = !KEYS_FORMULA_IGNORE.includes(e.key) && this.tmpFormulaPrefEdit.canValidateOnFly();
      if (!canValidate) {
        this.tmpFormulaPrefEdit.mathJsParsableFormula = undefined;
      }
      if ((isDown && e.code === "Tab") || canValidate) {
        this.tmpFormulaPrefEdit = this.userFormulaService.validateFormula(this.tmpFormulaPrefEdit);
      }
      if (this.tmpFormulaPrefEdit.isValid() && e.code === "Enter") {
        this.confirmDialogFormulaPref(true);
      }
    }
  }

  public saveDialogFormulaPrefs(): void {
    this.saveFormulaPrefs("save", () => this.cancelDialogFormulaPref());
    this.cancelDialogFormulaPref();
  }

  /**
   * @param msgKey
   * @param cancelCallback
   */
  private saveFormulaPrefs(msgKey: string, cancelCallback: Function) {
    let replacement: FormulaPreference[] = new Array<FormulaPreference>();
    if (this.tmpFormulaPrefDeleteIdx >= 0 || (this.tmpFormulaPrefEditIdx >= 0 && this.tmpFormulaPrefEdit)) {
      // remove (jump over) from displayed
      this.formulaPreferences.forEach((pref, idx) => {
        if (this.tmpFormulaPrefEditIdx === idx) {
          replacement.push(this.userFormulaService.getFormulaPreferenceCloneForSave(this.tmpFormulaPrefEdit));
        } else if (idx !== this.tmpFormulaPrefDeleteIdx) {
          replacement.push(this.userFormulaService.getFormulaPreferenceCloneForSave(pref));
        }
      });
    } else {
      replacement = this.formulaPreferences.map((pref) => this.userFormulaService.getFormulaPreferenceCloneForSave(pref));
    }

    this.userFormulaService.saveFormulaPreferences(replacement.filter((pref) => pref !== undefined)).subscribe(
      () => {
        cancelCallback.call(this);
        this.reloadFormulaPrefs();
      },
      (error) =>
        this.notificationService.notifyError(this.translateService.instant(this.MSG_ERR_PREFIX + msgKey + this.MSG_POSTFIX), error),
    );
  }

  /**
   *
   * @param msgKey
   * @param cancelCallback
   */
  private deleteAllFormulaPrefs(msgKey: string, cancelCallback: Function) {
    const replacement: FormulaPreference[] = new Array<FormulaPreference>();

    this.userFormulaService.saveFormulaPreferences(replacement).subscribe(
      () => {
        cancelCallback.call(this);
        this.reloadFormulaPrefs();
      },
      (error) =>
        this.notificationService.notifyError(this.translateService.instant(this.MSG_ERR_PREFIX + msgKey + this.MSG_POSTFIX), error),
    );
  }

  public onDeleteFormulaPrefs(e: any): void {
    e.stopPropagation();
    this.tmpFormulaPrefDeleteIdx = -1;
    this.isShowDialogFormulaPrefDelete = true;
  }

  public onEditFormulaPref(e: any, rowIdx: number) {
    e.stopPropagation();
    this.tmpFormulaPrefEditIdx = rowIdx;
    this.tmpFormulaPrefEdit = this.userFormulaService.getFormulaPreferenceCloneForEdit(this.formulaPreferences[rowIdx]);
    this.isShowDialogFormulaPrefEdit = true;
  }

  public onRemoveFormulaPref(e: any, rowIdx: number): void {
    e.stopPropagation();
    this.tmpFormulaPrefDeleteIdx = rowIdx;
    this.isShowDialogFormulaPrefDelete = true;
  }

  public cancelRemoveFormulaPref(): void {
    this.tmpFormulaPrefDeleteIdx = -1;
    this.isShowDialogFormulaPrefDelete = false;
  }

  public confirmedRemoveFormulaPref(): void {
    if (this.isShowDialogFormulaPrefDelete) {
      if (this.tmpFormulaPrefDeleteIdx === -1) {
        this.deleteAllFormulaPrefs("delete-all", () => this.cancelRemoveFormulaPref());
      } else {
        this.saveFormulaPrefs("delete", () => this.cancelRemoveFormulaPref());
      }
    }
    this.cancelRemoveFormulaPref();
  }

  private getNewFormulaPreference(): FormulaPreference {
    return new FormulaPreference("", "");
  }
}
