/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Injectable } from "@angular/core";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { forkJoin, Observable } from "rxjs";
import { concatMap, map, take } from "rxjs/operators";
import { AuthenticationService } from "../../core/authentication/authentication.service";

export class UnitFilter {
  physDim: string;
  meaQuant: string;
  unit: string;

  constructor(physDim: string, unit: string, meaQuant = "*") {
    this.physDim = physDim;
    this.meaQuant = meaQuant;
    this.unit = unit;
  }
}

export class UnitPreference {
  source: string;
  displName: string;
  unitFilters: UnitFilter[] = [];
  isDefault: boolean;

  constructor(displName: string, unitFilters = [], source = "*", isDefault = false) {
    this.source = source;
    this.displName = displName;
    this.unitFilters = unitFilters;
    this.isDefault = isDefault;
  }
}
/**
 * for grouping by Scope when displayed in the UI
 */
export class ScopedUnitPreference extends UnitPreference {
  scope: string;
  constructor(scope: string, pref: UnitPreference) {
    super(pref.displName, pref.unitFilters, pref.source, pref.isDefault);
    this.scope = scope;
  }
}

@Injectable()
export class UserUnitPreferenceService {
  public readonly UNIT_PREF_KEY = "user-prefs.unit-prefs";
  public readonly NEW_UNIT_PREF_NAME_KEY = this.UNIT_PREF_KEY + ".no-unit-pref-selected";
  public readonly EMPTY_UNIT_PREF: UnitPreference = new UnitPreference(this.NEW_UNIT_PREF_NAME_KEY);

  constructor(private preferenceService: PreferenceService, private authService: AuthenticationService) {}

  /**
   *
   * @returns observable list of UnitPreferences from Scope SYSTEM and Scope USER
   */
  getAllUnitPreferences(): Observable<ScopedUnitPreference[]> {
    return forkJoin([
      this.authService.getCurrentUserName().pipe(take(1)),
      this.preferenceService.getPreferenceForScope(Scope.SYSTEM, this.UNIT_PREF_KEY),
      this.preferenceService.getPreferenceForScope(Scope.USER, this.UNIT_PREF_KEY),
    ]).pipe(
      map(([username, res1, res2]) =>
        this.preferenceToScopedUnitPref(
          res1,
          res2.filter((pref) => pref.user === undefined || pref.user === "" || pref.user === username),
        ),
      ),
    );
  }

  getUserUnitPreferences(): Observable<UnitPreference[]> {
    return forkJoin([
      this.authService.getCurrentUserName().pipe(take(1)),
      this.preferenceService.getPreferenceForScope(Scope.USER, this.UNIT_PREF_KEY),
    ]).pipe(
      map(([username, preferences]) =>
        this.preferenceToUnitPref(preferences.filter((pref) => pref.user === undefined || pref.user === "" || pref.user === username)),
      ),
    );
  }

  saveUserUnitPreferences(userUnitPrefs: UnitPreference[]) {
    return this.authService.getCurrentUserName().pipe(
      take(1),
      concatMap((username) => this.preferenceService.savePreference(this.unitPreferenceToPreference(userUnitPrefs, username))),
    );
  }

  getUnitPreferenceClone(unitPref: UnitPreference): UnitPreference {
    if (unitPref === undefined) {
      return undefined;
    }
    return new UnitPreference(
      unitPref.displName,
      unitPref.unitFilters.map((uf) => new UnitFilter(uf.physDim, uf.unit, uf.meaQuant)),
      unitPref.source,
      unitPref.isDefault,
    );
  }

  getUnitPreferenceCloneForSave(unitPref: UnitPreference): UnitPreference {
    if (unitPref === undefined) {
      return undefined;
    }
    const ufClones: UnitFilter[] = unitPref.unitFilters
      .filter((uf) => uf.physDim !== undefined && uf.physDim.trim() !== "" && uf.unit !== undefined && uf.unit.trim() !== "")
      .map((uf) => new UnitFilter(uf.physDim, uf.unit, uf.meaQuant === undefined || uf.meaQuant.trim() === "" ? "*" : uf.meaQuant));
    return new UnitPreference(unitPref.displName, ufClones, unitPref.source, unitPref.isDefault);
  }

  public sortUnitPreferences<T extends UnitPreference>(unitPrefs: T[]): T[] {
    return unitPrefs.sort((up1, up2) => {
      let retVal = 0;
      if (up1 instanceof ScopedUnitPreference && up2 instanceof ScopedUnitPreference) {
        retVal = up2["scope"].localeCompare(up1["scope"]);
      }
      if (retVal === 0) {
        retVal = up1.displName.localeCompare(up2.displName);
      }
      if (retVal === 0) {
        retVal = up1.source.localeCompare(up2.source);
      }
      return retVal;
    });
  }

  private preferenceToScopedUnitPref(sysPrefs: Preference[], userPrefs: Preference[]): ScopedUnitPreference[] {
    const sysUnitPrefs = this.preferenceToUnitPref(sysPrefs).map((up) => new ScopedUnitPreference(Scope.SYSTEM, up));
    const userUnitPrefs = this.preferenceToUnitPref(userPrefs).map((up) => new ScopedUnitPreference(Scope.USER, up));
    return sysUnitPrefs.concat(userUnitPrefs);
  }

  private preferenceToUnitPref(prefs: Preference[]): UnitPreference[] {
    let result: UnitPreference[] = [];
    if (prefs !== undefined && prefs.length > 0) {
      prefs.forEach((p) => {
        result = result.concat(JSON.parse(p.value) as UnitPreference[]);
      });
      result.forEach((pref) => {
        if (pref.isDefault === undefined) {
          pref.isDefault = false;
        }
      });
    }
    return result;
  }

  private unitPreferenceToPreference(userUnitPrefs: UnitPreference[], username: string) {
    const pref = new Preference();
    userUnitPrefs.forEach((pref) => {
      if (pref.isDefault === undefined) {
        pref.isDefault = false;
      }
    });
    pref.value = JSON.stringify(userUnitPrefs);
    pref.key = this.UNIT_PREF_KEY;
    pref.scope = Scope.USER;
    pref.user = username;
    return pref;
  }
}
