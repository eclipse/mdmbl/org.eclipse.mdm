import { Injectable } from "@angular/core";
import { AuthenticationService } from "@core/authentication/authentication.service";
import { addErrorDescription } from "@core/core.functions";
import { Preference, PreferenceService, Scope } from "@core/preference.service";
import { NavigatorService } from "@navigator/navigator.service";
import { catchError, concatMap, forkJoin, map, Observable, take } from "rxjs";

export class UserSetting {
  preference: string;
  value: boolean | string | number;
}

@Injectable()
export class UserSettingsService {
  public readonly SETTINGS_PREF_KEY = "user-settings";

  constructor(
    private preferenceService: PreferenceService,
    private authService: AuthenticationService,
    private navigatorService: NavigatorService,
  ) {}

  public getUserSettingsPreferences(): Observable<UserSetting[]> {
    return forkJoin([
      this.authService.getCurrentUserName().pipe(take(1)),
      this.preferenceService.getPreferenceForScope(Scope.USER, this.SETTINGS_PREF_KEY),
    ]).pipe(
      map(([username, preferences]) =>
        this.preferenceToSettings(preferences.filter((pref) => pref.user === undefined || pref.user === "" || pref.user === username)),
      ),
    );
  }

  public saveSetting(setting: UserSetting): void {
    this.authService
      .getCurrentUserName()
      .pipe(
        take(1),
        concatMap((username) =>
          this.preferenceService
            .savePreference(this.settingToPreference(setting, username))
            .pipe(catchError((e) => addErrorDescription(e, "Could not save user setting!"))),
        ),
      )
      .subscribe(() => this.navigatorService.refreshNavigator.emit(true));
  }

  private preferenceToSettings(prefs: Preference[]): UserSetting[] {
    return prefs.map((p) => {
      const setting: UserSetting = {
        preference: p.key,
        value: this.convertValue(JSON.parse(p.value)),
      };

      return setting;
    });
  }

  private convertValue(value: string): boolean | string | number {
    if (value === "false" || value === "true") {
      return value === "true";
    } else if (this.isNumeric(value)) {
      return +value;
    }

    return JSON.parse(value);
  }

  private isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  private settingToPreference(setting: UserSetting, username: string) {
    const pref = new Preference();
    pref.value = JSON.stringify(setting.value);
    pref.key = setting.preference;
    pref.scope = Scope.USER;
    pref.user = username;
    return pref;
  }
}
