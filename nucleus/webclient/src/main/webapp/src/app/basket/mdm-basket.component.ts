/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { PreferenceConstants, PreferenceService, Scope } from "@core/preference.service";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { TranslateService } from "@ngx-translate/core";
import { saveAs } from "file-saver";
import { ConfirmationService, MenuItem } from "primeng/api";
import { FileUpload } from "primeng/fileupload";
import { forkJoin, Observable, throwError } from "rxjs";
import { flatMap, map } from "rxjs/operators";
import { ChartViewerService } from "../chartviewer/services/chart-viewer.service";
import { streamTranslate, TRANSLATE } from "../core/mdm-core.module";
import { MDMItem } from "../core/mdm-item";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { OverwriteDialogComponent } from "../core/overwrite-dialog.component";
import { NavigatorService } from "../navigator/navigator.service";
import { Node } from "../navigator/node";
import { NodeService } from "../navigator/node.service";
import { QueryService, Row, SearchResult } from "../tableview/query.service";
import { View } from "../tableview/tableview.service";
import { Basket, BasketService } from "./basket.service";

@Component({
  selector: "mdm-basket",
  templateUrl: "mdm-basket.component.html",
  styleUrls: ["./mdm-basket.component.css"],
})
export class MDMBasketComponent implements OnInit, OnDestroy, IStatefulComponent {
  @Output() activated = new EventEmitter<Node>();
  @Output() selected = new EventEmitter<Node>();
  @Input() activeNode: Node;

  exportableTypes = ["Project", "Pool", "Test", "TestStep", "Measurement", "ChannelGroup", "Channel"];

  isCaseSensitive = false;

  basketName = "";
  basketContent: SearchResult = new SearchResult();
  basket = "Warenkorb";
  basketOpen = false;

  baskets: Basket[] = [];
  selectedBasket: Basket;
  environments: Node[];

  public selectedRow: string;
  public lazySelectedRow: string;

  contextMenuItems: MenuItem[] = [
    { label: "Remove selection from basket", icon: "pi pi-times", command: (event) => this.removeSelected() },
    { label: "Add to chart viewer", icon: "fa fa-area-chart", command: (event) => this.addToChartViewer(event, true) },
  ];

  fixedLaunchers: MenuItem[] = [
    { label: "ATFX", command: (event) => this.saveBasketAsATFX(event.originalEvent, "atfx") },
    { label: "CSV", command: (event) => this.saveBasketAsATFX(event.originalEvent, "csv") },
    { label: "EXCEL", command: (event) => this.saveBasketAsATFX(event.originalEvent, "xlsx") },
  ];
  launchers: MenuItem[] = [];
  allowedExtensions: string[] = [];

  menuSelectedRow: Row;
  selectedRows: Row[];
  selectedView: View;
  public displayImportDialog = false;

  public selectedItems: Row[];
  public lgLoadModal = false;
  public lgSaveModal = false;
  public lgQuickPlotModal = false;

  @ViewChild(FileUpload)
  private fileUpload!: FileUpload;

  @ViewChild(OverwriteDialogComponent, { static: true })
  overwriteDialogComponent: OverwriteDialogComponent;

  constructor(
    private _basketService: BasketService,
    private queryService: QueryService,
    private navigatorService: NavigatorService,
    private sanitizer: DomSanitizer,
    private nodeService: NodeService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private confirmationService: ConfirmationService,
    private preferenceService: PreferenceService,
    private chartViewerService: ChartViewerService,
    private stateService: StateService,
  ) {
    this.stateService.register(this, "basket");
  }

  getState(): unknown {
    return {
      selectedBasket: this.selectedBasket,
      selectedItems: this._basketService.items,
    };
  }

  applyState(state: { selectedBasket: Basket; selectedItems: MDMItem[] }) {
    if (state["selectedBasket"]) {
      this.selectedBasket = state["selectedBasket"];
    } else if (state["selectedItems"]) {
      this._basketService.addAll(state["selectedItems"]);
    }
  }

  ngOnDestroy() {
    this.stateService.unregister(this);
  }

  removeSelected() {
    this._basketService.remove(Row.getItem(this.menuSelectedRow));
  }

  quickPlotMetadataTrends(e: Event) {
    this.selectedItems = [];
    if (this.selectedRows) {
      this.selectedItems = this.basketContent.rows.filter(
        (row: Row) => this.selectedRows.findIndex((selectedRow) => selectedRow.id === row.id) > -1,
      );
    }

    if (this.selectedItems.length > 0) {
      this.lgQuickPlotModal = true;
    }

    e.stopPropagation();
  }

  addToChartViewer(e: Event, fromContext: boolean) {
    let items = [];

    if (fromContext) {
      items = [Row.getItem(this.menuSelectedRow)];
    } else {
      e.stopPropagation();
      items = this.selectedRows.map((row) => Row.getItem(row));
    }

    this.chartViewerService.sendAppendMDMItem(items);
  }

  ngOnInit() {
    streamTranslate(this.translateService, TRANSLATE("basket.mdm-basket.remove-selection")).subscribe(
      (msg: string) => (this.contextMenuItems[0].label = msg),
    );
    streamTranslate(this.translateService, TRANSLATE("basket.mdm-basket.add-to-chart")).subscribe(
      (msg: string) => (this.contextMenuItems[1].label = msg),
    );

    this.preferenceService.getPreferenceForScope(Scope.SYSTEM, PreferenceConstants.SEARCH_CASE_SENSITIVE).subscribe((preferences) => {
      if (preferences.length > 0) {
        this.isCaseSensitive = "true" === preferences[0].value;
      }
    });

    this.nodeService.getRootNodes().subscribe(
      (envs) => (this.environments = envs),
      (error) =>
        this.translateService
          .getTranslation(TRANSLATE("basket.mdm-basket.err-cannot-load-sources"))
          .subscribe((msg) => this.notificationService.notifyError(msg, error)),
    );

    this._basketService
      .getFileExtensions()
      .pipe(
        map(
          (launchers) =>
            <{ menuItems: MenuItem[]; extensions: string[] }>{
              menuItems: launchers.map(
                (l: { label: string; extension: string }) =>
                  <MenuItem>{
                    label: l.label,
                    command: (event) => this.saveBasketWithExtension(event.originalEvent, l.extension),
                  },
              ),
              extensions: launchers.map((l: { extension: string }) => "." + l.extension).join(","),
            },
        ),
      )
      .subscribe(({ menuItems, extensions }) => {
        this.launchers = this.fixedLaunchers.concat(menuItems);
        this.allowedExtensions = ["application/xml"].concat(extensions);
      });

    this._basketService.itemsAdded$.subscribe(
      (items) => this.addItems(items),
      (error) =>
        this.translateService
          .getTranslation(TRANSLATE("basket.mdm-basket.err-cannot-add-selection"))
          .subscribe((msg) => this.notificationService.notifyError(msg, error)),
    );

    this._basketService.itemsRemoved$.subscribe(
      (items) => this.removeItems(items),
      (error) =>
        this.translateService
          .getTranslation(TRANSLATE("basket.mdm-basket.err-cannot-remove-selection"))
          .subscribe((msg) => this.notificationService.notifyError(msg, error)),
    );
  }

  onViewChanged(view: View) {
    this.selectedView = view;
    this.setItems(this._basketService.items);
  }

  onSelectedRowChanged(row: Row) {
    this.menuSelectedRow = row;
  }

  onSelectedRowsChanged(row: Row[]) {
    this.selectedRows = row;
  }

  onViewClick(e: Event) {
    e.stopPropagation();
  }

  showFileImportDialog(e: Event) {
    this.displayImportDialog = true;
  }

  setItems(items: MDMItem[]) {
    this.basketContent.rows = [];
    this.addItems(items);
  }

  addItems(items: MDMItem[]) {
    if (items.length > 0 && this.selectedView) {
      const hasVirtual = items.filter((item) => item.id === undefined).length > 0;
      this.queryService
        .queryItems(
          items,
          this.selectedView.columns.map((c) => c.type + "." + c.name),
          this.isCaseSensitive,
          true,
        )
        .forEach((q) =>
          q.subscribe(
            (r) => this.addData(r.rows, hasVirtual),
            (error) =>
              this.notificationService.notifyError(
                this.translateService.instant("basket.mdm-basket.err-cannot-add-items-to-shopping-basket"),
                error,
              ),
          ),
        );
    }
  }

  removeItems(items: MDMItem[]) {
    // copy and reasign to get new object refference triggering angular change detection in tableview.
    const tmp = Object.assign({}, this.basketContent);
    items.forEach(
      (item) => (tmp.rows = tmp.rows.filter((row) => !(row.source === item.source && row.type === item.type && row.id === item.id))),
    );
    this.basketContent = tmp;
  }

  setView(view: View) {
    console.log("setView", view);
  }

  saveBasket(e: Event) {
    if (e) {
      e.stopPropagation();
    }
    if (this.baskets.find((f) => f.name === this.basketName) != undefined) {
      this.lgSaveModal = false;
      this.overwriteDialogComponent
        .showOverwriteModal(this.translateService.instant("basket.mdm-basket.item-save-shopping-basket"))
        .subscribe(
          (needSave) => this.saveBasket2(needSave),
          (error) => {
            this.saveBasket2(false);
            this.notificationService.notifyError(this.translateService.instant("basket.mdm-basket.err-save-shopping-basket"), error);
          },
        );
    } else {
      this.saveBasket2(true);
    }
  }

  saveBasket2(save: boolean) {
    if (save) {
      this._basketService.saveBasketWithName(this.basketName);
      this.lgSaveModal = false;
    } else {
      this.lgSaveModal = true;
    }
  }

  loadBasket(basket?: Basket) {
    if (basket === undefined) {
      basket = this.selectedBasket;
      if (this.selectedBasket === undefined) {
        return;
      }
    }
    this.basketName = basket.name;
    this.setItems(basket.items);
    this._basketService.setItems(basket.items);
    this.basketOpen = true;
    this.lgLoadModal = false;
  }

  loadBaskets() {
    this._basketService.getBaskets().subscribe(
      (baskets) => (this.baskets = baskets),
      (error) =>
        this.notificationService.notifyError(this.translateService.instant("basket.mdm-basket.err-cannot-load-shopping-basket"), error),
    );
  }

  clearBasket(e: Event) {
    e.stopPropagation();
    this.basketContent = new SearchResult();
    this._basketService.removeAll();
    this.basketName = "";
  }

  showLoadModal(e: Event) {
    e.stopPropagation();
    this.selectedBasket = undefined;
    this.loadBaskets();
    this.lgLoadModal = true;
  }

  showSaveModal(e: Event) {
    e.stopPropagation();
    this.loadBaskets();
    this.basketName = this.selectedBasket ? this.selectedBasket.name : "";
    this.lgSaveModal = true;
  }

  stopEvent(event: Event) {
    event.stopPropagation();
  }

  saveBasketWithExtension(event: any, extension: string) {
    if (event) {
      event.stopPropagation();
    }
    const downloadContent = new Basket(this.basketName, this._basketService.getItems());

    this._basketService
      .getBasketAsXml(downloadContent)
      .pipe(map((xml) => new Blob([xml], { type: "application/xml" })))
      .subscribe((blob) => this.saveAsFile(blob, extension, "shoppingbasket"));
  }

  isExportable(item: MDMItem) {
    return this.exportableTypes.indexOf(item.type) >= 0;
  }

  saveBasketAsATFX(event: any, filetype: string) {
    if (event) {
      event.stopPropagation();
    }
    const notExportableItems = this._basketService.getItems().filter((i) => !this.isExportable(i));
    const exportableItems = this._basketService
      .getItems()
      .filter((i) => this.isExportable(i))
      .map((i) => i.type)
      .join(", ");

    if (exportableItems.length === 0) {
      // No exportable item in shopping basket -> show message and cancel export
      const msg =
        this.translateService.instant("basket.mdm-basket.msg-atfx-only-supports-types", { types: this.exportableTypes.join(", ") }) +
        " " +
        this.translateService.instant("basket.mdm-basket.msg-only-not-exportable-types-selected", {
          types: notExportableItems.map((i) => i.type).join(", "),
        });

      this.confirmationService.confirm({
        message: msg,
        acceptLabel: "OK",
        rejectVisible: false,
      });
    } else if (notExportableItems.length > 0) {
      // shopping basket contains not exportable items -> show message and ask if export should continue
      const msg =
        this.translateService.instant("basket.mdm-basket.msg-atfx-only-supports-types", { types: this.exportableTypes.join(", ") }) +
        " " +
        this.translateService.instant("basket.mdm-basket.msg-not-exportable-types-continue", { types: exportableItems });

      this.confirmationService.confirm({
        message: msg,
        acceptLabel: "Yes",
        rejectLabel: "No",
        icon: "pi pi-question-circle",
        accept: () =>
          this.exportItemsToAtfx(
            this._basketService.getItems().filter((i) => this.isExportable(i)),
            filetype,
          ),
        rejectVisible: true,
      });
    } else {
      this.exportItemsToAtfx(this._basketService.getItems(), filetype);
    }
  }

  exportItemsToAtfx(basket: MDMItem[], filetype: string) {
    this.notificationService.notifyInfo(
      this.translateService.instant("basket.mdm-basket.download-file"),
      this.translateService.instant("basket.mdm-basket.download-file-in-progress"),
    );

    const downloadContent = new Basket(this.basketName, basket);

    this._basketService
      .getBasketAsAtfx(downloadContent, filetype)
      .pipe(
        map((resp) => {
          const contentDisposition = resp.headers.get("content-disposition");
          const filename = contentDisposition.split(";")[1].split("filename")[1].split("=")[1].trim();
          return { filename: filename, data: resp.body };
        }),
      )
      .subscribe((r: { filename: string; data: Blob }) => saveAs(r.data, r.filename));
  }

  handle(error: any) {
    console.log("handle in subscribe", error);
    return throwError(error);
  }

  saveAsFile(blob: Blob, fileExtension: string, defaultFilename = "export") {
    let name = "export." + fileExtension;
    if (this.basketName && this.basketName.trim().length !== 0) {
      name = this.basketName + "." + fileExtension;
    }
    saveAs(blob, name);
  }

  toggleSelect(basket: Basket) {
    this.selectedBasket = this.selectedBasket === basket ? undefined : basket;
  }

  isDownloadDisabled() {
    return this.basketContent.rows.length <= 0;
  }

  getSaveBtnTitle() {
    return this.basketName
      ? TRANSLATE("basket.mdm-basket.tooltip-save-shopping-basket")
      : TRANSLATE("basket.mdm-basket.tooltip-no-name-set");
  }

  onUploadEvent(fileInput: any) {
    if (fileInput.files[0]) {
      const readFile = (blob: Blob): Observable<string> =>
        Observable.create((o) => {
          if (!(blob instanceof Blob)) {
            o.error(new Error("Parameter `blob` must be an instance of Blob."));
            return;
          }

          const fileReader = new FileReader();
          fileReader.onload = () => o.next(fileReader.result);
          fileReader.onloadend = () => o.complete();
          fileReader.onabort = (err) => o.error(err);
          fileReader.onerror = (err) => o.error(err);
          return fileReader.readAsText(blob);
        });

      readFile(fileInput.files[0])
        .pipe(flatMap((xml) => this.parseXmlShoppingBasket(xml)))
        .subscribe(
          (basket) => this.loadBasket(basket),
          (err) => this.notificationService.notifyWarn("Could not load shopping basket from file.", err),
        );
      this.fileUpload.clear();
    }
  }

  private parseXmlShoppingBasket(xml: string) {
    const oParser = new DOMParser();
    const oDOM = oParser.parseFromString(xml, "application/xml");
    // print the name of the root element or error message
    if (oDOM.documentElement.tagName !== "shoppingbasket") {
      if (oDOM.documentElement.innerText) {
        return throwError(() => new Error("Error while parsing shopping basket: " + oDOM.documentElement.innerText));
      } else {
        return throwError(() => new Error("Error while parsing shopping basket: XML is missing shoppingbasket root tag"));
      }
    }

    // load the name of the shopping basket
    let name = "";
    const nameTags = oDOM.documentElement.getElementsByTagName("name");
    if (nameTags.length > 0) {
      name = nameTags[0].textContent;
    }

    // load the business objectes defined by the resturi values in the xml file
    const items: Observable<Node[]>[] = [];
    const uris = oDOM.documentElement.getElementsByTagName("resturi");
    if (uris.length === 0) {
      return throwError(() => new Error("No resturis found!"));
    }
    for (const uri of <any>uris) {
      const url = uri.textContent;
      items.push(this.nodeService.getNodesByAbsoluteUrl(url));
    }

    // map business objects to MDMItems and load basket
    return forkJoin(items).pipe(
      map((n) => n.map((x) => new MDMItem(x[0].sourceName, x[0].type, x[0].id))),
      map((i) => new Basket(name, i)),
    );
  }

  private addData(rows: Row[], hasVirtual: boolean) {
    this.basketOpen = true;
    // late-synchronize the real nodes with the basket service if we are returning from a virtual node query
    if (hasVirtual) {
      rows.forEach((row) => this._basketService.addOnly(new MDMItem(row.source, row.type, row.id)));
    }

    this.basketContent.rows.push(...rows);
    // copy and reasign to get new object refference triggering angular change detection in tableview.
    this.basketContent = Object.assign({}, this.basketContent);
  }

  onBasketSelect(e: any) {
    if (this.lazySelectedRow !== e.data) {
      this.selectedRow = e.data;
      this.basketName = e.data.name;
    } else {
      this.selectedRow = undefined;
      this.basketName = "";
    }
    this.lazySelectedRow = this.selectedRow;
  }
}
