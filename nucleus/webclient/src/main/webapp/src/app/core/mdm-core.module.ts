/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { MdmTranslatePipe } from "@localization/mdm-translate.pipe";
import { MDMDataSourceTranslationPipe } from "@localization/mdmdatasourcetranslation.pipe";
import { MimeTypePipe } from "@localization/mime-type.pipe";
import { TranslateModule, TranslateService } from "@ngx-translate/core";
import { MessageService } from "primeng/api";
import { BadgeModule } from "primeng/badge";
import { ButtonModule } from "primeng/button";
import { CalendarModule } from "primeng/calendar";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { DropdownModule } from "primeng/dropdown";
import { MessageModule } from "primeng/message";
import { MessagesModule } from "primeng/messages";
import { MultiSelectModule } from "primeng/multiselect";
import { OverlayPanelModule } from "primeng/overlaypanel";
import { TableModule } from "primeng/table";
import { TabViewModule } from "primeng/tabview";
import { ToastModule } from "primeng/toast";
import { TreeModule } from "primeng/tree";
import { Observable } from "rxjs";
import { startWith, switchMap } from "rxjs/operators";
import { AttributeValuePipe } from "../navigator/attribute-value.pipe";
import { HasFunctionalRightPipe } from "./authentication/has-functional-right.pipe";
import { LoginDialogComponent } from "./authentication/login-dialog/login-dialog.component";
import { MDMNotificationComponent } from "./mdm-notification.component";
import { MdmPCalendarWrapperComponent } from "./mdm-p-calendar-wrapper/mdm-p-calendar-wrapper.component";
import { OnlyNumberDirective } from "./only-number-directive";
import { OverwriteDialogComponent } from "./overwrite-dialog.component";
import { DateTimeDiffPipe } from "./pipes/date-time-diff.pipe";
import { DateTimePipe } from "./pipes/date-time.pipe";
import { PreferenceService } from "./preference.service";
import { PropertyService } from "./property.service";
import { SharelinkComponent } from "./sharelink/sharelink.component";

// Marker function for ngx-translate-extract:
export function TRANSLATE(str: string) {
  return str;
}

export function streamTranslate(translateService: TranslateService, keys: string | Array<string>, params?: any): Observable<any> {
  return translateService.onLangChange.pipe(
    startWith({}),
    switchMap(() => (params ? translateService.get(keys, params) : translateService.get(keys))),
  );
}

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    CalendarModule,
    TreeModule,
    TableModule,
    ContextMenuModule,
    DropdownModule,
    MultiSelectModule,
    ToastModule,
    DialogModule,
    ButtonModule,
    OverlayPanelModule,
    MessagesModule,
    MessageModule,
    TranslateModule.forChild(),
    BadgeModule,
    TabViewModule,
  ],
  declarations: [
    MDMNotificationComponent,
    OverwriteDialogComponent,
    MDMDataSourceTranslationPipe,
    OnlyNumberDirective,
    MdmTranslatePipe,
    AttributeValuePipe,
    MimeTypePipe,
    DateTimePipe,
    DateTimeDiffPipe,
    MdmPCalendarWrapperComponent,
    SharelinkComponent,
    HasFunctionalRightPipe,
    MdmPCalendarWrapperComponent,
    SharelinkComponent,
    LoginDialogComponent,
  ],
  exports: [
    CommonModule,
    FormsModule,
    DropdownModule,
    MultiSelectModule,
    CalendarModule,
    TreeModule,
    TableModule,
    ContextMenuModule,
    ToastModule,
    OverlayPanelModule,
    MDMNotificationComponent,
    OverwriteDialogComponent,
    TranslateModule,
    MDMDataSourceTranslationPipe,
    AttributeValuePipe,
    OnlyNumberDirective,
    MdmTranslatePipe,
    MimeTypePipe,
    TabViewModule,
    DialogModule,
    ButtonModule,
    DateTimePipe,
    DateTimeDiffPipe,
    MdmPCalendarWrapperComponent,
    SharelinkComponent,
    HasFunctionalRightPipe,
    MdmPCalendarWrapperComponent,
    SharelinkComponent,
    LoginDialogComponent,
  ],
  providers: [PropertyService, PreferenceService, MessageService, MdmTranslatePipe],
})
export class MDMCoreModule {}
