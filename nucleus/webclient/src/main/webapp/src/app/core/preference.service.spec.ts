/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClientTestingModule, HttpTestingController, RequestMatch } from "@angular/common/http/testing";
import { async, inject, TestBed } from "@angular/core/testing";
import { Preference, PreferenceService, Scope } from "./preference.service";
import { PropertyService } from "./property.service";

describe("PreferenceService", () => {
  let httpTestingController: HttpTestingController;

  const reqGetMatch: RequestMatch = { method: "GET" };
  const reqPutMatch: RequestMatch = { method: "PUT" };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PropertyService, PreferenceService],
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe("check init", () => {
    it("create service", async(
      inject([PreferenceService], (prefService) => {
        expect(prefService).toBeTruthy();
      }),
    ));
  });

  describe("getPreference()", () => {
    it("should return preferences", async(
      inject([PreferenceService], (prefService) => {
        const mockResponse = {
          preferences: [
            {
              id: 2,
              key: "preference.prefix.",
              scope: Scope.SYSTEM,
              source: null,
              user: null,
              value: "Test",
            },
          ],
        };

        prefService.getPreference(Scope.SYSTEM, "preference.prefix.").subscribe((prefs) => {
          expect(prefs.length).toBe(1);
          expect(prefs[0].scope).toBe(Scope.SYSTEM);
          expect(prefs[0].value).toBe("Test");
        });

        const req = httpTestingController.expectOne(reqGetMatch);
        req.flush(mockResponse);
      }),
    ));

    it("should return empty array if no preferences were found", async(
      inject([PreferenceService], (prefService) => {
        const mockResponse = {
          preferences: [],
        };

        prefService.getPreference(Scope.SYSTEM, "preference.prefix.").subscribe((prefs) => {
          expect(prefs.length).toBe(0);
        });

        const req = httpTestingController.expectOne(reqGetMatch);
        req.flush(mockResponse);
      }),
    ));
  });

  describe("savePreference()", () => {
    it("should post preference", async(
      inject([PreferenceService], (prefService) => {
        const mockResponse = {
          preferences: [],
        };

        const newPref = new Preference();
        newPref.scope = Scope.SYSTEM;
        newPref.key = "prefix.";
        newPref.value = "testValue";

        prefService.savePreference(newPref).subscribe((prefs) => {
          expect(prefs).toBeDefined();
        });

        const req = httpTestingController.expectOne(reqPutMatch);
        req.flush(mockResponse);
      }),
    ));
  });
});
