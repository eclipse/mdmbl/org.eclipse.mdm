/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Pipe, PipeTransform } from "@angular/core";
import { AuthenticationService } from "./authentication.service";

@Pipe({
  name: "hasFunctionalRight",
})
export class HasFunctionalRightPipe implements PipeTransform {
  constructor(private authService: AuthenticationService) {}

  transform(key: string, contextData: { [key: string]: string }) {
    return this.authService.hasFunctionalRight(key, contextData);
  }
}
