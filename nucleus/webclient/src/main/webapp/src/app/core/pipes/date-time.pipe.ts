import { Pipe, PipeTransform } from "@angular/core";
import { DateTime, Zone } from "luxon";
import { TimezoneService } from "src/app/timezone/timezone-service";

export type DateTimePartsType = "dateTime" | "date" | "time";
export type DateTimeConfig = {
  format?: DateTimePartsType | Intl.DateTimeFormatOptions;
  forceAbsolute?: boolean;
  zone?: string | Zone;
};

@Pipe({
  name: "dateTime",
  pure: false,
})
export class DateTimePipe implements PipeTransform {
  constructor(private timezoneService: TimezoneService) {}

  transform(input: number | string | Date | DateTime, config?: DateTimeConfig): string {
    const format = config?.format ?? "dateTime";
    const forceAbsolute = config?.forceAbsolute ?? false;
    const zone = config?.zone;

    let dateTime: DateTime;
    if (input == undefined) {
      return "";
    } else if (input instanceof Date) {
      dateTime = DateTime.fromJSDate(input);
    } else if (input instanceof DateTime) {
      dateTime = input;
    }
    let response: string;
    if (forceAbsolute) {
      switch (format) {
        case "dateTime":
          response = this.timezoneService.formatAbsoluteDateTime(dateTime, zone);
          break;
        case "date":
          response = this.timezoneService.formatAbsoluteDate(dateTime, zone);
          break;
        case "time":
          response = this.timezoneService.formatAbsoluteTime(dateTime, zone);
          break;
        default:
          if (this.timezoneService.getIsoFormat()) {
            if ((format.year || format.month || format.day) && !(format.hour || format.minute || format.second)) {
              response = this.timezoneService.formatAbsoluteDate(dateTime, zone);
            } else if (!(format.year || format.month || format.day) && (format.hour || format.minute || format.second)) {
              response = this.timezoneService.formatAbsoluteTime(dateTime, zone);
            } else {
              response = this.timezoneService.formatAbsoluteDateTime(dateTime, zone);
            }
          } else {
            response = this.timezoneService.formatDateTime(dateTime, zone, format);
          }
          break;
      }
    } else {
      switch (format) {
        case "dateTime":
        case "date":
        case "time":
          /**
           * @TODO jst 24.08.2023: Add support for date and time if required
           */
          response = this.timezoneService.formatDateTime(dateTime, zone);
          break;
        default:
          response = this.timezoneService.formatDateTime(dateTime, zone, format);
          break;
      }
    }
    return response;
  }
}
