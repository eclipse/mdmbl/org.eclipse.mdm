/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { of } from "rxjs";
import { catchError, filter, map, take, tap } from "rxjs/operators";

export interface IStatefulComponent extends OnDestroy {
  getState(): unknown;
  applyState(state: unknown);
}

export class StateDTO {
  id?: number;
  name?: string;
  state: string;
}

@Injectable({
  providedIn: "root",
})
export class StateService {
  readonly prefix = "state.";
  readonly stateStorage: "session" | "local" = "session";

  private stateEndpoint: string;

  private prop: PropertyService;

  private cache = new Map<number, StateDTO>();

  constructor(private http: HttpClient, private _prop: PropertyService, private route: ActivatedRoute) {
    this.stateEndpoint = _prop.getUrl("mdm/states");
    this.prop = _prop;
  }

  //private registeredComps: IStatefulComponent[] = [];
  private registeredComps = new Map<IStatefulComponent, string>();

  public register(comp: IStatefulComponent, key: string) {
    this.registeredComps.set(comp, key);

    this.triggerRestore(key, comp);
  }

  public unregister(comp: IStatefulComponent) {
    this.triggerSave(comp);

    this.registeredComps.delete(comp);
  }

  public loadState(id: number) {
    if (this.cache.has(id)) {
      return of(this.cache.get(id));
    } else {
      //const params = new HttpParams().set("id", id);

      return this.http.get<StateDTO>(this.stateEndpoint + "/" + id).pipe(
        tap((state) => this.cache.set(id, state)),
        take(1),
        catchError((e) => addErrorDescription(e, "Could not load state with id + " + id + "!")),
      );
    }
  }

  public triggerSave(comp: IStatefulComponent) {
    const key = this.registeredComps.get(comp);
    this.saveLocalState(key, comp.getState());
  }

  persistCurrentState(filter = ".*") {
    const state = this.getState(filter);
    return this.persistState(<StateDTO>{ state: JSON.stringify(state) });
  }

  persistCustomState(state: unknown) {
    return this.persistState(<StateDTO>{ state: JSON.stringify(state) });
  }

  public persistState(stateDTO: StateDTO) {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };

    return this.http.post<StateDTO>(this.stateEndpoint, stateDTO, options).pipe(
      tap((state) => this.cache.set(state.id, state)),
      take(1),
      catchError((e) => addErrorDescription(e, "Could not save state!")),
    );
  }

  public getState(filter: string) {
    const regexp = new RegExp(filter);
    const state = {} as unknown;
    this.registeredComps.forEach((k, c) => (regexp.test(k) ? (state[k] = c.getState()) : ""));
    return state;
  }

  private triggerRestore(stateKey: string, comp: IStatefulComponent) {
    const paramState = this.route.snapshot.queryParamMap.get("state") as unknown as number;

    if (paramState) {
      this.getRemoteState(paramState, stateKey)
        .pipe(filter((s) => s !== undefined))
        .subscribe((s) => {
          console.log("Applying remote state: ", comp, s);
          comp.applyState(s);
        });
    } else {
      const state = this.getLocalState(stateKey);
      if (state) {
        console.log("Applying local state: ", comp, state);
        comp.applyState(state);
      }
    }
  }

  private getRemoteState(paramState: number, stateKey: string) {
    return this.loadState(paramState).pipe(
      map((s) => JSON.parse(s.state)),
      map((s) => (s[stateKey] ? s[stateKey] : undefined)),
    );
  }

  private saveLocalState(stateKey: string, state: unknown) {
    const storage = this.getStorage();
    storage.setItem(this.prefix + stateKey, JSON.stringify(state));
    //this.onStateSave.emit(state);
  }

  private clearLocalState(stateKey: string) {
    const storage = this.getStorage();
    if (stateKey) {
      storage.removeItem(this.prefix + stateKey);
    }
  }

  private getLocalState(stateKey: string) {
    const storage = this.getStorage();
    const stateString = storage.getItem(this.prefix + stateKey);
    const dateFormat = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/;
    const reviver = function (key, value) {
      if (typeof value === "string" && dateFormat.test(value)) {
        return new Date(value);
      }
      return value;
    };
    if (stateString) {
      const state = JSON.parse(stateString, reviver);
      return state;
      //this.onStateRestore.emit(state);
    }
  }

  private getStorage() {
    switch (this.stateStorage) {
      case "local":
        return window.localStorage;
      case "session":
        return window.sessionStorage;
      default:
        throw new Error(this.stateStorage + ' is not a valid value for the state storage, supported values are "local" and "session".');
    }
  }
}
