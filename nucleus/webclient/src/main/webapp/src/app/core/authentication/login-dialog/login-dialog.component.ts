/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { LoginDialogService } from "./login-dialog.service";

@Component({
  selector: "mdm-login-modal",
  templateUrl: "./login-dialog.component.html",
})
export class LoginDialogComponent implements OnInit {
  showDialog = false;
  showError = undefined;
  onlyInfo = false;

  username: string;
  password: string;

  constructor(private loginService: LoginDialogService) {}

  ngOnInit() {
    this.loginService.showDialogEvent.subscribe((show) => {
      this.showDialog = show;
      this.onlyInfo = false;
    });

    this.loginService.showOnlyInfoDialogEvent.subscribe((show) => {
      this.showDialog = false;
      this.onlyInfo = show;
      console.log("onlyInfo: " + this.onlyInfo);
    });
  }

  public onSubmit() {
    this.loginService.login(this.username, this.password).subscribe(
      (success) => this.success(success),
      (error) => this.failed(error),
    );
  }

  public onRefresh() {
    window.location.reload();
  }

  private success(success) {
    this.showDialog = false;
    this.showError = undefined;
    this.loginService.reloginSuccessful.next(true);
    this.password = "";
  }

  private failed(error) {
    this.showError = "Not successful";
    console.log(error);
    this.password = "";
  }
}
