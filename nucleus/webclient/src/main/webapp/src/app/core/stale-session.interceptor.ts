/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponseBase } from "@angular/common/http";
import { Injectable, isDevMode } from "@angular/core";
import { Observable, Subject, throwError } from "rxjs";
import { catchError, switchMap, tap } from "rxjs/operators";
import { LoginDialogService } from "./authentication/login-dialog/login-dialog.service";

@Injectable()
export class StaleSessionInterceptor implements HttpInterceptor {
  refreshLoginInProgress = false;
  loginRefreshedSource = new Subject();
  loginRefreshed$ = this.loginRefreshedSource.asObservable();

  infoShownSource = new Subject();
  infoShowed$ = this.infoShownSource.asObservable();

  constructor(private loginService: LoginDialogService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError((err: HttpErrorResponse) => this.handleError(err, req, next)));
  }

  handleError(err: HttpErrorResponse, request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Result is the Login-Page?
    if (this.isLoginPage(err)) {
      if (isDevMode()) {
        return this.refreshLogin(err).pipe(
          switchMap(() => next.handle(request)),
          catchError(() => throwError(() => this.clone(err, "Received login page instead of JSON data. Please reload page."))),
        );
      } else {
        console.log("Received login page instead of JSON data. Reloading page.", err);
        //window.location.reload();
        return this.showInfoDialog(err).pipe(
          switchMap(() => next.handle(request)),
          catchError(() => throwError(() => this.clone(err, "Received login page instead of JSON data. Please reload page."))),
        );
      }
    }

    if (err.status === 424) {
      if (isDevMode()) {
        return throwError(() => this.clone(err, "Received HTTP 424. Please relogin."));
      } else {
        console.log("Received HTTP 424. Loging out.", err);
        this.logout();
      }
    }

    return throwError(() => err);
  }

  private clone(err: HttpErrorResponse, errorMsg: string) {
    return new HttpErrorResponse({
      error: { error: errorMsg, text: err.error.text },
      headers: err.headers,
      status: err.status,
      statusText: err.statusText,
      url: err.url,
    });
  }

  private refreshLogin(err: HttpErrorResponse) {
    // only watch loginRefreshed$ if it was not login request
    if (this.refreshLoginInProgress && !this.isLoginRequest(err)) {
      return new Observable((observer) => {
        this.loginRefreshed$.subscribe(() => {
          observer.next();
          observer.complete();
        });
      });
    } else {
      this.refreshLoginInProgress = true;

      return this.loginService.showDialog().pipe(
        tap(() => {
          this.refreshLoginInProgress = false;
          this.loginRefreshedSource.next((v) => v);
        }),
      );
    }
  }

  private showInfoDialog(err: HttpErrorResponse) {
    return this.loginService.showOnlyInfoDialog().pipe();
  }

  private logout() {
    window.location.assign(window.origin + "/org.eclipse.mdm.nucleus/mdm/logout");
  }

  private isLoginRequest(err: HttpResponseBase) {
    return err.hasOwnProperty("url") && err.url.indexOf("j_security_check") > -1;
  }

  private isLoginPage(err: HttpErrorResponse) {
    if (err.error && err.error.error && err.error.error instanceof SyntaxError) {
      const errMsg = err.error.text;

      if (typeof errMsg === "string") {
        return errMsg.includes("Login</button>") || errMsg.includes("SAMLRequest");
      }
    }

    // Probably CORS error!
    if (err.status === 0 && err.statusText === "Unknown Error") return true;
    return false;
  }
}
