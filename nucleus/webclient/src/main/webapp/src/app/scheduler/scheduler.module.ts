/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

/**
 *
 * - Include in navigation links
 * - Include in routes via:
 *     { path: 'scheduler', loadChildren:  () => import('./scheduler/scheduler.module').then(m => m.SchedulerModule) },
 *
 */

import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DialogModule } from "primeng/dialog";
import { OverlayPanelModule } from "primeng/overlaypanel";
import { TableModule } from "primeng/table";
import { MDMCoreModule } from "../core/mdm-core.module";
import { SchedulerRoutingModule } from "./scheduler-routing.module";
import { SchedulerComponent } from "./scheduler.component";
import { SchedulerService } from "./scheduler.service";

@NgModule({
  imports: [SchedulerRoutingModule, MDMCoreModule, FormsModule, ReactiveFormsModule, TableModule, DialogModule, OverlayPanelModule],
  declarations: [SchedulerComponent],
  exports: [SchedulerComponent],
  providers: [SchedulerService],
})
export class SchedulerModule {}
