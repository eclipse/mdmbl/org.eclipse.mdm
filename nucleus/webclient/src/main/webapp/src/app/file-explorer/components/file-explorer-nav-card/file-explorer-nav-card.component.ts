/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { MDMItem } from "@core/mdm-item";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { NavigatorService } from "@navigator/navigator.service";
import { Attribute, Node } from "@navigator/node";
import { filter } from "rxjs/operators";
import { FilesAttachableService } from "../../services/files-attachable.service";

@Component({
  selector: "mdm-file-explorer-nav-card",
  templateUrl: "./file-explorer-nav-card.component.html",
})
export class FileExplorerNavCardComponent implements OnInit {
  private static readonly FILE_ATTACHABLE_TYPES = ["Test", "TestStep", "Measurement"];

  // holding node selected in navigator
  public selectedNode: Node;
  // holding node selected in navigator
  public filesAttachable = false;

  public linkAttr: Attribute;

  public status = "file-explorer.file-explorer-nav-card.status-no-node-selected";

  public constructor(private navigatorService: NavigatorService, private notificationService: MDMNotificationService) {}

  public ngOnInit() {
    // init data on selected node change via navigator
    this.navigatorService
      .onSelectionChanged()
      .pipe(filter((obj: Node | MDMItem): obj is Node => obj instanceof Node))
      .subscribe(
        (node) => this.init(node),
        (error) => this.notificationService.notifyError("Daten können nicht geladen werden.", error),
      );
  }

  // initialize component data
  private init(node: Node) {
    if (node != undefined) {
      this.selectedNode = node;
      this.status = "file-explorer.file-explorer-nav-card.status-node-is-no-files-attachable";
      this.filesAttachable = this.isFileAttachable(this.selectedNode);
      this.linkAttr = this.findLinkAttribute();
    }
  }

  // extract MDMLinkExt attribute from node
  private findLinkAttribute() {
    let linkAttr: Attribute;
    if (this.selectedNode != undefined && this.selectedNode.attributes != undefined && this.selectedNode.attributes.length > 0) {
      linkAttr = this.selectedNode.attributes.find((attr) => attr.name === FilesAttachableService.MDM_LINKS);
    }
    return linkAttr;
  }

  // returns true, if node.type is configured as file-attachable.
  private isFileAttachable(node: Node) {
    let isAttachable = false;
    if (node != undefined) {
      isAttachable = FileExplorerNavCardComponent.FILE_ATTACHABLE_TYPES.find((t) => t === node.type) != undefined;
    }
    return isAttachable;
  }
}
