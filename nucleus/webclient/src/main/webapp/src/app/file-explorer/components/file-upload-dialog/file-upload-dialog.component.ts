/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { ContextAttributeIdentifier } from "@details/model/details.model";
import { Attribute, ContextGroup, Node } from "@navigator/node";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { FileUploadRow, FormatSize } from "../../model/file-explorer.model";
import { FileReaderService } from "../../services/file-reader.service";
import { FilesAttachableService } from "../../services/files-attachable.service";
import { ContextFilesService } from "./../../services/context-files.service";

@Component({
  selector: "mdm-file-upload-dialog",
  templateUrl: "./file-upload-dialog.component.html",
  styleUrls: ["../file-explorer.css"],
})
export class FileUploadDialogComponent implements OnInit, AfterViewInit {
  @ViewChild("fileinput") input: ElementRef;

  // make enum accessible from html template
  public formatSize = FormatSize;

  // holding table selection for context menu
  public selectedFileMetasUpload: FileUploadRow[] = [];
  // holding selected node and context info incase files is uploaded to context attribute
  public node: Node;
  public contextDescribable?: Node;
  public attribute: Attribute;
  public contextGroup?: ContextGroup;
  public contextType?: string;

  constructor(
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private filesAttachableService: FilesAttachableService,
    private contextFilesService: ContextFilesService,
    private fileReaderService: FileReaderService,
    private notificationService: MDMNotificationService,
  ) {}

  ngOnInit() {
    this.node = this.config.data.node;
    this.contextDescribable = this.config.data.contextDescribable;
    this.attribute = this.config.data.attribute;
    this.contextGroup = this.config.data.contextGroup;
    this.contextType = this.config.data.contextType;
  }

  // opens file select menu for upload on initialization
  ngAfterViewInit(): void {
    if (this.input != undefined) {
      setTimeout(() => this.input.nativeElement.click());
    }
  }

  // helping function to clear file selection for upload overlay panel
  private resetFileUploadSelection() {
    this.selectedFileMetasUpload = [];
  }

  // listener to removeFiles from selection for upload
  public onRemoveFile(event: MouseEvent, row: FileUploadRow) {
    const index = this.selectedFileMetasUpload.findIndex((fm) => fm === row);
    if (index > -1) {
      this.selectedFileMetasUpload.splice(index, 1);
    }
  }

  // listener to clear file selection for upload
  public onClearSelectedFileMetasUpload(event: MouseEvent) {
    this.resetFileUploadSelection();
  }

  // listener to add files to upload selection
  public async onSelectFileForUpload(event: MouseEvent) {
    const target = event.target as HTMLInputElement;
    const files: FileList = target.files;
    for (let i = 0; i < files.length; i++) {
      const dataUrl = await this.fileReaderService.readFile(files[i]);
      if (this.selectedFileMetasUpload.findIndex((fm) => fm.file.name === files[i].name) === -1) {
        this.selectedFileMetasUpload.push(new FileUploadRow(files[i], "", dataUrl));
      } else {
        this.notificationService.notifyWarn(
          "File could not be added.",
          "The selection already contains a file with the name '" + files[i].name + "'.",
        );
      }
    }
  }

  // listener to upload selected files
  // uploads selected files, closes dialog, and returns Observable for server response
  public onUploadSelectedFiles(event: MouseEvent) {
    this.ref.close(this.uploadFiles(this.selectedFileMetasUpload));
  }

  // upload selected files
  public uploadFiles(fileUploadRows: FileUploadRow[]) {
    if (fileUploadRows != undefined && this.node != undefined) {
      if (
        this.contextDescribable != undefined &&
        this.contextType != undefined &&
        this.contextGroup != undefined &&
        this.attribute != undefined
      ) {
        return this.contextFilesService.uploadFiles(fileUploadRows, this.getIdent());
      } else {
        return this.filesAttachableService.uploadFiles(this.node, fileUploadRows);
      }
    }
  }

  private getIdent() {
    return new ContextAttributeIdentifier(this.contextDescribable, this.node, this.attribute, this.contextGroup, this.contextType);
  }
}
