/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { PropertyService } from "@core/property.service";
import { FileSize, MDMLinkExt, Node } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import { from } from "rxjs";
import { catchError, concatMap, tap } from "rxjs/operators";
import { FileUploadRow } from "../model/file-explorer.model";

@Injectable()
export class FilesAttachableService {
  public static readonly MDM_LINKS = "MDMLinks";

  private contextUrl: string;

  constructor(
    private http: HttpClient,
    private notificationService: MDMNotificationService,
    private _prop: PropertyService,
    private translateService: TranslateService,
  ) {
    this.contextUrl = _prop.getUrl("mdm/environments");
  }

  public getFilesEndpoint(node: Node) {
    const urlType = this.typeToUrl(node.sourceType);

    return this.contextUrl + "/" + node.sourceName + "/" + urlType + "/" + node.id + "/files";
  }

  // loads file for fileattachable from server, returns file as blob.
  public loadFile(identifier: string, node: Node) {
    const endpoint = this.getFilesEndpoint(node) + "/" + this.escapeUrlCharacters(identifier);

    return this.http
      .get<Blob>(endpoint, { responseType: "blob" as "json" })
      .pipe(catchError((e) => addErrorDescription(e, "Could load file!")));
  }

  // loads file size from server for file with given remote path
  public loadFileSize(identifier: string, node: Node) {
    const endpoint = this.getFilesEndpoint(node) + "/size/" + this.escapeUrlCharacters(identifier);

    return this.http.get<FileSize>(endpoint).pipe(catchError((e) => addErrorDescription(e, "Could load file size!")));
  }

  // loads file sizes from server for all files attached to given entity
  public loadFileSizes(node: Node) {
    const endpoint = this.getFilesEndpoint(node) + "/sizes";

    return this.http.get<FileSize[]>(endpoint).pipe(catchError((e) => addErrorDescription(e, "Could load file sizes!")));
  }

  // Uploads multiple files. Http requests are chained to avoid concurrency in server side update process.
  public uploadFiles(node: Node, files: FileUploadRow[]) {
    return from(files).pipe(concatMap((file) => this.uploadFile(node, file)));
  }

  // upload file
  public uploadFile(node: Node, fileData: FileUploadRow) {
    const fd = new FormData();
    fd.append("file", fileData.file, fileData.file.name);
    fd.append("lastModified", fileData.file.lastModified.toString());
    // fd.append('size', fileData.file.size.toString());
    fd.append("mimeType", fileData.file.type);
    fd.append("description", fileData.description);

    const endpoint = this.getFilesEndpoint(node);
    return this.http.post<MDMLinkExt>(endpoint, fd).pipe(
      tap((link) =>
        this.notificationService.notifySuccess(
          this.translateService.instant("file-explorer.files-attachable.msg-ttl-file-created"),
          this.translateService.instant("file-explorer.files-attachable.msg-detail-file-created", {
            filename: link.fileName === undefined ? "???" : link.fileName,
          }),
        ),
      ),
      catchError((e) => addErrorDescription(e, "Could upload file!")),
    );
  }

  // delete file from server
  public deleteFile(identifier: string, node: Node) {
    const endpoint = this.getFilesEndpoint(node) + "/" + this.escapeUrlCharacters(identifier);

    return this.http.delete<MDMLinkExt>(endpoint).pipe(
      tap((link) =>
        this.notificationService.notifySuccess(
          this.translateService.instant("file-explorer.files-attachable.msg-ttl-file-deleted"),
          this.translateService.instant("file-explorer.files-attachable.msg-detail-file-deleted", {
            filename: link.fileName === undefined ? "???" : link.fileName,
          }),
        ),
      ),
      catchError((e) => addErrorDescription(e, "Could delete file!")),
    );
  }

  // get file information from server
  public getMDMFileExts(node: Node) {
    const endpoint = this.getFilesEndpoint(node) + "/";

    return this.http.get<MDMLinkExt[]>(endpoint).pipe(catchError((e) => addErrorDescription(e, "Could load file information!")));
  }

  // helping function to map sourceType to proper url for endpoint.
  private typeToUrl(type: string) {
    if (type != undefined) {
      switch (type) {
        case "MeaResult":
          return "measurements";
        default:
          return type.toLowerCase() + "s";
      }
    }
  }

  // replaces / by %2F to not break url
  private escapeUrlCharacters(urlString: string) {
    return urlString.replace(/\//g, "%2F").replace(/\./g, "%2E");
  }
}
