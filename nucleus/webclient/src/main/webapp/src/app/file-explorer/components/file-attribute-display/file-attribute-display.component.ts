/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input } from "@angular/core";
import { Attribute, ContextGroup, Node } from "@navigator/node";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { FileExplorerDialogComponent } from "src/app/file-explorer/components/file-explorer-dialog/file-explorer-dialog.component";

@Component({
  selector: "mdm-file-attribute-display",
  templateUrl: "./file-attribute-display.component.html",
})
export class FileAttributeDisplayComponent {
  @Input() attribute: Attribute;
  @Input() contextDescribable: Node;
  @Input() contextComponent: Node;
  @Input() contextGroup?: ContextGroup;
  @Input() contextType?: string;
  @Input() readOnly: boolean;

  // public link: MDMLink;
  // public links: MDMLink[];

  public menuItems: MenuItem[];

  constructor(private dialogService: DialogService, private translateService: TranslateService) {}

  //ngOnInit() {
  // if (this.attribute != undefined && this.attribute.value != undefined) {
  //   if (this.attribute.dataType === 'FILE_LINK') {
  //     this.link = Object.assign(new MDMLink(), this.getValue<MDMLink>());
  //   }
  //   if (this.attribute.dataType === 'FILE_LINK_SEQUENCE' && this.getValues() != undefined) {
  //     this.links = (this.getValues<MDMLink[]>()).map(l => Object.assign(new MDMLink(), l));
  //   }
  // }
  //}

  // private getValue<T>() {
  //   if (this.attribute != undefined && this.attribute.value != undefined) {
  //     return <T> (this.contextGroup != undefined ? this.attribute.value[this.contextGroup] : this.attribute.value);
  //   }
  // }

  // private getValues<T>() {
  //   if (this.attribute != undefined && this.attribute.value != undefined) {
  //     const tmp = this.contextGroup != undefined ? this.attribute.value[this.contextGroup] : this.attribute.value;
  //     return tmp !== '' ? tmp : undefined;
  //   }
  // }

  public onShowFileExplorerDialog() {
    const ref = this.dialogService.open(FileExplorerDialogComponent, {
      data: {
        contextDescribable: this.contextDescribable,
        contextComponent: this.contextComponent,
        attribute: this.attribute,
        contextGroup: this.contextGroup,
        contextType: this.contextType,
        readOnly: this.readOnly,
      },
      header: this.translateService.instant("file-explorer.file-attribute-display.hdr-file-upload-wizard", {
        name: this.contextComponent.name,
      }),
      width: "70%",
    });

    // ref.onClose.subscribe(linkObs => this.handleUploadDialogResponse(linkObs));
  }

  // private handleUploadDialogResponse(linkObs: Observable<MDMLink>) {
  //   if (linkObs != undefined) {
  //     linkObs.subscribe(link => console.log(link));
  //   }
  // }
}
