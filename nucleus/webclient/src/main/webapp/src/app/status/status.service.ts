/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { EventEmitter, Injectable, Output } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { NodeService } from "@navigator/node.service";
import { EMPTY, Observable, of } from "rxjs";
import { catchError, flatMap, map, tap } from "rxjs/operators";
import { Node, NodeArray } from "../navigator/node";

@Injectable()
export class StatusService {
  private endpointEnv: string;
  @Output() statusUpdated: EventEmitter<void> = new EventEmitter<void>();

  constructor(private http: HttpClient, private _prop: PropertyService, private nodeService: NodeService) {
    this.endpointEnv = _prop.getUrl("mdm/environments/");
  }

  findAllStatus(environment: string): Observable<Node[]> {
    if (!environment || environment === "") {
      return EMPTY;
    }
    return this.http.get<NodeArray>(this.endpointEnv + environment + "/status").pipe(
      map((response) => response.data),
      catchError((e) => addErrorDescription(e, "Could not load data for status!")),
    );
  }

  findSystemParameter4Status(environment: string): Observable<Node[]> {
    if (!environment || environment === "") {
      return EMPTY;
    }
    return this.http
      .get<NodeArray>(this.endpointEnv + environment + '/systemparameters?filter=SystemParameter.Name lk "org.eclipse.mdm.status.test*"')
      .pipe(
        map((response) => response.data),
        catchError((e) => addErrorDescription(e, 'Could not load SystemParameters "org.eclipse.mdm.status.test*"')),
      );
  }

  updateStatus(environment: string, target: string, targetId: string, statusId: string): Observable<NodeArray> {
    if (!environment || environment === "") {
      return EMPTY;
    }
    const url = this.endpointEnv + environment + "/" + target.toLocaleLowerCase() + "s/" + targetId + "/status";
    const body = { Status: statusId };

    return this.http.put<NodeArray>(url, body).pipe(
      tap(() => this.statusUpdated.emit()),
      catchError((e) => addErrorDescription(e, "Could not update Status of '" + target + "' with ID=" + statusId + ".")),
    );
  }

  getClassification(env: string, id: string) {
    return this.nodeService.getNodesByUrl("/" + env + "/classifications/" + id);
  }

  getStatus(env: string, id: string) {
    return this.nodeService.getNodesByUrl("/" + env + "/status/" + id);
  }

  loadStatus(node: Node) {
    if (node?.type?.toLowerCase() === "test" || node?.type?.toLowerCase() === "teststep" || node?.type?.toLowerCase() === "measurement") {
      const statusAttr = node.attributes.find((attr) => attr.name === "Status");
      if (!statusAttr) {
        const relation = node.relations.find((rel) => rel.entityType === "Classification");
        if (relation && relation.ids && relation.ids.length > 0) {
          return this.getClassification(node.sourceName, relation.ids[0]).pipe(
            map((classfications) => classfications.shift()),
            map((classification) => classification.relations.filter((rel) => rel.entityType === "Status").shift().ids),
            map((statusIds) => statusIds.shift()),
            flatMap((statusId) => this.getStatus(node.sourceName, statusId)),
            map((statusNodes) => statusNodes.shift()),
            map((statusNode) => statusNode.attributes.filter((attr) => attr.name === "Name").shift()),
            map((attribute) => <string>attribute.value),
          );
        }
      }
    }
    return of("");
  }
}
