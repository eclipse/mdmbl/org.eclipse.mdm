/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { PropertyService } from "@core/property.service";
import { Node, NodeArray } from "@navigator/node";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";

@Injectable()
export class ExtSystemService {
  private prefEndpoint: string;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this.prefEndpoint = _prop.getUrl("mdm/administration/");
  }

  getExtSystemForScope(scope: string, key?: string): Observable<Node[]> {
    if (key === null || key === undefined) {
      key = "";
    }

    return this.http.get<NodeArray>(this.prefEndpoint + scope + "/externalsystems").pipe(
      map((response) => response.data),
      catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")),
    );
  }

  getExtSystemAttributesForScope(scope: string, id: string, key?: string): Observable<Node[]> {
    if (key === null || key === undefined) {
      key = "";
    }

    return this.http.get<NodeArray>(this.prefEndpoint + scope + "/externalsystems/attributes/" + id).pipe(
      map((response) => response.data),
      catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")),
    );
  }

  getExtSystem(key?: string) {
    if (key === null || key === undefined) {
      key = "";
    }
    return this.http.get<NodeArray>(this.prefEndpoint + "?key=" + key).pipe(
      map((response) => response.data),
      catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")),
    );
  }

  getAttributeValueFromNode(node: Node, attribute: string) {
    if (node.attributes !== undefined) {
      for (const i in node.attributes) {
        if (node.attributes[i].name === attribute) {
          return node.attributes[i].value;
        }
      }
    }
    return "";
  }

  saveExtSystemAttr(scope: string, extSystemAttr: Node, extSystem: Node) {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };

    const structure = {};
    structure["Name"] = extSystemAttr.name;

    if (parseInt(extSystemAttr.id, 10) > 0) {
      structure["Description"] = this.getAttributeValueFromNode(extSystemAttr, "Description");
      // update
      return this.http
        .put<any>(this.prefEndpoint + scope + "/externalsystems/attribute/" + extSystemAttr.id, JSON.stringify(structure), options)
        .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
    } else {
      structure["ExtSystemAttribute"] = extSystemAttr;
      // only provide the ID as the backend will evaluate the whole json string
      structure["ExtSystem"] = extSystem.id;
      // create
      return this.http
        .post<any>(this.prefEndpoint + scope + "/externalsystems/attribute", JSON.stringify(structure), options)
        .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
    }
  }

  saveExtSystemMDMAttr(scope: string, extSystemMDMAttr: Node, extSystemAttr: Node) {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };

    const structure = {};
    structure["Name"] = extSystemMDMAttr.name;

    if (parseInt(extSystemMDMAttr.id, 10) > 0) {
      structure["CompType"] = this.getAttributeValueFromNode(extSystemMDMAttr, "CompType");
      structure["CompName"] = this.getAttributeValueFromNode(extSystemMDMAttr, "CompName");
      structure["AttrName"] = this.getAttributeValueFromNode(extSystemMDMAttr, "AttrName");
      // update
      return this.http
        .put<any>(this.prefEndpoint + scope + "/externalsystems/mdmattribute/" + extSystemMDMAttr.id, JSON.stringify(structure), options)
        .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
    } else {
      structure["MDMAttribute"] = extSystemMDMAttr;
      structure["ExtSystemAttribute"] = extSystemAttr.id;
      // create
      return this.http
        .post<any>(this.prefEndpoint + scope + "/externalsystems/mdmattribute", JSON.stringify(structure), options)
        .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
    }
  }

  saveExtSystem(scope: string, extSystem: Node) {
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };

    const structure = {};
    structure["Name"] = extSystem.name;

    if (parseInt(extSystem.id, 10) > 0) {
      // update
      structure["Description"] = this.getAttributeValueFromNode(extSystem, "Description");
      return this.http
        .put<any>(this.prefEndpoint + scope + "/externalsystems/" + extSystem.id, JSON.stringify(structure), options)
        .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
    } else {
      structure["ExtSystem"] = extSystem;
      // create
      return this.http
        .post<any>(this.prefEndpoint + scope + "/externalsystems", JSON.stringify(structure), options)
        .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
    }
  }

  deleteExtSystem(scope: string, id: string) {
    return this.http
      .delete(this.prefEndpoint + scope + "/externalsystems/" + id)
      .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
  }

  deleteExtSystemAttr(scope: string, id: string) {
    return this.http
      .delete(this.prefEndpoint + scope + "/externalsystems/attribute/" + id)
      .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
  }

  deleteExtSystemMDMAttr(scope: string, id: string) {
    return this.http
      .delete(this.prefEndpoint + scope + "/externalsystems/mdmattribute/" + id)
      .pipe(catchError((e) => addErrorDescription(e, "Could not request ExternalSystems! ")));
  }
}
