/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Node } from "@navigator/node";
import { CatalogManagerService } from "../catalog-manager.service";
import { CatalogComponent } from "../models/catalog-component.model";
@Component({
  selector: "mdm-catalog-component",
  templateUrl: "./catalog-component.component.html",
  styleUrls: ["./catalog-component.component.css"],
})
export class CatalogComponentComponent implements OnInit {
  selectedContextType: string;
  catComps: CatalogComponent[];
  catalogSelected: CatalogComponent;
  selectedEnvironment: string;
  visible: boolean;
  newCatCompName: string;
  constructor(private catalogManagerService: CatalogManagerService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.selectedEnvironment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.selectedContextType = this.route.snapshot.paramMap.get("contextType");
    this.loadCatComps();
  }

  /**
   * load catalogs
   */
  loadCatComps() {
    this.catalogManagerService.loadCatalogComponents(this.selectedEnvironment, this.selectedContextType).subscribe((data) => {
      this.catComps = data;
    });
  }
  onRowEditInit(catalog: CatalogComponent) {
    this.catComps[catalog.id];
    this.catalogSelected = catalog;
  }
  /**
   * update the description of catalog
   */
  updateCatalogDescription(catData) {
    this.catalogManagerService
      .updateCatalogComponent(this.selectedEnvironment, this.selectedContextType, this.catalogSelected.id, {
        Description: catData.description,
      })
      .subscribe((response) => {
        this.loadCatComps();
      });
  }

  /**
   * select row in catalog table in order to see the attributes and get the id of the selected one
   */
  onCatalogRowSelect(event: any) {
    this.catalogSelected = event.data;
    this.router.navigate([this.catalogSelected.name, this.catalogSelected.id], { relativeTo: this.route });
  }

  onRowEditCancel(catalog: Node, index: number) {
    //
  }
  showcreateCatCompononetDialog() {
    this.visible = true;
  }
  createNewCatComponent() {
    this.catalogManagerService
      .createCatalogComponent(this.selectedEnvironment, this.selectedContextType, {
        Name: this.newCatCompName,
      })
      .subscribe((response) => {
        this.loadCatComps();
      });
    this.visible = false;
  }
}
