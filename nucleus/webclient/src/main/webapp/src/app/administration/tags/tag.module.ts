import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDMCoreModule } from "@core/mdm-core.module";
import { AutoCompleteModule } from "primeng/autocomplete";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { OrderListModule } from "primeng/orderlist";
import { TableModule } from "primeng/table";
import { MDMTagService } from "src/app/mdmtag/mdmtag.service";
import { TagRoutingModule } from "./tag-routing.module";
import { TagViewerComponent } from "./tag-viewer.component";
import { TagComponent } from "./tag.component";
@NgModule({
  imports: [
    MDMCoreModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ContextMenuModule,
    DialogModule,
    CheckboxModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    ContextMenuModule,
    TagRoutingModule,
    OrderListModule,
  ],
  declarations: [TagComponent, TagViewerComponent],
  exports: [],
  providers: [MDMTagService],
})
export class TagModule {}
