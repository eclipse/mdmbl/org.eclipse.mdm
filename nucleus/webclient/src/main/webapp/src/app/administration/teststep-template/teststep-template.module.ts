import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDMCoreModule } from "@core/mdm-core.module";
import { AutoCompleteModule } from "primeng/autocomplete";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { TableModule } from "primeng/table";
import { TeststepTemplateComponent } from "./teststep-template-component/teststep-template.component";
import { TeststepTemplateDetailsComponent } from "./teststep-template-details/teststep-template-details.component";
import { TestStepTemplateRoutingModule } from "./teststep-template-routing.module";
import { TeststepTemplateViewerComponent } from "./teststep-template-viewer/teststep-template-viewer.component";
import { TestStepTemplateManagerService } from "./teststep-template.service";

@NgModule({
  imports: [
    MDMCoreModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ContextMenuModule,
    DialogModule,
    CheckboxModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    ContextMenuModule,
    TestStepTemplateRoutingModule,
  ],
  declarations: [TeststepTemplateComponent, TeststepTemplateViewerComponent, TeststepTemplateDetailsComponent],
  exports: [],
  providers: [TestStepTemplateManagerService],
})
export class TestStepTemplateModule {}
