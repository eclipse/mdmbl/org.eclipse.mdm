import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { DateTime } from "luxon";
import { MenuItem } from "primeng/api";
import { TimezoneService } from "src/app/timezone/timezone-service";
import { TranslationService } from "../../translation/translation.service";
import { TestStepTemplate } from "../models/teststep-template.model";
import { TestStepTemplateManagerService } from "../teststep-template.service";
@Component({
  selector: "mdm-teststep-template",
  templateUrl: "./teststep-template.component.html",
  styleUrls: ["./teststep-template.component.css"],
})
export class TeststepTemplateComponent implements OnInit {
  selectedContextType: string;
  selectedEnvironment: string;
  testStepTpls: TestStepTemplate[];
  selectedTestStepTpl: TestStepTemplate;
  showCreateDialog = false;
  showDeleteDialog = false;
  newTestSteplTplName: string;
  items: MenuItem[];
  showArchivedTpls = false;
  editing = false;

  constructor(
    private teststepTplService: TestStepTemplateManagerService,
    private router: Router,
    private route: ActivatedRoute,
    private timezoneService: TimezoneService,
    private translateService: TranslateService,
    private translationService: TranslationService,
  ) {}

  ngOnInit() {
    this.selectedEnvironment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.getTestStepTpls();
  }

  getTestStepTpls() {
    this.teststepTplService.getTplTests(this.selectedEnvironment).subscribe((res) => {
      this.testStepTpls = res;
      this.testStepTpls.map((res) => (res.dateCreated = this.timezoneService.formatDateTime(DateTime.fromISO(res.dateCreated))));
    });
  }
  onRowEditInit(tpl) {
    this.testStepTpls[tpl.id];
    this.selectedTestStepTpl = tpl;
  }

  updateTeststepTpl(tpl) {
    this.teststepTplService
      .updateTestStepTpl(this.selectedEnvironment, this.selectedTestStepTpl.id, {
        Name: tpl.name,
        Description: tpl.description,
      })
      .subscribe((response) => {
        this.getTestStepTpls();
      });
  }

  onRowSelect(tpl) {
    this.selectedTestStepTpl = tpl.data;
    this.router.navigate([this.selectedTestStepTpl.name, this.selectedTestStepTpl.id], { relativeTo: this.route });
  }

  showCreateNewTeststepTpl() {
    this.showCreateDialog = true;
  }
  createNewTeststepTpl() {
    this.teststepTplService
      .CreateNewTestStepTpl(this.selectedEnvironment, {
        Name: this.newTestSteplTplName,
      })
      .subscribe((response: any) => {
        this.showCreateDialog = false;
        this.newTestSteplTplName = "";
        this.getTestStepTpls();
        this.router.navigate([response.data[0].name, response.data[0].id], { relativeTo: this.route });
      });
  }
  showDeleteTeststepTpl(tpl) {
    this.showDeleteDialog = true;
    this.selectedTestStepTpl = tpl;
  }
  deleteTestStepTpl() {
    this.teststepTplService.deleteTestStepTpl(this.selectedEnvironment, this.selectedTestStepTpl.id).subscribe((res) => {
      this.getTestStepTpls();
      this.showDeleteDialog = false;
    });
  }

  selectedTpl(selectedTestStepTpl) {
    this.items = [
      {
        label: this.translateService.instant("administration.testStepTpl.setValid"),
        icon: "pi pi-check-circle",
        command: () => this.setValid(this.selectedTestStepTpl),
        disabled: selectedTestStepTpl.validFlag !== "EDITABLE",
      },
      {
        label: this.translateService.instant("administration.testStepTpl.setArchived"),
        icon: "pi pi-download",
        command: () => this.setToArchived(this.selectedTestStepTpl),
        disabled: selectedTestStepTpl.validFlag !== "VALID",
      },
      {
        label: this.translateService.instant("administration.catalog.showTranslations"),
        icon: "pi pi-language",
        command: () => this.onShowTranslations(),
      },
      {
        label: this.translateService.instant("administration.testStepTpl.delete"),
        icon: "pi pi-trash",
        command: () => {
          this.showDeleteDialog = true;
        },
        disabled:
          selectedTestStepTpl.validFlag === "VALID" ||
          selectedTestStepTpl.validFlag === "ARCHIVED" ||
          selectedTestStepTpl.parent?.validFlag === "ARCHIVED" ||
          selectedTestStepTpl.parent?.validFlag === "VALID",
      },
    ];
  }
  showArchived(e) {
    if (e.checked === "true") {
      this.showArchivedTpls = true;
    } else {
      this.showArchivedTpls = false;
    }
  }
  setToArchived(tplData) {
    this.teststepTplService.updateTestStepTpl(this.selectedEnvironment, tplData.id, { ValidFlag: "ARCHIVED" }).subscribe((response) => {
      this.getTestStepTpls();
    });
  }
  setValid(tplData) {
    this.teststepTplService.updateTestStepTpl(this.selectedEnvironment, tplData.id, { ValidFlag: "VALID" }).subscribe((response) => {
      this.getTestStepTpls();
    });
  }

  showTranslations = false;
  translations: {
    languages: string[];
    nameTranslations: string[];
  };

  onShowTranslations() {
    this.showTranslations = true;
    this.loadTranslations();
  }

  loadTranslations() {
    this.translationService
      .loadTranslationsForTestStepTemplate(this.selectedEnvironment, this.testStepTpls[0].name)
      .subscribe((t) => (this.translations = t));
  }

  saveTranslations() {
    this.translationService
      .saveTranslationsForTestStepTemplate(this.selectedEnvironment, this.testStepTpls[0].name, this.translations.nameTranslations)
      .subscribe();
    this.showTranslations = false;
  }

  closeTranslationsDialog() {
    this.showTranslations = false;
  }
}
