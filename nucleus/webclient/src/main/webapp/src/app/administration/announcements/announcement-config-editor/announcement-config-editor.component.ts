import { Component, OnInit } from "@angular/core";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { AnnouncementConfig } from "../announcement-config.component";

@Component({
  selector: "mdm-announcement-config-editor",
  templateUrl: "./announcement-config-editor.component.html",
  styleUrls: ["./announcement-config-editor.component.css"],
})
export class AnnouncementConfigEditorComponent implements OnInit {
  public announcementConfig: AnnouncementConfig;
  public cols = [
    { field: "header", header: "administration.announcements.header" },
    { field: "body", header: "administration.announcements.body", type: "html" },
    { field: "validFrom", header: "administration.announcements.valid-from", type: "dateTime" },
  ];

  constructor(private dialogRef: DynamicDialogRef, private config: DynamicDialogConfig) {}

  ngOnInit(): void {
    this.announcementConfig = Object.assign({}, this.config.data?.announcementConfig);
  }

  /**
   * Listener for succsefully complete editor
   */
  public onApply() {
    this.dialogRef.close(this.announcementConfig);
  }
  /**
   * Listener for canceling editor
   */
  public onCancel() {
    this.dialogRef.close();
  }
}
