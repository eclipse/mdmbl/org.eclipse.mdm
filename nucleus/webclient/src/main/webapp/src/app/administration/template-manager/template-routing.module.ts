import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TemplateAttributesComponent } from "./template-attributes/template-attributes.component";
import { TemplateComponentComponent } from "./template-component/template-component.component";
import { TemplateManagerViewerComponent } from "./template-manager-viewer/template-manager-viewer.component";
import { TemplateManagerComponent } from "./template-manager.component";

const TemplateRoutes: Routes = [
  {
    path: "",
    component: TemplateManagerViewerComponent,

    children: [
      {
        path: "",
        component: TemplateManagerComponent,
        data: { breadcrumb: "Template" },
      },
      {
        path: ":contextType",
        component: TemplateComponentComponent,
        data: { breadcrumb: ":contextType" },
      },
      {
        path: ":contextType/:selectedTplName/:parentId/:selectedTplId",
        component: TemplateAttributesComponent,
        data: { breadcrumb: ":selectedTplName" },
      },
      {
        path: ":contextType/:selectedTplName/:selectedTplChildName/:parentId/:selectedTplId/:selectedTplChildId",
        component: TemplateAttributesComponent,
        data: { breadcrumb: ":selectedTplChildName" },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(TemplateRoutes)],
  exports: [RouterModule],
})
export class TemplateRoutingModule {}
