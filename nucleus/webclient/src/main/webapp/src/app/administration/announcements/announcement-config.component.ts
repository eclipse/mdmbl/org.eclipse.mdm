import { Component, OnInit } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { Preference, PreferenceConstants, PreferenceService, Scope } from "@core/preference.service";
import { TranslateService } from "@ngx-translate/core";
import { DateTime } from "luxon";
import { ConfirmationService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { concatMap, filter, finalize, map, take } from "rxjs";
import { AnnouncementConfigEditorComponent } from "./announcement-config-editor/announcement-config-editor.component";

export interface AnnouncementConfig {
  id: string;
  message: AnnouncementMessage;
}

export interface AnnouncementMessage {
  header: string;
  body?: SafeHtml;
  validFrom: DateTime;
}

@Component({
  selector: "mdm-announcement-config",
  templateUrl: "./announcement-config.component.html",
  styleUrls: ["./announcement-config.component.css"],
})
export class AnnouncementConfigComponent implements OnInit {
  public announcementConfigs: AnnouncementConfig[];
  public selectedAnnouncementConfig: AnnouncementConfig;
  public cols = [
    { field: "header", header: "administration.announcements.header" },
    { field: "body", header: "administration.announcements.body" },
    { field: "validFrom", header: "administration.announcements.valid-from", type: "dateTime" },
  ];
  public isLoading = false;

  constructor(
    private preferenceService: PreferenceService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private confirmationService: ConfirmationService,
    private sanitizer: DomSanitizer,
  ) {}

  ngOnInit(): void {
    this.reloadAnnouncementConfigs();
  }

  private reloadAnnouncementConfigs() {
    this.isLoading = true;
    this.preferenceService
      .getPreference(PreferenceConstants.ANNOUNCEMENT_MESSAGE)
      .pipe(
        map((data) => {
          data = data.filter((pref) => pref.key !== PreferenceConstants.ANNOUNCEMENT_MESSAGE_REMIND);
          const config: AnnouncementConfig[] = [];
          data.map((pref) => {
            const id = pref.key.substring(PreferenceConstants.ANNOUNCEMENT_MESSAGE.length + 1);
            const parsedValue = JSON.parse(pref.value);
            config.push({
              id: id,
              message: {
                header: parsedValue.header,
                body: parsedValue.body,
                validFrom: this.toDateTime(parsedValue.validFrom),
              },
            });
          });
          return config;
        }),
        finalize(() => (this.isLoading = false)),
      )
      .subscribe((announcementConfigs) => (this.announcementConfigs = announcementConfigs));
  }

  public onCreateOrEditAnnouncementConfig(announcementConfig: AnnouncementConfig) {
    if (announcementConfig === undefined) {
      announcementConfig = { id: crypto.randomUUID(), message: { validFrom: DateTime.now(), header: "", body: "" } };
    }
    this.dialogService
      .open(AnnouncementConfigEditorComponent, {
        header: this.translate.instant("administration.announcements.announcement-editor-hdr"),
        data: { announcementConfig },
        width: "44.44%",
      })
      .onClose.pipe(
        take(1),
        filter((msg) => !!msg),
        concatMap((msg) => this.createOrUpdateAnnouncementConfig(msg)),
      )
      .subscribe();
  }

  public onDeleteAnnouncementConfig(selectedAnnouncementConfig) {
    if (this.selectedAnnouncementConfig) {
      this.confirmationService.confirm({
        accept: () => {
          this.deleteAnnouncementConfig(selectedAnnouncementConfig).subscribe();
          this.selectedAnnouncementConfig = undefined;
        },
      });
    }
  }

  private createOrUpdateAnnouncementConfig(config: AnnouncementConfig) {
    const pref: Preference = new Preference();
    pref.scope = Scope.SYSTEM;
    pref.key = PreferenceConstants.ANNOUNCEMENT_MESSAGE + "." + config.id;
    pref.value = JSON.stringify(config.message);
    return this.preferenceService.savePreference(pref).pipe(finalize(() => this.reloadAnnouncementConfigs()));
  }

  private deleteAnnouncementConfig(config: AnnouncementConfig) {
    return this.preferenceService
      .deletePreferenceByScopeAndKey(Scope.SYSTEM, PreferenceConstants.ANNOUNCEMENT_MESSAGE + "." + config.id)
      .pipe(finalize(() => this.reloadAnnouncementConfigs()));
  }

  private toDateTime(dateTimeString: string) {
    return dateTimeString == null || dateTimeString === "" ? null : DateTime.fromISO(dateTimeString);
  }
}
