/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, Input, OnChanges, SimpleChanges, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationService } from "primeng/api";
import { finalize } from "rxjs";
import { MDMNotificationService } from "../../core/mdm-notification.service";
import { Preference, PreferenceService } from "../../core/preference.service";
import { EditPreferenceComponent } from "./edit-preference.component";

@Component({
  selector: "mdm-preference",
  templateUrl: "./preference.component.html",
  styleUrls: ["./preference.component.css"],
  providers: [ConfirmationService],
})
export class PreferenceComponent implements OnChanges {
  @Input() preferences: Preference[];
  @Input() scope: string;

  @ViewChild(EditPreferenceComponent)
  editPreferenceComponent: EditPreferenceComponent;

  constructor(
    private preferenceService: PreferenceService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private confirmationService: ConfirmationService,
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        switch (propName) {
          case "scope": {
            this.loadPreferences(this.scope);
          }
        }
      }
    }
  }

  loadPreferences(scope: string) {
    this.preferenceService.getPreferenceForScope(scope).subscribe((pref) => (this.preferences = pref));
  }

  editPreference(preference: Preference) {
    this.editPreferenceComponent.showDialog(preference);
  }

  newPreference() {
    this.editPreferenceComponent.showDialog(null);
  }

  removePreference(preference: Preference) {
    this.confirmationService.confirm({
      accept: () => {
        if (preference.id) {
          this.preferenceService
            .deletePreference(preference.id)
            .pipe(finalize(() => this.loadPreferences(this.scope)))
            .subscribe();
        }
      },
    });
  }

  reloadPreference(preference: Preference) {
    this.preferenceService.getPreferenceForScope(preference.scope, preference.key).subscribe({
      next: (p) => this.preferences.push(p[0]),
      error: (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("administration.preference.err-cannot-update-preference"),
          error,
        ),
    });
  }

  preferenceEqualsWithoutId(pref1: Preference, pref2: Preference) {
    return pref1.key === pref2.key && pref1.source === pref2.source && pref1.user === pref2.user;
  }
}
