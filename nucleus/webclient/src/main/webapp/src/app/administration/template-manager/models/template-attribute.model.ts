import { Relation } from "@navigator/node";
export class TemplateAttribute {
  id: string;
  name: string;
  defaultValue: string;
  valueReadonly: boolean;
  obligatory: boolean;
  mimeType: string;
  relations: Relation[];
}
