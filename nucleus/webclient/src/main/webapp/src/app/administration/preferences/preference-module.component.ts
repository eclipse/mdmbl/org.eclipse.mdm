/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { map, Subscription } from "rxjs";
import { streamTranslate, TRANSLATE } from "../../core/mdm-core.module";
import { Preference, PreferenceService } from "../../core/preference.service";

@Component({
  selector: "mdm-preference-module",
  templateUrl: "preference-module.component.html",
  styleUrls: ["./preference-module.component.css"],
})
export class PreferenceModuleComponent implements OnInit, OnDestroy {
  preferences: Preference[];
  scope: string;
  sub: any;
  scope$: Subscription;

  links = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private translateService: TranslateService,
    private preferenceService: PreferenceService,
  ) {
    streamTranslate(this.translateService, [
      TRANSLATE("administration.admin-modules.system"),
      TRANSLATE("administration.admin-modules.source"),
      TRANSLATE("administration.admin-modules.units"),
      TRANSLATE("administration.admin-modules.user"),
    ]).subscribe((msg: string) => {
      this.links = [
        { label: msg["administration.admin-modules.system"], routerLink: "../system" },
        { label: msg["administration.admin-modules.source"], routerLink: "../source" },
        { label: msg["administration.admin-modules.user"], routerLink: "../user" },
      ];
    });
  }

  ngOnInit() {
    this.scope$ = this.route.paramMap.pipe(map((params: ParamMap) => params.get("scope")!)).subscribe((scope) => (this.scope = scope));
  }

  ngOnDestroy() {
    this.scope$.unsubscribe();
  }

  onScopeChange(path: string) {
    this.scope = path.toUpperCase();
    if (this.scope !== undefined) {
      this.preferenceService.getPreferenceForScope(this.scope).subscribe((pref) => (this.preferences = pref));
    }
  }
}
