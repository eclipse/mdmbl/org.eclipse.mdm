import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CatalogAttributesComponent } from "./catalog-attributes/catalog-attributes.component";
import { CatalogComponentComponent } from "./catalog-component/catalog-component.component";
import { CatalogManagerViewerComponent } from "./catalog-manager-viewer/catalog-manager-viewer.component";
import { CatalogManagerComponent } from "./catalog-manager.component";
const catalogRoutes: Routes = [
  {
    path: "",
    component: CatalogManagerViewerComponent,
    children: [
      {
        path: "",
        component: CatalogManagerComponent,
        data: { breadcrumb: "Catalog" },
      },
      {
        path: ":contextType",
        component: CatalogComponentComponent,
        data: { breadcrumb: ":contextType" },
      },
      {
        path: ":contextType/:componentName/:id",
        component: CatalogAttributesComponent,
        data: { breadcrumb: ":componentName" },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(catalogRoutes)],
  exports: [RouterModule],
})
export class CatalogRoutingModule {}
