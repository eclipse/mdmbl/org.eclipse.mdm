import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { DateTime } from "luxon";
import { MenuItem } from "primeng/api";
import { TimezoneService } from "src/app/timezone/timezone-service";
import { TranslationService } from "../../translation/translation.service";
import { TestTemplate } from "../models/test-template.model";
import { TestTemplateManagerService } from "../test-template.service";
@Component({
  selector: "mdm-test-template",
  templateUrl: "./test-template.component.html",
  styleUrls: ["./test-template.component.css"],
})
export class TestTemplateComponent implements OnInit {
  selectedTestTpl: TestTemplate;
  testTemplates: TestTemplate[];
  selectedContextType: string;
  selectedEnvironment: string;
  showCreateDialog = false;
  showDeleteDialog = false;
  newTestTplName: string;
  items: MenuItem[];
  showArchivedTpls = false;
  editing = false;
  constructor(
    private testTplService: TestTemplateManagerService,
    private router: Router,
    private route: ActivatedRoute,
    private timezoneService: TimezoneService,
    private translateService: TranslateService,
    private translationService: TranslationService,
  ) {}

  ngOnInit() {
    this.selectedEnvironment = this.route.snapshot.paramMap.get("selectedEnvironment");
    this.getTestTpls();
  }
  getTestTpls() {
    this.testTplService.loadTplTests(this.selectedEnvironment).subscribe((res) => {
      this.testTemplates = res;
      this.testTemplates.map((res) => (res.dateCreated = this.timezoneService.formatDateTime(DateTime.fromISO(res.dateCreated))));
    });
  }
  onRowEditInit(tpl) {
    this.testTemplates[tpl.id];
    this.selectedTestTpl = tpl;
  }

  updateTestTpl(tpl) {
    this.testTplService
      .updateTestTpl(this.selectedEnvironment, this.selectedTestTpl.id, {
        Name: tpl.name,
        Description: tpl.description,
      })
      .subscribe((response) => {
        this.getTestTpls();
      });
  }

  onRowSelect(tpl) {
    this.selectedTestTpl = tpl.data;
    this.router.navigate([this.selectedTestTpl.name, this.selectedTestTpl.id], { relativeTo: this.route });
  }

  showCreateNewTestTpl() {
    this.showCreateDialog = true;
  }
  createNewTestTpl() {
    this.testTplService
      .CreateNewTestTpl(this.selectedEnvironment, {
        Name: this.newTestTplName,
      })
      .subscribe((response: any) => {
        this.showCreateDialog = false;
        this.newTestTplName = "";
        this.getTestTpls();
        this.router.navigate([response.data[0].name, response.data[0].id], { relativeTo: this.route });
      });
  }
  showDeleteTestTpl(tpl) {
    this.showDeleteDialog = true;
    this.selectedTestTpl = tpl;
  }
  deleteTestTpl() {
    this.testTplService.deleteTestTpl(this.selectedEnvironment, this.selectedTestTpl.id).subscribe((res) => {
      this.getTestTpls();
      this.showDeleteDialog = false;
    });
  }

  selectedTpl(selectedTestTpl) {
    this.items = [
      {
        label: this.translateService.instant("administration.testTpl.setValid"),
        icon: "pi pi-check-circle",
        command: () => this.setValid(this.selectedTestTpl),
        disabled: selectedTestTpl.validFlag !== "EDITABLE",
      },
      {
        label: this.translateService.instant("administration.testTpl.setArchived"),
        icon: "pi pi-download",
        command: () => this.setToArchived(this.selectedTestTpl),
        disabled: selectedTestTpl.validFlag !== "VALID",
      },
      {
        label: this.translateService.instant("administration.catalog.showTranslations"),
        icon: "pi pi-language",
        command: () => this.onShowTranslations(),
      },
      {
        label: this.translateService.instant("administration.testTpl.delete"),
        icon: "pi pi-trash",
        command: () => {
          this.showDeleteDialog = true;
        },
        disabled:
          selectedTestTpl.validFlag === "VALID" ||
          selectedTestTpl.validFlag === "ARCHIVED" ||
          selectedTestTpl.parent?.validFlag === "ARCHIVED" ||
          selectedTestTpl.parent?.validFlag === "VALID",
      },
    ];
  }
  showArchived(e) {
    if (e.checked === "true") {
      this.showArchivedTpls = true;
    } else {
      this.showArchivedTpls = false;
    }
  }
  setToArchived(tplData) {
    this.testTplService.updateTestTpl(this.selectedEnvironment, tplData.id, { ValidFlag: "ARCHIVED" }).subscribe((response) => {
      this.getTestTpls();
    });
  }
  setValid(tplData) {
    this.testTplService.updateTestTpl(this.selectedEnvironment, tplData.id, { ValidFlag: "VALID" }).subscribe((response) => {
      this.getTestTpls();
    });
  }

  showTranslations = false;
  translations: {
    languages: string[];
    nameTranslations: string[];
  };

  onShowTranslations() {
    this.showTranslations = true;
    this.loadTranslations();
  }

  loadTranslations() {
    this.translationService
      .loadTranslationsForTestStepTemplate(this.selectedEnvironment, this.selectedTestTpl.name)
      .subscribe((t) => (this.translations = t));
  }

  saveTranslations() {
    this.translationService
      .saveTranslationsForTestStepTemplate(this.selectedEnvironment, this.selectedTestTpl.name, this.translations.nameTranslations)
      .subscribe();
    this.showTranslations = false;
  }

  closeTranslationsDialog() {
    this.showTranslations = false;
  }
}
