import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MDMCoreModule } from "@core/mdm-core.module";
import { AutoCompleteModule } from "primeng/autocomplete";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { ButtonModule } from "primeng/button";
import { CheckboxModule } from "primeng/checkbox";
import { ContextMenuModule } from "primeng/contextmenu";
import { DialogModule } from "primeng/dialog";
import { OrderListModule } from "primeng/orderlist";
import { TableModule } from "primeng/table";
import { QuantityRoutingModule } from "./quantity-routing.module";
import { QuantityViewerComponent } from "./quantity-viewer.component";
import { QuantityComponent } from "./quantity.component";
import { QuantityService } from "./quantity.service";
@NgModule({
  imports: [
    MDMCoreModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ContextMenuModule,
    DialogModule,
    CheckboxModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    ContextMenuModule,
    QuantityRoutingModule,
    OrderListModule,
  ],
  declarations: [QuantityComponent, QuantityViewerComponent],
  exports: [],
  providers: [QuantityService],
})
export class QuantityModule {}
