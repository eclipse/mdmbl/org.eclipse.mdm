import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { TranslateService } from "@ngx-translate/core";
import { BehaviorSubject } from "rxjs";
import { ValueListValue } from "./value-list-value.interface";
import { ValueList } from "./value-list.interface";
import { ValueListService } from "./valuelist.service";

@Component({
  selector: "mdm-value-list-viewer",
  templateUrl: "./value-list-viewer.component.html",
  styleUrls: ["./value-list-viewer.component.css"],
})
export class ValueListViewerComponent implements OnInit {
  @Input() valueLists: ValueList[];
  @Input() selectedEnvironment: string;

  @Output() editMode = new EventEmitter<boolean>();
  @Output() selectedVL = new EventEmitter<string>();

  tmpValueListCreate: ValueList;
  tmpValueListDelete: ValueList;
  dialogValueListCreate = false;
  dialogValueListDelete = false;
  loadingValueListValues = false;

  selectedValueList: ValueList;
  valueListValues: ValueListValue[];
  bsValueListValues: BehaviorSubject<ValueListValue[]> = new BehaviorSubject<ValueListValue[]>(undefined);

  constructor(
    private valueListService: ValueListService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.bsValueListValues.subscribe((value) => {
      this.valueListValues = value;
    });

    this.bsValueListValues.next(undefined);
  }

  addValueList() {
    this.tmpValueListCreate = {};
    this.dialogValueListCreate = true;
  }

  removeValueList(valueList: ValueList) {
    this.tmpValueListDelete = valueList;
    this.dialogValueListDelete = true;
  }

  editValueList(valueList: ValueList) {
    if (valueList != undefined) {
      this.selectedVL.next(valueList.id);
      this.editMode.next(true);
    }
  }

  cancelRemoveValueList() {
    this.tmpValueListDelete = undefined;
    this.dialogValueListDelete = false;
  }

  confirmRemoveValueList() {
    if (this.tmpValueListDelete != undefined) {
      if (this.tmpValueListDelete.id !== undefined && parseInt(this.tmpValueListDelete.id, 10) > 0) {
        this.valueListService.deleteValueList(this.selectedEnvironment, this.tmpValueListDelete.id).subscribe(
          () => {
            const idxES: number = this.valueLists.indexOf(this.tmpValueListDelete);
            if (idxES !== -1) {
              this.valueLists.splice(idxES, 1);
              this.tmpValueListDelete = undefined;
            }
          },
          (error) =>
            this.notificationService.notifyError(
              this.translateService.instant("administration.value-list.err-cannot-delete-value-list"),
              error,
            ),
        );
      }
    }

    this.dialogValueListDelete = false;
  }

  saveDialogValueList() {
    // update the name attribute
    this.valueListService.saveValueList(this.selectedEnvironment, this.tmpValueListCreate).subscribe(
      (response) => this.patchResponse(response),
      (error) =>
        this.notificationService.notifyError(this.translateService.instant("administration.value-list.err-cannot-save-value-list"), error),
    );
    this.dialogValueListCreate = false;
  }

  cancelDialogValueList() {
    this.dialogValueListCreate = false;
    this.tmpValueListCreate = undefined;
  }

  patchResponse(node: ValueList) {
    if (node.name === this.tmpValueListCreate.name) {
      if (this.tmpValueListCreate.id === undefined) {
        this.valueLists.push(this.tmpValueListCreate);
      }
      this.tmpValueListCreate.id = node.id;
    }
    this.tmpValueListCreate = undefined;
  }

  onValueListRowSelect() {
    this.bsValueListValues.next(undefined);
    this.loadingValueListValues = true;
    this.valueListService.readValueListValues(this.selectedEnvironment, this.selectedValueList.id).subscribe((attrs) => {
      this.bsValueListValues.next(attrs);
      this.loadingValueListValues = false;
    });
  }

  onValueListRowUnselect() {
    this.bsValueListValues.next(undefined);
  }
}
