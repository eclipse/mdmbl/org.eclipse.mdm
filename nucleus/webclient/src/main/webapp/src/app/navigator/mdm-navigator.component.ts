/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { streamTranslate, TRANSLATE } from "@core/mdm-core.module";
import { MDMIdentifier } from "@core/mdm-identifier";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { PreferenceService } from "@core/preference.service";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { TranslateService } from "@ngx-translate/core";
import { MenuItem, TreeNode } from "primeng/api";
import { Tree } from "primeng/tree";
import { combineLatest, forkJoin, Observable, of, Subscription } from "rxjs";
import { delay, filter, finalize, map, mergeMap, startWith, take, tap } from "rxjs/operators";
import { BasketService } from "../basket/basket.service";
import { ChartViewerService } from "../chartviewer/services/chart-viewer.service";
import { MDMItem } from "../core/mdm-item";
import { CreateEntityMenuItemService } from "../entity-creator/menuitem.service";
import { NodeService } from "../navigator/node.service";
import { StatusService } from "../status/status.service";
import { UserSettingsService } from "../user-prefs/services/user-settings.service";
import { MdmNavigatorContextMenuService } from "./mdm-navigator-context-menu.service";
import { MdmTagContextMenuService } from "./mdmtag-context-menu.service";
import { NavigatorService } from "./navigator.service";
import { Node } from "./node";
import { NodeProviderUIWrapper } from "./nodeprovider-ui-wrapper";
import { MDMTreeNode, NodeproviderService } from "./nodeprovider.service";
import { StatusContextMenuService } from "./status-context-menu.service";

class ExpandedNode {
  [label: string]: ExpandedNode;
}

@Component({
  selector: "mdm-navigator",
  templateUrl: "./mdm-navigator.component.html",
  styleUrls: ["./mdm-navigator.component.css"],
  encapsulation: ViewEncapsulation.None,
  providers: [UserSettingsService],
})
export class MDMNavigatorComponent implements OnInit, OnDestroy, IStatefulComponent {
  AlrtItemTypeNotSupported = "This node type is not supported by the current view!";
  selectedNodes: TreeNode<MDMTreeNode>[] = [];
  nodes: TreeNode<MDMTreeNode>[] = [];
  lastClickTime = 0;

  nodeServiceSubscription: Subscription;

  loadingNode = <TreeNode>{
    label: "Loading subordinate items...",
    leaf: true,
    icon: "fa fa-spinner fa-pulse fa-fw",
  };

  // import dialog
  public importTargetSource: string;
  public displayImportDialog = false;

  contextMenuItems: Observable<MenuItem[]> = of([]);

  @ViewChild("navTree") navigatorTree: Tree;

  nodeProviders: NodeProviderUIWrapper[];
  nodeprovider: NodeProviderUIWrapper;
  nodeproviderIdToSelect: string;

  private state: {
    selectedNodeProviderId: string;
    states: { [nodeProviderId: string]: { expandedNodes?: ExpandedNode; selectedNodes?: MDMIdentifier[] } };
  };

  showNavigatorIcons = true;
  navigatorChunkSize = 0;

  constructor(
    private nodeService: NodeService,
    private basketService: BasketService,
    private nodeproviderService: NodeproviderService,
    private navigatorService: NavigatorService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private createEntityMenuitemService: CreateEntityMenuItemService,
    private chartViewerService: ChartViewerService,
    private stateService: StateService,
    private nodeProviderService: NodeproviderService,
    private menuService: MdmNavigatorContextMenuService,
    private preferenceService: PreferenceService,
    private statusServiceMenuService: StatusContextMenuService, // Needed to register context menu items
    private tagServiceMenuService: MdmTagContextMenuService, // Needed to register context menu items
    private statusService: StatusService,
  ) {
    this.stateService.register(this, "navigator");

    this.nodeProviderService
      .getNodeproviders()
      .pipe(take(1))
      .subscribe((nps) => {
        this.nodeProviders = nps;
        if (!this.nodeproviderIdToSelect) {
          this.nodeproviderIdToSelect = this.state?.selectedNodeProviderId;
        }

        of(this.nodeProviders.find((np) => np.id === this.nodeproviderIdToSelect))
          .pipe(delay(10))
          .subscribe((x) => {
            if (x) {
              this.nodeprovider = x;
            } else {
              this.nodeprovider = nps[0];
            }
            console.log("Selected Nodeprovider", this.nodeProviders, this.nodeprovider);
            this.afterNodeproviderInit();
          });
      });
    this.registerMenuItems();
  }

  afterNodeproviderInit() {
    this.reloadTree();
    this.onDatasourceChanged();
    this.subscribeToDatasourceChanged();

    this.navigatorService.openInTree.subscribe(
      (items) => this.openInTree(items),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("navigator.mdm-navigator.err-cannot-open-node-in-navigation-tree"),
          error,
        ),
    );
  }

  activateNodeProvider() {
    this.reloadTree();
    this.restoreLastNavigatorState()
      .pipe(tap(() => this.stateService.triggerSave(this)))
      .subscribe();
  }

  getState(): unknown {
    if (!this.state) {
      this.state = {
        selectedNodeProviderId: this.nodeprovider.id,
        states: {
          [this.nodeprovider.id]: { expandedNodes: {}, selectedNodes: [] },
        },
      };
    }

    this.state.selectedNodeProviderId = this.nodeprovider.id;
    this.state.states[this.nodeprovider.id] = {
      expandedNodes: this.getOpenNodes(this.nodes),
      selectedNodes: this.getSelectedNodesForStorage(),
    };
    return this.state;
  }

  applyState(state: unknown) {
    this.state = state as {
      selectedNodeProviderId: string;
      states: { [nodeProviderId: string]: { expandedNodes?: ExpandedNode; selectedNodes?: MDMIdentifier[] } };
    };
  }

  ngOnInit() {
    this.navigatorService.refreshNavigator.subscribe(() => this.reloadTreeAndState());

    this.getNavigatorUserSettings();

    streamTranslate(this.translateService, TRANSLATE("navigator.mdm-navigator.loading-subordinate-items")).subscribe(
      (msg: string) => (this.loadingNode.label = msg),
    );

    streamTranslate(this.translateService, TRANSLATE("navigator.mdm-navigator.msg-item-type-not-supported")).subscribe(
      (msg: string) => (this.AlrtItemTypeNotSupported = msg),
    );

    // handle new node provider from URL
    this.getNodeProviderFromURL().subscribe((newNodeProvider) => {
      this.nodeproviderIdToSelect = newNodeProvider;
    });

    this.nodeService.parentNodeRequestEvent.subscribe((requestNode) =>
      this.nodeService.parentNodeResponseEvent.emit(this.getParentNodes(requestNode)),
    );

    this.statusService.statusUpdated.subscribe(() => {
      this.selectionChanged(this.selectedNodes);
    });
  }

  ngOnDestroy() {
    if (this.nodeServiceSubscription) {
      this.nodeServiceSubscription.unsubscribe;
    }
    this.stateService.unregister(this);
  }

  getNavigatorUserSettings() {
    this.preferenceService
      .getPreference("user-settings.show-navigator-icons")
      .pipe(map((prefs) => prefs.map((p) => p.value)))
      .subscribe((pref) => {
        if (pref.length > 0) this.showNavigatorIcons = pref.some((p) => p === "true");
      });

    this.preferenceService
      .getPreference("user-settings.navigator-chunk-size")
      .pipe(map((prefs) => prefs.map((p) => p.value)))
      .subscribe((pref) => {
        if (pref.length > 0) this.navigatorChunkSize = +JSON.parse(pref[0]);
      });
  }

  onDatasourceChanged() {
    this.loadNavigateParameter();
  }

  subscribeToDatasourceChanged() {
    this.nodeServiceSubscription = this.nodeService.datasourcesChanged.subscribe(
      () => this.onDatasourceChanged(),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("navigator.mdm-navigator.err-cannot-update-navigation-tree"),
          error,
        ),
    );
  }

  loadNavigateParameter() {
    this.loadRootNodes(this.nodeprovider.id).subscribe({
      next: (treeNodes) => {
        this.nodes = treeNodes;
        this.restoreLastNavigatorState().subscribe();
        this.loadNavigateItems().subscribe((selectItems) => {
          const currentDatasources = this.nodeService.getActiveDatasources();
          if (!currentDatasources.includes(selectItems[0].source)) {
            currentDatasources.push(selectItems[0].source);
            this.nodeService.setActiveDatasource(currentDatasources);
          }
          this.navigatorService.fireOnOpenInTree(selectItems);
        });
      },
      error: (e) =>
        this.notificationService.notifyError(this.translateService.instant("navigator.mdm-navigator.err-cannot-update-navigation-tree"), e),
    });

    // refresh parent component in navigation tree after create any child entity
    this.createEntityMenuitemService.needRefresh.subscribe((needsRefresh) => {
      if (needsRefresh) {
        this.refreshAfterCreateNewChildEntity();
      }
    });
  }

  getParentNodes(node: Node) {
    const arr = [];
    arr.push(node);
    const parents = this.returnMatchingParent(node, this.nodes);
    // add only non-virtual nodes
    parents.filter((tmp) => tmp.id !== undefined).forEach((tmp2) => arr.push(tmp2));
    return arr;
  }

  returnMatchingParent(nodeToSearch: Node, treeNodesToSearch: TreeNode[]) {
    const foundNode = [];
    if (treeNodesToSearch) {
      let found = false;
      for (const tmp of treeNodesToSearch) {
        if (tmp.data.id === nodeToSearch.id && tmp.data.type === nodeToSearch.type) {
          foundNode.push(tmp.parent.data);
          found = true;
          break;
        }
      }
      if (!found) {
        // search children
        for (const tmp of treeNodesToSearch) {
          const tmpNode = this.returnMatchingParent(nodeToSearch, tmp.children);
          if (tmpNode && tmpNode.length > 0) {
            // push the children
            tmpNode.forEach((tmp2) => foundNode.push(tmp2));
            // push the parent, if it is not the same as the current node
            if (!(tmp.data.type === tmpNode[0].type && tmp.data.id === tmpNode[0].id)) {
              foundNode.push(tmp.data);
            }
            break;
          }
        }
      }
    }
    return foundNode;
  }

  getNodeProviderFromURL() {
    return combineLatest([this.route.paramMap, this.route.queryParamMap]).pipe(
      map((params) => (params[0].get("nodeprovider") === null ? params[1].get("nodeprovider") : params[0].get("nodeprovider"))),
      filter((nodeproviderId) => nodeproviderId !== null),
    );
  }

  restoreLastNavigatorState() {
    if (this.state && this.state.states[this.nodeprovider.id]) {
      return this.loadRootNodes(this.nodeprovider.id).pipe(
        tap((rootNodes) => (this.nodes = rootNodes)),
        mergeMap((rootNodes) =>
          this.restoreTreeNodes(
            rootNodes,
            this.state.states[this.nodeprovider.id].expandedNodes,
            this.state.states[this.nodeprovider.id].selectedNodes,
          ),
        ),
        finalize(() => {
          this.selectionChanged(this.selectedNodes);
          this.navigatorService.fireSelectedItem(this.toMDMItem(this.selectedNodes[this.selectedNodes.length - 1]));
          this.navigatorService.updateSelectedNodes(this.selectedNodes.map((tn) => this.toMDMItem(tn)));
        }),
      );
    } else {
      return of({});
    }
  }

  restoreTreeNodes(treeNodes: TreeNode[], labelsToExpand: ExpandedNode, nodesToSelect: MDMIdentifier[]): Observable<TreeNode[]> {
    this.selectedNodes.push(...this.findMatchingNodes(treeNodes, nodesToSelect));

    const childObservables: Observable<TreeNode[]>[] = [];
    for (const label of Object.keys(labelsToExpand)) {
      const nodeToExpand = this.findTreeNodeByLabel(treeNodes, label);
      if (nodeToExpand) {
        childObservables.push(this.loadNodeItemAndExpand(nodeToExpand, labelsToExpand[label], nodesToSelect));
      } else {
        for (const node of treeNodes) {
          if (node.children && node.children.length > 0) {
            this.restoreTreeNodes(node.children, labelsToExpand, nodesToSelect)
              .pipe(
                tap((children) => {
                  if (children.length > 0 && children !== node.children) node.expanded = true;
                }),
              )
              .subscribe();
          }
        }
        childObservables.push(this.restoreTreeNodes(treeNodes, labelsToExpand[label], nodesToSelect));
      }
    }

    if (childObservables.length > 0) {
      return forkJoin(childObservables).pipe(map((nestedNodes) => nestedNodes.flat()));
    } else {
      return of(treeNodes);
    }
  }

  loadNodeItemAndExpand(parentNode: TreeNode, labelsToExpand: ExpandedNode, nodesToSelect: MDMIdentifier[]): Observable<TreeNode[]> {
    if (parentNode) {
      if (parentNode.data) {
        return this.getChildren(parentNode.data).pipe(
          mergeMap((childNodes) => {
            parentNode.children = childNodes;
            parentNode.expanded = true;

            return this.restoreTreeNodes(parentNode.children, labelsToExpand, nodesToSelect);
          }),
          tap({
            error: (error) =>
              this.notificationService.notifyError(this.translateService.instant("navigator.mdm-navigator.err-cannot-load-nodes"), error),
          }),
        );
      } else {
        parentNode.expanded = true;
        return this.restoreTreeNodes(parentNode.children, labelsToExpand, nodesToSelect);
      }
    } else {
      return of([] as TreeNode[]);
    }
  }

  /**
   * Selectes the node that match on of the selectedItems. @see matchesMDMIdentifier
   * @param nodes
   * @param selectedItems
   */
  private findMatchingNodes(nodes: TreeNode<MDMTreeNode>[], selectedItems: MDMIdentifier[]) {
    const found: TreeNode<MDMTreeNode>[] = [];
    for (const treeNode of nodes) {
      if (this.matchesMDMIdentifier(treeNode, selectedItems)) {
        found.push(treeNode);
      }
    }
    return found;
  }

  /**
   *
   * @param treeNode
   * @param selectedItems
   * @returns true if treeNode matches any of the given selectedItems based on source, type and id or serial
   */
  private matchesMDMIdentifier(treeNode: TreeNode, selectedItems: MDMIdentifier[]) {
    return selectedItems?.some(
      (n) =>
        treeNode.data &&
        treeNode.data.source === n.source &&
        treeNode.data.type === n.type &&
        (treeNode.data.id === n.id || treeNode.data.serial === n.id),
    );
  }

  findTreeNodeByLabel(nodes: TreeNode[], nodeLabel: string) {
    for (const node of nodes) {
      if (node.label === nodeLabel) return node;
    }
    return null;
  }

  loadNavigateItems() {
    return combineLatest([this.route.paramMap, this.route.queryParamMap]).pipe(
      map((params) => (params[0].get("navigate") === null ? params[1].get("navigate") : params[0].get("navigate"))),
      filter((url) => url !== null),
      mergeMap((url) => this.nodeService.getNodesByUrl(url)),
      map((nodes) =>
        nodes.map(
          (node) =>
            new MDMItem(node.source ? node.source : node.sourceName, node.type, node.id, node.serial, node.filter, node["idAttribute"]),
        ),
      ),
    );
  }

  toMDMItem(node: TreeNode<MDMTreeNode> | MDMTreeNode) {
    if ("source" in node) {
      const i = <MDMTreeNode>node;
      if (i.id === null || i.id === "NO_ID") {
        return null;
      } else {
        return new MDMItem(i.source, i.type, i.id, i.serial, i.filter, i.idAttribute);
      }
    } else {
      const i = <MDMTreeNode>node.data;
      if (!i) return null;
      if (i.id === null || i.id === "NO_ID") {
        return null;
      } else {
        return new MDMItem(i.source, i.type, i.id, i.serial, i.filter, i.idAttribute);
      }
    }
  }

  /**
   * Copies the URL of the currently selected node to the clipboard.
   * If multiple nodes are selected only the first node is used.
   * The URL looks as follows: https://HOSTNAME:PORT/CONTEXT_ROOT/CURRENT_ROUTE?navigate=/{SOURCE}/{TYPE}/{ID}.
   * The navigate parameter is handled by MdmNavigatorComponent#loadNavigateItems().
   */
  copyLink(event: { item: MenuItem }) {
    const selectedNodes = (event.item.state["selectedNodes"] || []) as MDMTreeNode[];
    selectedNodes
      .map((node) => {
        return {
          source: node.source,
          type: node.type,
          id: node.id,
          serial: node.serial,
          filter: node.filter,
          idAttribute: node.idAttribute,
        } as MDMItem;
      })
      .forEach((nodeData) => {
        this.internalCopyLink(this.nodeService.nodeToUrl(nodeData, this.nodeprovider.id), this.nodeprovider.id);
      });
  }

  internalCopyLink(urls, nodeProvider: string) {
    if (!urls || urls.length === 0) {
      console.debug("No node selected. Cannot copy link to clipboard.");
      return;
    }

    const urlTree = this.router.parseUrl(this.router.url);
    urlTree.queryParams["navigate"] = urls;
    urlTree.queryParams["nodeprovider"] = nodeProvider;
    const navigateUrl = document.baseURI.replace(/\/$/, "") + urlTree.toString();

    const createCopy = (e: ClipboardEvent) => {
      e.clipboardData.setData("text/plain", navigateUrl);
      console.debug("Copied link to clipboard", navigateUrl);
      e.preventDefault();
    };

    document.addEventListener("copy", createCopy);
    document.execCommand("copy");
    document.removeEventListener("copy", createCopy);
  }

  importFile() {
    if (this.selectedNodes.length == 1) {
      const env = this.toMDMItem(this.selectedNodes[0]);
      this.importTargetSource = env.source;
      this.displayImportDialog = true;
    }
  }

  onNodeSelect(event) {
    if (event.node.data) {
      this.navigatorService.fireSelectedItem(this.toMDMItem(<TreeNode>event.node));
      this.navigatorService.updateSelectedNodes(this.selectedNodes.map((tn) => this.toMDMItem(<TreeNode>tn)));
    } else {
      this.navigatorService.fireSelectedItem(this.toMDMItem(<TreeNode>event.node));
    }

    if (event.originalEvent.timeStamp - this.lastClickTime < 300) {
      if (!event.node.expanded && !event.node.children) {
        this.onNodeExpand(event);
      } else {
        this.onNodeCollapse(event);
      }
      event.node.expanded = !event.node.expanded;
    }
    this.lastClickTime = event.originalEvent.timeStamp;
  }

  onNodeUnselect() {
    this.navigatorService.fireSelectedItem(undefined);
  }

  onNodeExpand(event: { node: TreeNode<MDMTreeNode> }) {
    if (event.node) {
      this.loadChildren(event.node as TreeNode<MDMTreeNode>);
      this.stateService.triggerSave(this);
    }
  }

  selectionChanged(event: TreeNode<MDMTreeNode>[]) {
    let data: MDMTreeNode = null;
    if (event !== null && event.length > 0) {
      data = event[event.length - 1].data;
    }

    this.contextMenuItems = this.menuService.buildContextMenu(
      data,
      this.selectedNodes.map((tn) => tn.data),
    ).pipe(
      // Make sure loading state is emitted immediately, in case of a slow service response
      startWith([{
        label: this.translateService.instant("navigator.mdm-navigator.loading"),
        disabled: true
      }])
    );

    this.stateService.triggerSave(this);
  }

  onNodeCollapse(event) {
    if (event.node) {
      this.unloadChildren(event.node as TreeNode);
      this.stateService.triggerSave(this);
    }
  }

  loadChildren(parentNode: TreeNode, loadAlsoLeaf = false) {
    if (parentNode && parentNode.children === undefined && (loadAlsoLeaf || !parentNode.leaf) && parentNode.data) {
      return this.getChildren(parentNode.data as MDMTreeNode)
        .pipe(
          startWith([this.loadingNode]),
          tap((nodes) => (nodes && nodes.length === 0 ? (parentNode.leaf = true) : (parentNode.leaf = false))),
        )
        .subscribe(
          (nodes) => (parentNode.children = nodes),
          (error) =>
            this.notificationService.notifyError(this.translateService.instant("navigator.mdm-navigator.err-cannot-load-nodes"), error),
        );
    }
  }

  unloadChildren(parentNode: TreeNode) {
    if (parentNode) {
      this.expandOrCollapseRecursive(parentNode, false);
    }
  }

  private expandOrCollapseRecursive(node: TreeNode, isExpand: boolean): void {
    node.expanded = isExpand;
    if (node.data) {
      node.children = undefined;
    }
    if (node.children) {
      node.children.forEach((childNode: TreeNode) => {
        this.expandOrCollapseRecursive(childNode, isExpand);
      });
    }
  }

  reloadTree() {
    if (this.nodeprovider) {
      this.loadRootNodes(this.nodeprovider.id).subscribe(
        (n) => (this.nodes = n),
        (error) =>
          this.notificationService.notifyError(
            this.translateService.instant("navigator.mdm-navigator.err-cannot-update-navigation-tree"),
            error,
          ),
      );
    }
  }

  reloadTreeAndState() {
    this.getNavigatorUserSettings();

    if (this.nodeprovider) {
      this.loadRootNodes(this.nodeprovider.id).subscribe(
        (n) => (this.nodes = n),
        (error) =>
          this.notificationService.notifyError(
            this.translateService.instant("navigator.mdm-navigator.err-cannot-update-navigation-tree"),
            error,
          ),
      );
    }

    this.restoreLastNavigatorState();
  }

  onFocus(event: { eventName: string; node: TreeNode }) {
    this.navigatorService.fireSelectedItem(event.node.data.item);
  }

  getNodeClass(item: MDMTreeNode) {
    return "icon " + item.type.toLowerCase();
  }

  loadRootNodes(nodeproviderId: string) {
    return this.nodeproviderService.getRoots(nodeproviderId).pipe(map((treeNodes) => treeNodes.map((treeNode) => this.mapNode(treeNode))));
  }

  getChildren(node: MDMTreeNode) {
    return this.nodeproviderService.getChildren(this.nodeprovider.id, node).pipe(
      map((nodes) => nodes.map((n) => this.mapNode(n))),
      map((treenodes) => treenodes.sort((n1, n2) => n1.label.localeCompare(n2.label))),
      map((nodes) => {
        const chunkSize = this.navigatorChunkSize;
        if (chunkSize > 0 && nodes.length > chunkSize) {
          const groups = nodes.map((_, index) => index % chunkSize === 0 && nodes.slice(index, index + chunkSize)).filter((node) => node);
          let currentChunk = 1;
          return groups.map((group) => {
            const virtualNode = <TreeNode>{
              label: `[${currentChunk}...${currentChunk + group.length}]`,
              leaf: node.leaf === true, // this.nodeproviderService.getSubNodeprovider(item) === undefined,
              children: group,
            };
            currentChunk += group.length;
            return virtualNode;
          });
        }
        return nodes;
      }),
    );
  }

  mapNode(node: MDMTreeNode) {
    const treeNode = <TreeNode>{
      label: node.label,
      leaf: node.leaf === true,
      data: node,
    };

    if (this.showNavigatorIcons) {
      treeNode.icon = this.getNodeClass(node);
    }

    // let item = new MDMItem(node.source, node.type, node.id, node.serial);
    return treeNode;
  }

  /**
   * Add all nodes of the type below the virtual node to the basket
   */
  addVirtualSelectionToBasket(dataType: "Test" | "TestStep" | "Measurement") {
    this.basketService.addAll(
      this.selectedNodes.map((node) => new MDMItem(node.data.source, dataType, node.data.id, node.data.serial, node.data.filter)),
    );
  }

  addSelectionToBasket(event: { item: MenuItem }) {
    const selectedNodes = (event.item.state["selectedNodes"] || []) as MDMTreeNode[];

    this.basketService.addAll(selectedNodes.map((node) => new MDMItem(node.source, node.type, node.id, node.serial, node.filter)));
  }

  addToChartViewer(event: { item: MenuItem }) {
    const selectedNodes = (event.item.state["selectedNodes"] || []) as MDMTreeNode[];

    if (selectedNodes && selectedNodes.length >= 1) {
      for (const node of selectedNodes) {
        this.chartViewerService.sendAppendNode({
          id: node.id,
          sourceName: node.source,
          type: node.type,
        } as Node);
      }
    }
  }

  refreshAfterCreateNewChildEntity() {
    // create child entity should be possible only by one selected parent entity
    if (this.selectedNodes.length === 1) {
      const parentNode: TreeNode = this.selectedNodes[0];

      // load children is possible by undefined children
      parentNode.children = undefined;
      this.loadChildren(parentNode, true);
    }
  }

  refresh() {
    this.selectedNodes.forEach((n) => {
      this.unloadChildren(n);
      this.loadChildren(n);
      n.expanded = true;
    });
  }

  refreshAll() {
    this.restoreTreeNodes(this.nodes, this.getOpenNodes(this.nodes), this.getSelectedNodesForStorage()).subscribe();
  }

  getSelectedNodesForStorage() {
    return this.selectedNodes.map((n) => {
      if (n.data.id) {
        return { source: n.data.source, type: n.data.type, id: n.data.id } as MDMIdentifier;
      } else {
        return { source: n.data.source, type: n.data.type, id: n.data.serial } as MDMIdentifier;
      }
    });
  }

  getOpenNodes(treeNodes: TreeNode[]): ExpandedNode {
    return treeNodes
      .filter((n) => n.expanded)
      .reduce((prev, current) => {
        prev[current.label] = this.getOpenNodes(current.children);
        return prev;
      }, {} as ExpandedNode);
  }

  private openInTree(items: MDMItem[]) {
    this.selectedNodes = [];
    items.forEach((item) =>
      this.nodeproviderService.getTreePath(this.nodeprovider.id, item).subscribe((treepath) => {
        if (treepath && treepath.length > 0) {
          this.expandTreePath(treepath);
        } else {
          this.notificationService.notifyError(
            this.translateService.instant("navigator.mdm-navigator.err-cannot-open-node"),
            this.translateService.instant("navigator.mdm-navigator.err-cannot-find-node-in-navigation-tree"),
          );
        }
      }),
    );
  }

  private expandTreePath(treePath: MDMTreeNode[]) {
    this.expandChildWrapper(this.nodes, treePath);
  }

  private expandChild(children: TreeNode[], treePath: MDMTreeNode[]) {
    const n = treePath.shift();
    if (!n) {
      // return now if the node is not available
      return;
    }
    // either the ID matches for real nodes or the attributes match for virtual nodes which do not have an id
    let nodeToExpand = this.findChildNode(children, n);
    if (!nodeToExpand) {
      // if children are grouped into chunks
      for (let node of children) {
        if (node.children && node.children.length > 0 && !nodeToExpand) {
          nodeToExpand = this.findChildNode(node.children, n);
          if (nodeToExpand) {
            node.expanded = true;
            break;
          }
        }
      }
    }

    if (nodeToExpand) {
      return this.getChildren(nodeToExpand.data).pipe(
        tap((nodes) => {
          nodeToExpand.children = nodes;
          nodeToExpand.expanded = true;
          if (treePath.length > 0) {
            this.expandChildWrapper(nodeToExpand.children, treePath);
          } else {
            this.selectedNodes = [nodeToExpand];
            const item: MDMItem = this.toMDMItem(nodeToExpand);
            this.navigatorService.fireSelectedItem(item);
            this.navigatorService.updateSelectedNodes([item]);
            setTimeout(() => this.scrollToSelectionPrimeNgDataTable(nodeToExpand), 150);
          }
        }),
      );
    }
  }

  private findChildNode(children: TreeNode<any>[], n: MDMTreeNode) {
    return children.filter((node) => this.nodeEquals(node.data, n)).find((node) => node.data.label === n.label);
  }

  private nodeEquals(node1: MDMTreeNode, node2: MDMTreeNode) {
    // if id is defined, it must match. Otherwise fallback to label, type and idAttribute for virtual nodes
    return (
      node1 &&
      node1.source === node2.source &&
      ((node2.id !== undefined && node1.id === node2.id) ||
        (node2.id === undefined && node1.label === node2.label && node1.type === node2.type && node1.idAttribute === node2.idAttribute))
    );
  }

  private expandChildWrapper(children: TreeNode[], treePath: MDMTreeNode[]) {
    this.expandChild(children, treePath)?.subscribe(
      (next) => {
        this.stateService.triggerSave(this);
      },
      (error) =>
        this.notificationService.notifyError(this.translateService.instant("navigator.mdm-navigator.err-cannot-load-nodes"), error),
    );
  }

  /**
   * PrimeNG does not support scroll to view. This methods implements a
   * workaround by using HTML element IDs.
   * @see https://github.com/primefaces/primeng/issues/1278
   */
  scrollToSelectionPrimeNgDataTable(node: TreeNode) {
    const tree = this.navigatorTree.el.nativeElement.firstElementChild as Element;
    const elemId = this.getId(node);
    const list = tree.querySelectorAll("span#" + elemId);

    if (list && list.length > 0) {
      const scrollTarget: Element = list.item(0);
      scrollTarget.scrollIntoView();
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      // ! be aware, that all client rects are relative to the current scroll position !
      // ! meaning: tree.scrollTop and/or tree.scrollLeft may be > 0 and must be added !
      // !          as offset to Y / X coordinates                                     !
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      const scrollTargetRect = scrollTarget.getBoundingClientRect() as DOMRect;
      const treeRect = tree.getBoundingClientRect() as DOMRect;

      const totalScrollableAmount = treeRect.height < tree.scrollHeight ? tree.scrollHeight + 2 - treeRect.height : 0;
      if (totalScrollableAmount > 0 && scrollTargetRect !== undefined && treeRect !== undefined) {
        const scrollTop = tree.scrollTop + scrollTargetRect.y - scrollTargetRect.height - treeRect.height;
        tree.scrollTop = scrollTop > totalScrollableAmount ? totalScrollableAmount : scrollTop;
      }
    }
  }

  getId(node: TreeNode) {
    if (node && node.data) {
      return "node_" + node.data.source + "_" + node.data.type + "_" + node.data.id;
    } else {
      return "";
    }
  }

  registerMenuItems() {
    this.menuService.register({
      menuItem: {
        id: "refresh",
        label: "navigator.mdm-navigator.refresh-navigator",
        icon: "fa fa-refresh",
        command: () => this.refresh(),
      },
      sortIndex: 10,
    });
    this.menuService.register({
      menuItem: {
        id: "refresh-all",
        label: "navigator.mdm-navigator.refresh-all-navigator",
        icon: "fa fa-refresh",
        command: () => this.refreshAll(),
      },
      sortIndex: 20,
    });
    this.menuService.register({
      menuItem: { id: "copy", label: "navigator.mdm-navigator.copy-link", icon: "fa fa-link", command: (event) => this.copyLink(event) },
      sortIndex: 30,
    });

    this.menuService.register({
      menuItem: {
        id: "basket",
        label: "navigator.mdm-navigator.add-to-shopping-basket",
        icon: "fa fa-shopping-cart",
        command: (event) => this.addSelectionToBasket(event),
      },
      predicate: (node) => node.id !== undefined,
      sortIndex: 40,
    });

    this.menuService.register({
      menuItem: {
        id: "basket-virtual",
        label: "navigator.mdm-navigator.add-to-shopping-basket",
        icon: "fa fa-shopping-cart",
      },
      predicate: (node) => node.id === undefined, // virtual nodes don't have an id
      lazyChildren: () =>
        of([
          {
            id: "basket",
            label: "navigator.mdm-navigator.add-test",
            icon: "icon test",
            command: () => this.addVirtualSelectionToBasket("Test"),
          },
          {
            id: "basket",
            label: "navigator.mdm-navigator.add-teststep",
            icon: "icon teststep",
            command: () => this.addVirtualSelectionToBasket("TestStep"),
          },
          {
            id: "basket",
            label: "navigator.mdm-navigator.add-measurement",
            icon: "icon measurement",
            command: () => this.addVirtualSelectionToBasket("Measurement"),
          },
        ]),
      sortIndex: 40,
    });

    this.menuService.register({
      menuItem: {
        id: "import",
        label: "navigator.mdm-navigator.import-file",
        icon: "pi pi-file-import",
        command: () => this.importFile(),
      },
      predicate: (node) => node.type === "Environment",
      sortIndex: 50,
    });

    this.menuService.register({
      menuItem: {
        id: "chart-viewer",
        label: "navigator.mdm-navigator.add-to-chart-viewer",
        icon: "fa fa-area-chart",
        command: (event) => this.addToChartViewer(event),
      },
      predicate: (node) => node.type === "Measurement" || node.type === "ChannelGroup",
      sortIndex: 110,
    });
  }
}
