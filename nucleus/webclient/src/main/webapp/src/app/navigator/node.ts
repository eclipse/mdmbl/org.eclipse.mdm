/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

export enum ContextGroup {
  ORDERED = 0,
  MEASURED = 1,
}

export class Attribute {
  name: string;
  unit: string;
  dataType: string;
  value: string | string[] | MDMLinkExt | MDMLinkExt[] | MDMLinkExt[][] | EnumAttribute;
  enumName?: string;
  sortIndex?: number;
  readOnly?: boolean;
  mandatory?: boolean;
  description?: string;
  valueListRef?: boolean;
  valueList?: string;
}

export class MergedSensorAttribute {
  name: string[];
  unit: string[];
  dataType: string[];
  value: (string | MDMLinkExt | MDMLinkExt[])[];
}

export class MDMLink {
  remotePath: string;
  mimeType: string;
  description: string;
  size: string;

  public getFileName() {
    let fileName = "";
    if (this.remotePath != undefined) {
      const index = this.remotePath.lastIndexOf("/") + 1;
      fileName = this.remotePath.substring(index);
    }
    return fileName;
  }
}

export class MDMLinkExt {
  identifier: string;
  mimeType: string;
  description: string;
  fileName: string;
  size: string;
}

export class EnumAttribute {
  value: string;
  enumName: string;
  enums: MDMEnum;
}

export class MDMEnum {
  name: string;
  values: MdmEnumValue[];
}

export class MdmEnumValue {
  label: string;
  value: string;
  ordinal: number;
}

export class FileSize {
  identifier: string;
  size: string;
}

export class Relation {
  name: string;
  type: string;
  entityType: string;
  contextType: string;
  ids: string[];
  parentId: string;
}

export class Node {
  name: string;
  id: string;
  type: string;
  sourceType: string;
  sourceName: string;
  attributes: Attribute[];
  relations: Relation[];
  active: boolean;
  optional: boolean;
  defaultActive: boolean;
  testStepSeriesVariable: boolean;

  /* Attributes by the new nodeprovider */
  source: string;
  label: string;
  filter: string;
  serial: string;
  /* end */

  // temporary attribute populated in node service
  idAttribute?: string;
}

export function getNodeClass(n: Node) {
  switch (n.type) {
    case "StructureLevel":
      return "pool";
    case "MeaResult":
      return "measurement";
    case "SubMatrix":
      return "channelgroup";
    case "MeaQuantity":
      return "channel";
    default:
      return n.type.toLowerCase();
  }
}

export class NodeArray {
  type: string;
  data: Node[];
}
