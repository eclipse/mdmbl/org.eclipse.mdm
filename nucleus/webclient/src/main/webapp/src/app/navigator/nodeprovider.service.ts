/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { MDMIdentifier } from "@core/mdm-identifier";
import { MDMItem } from "@core/mdm-item";
import { PropertyService } from "@core/property.service";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { NodeProviderUIWrapper } from "./nodeprovider-ui-wrapper";

export class MDMTreeNodeList {
  data: MDMTreeNode[];
}
export class MDMTreeNode implements MDMIdentifier {
  source: string;
  type: string;
  id: string;
  idAttribute?: string;
  label: string;
  filter: string;
  serial: string;
  leaf: boolean;
}

@Injectable()
export class NodeproviderService {
  private _nodeProviderEndpoint: string;

  private nodeproviders: Observable<NodeProviderUIWrapper[]>;

  constructor(private http: HttpClient, private _prop: PropertyService) {
    this._nodeProviderEndpoint = _prop.getUrl("mdm/nodeprovider");
    this.nodeproviders = this.loadNodeproviders();
  }

  getNodeproviders() {
    return this.nodeproviders;
  }

  loadNodeproviders(): Observable<NodeProviderUIWrapper[]> {
    return this.http
      .get<NodeProviderUIWrapper[]>(this._nodeProviderEndpoint)
      .pipe(catchError((e) => addErrorDescription(e, "Could not request nodeproviders!")));
  }

  getRoots(nodeproviderId: string) {
    return this.http.get<MDMTreeNodeList>(this._nodeProviderEndpoint + "/" + nodeproviderId).pipe(
      map((list) => list.data || []),
      catchError((e) => addErrorDescription(e, "Could not request nodeprovider!")),
    );
  }

  getChildren(nodeproviderId: string, item: MDMTreeNode) {
    return this.http.get<MDMTreeNodeList>(this._nodeProviderEndpoint + "/" + nodeproviderId + "/" + item.serial).pipe(
      map((list) => list.data || []),
      catchError((e) => addErrorDescription(e, "Could not request child nodes!")),
    );
  }

  getTreePath(nodeproviderId: string, item: MDMItem): Observable<MDMTreeNode[]> {
    return this.http.post<MDMTreeNodeList>(this._nodeProviderEndpoint + "/" + nodeproviderId, item).pipe(
      map((list) => list.data || []),
      catchError((e) => addErrorDescription(e, "Could not request tree path!")),
    );
  }
}
