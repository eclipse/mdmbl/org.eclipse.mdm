/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { EventEmitter, Injectable, Output } from "@angular/core";
import { forkJoin, ReplaySubject } from "rxjs";
import { tap } from "rxjs/operators";
import { MDMItem } from "../core/mdm-item";
import { Node } from "./node";
import { NodeService } from "./node.service";

@Injectable()
export class NavigatorService {
  @Output() openInTree = new EventEmitter<MDMItem[]>();
  private selectedNodeSubject = new ReplaySubject<Node | MDMItem>(1);
  private selectedNodes = new ReplaySubject<(Node | MDMItem)[]>(1);
  public refreshNavigator = new EventEmitter();

  constructor(private nodeService: NodeService) {}

  /**
   * Returns an Observable to listen to navigator tree selection.
   * On subscribe the Observable replays the last selection.
   * If selected TreeNode is a virtual node the Observable contains a MDMItem describing the selection.
   * If selected TreeNode is a real node the Observable contains a Node describing the selection.
   */
  public onSelectionChanged() {
    return this.selectedNodeSubject.asObservable();
  }

  /**
   * Emitter for navigator tree selection.
   * @param obj
   * @deprecated FireSelectedItem should be favoured over this.
   * Should be used in navigator service internally only.
   *
   * @TODO It should be private for above reasons. For now it is still used in table service.
   */
  public fireSelectedNodeChanged(obj: Node | MDMItem) {
    this.selectedNodeSubject.next(obj);
  }

  /**
   * Emitter for navigator tree selection. If item describing a real node it is converted into a Node.
   * @param obj
   */
  public fireSelectedItem(item: MDMItem) {
    if (item != undefined && item.id != undefined) {
      this.nodeService
        .getNodeFromItem(item)
        .pipe(tap((node) => this.fireSelectedNodeChanged(node)))
        .subscribe();
    } else {
      this.fireSelectedNodeChanged(item);
    }
  }

  public fireOnOpenInTree(items: MDMItem[]) {
    this.openInTree.emit(items);
  }

  public updateSelectedNodes(selectedNodes: (Node | MDMItem)[]) {
    forkJoin(selectedNodes.map((nd) => this.nodeService.getNodeFromItem(nd as MDMItem))).subscribe((mn) => this.selectedNodes.next(mn));
  }

  public onSelectedNodesChanged() {
    return this.selectedNodes.asObservable();
  }
}
