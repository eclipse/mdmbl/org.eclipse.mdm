/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from "@angular/core";
import { IStatefulComponent, StateService } from "@core/services/state.service.ts";
import { TranslateService } from "@ngx-translate/core";
import { ConfirmationService } from "primeng/api";
import { take } from "rxjs";
import { TRANSLATE } from "../core/mdm-core.module";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { OverwriteDialogComponent } from "../core/overwrite-dialog.component";
import { Scope } from "../core/preference.service";
import { EditViewComponent } from "./editview.component";
import { PreferenceView, View, ViewService } from "./tableview.service";

@Component({
  selector: "mdm-view",
  templateUrl: "view.component.html",
  providers: [ConfirmationService],
})
export class ViewComponent implements OnInit, OnDestroy, IStatefulComponent {
  @Input() stateKey = "view";
  @Output() view = new EventEmitter<View>();

  public selectedView: View;
  public currentDefaultView: View;
  public isDefaultView: boolean;

  public groupedViews: { scope: string; view: View[]; label: string }[] = [];

  public viewName = "";
  public userViewNames: string[];
  public selectedRow = "";
  public dropdownPlaceholder = "tableview.view.select-view";

  @ViewChild(EditViewComponent)
  editViewComponent: EditViewComponent;

  public selectSearchComponents = false;
  public lgSaveModal = false;

  @ViewChild(OverwriteDialogComponent)
  overwriteDialogComponent: OverwriteDialogComponent;

  constructor(
    private viewService: ViewService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
    private confirmationService: ConfirmationService,
    private stateService: StateService,
  ) {}

  getState(): unknown {
    return {};
  }

  takeOverDialogVisible = false;
  takeOverView = {} as View;
  takeOverNameExists = false;

  applyState(state: unknown) {
    if (state["takeover-view"]) {
      this.takeOverView = state["takeover-view"] as View;
      this.takeOverDialogVisible = true;
    }
  }

  takeOver() {
    this.viewService.getViews().subscribe((views) => {
      if (views.findIndex((v) => v.view.name === this.takeOverView.name) >= 0) {
        this.takeOverNameExists = true;
      } else {
        this.takeOverNameExists = false;
        this.viewService.saveView(this.takeOverView).subscribe(() => {
          this.takeOverDialogVisible = false;
          this.viewService.getViews().subscribe((views) => {
            this.setViews(views, views.find((prefView) => prefView.view.name === this.takeOverView.name && prefView.scope === "USER").view);
            this.viewService.viewSaved$.emit(this);
          });
        });
      }
    });
  }

  getStateForTakeOver(): unknown {
    return {
      [this.stateKey]: {
        "takeover-view": this.selectedView,
      },
    };
  }
  ngOnDestroy() {
    this.stateService.unregister(this);
  }

  ngOnInit() {
    this.stateService.register(this, this.stateKey);
    this.viewService.getViews().subscribe(
      (views) => this.setViews(views),
      (error) => this.notificationService.notifyError(this.translateService.instant("tableview.view.err-cannot-load-views"), error),
    );
    this.viewService.viewSaved$.subscribe(
      (emitter) => this.onViewSaved(emitter),
      (error) => this.notificationService.notifyError(this.translateService.instant("tableview.view.err-saving-view"), error),
    );
    this.viewService.viewDeleted$.subscribe(
      (emitter) => this.onDeleteView(emitter),
      (error) => this.notificationService.notifyError(this.translateService.instant("tableview.view.err-deleting-view"), error),
    );
  }

  selectViewByUser() {
    this.isDefaultView = this.currentDefaultView === this.selectedView;
    this.view.emit(this.selectedView);
  }

  selectView(view: View) {
    this.dropdownPlaceholder = view.name === "" ? "tableview.view.new-view" : "tableview.view.select-view";
    this.selectedView = view;
    this.isDefaultView = this.currentDefaultView === this.selectedView;
    this.view.emit(this.selectedView);
  }

  public editSelectedView(e: Event) {
    e.stopPropagation();
    this.editViewComponent.showDialog(this.selectedView).subscribe(
      (v) => this.selectView(v),
      (error) => this.notificationService.notifyError(this.translateService.instant("tableview.view.err-cannot-select-view"), error),
    );
  }

  public newView(e: Event) {
    e.stopPropagation();
    this.editViewComponent
      .showDialog({
        columns: [],
        name: "",
      } as View)
      .subscribe(
        (v) => this.selectView(v),
        (error) =>
          this.notificationService.notifyError(this.translateService.instant("tableview.view.err-cannot-display-view-editor"), error),
      );
  }

  private onDeleteView(emitter: Component) {
    if (emitter !== this) {
      this.viewService.getViews().subscribe(
        (prefViews) => this.setViews(prefViews, this.selectedView),
        (error) => this.notificationService.notifyError(this.translateService.instant("tableview.view.err-cannot-update-views"), error),
      );
    }
  }

  private onViewSaved(emitter: Component) {
    if (emitter !== this) {
      this.viewService.getViews().subscribe(
        (prefViews) => this.setViews(prefViews, this.selectedView),
        (error) => this.notificationService.notifyError(this.translateService.instant("tableview.view.err-cannot-update-views"), error),
      );
    }
  }

  defaultViewChanged() {
    this.currentDefaultView = this.selectedView;
  }

  private setViews(prefViews: PreferenceView[], viewToSelect: View = undefined) {
    this.viewService.getDefaultView().subscribe((defaultView) => {
      this.getGroupedView(prefViews);
      this.currentDefaultView = prefViews.find((view) => view.scope === "USER" && view.view.name === defaultView)?.view;
      setTimeout(() => {
        let checkedViewToSelect = viewToSelect;
        if (viewToSelect) {
          if (!prefViews.find((prefView) => prefView.view.name === viewToSelect.name && prefView.scope === "USER")) {
            if (!prefViews.find((prefView) => prefView.view.name === viewToSelect.name && prefView.scope === "SOURCE")) {
              checkedViewToSelect = this.currentDefaultView ? this.currentDefaultView : prefViews[0].view;
            }
          }
        } else {
          checkedViewToSelect = this.currentDefaultView ? this.currentDefaultView : prefViews[0].view;
        }
        this.selectView(checkedViewToSelect);
      });
    });
  }

  private getGroupedView(prefViews: PreferenceView[]) {
    this.groupedViews = [];
    for (let i = 0; i < prefViews.length; i++) {
      let pushed = false;
      for (let j = 0; j < this.groupedViews.length; j++) {
        if (prefViews[i].scope === this.groupedViews[j].scope) {
          this.groupedViews[j].view.push(prefViews[i].view);
          pushed = true;
        }
      }
      if (pushed === false) {
        this.groupedViews.push({
          scope: prefViews[i].scope,
          view: [prefViews[i].view],
          label: Scope.toLabel(prefViews[i].scope),
        });
      }
    }
    this.updateUserViewNames();
  }

  private updateUserViewNames() {
    this.viewService.getViews().subscribe(
      (prefViews) => {
        this.userViewNames = prefViews.filter((pv) => pv.scope === Scope.USER).map((pv) => pv.view.name);
      },
      (error) => this.notificationService.notifyError(this.translateService.instant("tableview.view.err-cannot-update-views"), error),
    );
  }

  saveView(e: Event) {
    e.stopPropagation();
    if (this.groupedViews.find((gv) => gv.view.find((v) => v.name === this.viewName) != undefined && gv.scope === Scope.USER)) {
      this.selectSearchComponents = false;
      this.overwriteDialogComponent
        .showOverwriteModal(this.translateService.instant("tableview.view.a-view"))
        .pipe(take(1))
        .subscribe(
          (needSave) => this.saveView2(needSave),
          (error) => {
            this.saveView2(false);
            this.notificationService.notifyError(this.translateService.instant("tableview.view.err-cannot-save-view"), error);
          },
        );
    } else {
      this.saveView2(true);
    }
  }

  saveView2(save: boolean) {
    if (save) {
      const view = this.selectedView; // classToClass
      view.name = this.viewName;
      this.viewService.saveView(view).subscribe(
        () => {
          if (this.isDefaultView) {
            this.viewService.setDefaultView(view).subscribe();
          }

          this.viewService.getViews().subscribe((views) => {
            this.setViews(views, views.find((prefView) => prefView.view.name === view.name && prefView.scope === "USER").view);
            this.viewService.viewSaved$.emit(this);
          });
        },
        (error) => this.notificationService.notifyError(this.translateService.instant("tableview.tableview.err-cannot-save-view"), error),
      );
      this.selectSearchComponents = false;
    } else {
      this.selectSearchComponents = true;
    }
  }

  showSaveModal(e: Event) {
    e.stopPropagation();
    this.viewName = this.selectedView.name;
    this.selectedRow = "";
    this.selectSearchComponents = true;
  }

  deleteView(e: Event) {
    e.stopPropagation();
    const userGroup = this.groupedViews.find((gv) => gv.scope === Scope.USER);
    if (userGroup && userGroup.view.length > 0) {
      this.translateService.get("tableview.view.msg-confirm-delete-view-from-db").subscribe((msg) =>
        this.confirmationService.confirm({
          message: msg,
          key: "viewConfirmation",
          accept: () => this.deleteViewConfirmed(),
        }),
      );
    } else {
      this.notificationService.notifyError("Forbidden.", this.translateService.instant("tableview.view.err-cannot-modify-system-settings"));
    }
  }

  deleteViewConfirmed() {
    this.viewService.deleteView(this.selectedView.name).subscribe(
      () =>
        this.viewService.getViews().subscribe((views) => {
          this.setViews(views);
          this.viewService.viewDeleted$.emit(this);
        }),
      (error) => this.notificationService.notifyError(this.translateService.instant("tableview.view.err-cannot-delete-view"), error),
    );
  }

  getSaveBtnTitle() {
    return this.viewName ? TRANSLATE("tableview.view.tooltip-save-view") : TRANSLATE("tableview.view.tooltip-no-name-set");
  }

  onRowSelect(e: any) {
    this.viewName = e.data;
  }

  onRowUnselect() {
    this.viewName = "";
  }
}
