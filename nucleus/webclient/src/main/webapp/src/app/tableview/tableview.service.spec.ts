/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClientTestingModule, HttpTestingController, RequestMatch } from "@angular/common/http/testing";
import { async, inject, TestBed } from "@angular/core/testing";
import { MDMNotificationService } from "@core/mdm-notification.service";
import { PreferenceService, Scope } from "@core/preference.service";
import { PropertyService } from "@core/property.service";
import { TranslateModule } from "@ngx-translate/core";
import { ViewService } from "./tableview.service";

describe("TableviewService", () => {
  let httpTestingController: HttpTestingController;

  const reqGetMatch: RequestMatch = { method: "GET" };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],
      providers: [ViewService, PropertyService, PreferenceService, MDMNotificationService],
    });

    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  describe("getViews()", () => {
    it("should return view from preference", async(
      inject([ViewService], (tableviewService) => {
        const mockResponse = {
          preferences: [
            {
              id: 22,
              key: "tableview.view.Test",
              scope: Scope.USER,
              source: null,
              user: "sa",
              value: '{"columns":[{"type":"Test","name":"Name"},{"type":"TestStep","name":"Name"}],"name":"Test"}',
            },
          ],
        };

        tableviewService.getViews().subscribe((prefViews) => {
          expect(prefViews.length).toBe(1);
          expect(prefViews[0].scope).toBe(Scope.USER);
          expect(prefViews[0].view.columns.length).toBe(2);
        });

        const req = httpTestingController.expectOne(reqGetMatch);
        req.flush(mockResponse);
      }),
    ));

    it("should return default view, if no view preferences are available", async(
      inject([ViewService], (tableviewService) => {
        const mockResponse = { preferences: [] };

        tableviewService.getViews().subscribe((prefViews) => {
          expect(prefViews.length).toBe(1);
          expect(prefViews[0].scope).toBe(Scope.SYSTEM);
          expect(prefViews[0].view.columns.length).toBe(1);
        });

        const req = httpTestingController.expectOne(reqGetMatch);
        req.flush(mockResponse);
      }),
    ));
  });
});
