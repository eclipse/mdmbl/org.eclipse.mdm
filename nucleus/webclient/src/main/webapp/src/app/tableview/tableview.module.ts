/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { AutoCompleteModule } from "primeng/autocomplete";
import { CheckboxModule } from "primeng/checkbox";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { DialogModule } from "primeng/dialog";
import { TableModule } from "primeng/table";
import { MDMCoreModule } from "../core/mdm-core.module";
import { SearchattributeTreeModule } from "../searchattribute-tree/searchattribute-tree.module";
import { EditViewComponent } from "./editview.component";
import { TableviewComponent } from "./tableview.component";
import { ViewComponent } from "./view.component";
import { ViewValuePipe } from "./viewvalue.pipe";

@NgModule({
  imports: [MDMCoreModule, SearchattributeTreeModule, TableModule, DialogModule, AutoCompleteModule, ConfirmDialogModule, CheckboxModule],
  declarations: [TableviewComponent, EditViewComponent, ViewComponent, ViewValuePipe],
  exports: [AutoCompleteModule, TableviewComponent, EditViewComponent, ViewComponent],
  providers: [],
})
export class TableViewModule {}
