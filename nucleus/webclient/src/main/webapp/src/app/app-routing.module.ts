/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { functionalRightGuard } from "@core/authentication/authentication.service";

const appRoutes: Routes = [
  { path: "navigator", loadChildren: () => import("./navigator-view/mdm-navigator-view.module").then((mod) => mod.MDMNavigatorViewModule) },
  {
    path: "administration",
    loadChildren: () => import("./administration/admin.module").then((mod) => mod.AdminModule),
    canActivate: [functionalRightGuard("administration.show")],
  },
  {
    path: "scheduler",
    loadChildren: () => import("./scheduler/scheduler.module").then((mod) => mod.SchedulerModule),
    canActivate: [functionalRightGuard("administration.show")],
  },
  { path: "", redirectTo: "navigator", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { paramsInheritanceStrategy: "always", enableTracing: false })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
