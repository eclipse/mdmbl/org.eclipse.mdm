/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { Component, EventEmitter, Input, Output } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { MDMNotificationService } from "../core/mdm-notification.service";
import { Node } from "../navigator/node";
import { FilereleaseService, Release } from "./filerelease.service";

@Component({
  selector: "mdm-filerelease-create",
  templateUrl: "mdm-filerelease-create.component.html",
})
export class MDMFilereleaseCreateComponent {
  @Input() disabled: boolean;
  @Input() node: Node;
  @Output() submitted = new EventEmitter<boolean>();
  release: Release = new Release();
  options = ["PAK2RAW", "PAK2ATFX"];
  expire = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  lgModal = false;

  constructor(
    private service: FilereleaseService,
    private notificationService: MDMNotificationService,
    private translateService: TranslateService,
  ) {}

  getFormat(key) {
    return this.service.formatMap[key];
  }

  createRelease() {
    this.release.identifier = "";
    this.release.state = "";
    this.release.name = this.node.name;
    this.release.sourceName = this.node.sourceName;
    this.release.typeName = this.node.type;
    this.release.id = this.node.id;
    this.release.sender = "";
    this.release.receiver = "";
    this.release.rejectMessage = "";
    this.release.errorMessage = "";
    this.release.fileLink = "";
    this.release.expire = 0;
    this.service.create(this.release).subscribe(
      (release) => (this.release = release),
      (error) =>
        this.notificationService.notifyError(
          this.translateService.instant("filerelease.mdm-filerelease-create.err-cannot-create-release"),
          error,
        ),
    );
    this.clear();
    this.submitted.emit(true);
    this.lgModal = false;
  }

  clear() {
    this.release = new Release();
  }
}
