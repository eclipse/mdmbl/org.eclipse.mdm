/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { addErrorDescription } from "@core/core.functions";
import { TRANSLATE } from "@core/mdm-core.module";
import { PropertyService } from "@core/property.service";
import { catchError, map } from "rxjs/operators";

@Injectable()
export class FilereleaseService {
  url: string;
  stateMap = [];
  formatMap = [];
  month = [];

  constructor(private http: HttpClient, private prop: PropertyService) {
    this.url = prop.getUrl("mdm/filereleases");

    this.formatMap["PAK2RAW"] = TRANSLATE("filerelease.filerelease.format-pak2raw");
    this.formatMap["PAK2ATFX"] = TRANSLATE("filerelease.filerelease.format-pak2atfx");

    this.stateMap["RELEASE_ORDERED"] = TRANSLATE("filerelease.filerelease.state-release-ordered");
    this.stateMap["RELEASE_APPROVED"] = TRANSLATE("filerelease.filerelease.state-release-approved");
    this.stateMap["RELEASE_RELEASED"] = TRANSLATE("filerelease.filerelease.state-release-released");
    this.stateMap["RELEASE_EXPIRED"] = TRANSLATE("filerelease.filerelease.state-release-expired");
    this.stateMap["RELEASE_PROGRESSING_ERROR"] = TRANSLATE("filerelease.filerelease.state-release-processing-error");
    this.stateMap["RELEASE_PROGRESSING"] = TRANSLATE("filerelease.filerelease.state-release-in-process");
    this.stateMap["RELEASE_REJECTED"] = TRANSLATE("filerelease.filerelease.state-release-declined");

    this.month[0] = "1";
    this.month[1] = "2";
    this.month[2] = "3";
    this.month[3] = "4";
    this.month[4] = "5";
    this.month[5] = "6";
    this.month[6] = "7";
    this.month[7] = "8";
    this.month[8] = "9";
    this.month[9] = "10";
    this.month[10] = "11";
    this.month[11] = "12";
  }

  readAll() {
    return this.read("");
  }

  readIncomming() {
    return this.read("?direction=incomming");
  }

  readOutgoging() {
    return this.read("?direction=outgoing");
  }

  create(release: Release) {
    const body = JSON.stringify(release);
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };
    return this.http.post<any>(this.url, body, options).pipe(
      map(this.extractData),
      catchError((e) => addErrorDescription(e, "Could not request Release!")),
    );
  }

  delete(release: Release) {
    return this.http
      .delete<any>(this.url + "/" + release.identifier)
      .pipe(catchError((e) => addErrorDescription(e, "Could not request Release!")));
  }

  approve(release: Release) {
    release.state = "RELEASE_APPROVED";
    return this.update(release).pipe(catchError((e) => addErrorDescription(e, "Could not request Release!")));
  }

  reject(release: Release) {
    release.state = "RELEASE_REJECTED";
    return this.update(release).pipe(catchError((e) => addErrorDescription(e, "Could not request Release!")));
  }

  formatDate(date) {
    const d = new Date(date);
    const day = d.getDate();
    const month = this.month[d.getMonth()];
    const year = d.getFullYear();
    const hours = (d.getHours() < 10 ? "0" : "") + d.getHours();
    const min = (d.getMinutes() < 10 ? "0" : "") + d.getMinutes();
    const sec = (d.getSeconds() < 10 ? "0" : "") + d.getSeconds();
    return day + "." + month + "." + year + " " + hours + ":" + min + ":" + sec;
  }

  private read(query: string) {
    return this.http.get<any>(this.url + query).pipe(
      map((res) => <Release[]>res.data),
      catchError((e) => addErrorDescription(e, "Could not request Release!")),
    );
  }

  private update(release: Release) {
    const body = JSON.stringify(release);
    const headers = new HttpHeaders({ "Content-Type": "application/json" });
    const options = { headers: headers };
    return this.http.post<any>(this.url + "/" + release.identifier, body, options).pipe(
      map(this.extractData),
      catchError((e) => addErrorDescription(e, "Could not request Release!")),
    );
  }

  private extractData(res: any) {
    return res || {};
  }
}

export class Release {
  identifier: string;
  state: string;
  name: string;
  sourceName: string;
  typeName: string;
  id: string;
  sender: string;
  receiver: string;
  orderMessage: string;
  rejectMessage: string;
  errorMessage: string;
  format: string;
  fileLink: string;
  validity: number;
  expire: number;
}
