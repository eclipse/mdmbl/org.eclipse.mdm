/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { functionalRightGuard } from "@core/authentication/authentication.service";
import { ChartViewerNavCardComponent } from "../chartviewer/components/chatviewer-nav-card/chart-viewer-nav-card.component";
import { XyChartViewerNavCardComponent } from "../chartviewer/components/xy-chart-viewer-nav-card/xy-chart-viewer-nav-card.component";
import { MdmContextDescribableProviderService } from "../context-bulk-editor/mdm-context-describable-provider.service";
import { FileExplorerNavCardComponent } from "../file-explorer/components/file-explorer-nav-card/file-explorer-nav-card.component";
import { MDMModulesComponent } from "../modules/mdm-modules.component";
import { MDMSearchComponent } from "../search/mdm-search.component";

const moduleRoutes: Routes = [
  {
    path: "",
    component: MDMModulesComponent,
    children: [
      { path: "", redirectTo: "search", pathMatch: "full" },
      {
        path: "details",
        loadChildren: () => import("../details/mdm-detail.module").then((mod) => mod.MDMDetailModule),
        canActivate: [functionalRightGuard("mdm-modules.details.show")],
      },
      {
        path: "context-bulk-editor",
        loadChildren: () => import("../context-bulk-editor/context-bulk-editor.module").then((mod) => mod.ContextBulkEditorModule),
        canActivate: [functionalRightGuard("mdm-modules.context-bulk-editor.show")],
        providers: [{ provide: "ContextDescribableProvider", useClass: MdmContextDescribableProviderService }],
      },
      { path: "search", component: MDMSearchComponent, canActivate: [functionalRightGuard("mdm-modules.mdm-search.show")] },
      {
        path: "quickviewer",
        component: ChartViewerNavCardComponent,
        canActivate: [functionalRightGuard("mdm-modules.quick-viewer.show")],
      },
      {
        path: "xychartviewer",
        component: XyChartViewerNavCardComponent,
        canActivate: [functionalRightGuard("mdm-modules.xy-chart-viewer.show")],
      },
      { path: "files", component: FileExplorerNavCardComponent, canActivate: [functionalRightGuard("mdm-modules.file-explorer.show")] },
      {
        path: "user-prefs",
        loadChildren: () => import("../user-prefs/user-prefs.module").then((mod) => mod.UserPrefsModule),
        canActivate: [functionalRightGuard("mdm-modules.user-prefs.show")],
      },
      {
        path: "recent-changes",
        loadChildren: () => import("../recent-changes/recent-changes.module").then((mod) => mod.RecentChangesModule),
        canActivate: [functionalRightGuard("mdm-modules.recent-changes.show")],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(moduleRoutes)],
  exports: [RouterModule],
})
export class MDMModulesRoutingModule {}
