/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.logging;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import org.eclipse.mdm.logging.AuditLogger;
import org.eclipse.mdm.logging.AuditLoggerFactory;
import org.eclipse.mdm.logging.EcsEventCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class AuditLoggingResponseFilter extends AuditLogging implements ContainerResponseFilter {

	private static final Logger LOG = LoggerFactory.getLogger(AuditLoggingResponseFilter.class);

	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException {
		if (isRelevant(requestContext)) {
			createEcsContextConfig(requestContext, responseContext)
					.log((m) -> LOG.info(m, "API response {}", requestContext.getUriInfo().getPath()));
		}
	}

	private AuditLogger createEcsContextConfig(ContainerRequestContext requestContext,
			ContainerResponseContext responseContext) {
		AuditLoggerFactory factory = new AuditLoggerFactory("rest-api-call", EcsEventCategory.Api,
				evaluateHttpVerbType(requestContext));
		int status = responseContext.getStatus();
		if (200 <= status && status < 300) { // 2xx
			return factory.success();
		} else if (400 <= status) { // 4xx, 5xx
			return factory.error();
		} else {
			return factory.end();
		}
	}
}
