/********************************************************************************
 * Copyright (c) 2015-2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.oidc;

import java.security.Principal;
import java.util.Collection;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.pac4j.core.profile.ProfileHelper;
import org.pac4j.core.profile.UserProfile;

public class Pac4JHttpServletRequestWrapper extends HttpServletRequestWrapper {
	private Collection<UserProfile> profiles;

	public Pac4JHttpServletRequestWrapper(HttpServletRequest request, Collection<UserProfile> profiles) {
		super(request);
		this.profiles = profiles;
	}

	@Override
	public String getRemoteUser() {
		return getPrincipal().map(p -> p.getName()).orElse(null);
	}

	private Optional<UserProfile> getProfile() {
		return ProfileHelper.flatIntoOneProfile(profiles);
	}

	private Optional<Principal> getPrincipal() {
		return getProfile().map(UserProfile::asPrincipal);
	}

	@Override
	public Principal getUserPrincipal() {
		return getPrincipal().orElse(null);
	}

	@Override
	public boolean isUserInRole(String role) {
		return this.profiles.stream().anyMatch(p -> p.getRoles().contains(role));
	}
}
