package org.eclipse.mdm.application;

import org.eclipse.mdm.connector.entity.MDMPrincipal;
import org.keycloak.adapters.saml.SamlPrincipal;

/**
 * Implementation of an basic {@link MDMPrincipal} for SAML authentication which
 * creates user with MDM attributes by a given {@link SamlPrincipal}
 * 
 * @author sch
 *
 */
public class MDMSamlPrincipal implements MDMPrincipal {

	private SamlPrincipal samlPrincipal;

	private String givenNameAttribute = "";
	private String surnameAttribute = "";
	private String emailAttribute = "";
	private String telephoneAttribute = "";
	private String departmentAttribute = "";

	/**
	 * @param samlPrincipal
	 * @param emailAttribute
	 * @param surnameAttribute
	 * @param givenNameAttribute
	 * @param departmentAttribute
	 * @param telephoneAttribute
	 */
	public MDMSamlPrincipal(SamlPrincipal samlPrincipal, String givenNameAttribute, String surnameAttribute,
			String emailAttribute, String telephoneAttribute, String departmentAttribute) {
		this.samlPrincipal = samlPrincipal;
		this.givenNameAttribute = givenNameAttribute;
		this.surnameAttribute = surnameAttribute;
		this.emailAttribute = emailAttribute;
		this.telephoneAttribute = telephoneAttribute;
		this.departmentAttribute = departmentAttribute;
	}

	@Override
	public String getName() {
		return this.samlPrincipal.getName();
	}

	@Override
	public String getGivenName() {
		return this.samlPrincipal.getAttribute(this.givenNameAttribute);
	}

	@Override
	public String getSurname() {
		return this.samlPrincipal.getAttribute(this.surnameAttribute);
	}

	@Override
	public String getEmail() {
		return this.samlPrincipal.getAttribute(this.emailAttribute);
	}

	@Override
	public String getTelephone() {
		return this.samlPrincipal.getAttribute(this.telephoneAttribute);
	}

	@Override
	public String getDepartment() {
		return this.samlPrincipal.getAttribute(this.departmentAttribute);
	}

}
