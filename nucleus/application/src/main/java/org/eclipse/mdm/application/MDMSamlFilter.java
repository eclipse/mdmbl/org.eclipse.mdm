/********************************************************************************
 * Copyright (c) 2015-2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.keycloak.adapters.saml.servlet.SamlFilter;

/**
 * MDMSamlFilter
 * 
 * This servlet filter handles the difference between SSO and non-SSO
 * authentication requests. For example for local logout and relogin without
 * SSO. Furthermore it takes MDM user attribute names from given filterConfig
 * and puts them into request wrapper for creating {@link MDMSamlPrincipal}
 * 
 * @author Silvio Christ, Peak Solution GmbH
 *
 */
public class MDMSamlFilter extends SamlFilter {

	private String givenNameAttribute = "";
	private String surnameAttribute = "";
	private String emailAttribute = "";
	private String telephoneAttribute = "";
	private String departmentAttribute = "";

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);
		this.givenNameAttribute = filterConfig.getInitParameter("givenNameAttribute");
		this.surnameAttribute = filterConfig.getInitParameter("surnameAttribute");
		this.emailAttribute = filterConfig.getInitParameter("emailAttribute");
		this.telephoneAttribute = filterConfig.getInitParameter("telephoneAttribute");
		this.departmentAttribute = filterConfig.getInitParameter("departmentAttribute");
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		
		// No caching at index.html to force Idp redirect
		if (request.getServletPath().endsWith("index.html")) {
			response.setHeader("Cache-Control", "no-cache, no-store");
			response.setHeader("Pragma", "no-cache");
		}

		if (request.getUserPrincipal() != null || request.getParameter("nosso") != null) {
			chain.doFilter(req, res);
			return;

		} else {
			FilterChain c = new FilterChain() {

				@Override
				public void doFilter(ServletRequest request, ServletResponse response)
						throws IOException, ServletException {
					HttpServletRequest req = (HttpServletRequest) request;
					chain.doFilter(new MDMSamlHTTPServletRequestWrapper(req, givenNameAttribute, surnameAttribute,
							emailAttribute, telephoneAttribute, departmentAttribute), response);
				}
			};
			super.doFilter(req, res, c);
		}
	}

	@Override
	public void destroy() {
		super.destroy();
	}

}
