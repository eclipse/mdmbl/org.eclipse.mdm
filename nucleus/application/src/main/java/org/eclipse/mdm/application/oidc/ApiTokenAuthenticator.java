package org.eclipse.mdm.application.oidc;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.eclipse.mdm.apitoken.controller.TokenGenerator;
import org.eclipse.mdm.apitoken.entity.UserApiToken;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.credentials.UsernamePasswordCredentials;
import org.pac4j.core.credentials.authenticator.Authenticator;
import org.pac4j.core.exception.CredentialsException;
import org.pac4j.core.profile.CommonProfile;
import org.pac4j.core.profile.ProfileHelper;
import org.pac4j.core.profile.definition.CommonProfileDefinition;
import org.pac4j.core.profile.definition.ProfileDefinitionAware;
import org.pac4j.core.util.CommonHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.enterprise.security.auth.login.common.LoginException;

public class ApiTokenAuthenticator extends ProfileDefinitionAware<MDMApiTokenProfile>
		implements Authenticator<UsernamePasswordCredentials> {

	private static final Logger logger = LoggerFactory.getLogger(AccessTokenRESTAuthenticator.class);

	private EntityManager em;

	@Override
	protected void internalInit() {
		defaultProfileDefinition(new CommonProfileDefinition<>(x -> new MDMApiTokenProfile()));
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("openMDM");
		em = emf.createEntityManager();
	}

	@Override
	public void validate(UsernamePasswordCredentials credentials, WebContext context) {
		init();

		String username = credentials.getUsername();
		String token = credentials.getPassword();

		if (CommonHelper.isBlank(username) || CommonHelper.isBlank(token)) {
			logger.info("Username or token empty.");
			return;
		}

		if (!token.startsWith("openmdm.")) {
			return;
			// throw new CredentialsException("Token is not a vaild openMDM API Token!");
		}

		String[] groups = authenticate(username, token);
		if (groups == null) {
			throw new CredentialsException("API token login failed for username " + username + "!");
		}

		final CommonProfile profile = ProfileHelper.restoreOrBuildProfile(getProfileDefinition(), username, null, null);

		profile.addRoles(Arrays.asList(groups));

		logger.debug("profile: {}", profile);
		credentials.setUserProfile(profile);
	}

	private String[] authenticate(String username, String token) {

		String tokenUser = username + "." + extractUsernameFromToken(token);
		String hashedToken = TokenGenerator.hashToken(token);

		List<UserApiToken> list = em.createQuery(
				"select t from UserApiToken t where LOWER(t.tokenUser) = LOWER(:tokenUser) and LOWER(t.hashedToken) = LOWER(:hashedToken)",
				UserApiToken.class).setParameter("tokenUser", tokenUser).setParameter("hashedToken", hashedToken)
				.getResultList().stream().collect(Collectors.toList());

		if (list.isEmpty()) {
			logger.debug("No matching api token found in database. Denying access.");
			return null;
		} else if (list.size() == 1) {
			return list.get(0).getGroups().toArray(new String[0]);
		} else {
			logger.warn("More then one matching api token found in database. Denying access.");
			// more than one result should not happen, be save an deny authentication.
			return null;
		}
	}

	/**
	 * Extracts the token name from an API token. For example, if the token is
	 * 'openmdm.mytoken.a3d4e3b13aabc', this method returns 'mytoken'.
	 * 
	 * @param token an openMDM API token
	 * @return extracts the token name from an API token
	 */
	String extractUsernameFromToken(String token) {
		Pattern separator = Pattern.compile(Pattern.quote("."));

		String[] parts = separator.split(token);

		if (parts.length < 3 || !"openmdm".equals(parts[0])) {
			throw new LoginException("Invalid token!");
		}

		return parts[1];
	}

}
