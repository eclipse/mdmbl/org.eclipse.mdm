/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;
import com.google.common.io.ByteStreams;

/**
 * MDMRequestFilter
 * 
 * For the angular web client all URLs that are routed by angular have to
 * fallback to index.html. Thus this filter tests, if the the requested URL is
 * in the configured passThroughPrefixes or if the URL matches a real file. If
 * this is the case the filter passes the processing to the next filter,
 * otherwise the contents of index.html is returned.
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
public class MDMRequestFilter implements Filter {

	private static final Logger LOG = LoggerFactory.getLogger(MDMRequestFilter.class);

	private List<String> passThroughPrefixes = new ArrayList<>();

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String passThrough = filterConfig.getInitParameter("passThroughPrefixes");
		if (passThrough == null) {
			passThroughPrefixes.add("mdm");
		} else {
			Splitter.on(',').trimResults().split(passThrough.toLowerCase()).forEach(passThroughPrefixes::add);
		}

		LOG.debug("Returning index.html for all requests except requests prefixed with: {}.", passThroughPrefixes);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (request instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;

			String requestedPath = httpRequest.getServletPath();
			if (!matchesPassthrough(requestedPath)) {
				InputStream in = request.getServletContext().getResourceAsStream(requestedPath);
				if (in == null) {
					response.setContentType("text/html");
					in = request.getServletContext().getResourceAsStream("/index.html");
					if (in != null) {
						ByteStreams.copy(in, response.getOutputStream());
					} else {
						response.getOutputStream().write("No index.html found!".getBytes());
					}
					return;
				}
			}
			chain.doFilter(request, response);
		}
	}

	/**
	 * Checks if the requestedURL has a (case-insensitive) prefix that is configured
	 * in passThroughPrefix.
	 * 
	 * @param requestedURL the requested URL
	 * @return true, if the requested URL is in the configured passThroughPrefix
	 *         list.
	 */
	private boolean matchesPassthrough(String requestedURL) {
		String trimmedURL = requestedURL.toLowerCase().trim();
		for (String e : passThroughPrefixes) {
			if (trimmedURL.startsWith(e)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public void destroy() {
	}

}
