/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.application.logging;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import org.eclipse.mdm.logging.AuditLoggerFactory;
import org.eclipse.mdm.logging.EcsEventCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
@PreMatching
public class AuditLoggingRequestFilter extends AuditLogging implements ContainerRequestFilter {

	private static final Logger LOG = LoggerFactory.getLogger(AuditLoggingRequestFilter.class);
	@Context
	private SecurityContext securityContext;

	@Override
	public void filter(ContainerRequestContext ctx) throws IOException {
		if (isRelevant(ctx)) {
			new AuditLoggerFactory("rest-api-call", EcsEventCategory.Api, evaluateHttpVerbType(ctx)).start()
					.log((m) -> LOG.info(m, "API call {}", ctx.getUriInfo().getPath()));
		}
	}
}