/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.property;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Producer for injectable properties.
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@ApplicationScoped
public class GlobalPropertyService {

	private static final Logger LOG = LoggerFactory.getLogger(GlobalPropertyService.class);

	public static final String PROPERTY_CONFIG_PATH = "org.eclipse.mdm.configPath";
	private static final String COMPONENT_CONFIG_FOLDER = "org.eclipse.mdm.property";
	private static final String GLOBAL_PROPERTIES_FILENAME = "global.properties";

	private Properties globalProperties = new Properties();

	/**
	 * Reads the properties from the configuration file.
	 */
	@PostConstruct
	public void readProperties() {

		try (InputStream in = getPropertyConfiguration()) {
			this.globalProperties.load(in);
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			throw new GlobalPropertyException(e.getMessage(), e);
		}
	}

	/**
	 * Producer method for global properties.
	 * 
	 * @param ip The injection point.
	 * @return The global property value.
	 * @throws GlobalPropertyException Thrown if the property is not set.
	 */
	@Produces
	@GlobalProperty
	public String getGlobalPropertyValue(InjectionPoint ip) throws GlobalPropertyException {
		GlobalProperty property = ip.getAnnotated().getAnnotation(GlobalProperty.class);
		String propertyName = property.value();
		if (!globalProperties.containsKey(propertyName)) {
			LOG.warn("global property with name '{}' not found!", propertyName);
			return "";
		}
		return globalProperties.getProperty(propertyName);
	}

	/**
	 * Producer method for a global property map.
	 * 
	 * @param ip The injection point.
	 * @return A map with the global properties
	 */
	@Produces
	@GlobalProperty
	public Map<String, String> getGlobalPropertyMap(InjectionPoint ip) {
		Map<String, String> map = new HashMap<>();
		for (String key : globalProperties.stringPropertyNames()) {
			map.put(key, globalProperties.getProperty(key));
		}
		return map;
	}

	private static InputStream getPropertyConfiguration() throws FileNotFoundException {
		String configRoot = System.getProperty(PROPERTY_CONFIG_PATH, ".");
		LOG.info("found system property {} with value {}", PROPERTY_CONFIG_PATH, configRoot);

		File componentConfigFolder = new File(configRoot, COMPONENT_CONFIG_FOLDER);
		if (!componentConfigFolder.exists() || !componentConfigFolder.isDirectory()) {
			throw new GlobalPropertyException("property configuration folder  at '"
					+ componentConfigFolder.getAbsolutePath() + "' does not exist! No properties available!");
		}
		File globalConfigFile = new File(componentConfigFolder, GLOBAL_PROPERTIES_FILENAME);
		if (!globalConfigFile.exists()) {
			throw new GlobalPropertyException("property configuration file for global properties at '"
					+ globalConfigFile.getAbsolutePath() + "' does not exist! no global properties available");
		}

		LOG.info("loading global properties from file: {}", globalConfigFile);
		return new FileInputStream(globalConfigFile);
	}
}