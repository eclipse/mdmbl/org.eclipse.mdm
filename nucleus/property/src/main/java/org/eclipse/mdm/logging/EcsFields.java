/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.logging;

public final class EcsFields {

	public static final String CLIENT_IP = "client.ip";

	public static final String EVENT_ACTION = "event.action";
	public static final String EVENT_CATEGORY = "event.category";
	public static final String EVENT_KIND = "event.kind";
	public static final String EVENT_OUTCOME = "event.outcome";
	public static final String EVENT_TYPE = "event.type";

	public static final String RELATED_IP = "related.ip";
	public static final String RELATED_USER = "related.user";
	
	public static final String TRACE_ID = "trace.id";
	
	
	public static final String USER_NAME = "user.name";

	/**
	 * Hides implicit  public constructor 
	 */
	private EcsFields() {
		// intentionally empty
	}
}
