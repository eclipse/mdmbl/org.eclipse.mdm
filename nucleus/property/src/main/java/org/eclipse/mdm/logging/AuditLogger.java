/********************************************************************************
 * Copyright (c) 2024 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.logging;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Johannes Stamm, Peak Solution GmbH
 */
public class AuditLogger {

	private static final Marker AUDIT = MarkerFactory.getMarker("audit");

	private String action;
	private EcsEventKind kind;
	private Set<EcsEventCategory> category;
	private Set<EcsEventType> type;
	private EcsEventOutcome outcome;

	AuditLogger(String action, EcsEventKind kind, Set<EcsEventCategory> category, Set<EcsEventType> type,
			EcsEventOutcome outcome) {
		this.action = action;
		this.kind = kind;
		this.category = category;
		this.type = type;
		this.outcome = outcome;
	}
    
    public void log(Consumer<Marker> logging) {
		try (EcsMdcContext ecs = new EcsMdcContext(this)) {
			logging.accept(AUDIT);
		}
    }

	/**
	 * @return the action
	 */
	String getAction() {
		return action;
	}

	/**
	 * @return the kind
	 */
	String getKind() {
		return kind.getValue();
	}

	/**
	 * @return the category
	 */
	String getCategory() {
		return serialize(category);
	}

	/**
	 * @return the type
	 */
	String getType() {
		return serialize(type);
	}

	/**
	 * @param type the type to add
	 */
	void addType(EcsEventType... type) {
		if (this.type == null) {
			this.type = new HashSet<>();
		}
		this.type.addAll(Arrays.asList(type));
	}

	/**
	 * @return the outcome
	 */
	String getOutcome() {
		return outcome.getValue();
	}


	private <T extends EcsEventCategorization> String serialize(Set<T> t) {
		String json = null;
		if (t != null) {
			try {
				json = new ObjectMapper().writeValueAsString(t);
			} catch (JsonProcessingException e) {
				json = t.stream().map(EcsEventCategorization::getValue)
						.collect(Collectors.joining("\",\"", "[\"", "\"]"));
			}
		}
		return json;
	}
}
