/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.eclipse.mdm.importscheduler.state.RunningImport;
import org.eclipse.mdm.importscheduler.state.RunningImportHandler;

/**
 * @author akn
 *
 */
public class TestRunningImportHandler implements RunningImportHandler {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.state.RunningImportHandler#
	 * getRunningImports()
	 */
	@Override
	public List<RunningImport> getRunningImports() {
		return Collections.emptyList();
	}

	@Override
	public void removeRunningImportOfDirectory(String directory) {

	}

	@Override
	public boolean insertRunningImport(String directory, int maxImports) {
		return true;
	}

	@Override
	public List<RunningImport> getRunningImportsByHostName(String hostName) {
		return Collections.emptyList();
	}

	@Override
	public List<RunningImport> getRunningImportsOlderThan(Date timestamp) {
		return new ArrayList<RunningImport>();
	}

	@Override
	public void removeRunningImportsOfHostName(String hostName) {
	}

	@Override
	public void removeRunningImportsOlderThen(Date timestamp) {
	}

	@Override
	public boolean insertRunningImport(String directory, String targetService, int maxImports) {
		return false;
	}

	@Override
	public List<RunningImport> getRunningImportsByHostName(String hostName, String targetService) {
		return Collections.emptyList();
	}

	@Override
	public List<RunningImport> getRunningImportsOlderThan(Date timestamp, String targetService) {
		return Collections.emptyList();
	}

	@Override
	public void removeRunningImportsOfHostName(String hostName, String targetService) {

	}

	@Override
	public void removeRunningImportsOlderThen(Date timestamp, String targetService) {

	}

}
