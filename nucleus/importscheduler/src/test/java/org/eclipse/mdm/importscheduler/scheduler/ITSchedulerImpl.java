/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicLong;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mockito;

@Ignore // test is flacky in eclipse infra
public class ITSchedulerImpl {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	private static final String STATE_FILE = "state.file";
	private AtomicLong jobId = new AtomicLong();

	private SchedulerImpl getScheduler(File folder, RunnableJob importJob) {

		return new SchedulerImpl(folder, STATE_FILE, 1L, 1, jobId, new TestRunningImportHandler(), 30, "MDM",
				"test@invalid", importJob);
	}

	@Test
	public void testNormalJob() throws Exception {
		RunnableJob importJob = Mockito.mock(RunnableJob.class);

		SchedulerImpl schedulerImpl = getScheduler(folder.getRoot(), importJob);

		try {
			File jobFolder = folder.newFolder("firstImport");
			Path stateFile = jobFolder.toPath().resolve(STATE_FILE);

			Files.write(stateFile, Arrays.asList("SCHEDULED"));

			Thread.sleep(200L);
			verify(importJob, times(1)).execute(any());

			assertThat(Files.readAllLines(stateFile).get(0)).isEqualTo("DONE");
		} finally {
			schedulerImpl.stopNow();
		}
	}

	@Test
	public void testFailedJob() throws Exception {
		RunnableJob importJob = Mockito.mock(RunnableJob.class);
		doThrow(new RuntimeException("Excepected Test Error")).when(importJob).execute(any());

		SchedulerImpl schedulerImpl = getScheduler(folder.getRoot(), importJob);

		try {
			File jobFolder = folder.newFolder("firstImport");
			Path stateFile = jobFolder.toPath().resolve(STATE_FILE);

			Files.write(stateFile, Arrays.asList("SCHEDULED"));

			Thread.sleep(500L);
			verify(importJob, times(1)).execute(any());

			assertThat(Files.readAllLines(stateFile)).startsWith("FAILED", "Excepected Test Error",
					"java.lang.RuntimeException: Excepected Test Error");
		} finally {
			schedulerImpl.stopNow();
		}
	}

	@Test
	public void testStartingWithExistingEmptyStateFile() throws Exception {
		RunnableJob importJob = mock(RunnableJob.class);

		File jobFolder = folder.newFolder("firstImport");

		SchedulerImpl schedulerImpl = getScheduler(folder.getRoot(), importJob);

		try {
			Path stateFile = jobFolder.toPath().resolve(STATE_FILE);
			Files.write(stateFile, Arrays.asList(""));

			Thread.sleep(200L);
			verify(importJob, never()).execute(any());

			Files.write(stateFile, Arrays.asList("SCHEDULED"));
			Thread.sleep(200);
			verify(importJob, times(1)).execute(any());
		} finally {
			schedulerImpl.stopNow();
		}
	}

	@Test
	public void testStartingWithExistingStateFile() throws Exception {
		RunnableJob importJob = mock(RunnableJob.class);

		File jobFolder = folder.newFolder("firstImport");
		Path stateFile = jobFolder.toPath().resolve(STATE_FILE);

		Files.write(stateFile, Arrays.asList("SCHEDULED"));

		SchedulerImpl schedulerImpl = getScheduler(folder.getRoot(), importJob);

		try {
			Thread.sleep(200L);
			verify(importJob, times(1)).execute(any());
		} finally {
			schedulerImpl.stopNow();
		}
	}

	@Test
	public void testDeleteStateFile() throws Exception {
		RunnableJob importJob = mock(RunnableJob.class);

		File jobFolder1 = folder.newFolder("firstImport");
		Path stateFile1 = jobFolder1.toPath().resolve(STATE_FILE);
		Files.write(stateFile1, Arrays.asList("SCHEDULED"));

		File jobFolder2 = folder.newFolder("secondImport");
		Path stateFile2 = jobFolder2.toPath().resolve(STATE_FILE);
		Files.write(stateFile2, Arrays.asList("SCHEDULED"));

		SchedulerImpl schedulerImpl = getScheduler(folder.getRoot(), importJob);

		try {
			Thread.sleep(200);
			assertEquals(2, schedulerImpl.getDoneJobs().size());

			stateFile1.toFile().delete();
			Thread.sleep(200);
			assertEquals(1, schedulerImpl.getDoneJobs().size());
		} finally {
			schedulerImpl.stopNow();
		}

	}

	@Test
	public void testStartingWithStateFileInSubDirectory() throws Exception {
		RunnableJob importJob = mock(RunnableJob.class);

		File jobFolder = folder.newFolder("parentFolder", "child");

		Path stateFile = jobFolder.toPath().resolve(STATE_FILE);

		Files.write(stateFile, Arrays.asList("SCHEDULED"));

		SchedulerImpl schedulerImpl = getScheduler(folder.getRoot(), importJob);

		try {
			Thread.sleep(200L);
			verify(importJob, times(1)).execute(any());
		} finally {
			schedulerImpl.stopNow();
		}
	}

	@Test
	public void testStateFileChanged() throws Exception {
		RunnableJob importJob = mock(RunnableJob.class);

		File jobFolder = folder.newFolder("firstImport");
		Path stateFile = jobFolder.toPath().resolve(STATE_FILE);
		Files.write(stateFile, Arrays.asList("FAILED"));

		SchedulerImpl schedulerImpl = getScheduler(folder.getRoot(), importJob);
		try {
			Thread.sleep(200);
			assertEquals(1, schedulerImpl.getFailedJobs().size());
			assertEquals(0, schedulerImpl.getDoneJobs().size());

			Files.write(stateFile, Arrays.asList("SCHEDULED"));
			Thread.sleep(200);
			assertEquals(0, schedulerImpl.getFailedJobs().size());
			assertEquals(1, schedulerImpl.getDoneJobs().size());
		} finally {
			schedulerImpl.stopNow();
		}
	}
}
