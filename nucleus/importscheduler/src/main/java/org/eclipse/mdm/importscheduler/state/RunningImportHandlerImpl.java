/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.state;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.eclipse.mdm.importscheduler.ImportScheduler;
import org.eclipse.mdm.importscheduler.scheduler.JobInfo;
import org.eclipse.mdm.importscheduler.scheduler.JobState;
import org.eclipse.mdm.importscheduler.scheduler.SchedulerException;
import org.eclipse.mdm.importscheduler.scheduler.util.StateFileHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class RunningImportHandlerImpl implements RunningImportHandler {
	private static final Logger LOG = LoggerFactory.getLogger(RunningImportHandlerImpl.class);

	@PersistenceContext(unitName = "openMDM")
	private EntityManager em;

	@Override
	public boolean insertRunningImport(String directory, int maxImports) {
		long start = System.currentTimeMillis();
		LOG.info("Start inserting RunningImport directory: {}, maxImports: {} ", directory, maxImports);

		String hostName = getHostName();

		String sql = "insert into running_imports(directory, hostname, timestamp) select '" + directory + "', '"
				+ hostName + "', current_timestamp from dual" + " where '" + directory
				+ "' not in (select directory from running_imports) and" + " (select count(*) from running_imports) < "
				+ maxImports;
		int countRows = em.createNativeQuery(sql).executeUpdate();

		boolean returnVal = countRows > 0;

		if (returnVal) {
			LOG.info("Finished inserting RunningImport successful. Directory: {} duration {} seconds", directory,
					(System.currentTimeMillis() - start) / 1000);
		} else {
			LOG.info(
					"Finished inserting RunningImport unsuccessful. MaxImports({}) reached! Directory: {} duration {} seconds",
					maxImports, directory, (System.currentTimeMillis() - start) / 1000);
		}

		return returnVal;
	}

	private String getHostName() {
		String hostName;
		try {
			hostName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			hostName = "unknown";
		}
		return hostName;
	}

	@Override
	public boolean insertRunningImport(String directory, String targetService, int maxImports) {
		long start = System.currentTimeMillis();
		LOG.info("Start inserting RunningImport directory: {}, targetService: {}, maxImports: {} ", directory,
				targetService, maxImports);

		String hostName = getHostName();

		String sql = "insert into running_imports(directory, hostname, target_service, timestamp) select '" + directory
				+ "', '" + hostName + "', '" + targetService + "', current_timestamp from dual" + " where '" + directory
				+ "' not in (select directory from running_imports) and"
				+ " (select count(*) from running_imports where target_service = '" + targetService + "') < "
				+ maxImports;
		int countRows = em.createNativeQuery(sql).executeUpdate();

		boolean returnVal = countRows > 0;

		if (returnVal) {
			LOG.info(
					"Finished inserting RunningImport successful. Directory: {}, targetService {}, duration {} seconds",
					directory, targetService, (System.currentTimeMillis() - start) / 1000);
		} else {
			LOG.info(
					"Finished inserting RunningImport unsuccessful. MaxImports({}) reached! Directory: {}, targetService {},  duration {} seconds",
					maxImports, directory, targetService, (System.currentTimeMillis() - start) / 1000);
		}

		return returnVal;
	}

	@Override
	public List<RunningImport> getRunningImports() {
		return em.createQuery("SELECT r FROM RunningImport r", RunningImport.class).getResultList();
	}

	@Override
	public void removeRunningImportOfDirectory(String directory) {
		RunningImport runningImport = getRunningImportByDirectory(directory);

		if (runningImport != null) {
			removeRunningImport(runningImport);
		}
	}

	public void removeRunningImport(RunningImport runningImport) {

		Path p = Paths.get(runningImport.getDirectory(), ImportScheduler.STATE_FILE);
		if (Files.exists(p)) {
			try {
				JobInfo jobInfo = StateFileHelper.getJobInfoFromFile(0, new File(runningImport.getDirectory()),
						p.toFile(), null);
				if (JobState.RUNNING.equals(jobInfo.getJobState())) {
					StateFileHelper.writeScheduleFile(p.toFile(), JobState.SCHEDULED.getValue(),
							jobInfo.getProperties(), "", Optional.empty(), null);
				}
			} catch (SchedulerException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}
		}

		em.remove(runningImport);
	}

	private RunningImport getRunningImportByDirectory(String directory) {
		RunningImport returnVal = null;

		try {
			returnVal = em
					.createQuery("SELECT r FROM RunningImport r where r.directory = :directory", RunningImport.class)
					.setParameter("directory", directory).getSingleResult();
		} catch (NoResultException e) {
			returnVal = null;
		}

		return returnVal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.state.RunningImportHandler#
	 * getRunningImportsByClusterName(java.lang.String)
	 */
	@Override
	public List<RunningImport> getRunningImportsByHostName(String hostName) {
		return em.createQuery("SELECT r FROM RunningImport r where r.hostName = :hostName", RunningImport.class)
				.setParameter("hostName", hostName).getResultList();

	}

	@Override
	public List<RunningImport> getRunningImportsByHostName(String hostName, String targetService) {
		return em.createQuery(
				"SELECT r FROM RunningImport r where r.hostName = :hostName and r.targetService = :targetService",
				RunningImport.class).setParameter("hostName", hostName).setParameter("targetService", targetService)
				.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.state.RunningImportHandler#
	 * getRunningImportsOlderThen(java.util.Date)
	 */
	@Override
	public List<RunningImport> getRunningImportsOlderThan(Date timestamp) {
		return em.createQuery("SELECT r FROM RunningImport r where r.timestamp < :timestamp", RunningImport.class)
				.setParameter("timestamp", timestamp).getResultList();
	}

	@Override
	public List<RunningImport> getRunningImportsOlderThan(Date timestamp, String targetService) {
		return em.createQuery(
				"SELECT r FROM RunningImport r where r.timestamp < :timestamp and r.targetService = :targetService",
				RunningImport.class).setParameter("timestamp", timestamp).setParameter("targetService", targetService)
				.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.state.RunningImportHandler#
	 * removeRunningImportsOfHostName(java.lang.String)
	 */
	@Override
	public void removeRunningImportsOfHostName(String hostName) {
		for (RunningImport runningImport : getRunningImportsByHostName(hostName)) {
			removeRunningImport(runningImport);
		}

	}

	@Override
	public void removeRunningImportsOfHostName(String hostName, String targetService) {
		for (RunningImport runningImport : getRunningImportsByHostName(hostName, targetService)) {
			removeRunningImport(runningImport);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.importscheduler.state.RunningImportHandler#
	 * removeRunningImportsOlderThen(java.util.Date)
	 */
	@Override
	public void removeRunningImportsOlderThen(Date timestamp) {
		for (RunningImport runningImport : getRunningImportsOlderThan(timestamp)) {
			removeRunningImport(runningImport);
		}
	}

	@Override
	public void removeRunningImportsOlderThen(Date timestamp, String targetService) {
		for (RunningImport runningImport : getRunningImportsOlderThan(timestamp, targetService)) {
			removeRunningImport(runningImport);
		}
	}

}
