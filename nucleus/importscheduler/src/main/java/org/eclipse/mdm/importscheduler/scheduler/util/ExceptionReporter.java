/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.google.common.base.Throwables;

/**
 * Exception reporter class
 */
public final class ExceptionReporter {

	private static Map<String, Long> exceptionCache = new HashMap<>();

	private static final String PROPERTY_SCHEDULER_URL = "org.eclipse.mdm.importscheduler.schedulerWebURL";

	/**
	 * Send an email to the defined recipient with an additional throwable as cause
	 *
	 * @param origin     The origin of the code
	 * @param throwable  an additional throwable which will be written into the
	 *                   email body
	 * @param directory  The job directory
	 * @param importFile Optional list of atfx files
	 * @param recipient  TODO
	 */
	public static void sendMail(String origin, Optional<Throwable> throwable, File directory, File importFile,
			String recipient) {
		if (throwable.isPresent()) {

			// check if this exception has already been sent, this may be the case from
			// StateFileHelper.writeJobState
			// depending on the code origin
			String key = throwable.map(Throwables::getRootCause).get().toString()
					+ throwable.map(Throwables::getRootCause).hashCode();
			if (exceptionCache.containsKey(key) && System.currentTimeMillis() - 5000L < exceptionCache.get(key)) {
				return;
				// we already sent this exception in the last 5 seconds
			}
			exceptionCache.put(key, System.currentTimeMillis());
			// clean entries older than 5 seconds
			exceptionCache.entrySet()
					.removeIf(stringLongEntry -> stringLongEntry.getValue() < System.currentTimeMillis() - 5000L);

			Map<String, Object> dataModel = new HashMap<>();
			dataModel.put("exception", throwable.map(Throwables::getRootCause).get().toString());
			dataModel.put("origin", origin);
			List<String> errorStack = new ArrayList<>();
			for (StackTraceElement stack : throwable.get().getStackTrace()) {
				if (stack.getClassName().startsWith("org.eclipse")) {
					errorStack.add(stack.toString());
					// only get the first 5 stack traces
					if (errorStack.size() > 5) {
						break;
					}
				}
			}
			if (errorStack.size() > 0) {
				dataModel.put("codeDetails", errorStack);
			}
			dataModel.put("schedulerUrl", System.getProperty(PROPERTY_SCHEDULER_URL));

			if (directory != null) {
				dataModel.put("path", directory.getAbsolutePath());
			}

			List<String> files = new ArrayList<>();
			if (importFile != null) {
				files.add(importFile.getName());
			}

			if (!files.isEmpty()) {
				dataModel.put("files", files);
			}

			dataModel.put("environment",
					System.getProperty("org.eclipse.mdm.importscheduler.targetService", System.getProperty("MDM")));

			String message = FreemarkerTemplate.getInstance().useTemplate(FreemarkerTemplate.TPL_IMPORT_EXCEPTIONS,
					dataModel);
			EmailService.getInstance().sendMail("Fehler beim Import-Scheduler", message, recipient);
		}
	}

}
