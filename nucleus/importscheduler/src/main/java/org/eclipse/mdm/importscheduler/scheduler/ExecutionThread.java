/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import org.eclipse.mdm.importscheduler.scheduler.util.StateFileHelper;
import org.eclipse.mdm.importscheduler.state.RunningImportHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author akn
 *
 */
public class ExecutionThread extends Thread {

	private static final Logger LOG = LoggerFactory.getLogger(ExecutionThread.class);

	private final JobInfo jobInfo;
	private final RunnableJob runnable;
	private final RunningImportHandler runningImportHandler;
	private final String emailRicipient;

	/**
	 * @param runnable
	 * @param emailRicipient
	 * 
	 */
	public ExecutionThread(JobInfo jobInfo, RunnableJob runnable, RunningImportHandler runningImportHandler,
			String emailRicipient) {
		this.jobInfo = jobInfo;
		this.runnable = runnable;
		this.runningImportHandler = runningImportHandler;
		this.emailRicipient = emailRicipient;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		super.run();

		try {
			jobInfo.setJobState(JobState.RUNNING);
			writeStateFile(jobInfo);
			runnable.execute(jobInfo);
			jobInfo.setJobState(JobState.DONE);
			jobInfo.setInfo("");
		} catch (Exception e) {
			jobInfo.setThrowable(e);
			jobInfo.setInfo(e.getLocalizedMessage());
			jobInfo.setJobState(JobState.FAILED);
		} finally {
			writeStateFile(jobInfo);
			runningImportHandler.removeRunningImportOfDirectory(
					JobObjectHelper.getDirectory(jobInfo.getJobObject()).getAbsolutePath());
		}
	}

	private void writeStateFile(JobInfo jobInfo) {
		try {
			StateFileHelper.writeJobState(jobInfo, emailRicipient);
		} catch (SchedulerException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
	}

}
