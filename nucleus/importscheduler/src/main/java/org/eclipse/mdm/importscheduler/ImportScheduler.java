/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.management.ServiceNotFoundException;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.ApplicationContextFactory;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.dflt.model.SystemParameter;
import org.eclipse.mdm.apicopy.control.ApiCopyException;
import org.eclipse.mdm.apicopy.control.ApiCopyService;
import org.eclipse.mdm.apicopy.control.ApiCopyServiceImpl;
import org.eclipse.mdm.apicopy.control.ApiCopyTask;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.importscheduler.scheduler.ImporterJobObject;
import org.eclipse.mdm.importscheduler.scheduler.JobInfo;
import org.eclipse.mdm.importscheduler.scheduler.JobObjectHelper;
import org.eclipse.mdm.importscheduler.scheduler.RunnableJob;
import org.eclipse.mdm.importscheduler.scheduler.Scheduler;
import org.eclipse.mdm.importscheduler.scheduler.SchedulerException;
import org.eclipse.mdm.importscheduler.scheduler.SchedulerImpl;
import org.eclipse.mdm.importscheduler.scheduler.util.ExceptionReporter;
import org.eclipse.mdm.importscheduler.scheduler.util.StateFileHelper;
import org.eclipse.mdm.importscheduler.state.RunningImportHandlerImpl;
import org.eclipse.mdm.property.GlobalProperty;
import org.eclipse.mdm.property.GlobalPropertyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

@Startup
@Singleton
public class ImportScheduler {

	private static final String PROP_EMAIL_RECIPIENT = "emailRecipient";

	private static final String DEFAULTVALUE_TIMEOUT = "30";

	private static final String DEFAULTVALUE_POLLINGINTERVAL = "10";

	private static final String VALUE_TARGETFROMDIR = "TARGETFROMDIR";

	private static final String PROP_MAX_PARALLEL_IMPORTS = "maxParallelImports";

	private static final String PROP_POLLING_INTERVAL_SECONDS = "pollingIntervalSeconds";

	private static final String PROP_ROOT_DIR = "rootDir";

	private enum IMPORT_TYPE {
		ATFX("atfxfile", ATFXCONTEXTFACTORYCLASSNAME, ".atfx"), CSV("csvFile", CSVCONTEXTFACTORYCLASSNAME, ".csv");

		private final String key;
		private final String contextFactoryClassname;
		private final String suffix;

		private IMPORT_TYPE(String key, String contextFactoryClassname, String suffix) {
			this.key = key;
			this.contextFactoryClassname = contextFactoryClassname;
			this.suffix = suffix;
		}

		/**
		 * @return the key
		 */
		public String getKey() {
			return key;
		}

		/**
		 * @return the contextFactoryClassname
		 */
		public String getContextFactoryClassname() {
			return contextFactoryClassname;
		}

		/**
		 * @return the suffix
		 */
		public String getSuffix() {
			return suffix;
		}
	}

	private static final Logger LOG = LoggerFactory.getLogger(ImportScheduler.class);

	private static final String MAPPING_TESTSTEPTEMPLATE = "TESTSTEPTEMPLATE";
	private static final String MAPPING_TESTTEMPLATE = "TESTTEMPLATE";
	private static final String MAPPING_UNIT = "UNIT";
	private static final String MAPPING_QUANTITY = "QUANTITY";

	private static final ImmutableMap<String, String> PARAS_MAPPINGS = new ImmutableMap.Builder<String, String>()
			.put(MAPPING_QUANTITY, "org.eclipse.mdm.importscheduler.%s.quantitymapping")
			.put(MAPPING_UNIT, "org.eclipse.mdm.importscheduler.%s.unitmapping")
			.put(MAPPING_TESTTEMPLATE, "org.eclipse.mdm.importscheduler.%s.testtemplatemapping")
			.put(MAPPING_TESTSTEPTEMPLATE, "org.eclipse.mdm.importscheduler.%s.teststeptemplatemapping").build();

	public static final String STATE_FILE = "state.file";
	public static final String STATE_FILE_SUFFIX_IMPORT = ".import";
	public static final String STATE_FILE_SUFFIX_DONE = ".done";
	public static final String STATE_FILE_SUFFIX_FAILED = ".failed";

	private static final String PROP_TARGET_SERVICE = "org.eclipse.mdm.importscheduler.targetService";
	private static final String PROP_CSV_PROPERTIES = "org.eclipse.mdm.importscheduler.csvProperties";

	private static final String ROOT_IMPORT_DIR = System.getProperty("org.eclipse.mdm.importscheduler.rootDir");

	private static final String TIMEOUT_RUNNINGIMPORT_MINUTES = System
			.getProperty("org.eclipse.mdm.importscheduler.runningImportTimeoutMinutes", DEFAULTVALUE_TIMEOUT);

	private static final String COMPONENT_CONFIG_FOLDER = "org.eclipse.mdm.importscheduler";
	private static final String DEFAULT_EMAIL_RECIPIENT = System
			.getProperty("org.eclipse.mdm.importscheduler.email.recipient");

	private ConnectorService connectorService;

	private ApiCopyService apiCopyService = new ApiCopyServiceImpl();

	private static final String ATFXCONTEXTFACTORYCLASSNAME = "org.eclipse.mdm.api.atfxadapter.ATFXContextFactory";
	private static final String CSVCONTEXTFACTORYCLASSNAME = "org.eclipse.mdm.csvadapter.CSVApplicationContextFactory";

	List<Scheduler> schedulerList = new ArrayList<>();

	@Inject
	@GlobalProperty
	private Map<String, String> globalProperties = Collections.emptyMap();

	@EJB
	private RunningImportHandlerImpl runningImportHandler;

	public ImportScheduler() {

	}

	@PostConstruct
	public void init() {

		if (Strings.isNullOrEmpty(ROOT_IMPORT_DIR) && Strings.isNullOrEmpty(getConfigDir())) {
			LOG.info(
					"No Import root directory configured (org.eclipse.mdm.importscheduler.rootDir), Importer cannot be initialized!");
		} else {
			initializeImporter();
		}

	}

	private String getConfigDir() {
		String configRoot = System.getProperty(GlobalPropertyService.PROPERTY_CONFIG_PATH, ".");
		File file = new File(configRoot, COMPONENT_CONFIG_FOLDER);
		if (file.exists() && file.isDirectory()) {
			return file.getAbsolutePath();
		}

		return null;
	}

	private void initializeImporter() {
		Map<String, Map<String, String>> importerConfigs = getImporterConfigs();

		Principal principal = new Principal() {

			@Override
			public String getName() {
				return null;
			}
		};

		LOG.debug("Creating connector service.");
		connectorService = new ConnectorService(principal, globalProperties);
		connectorService.connect();

		LOG.info("Initializing import scheduler on path {}.", ROOT_IMPORT_DIR);

		try {
			runningImportHandler.removeRunningImportsOfHostName(InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			LOG.error(e.getLocalizedMessage(), e);
		}
		AtomicLong nextJobId = new AtomicLong(0);
		for (String targetService : importerConfigs.keySet()) {
			Map<String, String> impProperties = importerConfigs.get(targetService);
			String emailRicipient = impProperties.get(PROP_EMAIL_RECIPIENT);
			try {

				int timeOutRunningImportMinutes = Integer.parseInt(TIMEOUT_RUNNINGIMPORT_MINUTES);

				Scheduler scheduler = new SchedulerImpl(new File(impProperties.get(PROP_ROOT_DIR)), STATE_FILE,
						Long.valueOf(impProperties.get(PROP_POLLING_INTERVAL_SECONDS)),
						Integer.valueOf(impProperties.get(PROP_MAX_PARALLEL_IMPORTS)), nextJobId, runningImportHandler,
						timeOutRunningImportMinutes, targetService, emailRicipient, new RunnableJob() {

							@Override
							public void execute(JobInfo jobInfo) {
								ImporterJobObject jobObject = (ImporterJobObject) jobInfo.getJobObject();
								File dir = jobObject.getDirectory();

								File importFile = null;

								if (StateFileHelper.hasStateFileSuffix(jobObject.getStateFile().getName())) {
									importFile = jobObject.getImportFile();
								} else {
									File[] atfxFiles = dir
											.listFiles(n -> n.getName().endsWith(IMPORT_TYPE.ATFX.getSuffix()));
									File[] csvFiles = dir
											.listFiles(n -> n.getName().endsWith(IMPORT_TYPE.CSV.getSuffix()));
									int atfxCount = atfxFiles != null ? atfxFiles.length : 0;
									int csvCount = csvFiles != null ? csvFiles.length : 0;
									if ((atfxCount == 1 && csvCount == 0) || (atfxCount == 0 && csvCount == 1)) {

										if (atfxFiles.length == 1) {
											importFile = atfxFiles[0];
										} else {
											importFile = csvFiles[0];
										}

									} else {
										throw new RuntimeException("Expected one import file!");
									}
								}

								if (importFile != null && importFile.exists()) {
									IMPORT_TYPE impType = null;

									if (importFile.getName().endsWith(IMPORT_TYPE.ATFX.getSuffix())) {
										impType = IMPORT_TYPE.ATFX;
									} else if (importFile.getName().endsWith(IMPORT_TYPE.CSV.getSuffix())) {
										impType = IMPORT_TYPE.CSV;
									} else {
										throw new RuntimeException("Unknown import type!");
									}

									importFile(impType, importFile, jobInfo, targetService, emailRicipient,
											impProperties.get(PROP_ROOT_DIR));

								} else {
									throw new RuntimeException("Expected one import file!");
								}

							}

						});
				schedulerList.add(scheduler);
			} catch (Exception e) {
				ExceptionReporter.sendMail("Execute scheduler", Optional.of(e), null, null, emailRicipient);
				LOG.error("Error initializing import scheduler!", e);
				throw new RuntimeException(e);
			}
		}

	}

	private Map<String, Map<String, String>> getImporterConfigs() {
		Map<String, Map<String, String>> importerConfigs = new HashMap<>();

		String configDir = getConfigDir();
		if (Strings.isNullOrEmpty(configDir)) {
			String targetService = System.getProperty(PROP_TARGET_SERVICE, VALUE_TARGETFROMDIR);
			Map<String, String> propertiesForTarget = importerConfigs.computeIfAbsent(targetService,
					c -> new HashMap<>());
			propertiesForTarget.put(PROP_ROOT_DIR, System.getProperty("org.eclipse.mdm.importscheduler.rootDir"));
			propertiesForTarget.put(PROP_POLLING_INTERVAL_SECONDS, System.getProperty(
					"org.eclipse.mdm.importscheduler.pollingIntervalSeconds", DEFAULTVALUE_POLLINGINTERVAL));
			propertiesForTarget.put(PROP_MAX_PARALLEL_IMPORTS, System.getProperty(
					"org.eclipse.mdm.importscheduler.maxParallelImports", String.valueOf(Integer.MAX_VALUE)));
			propertiesForTarget.put("runningImportTimeoutMinutes", System
					.getProperty("org.eclipse.mdm.importscheduler.runningImportTimeoutMinutes", DEFAULTVALUE_TIMEOUT));
			propertiesForTarget.put(PROP_EMAIL_RECIPIENT, DEFAULT_EMAIL_RECIPIENT);

		} else {
			Pattern propNamePattern = Pattern.compile("(.*)\\.(.*)");

			File configFilePath = new File(configDir, "importscheduler.properties");
			Properties props = new Properties();
			try {
				props.load(new FileInputStream(configFilePath));
			} catch (IOException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}

			for (String propName : props.stringPropertyNames()) {
				String propValue = props.getProperty(propName);

				Matcher m = propNamePattern.matcher(propName);

				if (m.matches()) {
					String targetService = m.group(1);
					String property = m.group(2);

					Map<String, String> propertiesForTarget = importerConfigs.computeIfAbsent(targetService,
							c -> new HashMap<>());
					propertiesForTarget.putIfAbsent(property, propValue);

				}
			}

			// set DefaultValues if property not available
			for (String targetService : importerConfigs.keySet()) {
				Map<String, String> propertiesForTarget = importerConfigs.get(targetService);
				propertiesForTarget.putIfAbsent(PROP_POLLING_INTERVAL_SECONDS, System.getProperty(
						"org.eclipse.mdm.importscheduler.pollingIntervalSeconds", DEFAULTVALUE_POLLINGINTERVAL));
				propertiesForTarget.putIfAbsent(PROP_MAX_PARALLEL_IMPORTS, System.getProperty(
						"org.eclipse.mdm.importscheduler.maxParallelImports", String.valueOf(Integer.MAX_VALUE)));
				propertiesForTarget.putIfAbsent("runningImportTimeoutMinutes", System.getProperty(
						"org.eclipse.mdm.importscheduler.runningImportTimeoutMinutes", DEFAULTVALUE_TIMEOUT));
				propertiesForTarget.putIfAbsent(PROP_EMAIL_RECIPIENT, DEFAULT_EMAIL_RECIPIENT);
			}

		}

		return importerConfigs;
	}

	private void importFile(IMPORT_TYPE type, File file, JobInfo jobInfo, String targetService, String recipient,
			String rootDir) {
		ApplicationContext contextSrc = null;
		ApplicationContext contextDst = null;

		File dir = JobObjectHelper.getDirectory(jobInfo.getJobObject());
		try {
			String targetDatasource;
			if (!VALUE_TARGETFROMDIR.equals(targetService)) {
				targetDatasource = targetService;
			} else {
				targetDatasource = getTargetDatasource(file.getAbsolutePath());
			}
			contextDst = getTargetContext(targetDatasource);

			String extSystemName = null;

			long start = 0;
			String filePath = file.getAbsolutePath();
			long fileSizeBytes = Files.size(Paths.get(file.toURI()));
			LOG.info("Start import of file: {} file size: {}  bytes", filePath, fileSizeBytes);

			start = System.currentTimeMillis();

			extSystemName = getExtSystemName(rootDir, file.getAbsolutePath());
			if (extSystemName == null) {
				extSystemName = "";
			}

			String quantitiyMappingFile = "";

			File f = new File(getConfigDir(), targetService + "_quantityMapping.txt");

			if (f.exists()) {
				quantitiyMappingFile = f.getAbsolutePath();
			}

			Map<String, String> params = getConnectionParameters(targetService, type, file, extSystemName,
					quantitiyMappingFile);
			contextSrc = connectSourceContext(type.getContextFactoryClassname(), file.getName(), params, contextDst);
			LOG.info("Connected to Source ApplicationContext of file: {} file size: {}  bytes in {} seconds", filePath,
					fileSizeBytes, (System.currentTimeMillis() - start) / 1000);
			List<Project> projects = contextSrc.getEntityManager().orElseThrow(() -> new ServiceNotFoundException())
					.loadAll(Project.class);

			Map<String, String> testTemplateMapping = getMapping(contextDst, MAPPING_TESTTEMPLATE, extSystemName);
			Map<String, String> testStepTemplateMapping = getMapping(contextDst, MAPPING_TESTSTEPTEMPLATE,
					extSystemName);

			ApiCopyTask apiCopyTask = apiCopyService.newApiCopyTask(contextSrc, contextDst,
					new MappedTemplateManager(testTemplateMapping, testStepTemplateMapping));
			apiCopyTask.setQuantityMapping(getMapping(contextDst, MAPPING_QUANTITY, extSystemName));
			apiCopyTask.setUnitMapping(getMapping(contextDst, MAPPING_UNIT, extSystemName));
			Map<String, String> properties = jobInfo.getProperties();
			properties.put("overwriteExistingElements", "true");
			apiCopyTask.setProperties(properties);

			LOG.info("Execute ApiCopy with extSystemName '{}' from source '{}' to target '{}' with parameters {}.",
					extSystemName, contextSrc.getAdapterType(), contextDst.getAdapterType(), properties);
			apiCopyTask.copy(projects);
			LOG.info("Finished import of file: {}, file size: {}  bytes, duration: {} seconds", filePath, fileSizeBytes,
					(System.currentTimeMillis() - start) / 1000);

		} catch (ApiCopyException e) {
			ExceptionReporter.sendMail("Execute scheduler", Optional.of(e), dir, file, recipient);
			throw e;
		} catch (Exception e) {
			ExceptionReporter.sendMail("Execute scheduler", Optional.of(e), dir, file, recipient);
			throw new RuntimeException("Error executing ApiCopyTask: " + e.getMessage(), e);
		} finally {
			closeContext(contextSrc);
			closeContext(contextDst);
		}
	}

	private Map<String, String> getConnectionParameters(String targetService, IMPORT_TYPE type, File file,
			String extSystemName, String quantitiyMappingFile) {
		Map<String, String> parameters = new HashMap<>();
		parameters.put(type.getKey(), file.getAbsolutePath());
		parameters.put("freetext.active", "false");
		parameters.put("extSystemName", extSystemName);
		parameters.put("quantitymapping", quantitiyMappingFile);

		File csvPropertiesFile = null;
		if (Strings.isNullOrEmpty(getConfigDir())) {
			String filePath = System.getProperty(PROP_CSV_PROPERTIES);
			if (!Strings.isNullOrEmpty(filePath)) {
				csvPropertiesFile = new File(filePath);
			}

		} else {
			csvPropertiesFile = new File(getConfigDir(), targetService + "_csv.properties");

		}

		if (CSVCONTEXTFACTORYCLASSNAME.equalsIgnoreCase(type.getContextFactoryClassname())
				&& (csvPropertiesFile != null && csvPropertiesFile.exists())) {

			Properties props = new Properties();
			try {
				props.load(new FileInputStream(csvPropertiesFile));
			} catch (IOException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}

			for (String propName : props.stringPropertyNames()) {
				parameters.put(propName, props.getProperty(propName));

			}
		}

		return parameters;
	}

	private void closeContext(ApplicationContext context) {
		if (context != null) {
			try {
				context.close();
			} catch (ConnectionException e) {
				throw new RuntimeException("Could not close context: " + e.getMessage(), e);
			}
		}
	}

	/**
	 * Determines the name of the target data source from the name of the first
	 * subdirectory within the import file path, starting from the root import
	 * directory.
	 * 
	 * @param impFilePath import file path
	 * @return
	 */
	private String getTargetDatasource(String impFilePath) {
		String patternString = new File(ROOT_IMPORT_DIR).getAbsolutePath() + File.separatorChar + "(.+?)"
				+ File.separatorChar + "(.+?)";
		patternString = patternString.replace("\\", "\\\\");

		Pattern p = Pattern.compile(patternString);
		Matcher matcher = p.matcher(impFilePath);

		String returnVal = null;

		if (matcher.matches()) {
			returnVal = matcher.group(1);
		}

		return returnVal;
	}

	/**
	 * Determines the name of the external system from the name of the second
	 * subdirectory within the import file path, starting from the root import
	 * directory.
	 * 
	 * @param impFilePath import file path
	 * @return
	 */
	private String getExtSystemName(String impRootDir, String impFilePath) {
		String patternString = new File(impRootDir).getAbsolutePath() + File.separatorChar + "(.+?)"
				+ File.separatorChar + "(.+?)" + File.separatorChar + "(.+?)";
		patternString = patternString.replace("\\", "\\\\");

		Pattern p = Pattern.compile(patternString);
		Matcher matcher = p.matcher(impFilePath);

		String returnVal = null;

		if (matcher.matches()) {
			returnVal = matcher.group(2);
		}

		return returnVal;
	}

	private Map<String, String> getMapping(ApplicationContext contextDst, String mappingType, String extSystemName) {
		Map<String, String> mapping = new HashMap<>();

		Optional<EntityManager> entityManager = contextDst.getEntityManager();

		String sysParaName = PARAS_MAPPINGS.get(mappingType);

		if (!Strings.isNullOrEmpty(sysParaName) && entityManager.isPresent()) {
			List<SystemParameter> systemParameters = entityManager.get().loadAll(SystemParameter.class,
					String.format(sysParaName, extSystemName));

			LOG.debug("Found system parameters for {} mapping for external system '{}': {}", mappingType, extSystemName,
					systemParameters);

			if (!systemParameters.isEmpty()) {
				mapping = splitToMap(systemParameters.get(0).getValue("Value").extract(ValueType.STRING));
			}
		}
		LOG.debug("Loaded {} mapping: {}", mappingType, mapping);
		return mapping;
	}

	private Map<String, String> splitToMap(String input) {
		return Splitter.on(";").withKeyValueSeparator("=").split(input);
	}

	public List<Scheduler> getScheduler() throws SchedulerException {
		if (this.schedulerList == null || schedulerList.isEmpty()) {
			throw new SchedulerException(
					"Scheduler not initialized. Make sure org.eclipse.mdm.importscheduler.rootDir is correctly configured.");
		}
		return this.schedulerList;
	}

	/**
	 * Gets the ApplicationContext with the specified name from ConnectorService.
	 * Executes a test query to check if the context is still valid. If not it tries
	 * to reconnect.
	 * 
	 * @param contextName name of the context
	 * @return
	 */
	private ApplicationContext getTargetContext(String contextName) throws ConnectionException {
		long start = System.currentTimeMillis();

		try {
			ApplicationContext contextDst = connectorService.getContextByName(contextName);

			if (contextDst == null) {
				throw new RuntimeException("Target ApplicationContext with name '" + contextName + "' not found!");
			}
			// simple Query to test if context is valid
			contextDst.getEntityManager().orElseThrow(ServiceNotFoundException::new).load(Project.class,
					Collections.singletonList("0"));
			LOG.info("Getting TargetContext (without reconnect) in {} s ", (System.currentTimeMillis() - start) / 1000);
			return contextDst;
		} catch (Exception e) {
			LOG.warn("Target ApplicationContext seems to be closed. Trying to reconnect.");
			LOG.info("Target ApplicationContext seems to be closed. Received exception:" + e.getMessage(), e);

			Optional<ApplicationContext> c = connectorService.reconnect(contextName);
			if (c.isPresent()) {
				LOG.info("Target ApplicationContext reconnected.");
				LOG.info("Getting TargetContext (with reconnect) in {} s ",
						(System.currentTimeMillis() - start) / 1000);
				return c.get();
			} else {
				LOG.info("Getting TargetContext failed in {} s ", (System.currentTimeMillis() - start) / 1000);
				throw new ConnectionException(
						"Could not reconnect to target ApplicationContext with name '" + contextName + "'!");
			}
		}
	}

	/**
	 * Connects to a {@link ApplicationContext}.
	 * 
	 * @param contextFactoryClassname classname of the
	 *                                {@link ApplicationContextFactory}
	 * @param sourceName
	 * @param parameters              connection parameters
	 * @return connected {@link ApplicationContext}
	 * @throws ConnectionException
	 */
	private ApplicationContext connectSourceContext(String contextFactoryClassname, String sourceName,
			Map<String, String> parameters, ApplicationContext contextDst) throws ConnectionException {
		try {

			ApplicationContextFactory applicationContextFactory = getApplicationContextFactory(contextFactoryClassname);
			Method method = applicationContextFactory.getClass().getMethod("connect", String.class, Map.class,
					ApplicationContext.class);
			return (ApplicationContext) method.invoke(applicationContextFactory, sourceName, parameters, contextDst);
		} catch (Exception e) {
			throw new RuntimeException(
					"failed to initialize entity manager using factory '" + contextFactoryClassname + "'", e);
		}
	}

	private ApplicationContextFactory getApplicationContextFactory(String contextFactoryClassname)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class<? extends ApplicationContextFactory> contextFactoryClass = Thread.currentThread().getContextClassLoader()
				.loadClass(contextFactoryClassname).asSubclass(ApplicationContextFactory.class);

		ApplicationContextFactory contextFactory = contextFactoryClass.newInstance();
		return contextFactory;
	}

}
