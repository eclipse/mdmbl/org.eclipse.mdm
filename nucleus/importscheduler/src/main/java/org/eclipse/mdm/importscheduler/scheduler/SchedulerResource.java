/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler.scheduler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.importscheduler.scheduler.entities.SchedulerEntityResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Implementation of a {@link Scheduler}
 * 
 * @author jkl
 *
 */
@Tag(name = "scheduler")
@Path("/importscheduler")
public class SchedulerResource {

	private static final Logger LOG = LoggerFactory.getLogger(SchedulerResource.class);

	@EJB
	private SchedulerService schedulerService;

	/**
	 * delegates the request to the {@link Scheduler}
	 *
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Checks if import scheduler is active", responses = {
			@ApiResponse(description = "Is Active", content = @Content(schema = @Schema(implementation = Boolean.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/active")
	public boolean isActive() throws SchedulerException {
		return schedulerService.isActive();
	}

	/**
	 * delegates the request to the {@link Scheduler}
	 *
	 * @return the result of the delegated request as {@link Response}
	 * @throws SchedulerException
	 */
	@GET
	@Operation(summary = "Find all jobs", description = "Get list of schedule jobs", responses = {
			@ApiResponse(description = "The Jobs", content = @Content(schema = @Schema(implementation = SchedulerEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/jobs")
	public Response getAllJobs() throws SchedulerException {
		SchedulerEntityResponse response = new SchedulerEntityResponse(schedulerService.getAllJobs());
		GenericEntity<Object> genEntity = new GenericEntity<>(response, response.getClass());
		return Response.ok().entity(genEntity).type(MediaType.APPLICATION_JSON).build();
	}

	/**
	 * delegates the request to the {@link Scheduler}
	 *
	 * @return the result of the delegated request as {@link Response}
	 * @throws SchedulerException
	 */
	@GET
	@Operation(summary = "Count all jobs", description = "Get the number of all scheduled jobs", responses = {
			@ApiResponse(description = "The Job count", content = @Content(schema = @Schema(implementation = SchedulerEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/jobcount")
	public Response getJobCount(@QueryParam("jobstate") String jobState, @QueryParam("stateFile") String stateFile,
			@QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo) throws SchedulerException {
		int jobCount = schedulerService.getJobCount(jobState, stateFile, getDateFromString(dateFrom),
				getDateFromString(dateTo));

		return Response.ok().entity(new GenericEntity<>(jobCount, Integer.class)).type(MediaType.APPLICATION_JSON)
				.build();
	}

	/**
	 * delegates the request to the {@link Scheduler}
	 *
	 * @return the result of the delegated request as {@link Response}
	 * @throws SchedulerException
	 */
	@GET
	@Operation(summary = "Count all jobstates", description = "Get the number of every job state", responses = {
			@ApiResponse(description = "The Job state count", content = @Content(schema = @Schema(implementation = SchedulerEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/statecount")
	public Response getStateCount(@QueryParam("stateFile") String stateFile, @QueryParam("dateFrom") String dateFrom,
			@QueryParam("dateTo") String dateTo) throws SchedulerException {
		StateCount stateInfo = schedulerService.getStateCount(stateFile, getDateFromString(dateFrom),
				getDateFromString(dateTo));

		GenericEntity<Object> genEntity = new GenericEntity<>(stateInfo, stateInfo.getClass());
		return Response.status(Status.OK).entity(genEntity).type(MediaType.APPLICATION_JSON).build();
	}

	private Date getDateFromString(String dateStr) {
		Date returnVal = null;

		if (!Strings.isNullOrEmpty(dateStr)) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
			try {
				returnVal = sdf.parse(dateStr);
			} catch (ParseException e) {
				LOG.error("Date string {} is not parsable! The format have to be yyyyMMddHHmm", dateStr);
			}
		}

		return returnVal;
	}

	/**
	 * delegates the request to the {@link Scheduler}
	 *
	 * @return the result of the delegated request as {@link Response}
	 * @throws SchedulerException
	 * @throws NumberFormatException
	 */
	@GET
	@Operation(summary = "Find all jobs limited to a pagination", description = "Get list of schedule jobs", responses = {
			@ApiResponse(description = "The Jobs", content = @Content(schema = @Schema(implementation = SchedulerEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/pagedjobs/{from}/{to}")
	public Response getPagedJobs(
			@Parameter(description = "The 'from' limiter for the pagination", required = true) @PathParam("from") String from,
			@Parameter(description = "The 'to' limiter for the pagination", required = true) @PathParam("to") String to,
			@QueryParam("jobstate") String jobState, @QueryParam("stateFile") String stateFile,
			@QueryParam("dateFrom") String dateFrom, @QueryParam("dateTo") String dateTo)
			throws NumberFormatException, SchedulerException {
		List<JobInfo> jobs = schedulerService.getPagedJobs(Integer.valueOf(from), Integer.valueOf(to), jobState,
				stateFile, getDateFromString(dateFrom), getDateFromString(dateTo));

		SchedulerEntityResponse response = new SchedulerEntityResponse(jobs);
		GenericEntity<Object> genEntity = new GenericEntity<>(response, response.getClass());
		return Response.ok().entity(genEntity).type(MediaType.APPLICATION_JSON).build();
	}

	/**
	 * delegates the request to the {@link Scheduler}
	 *
	 * @return the result of the delegated request as {@link Response}
	 * @throws SchedulerException
	 * @throws NumberFormatException
	 */
	@GET
	@Operation(summary = "Find a specific job", description = "Get a single scheduled job", responses = {
			@ApiResponse(description = "The Jobs", content = @Content(schema = @Schema(implementation = SchedulerEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/jobbyid/{id}")
	public Response getJobById(@Parameter(description = "The job id", required = true) @PathParam("id") String id)
			throws NumberFormatException, SchedulerException {
		SchedulerEntityResponse response = new SchedulerEntityResponse(
				Arrays.asList(schedulerService.getJobById(Integer.valueOf(id))));
		GenericEntity<Object> genEntity = new GenericEntity<>(response, response.getClass());
		return Response.ok().entity(genEntity).type(MediaType.APPLICATION_JSON).build();
	}

	/**
	 * Returns the status of the reschedule
	 *
	 * @param jobId The job id to reschedule
	 * @return status of the rescheduling as {@link Response}.
	 * @throws SchedulerException
	 * @throws NumberFormatException
	 */
	@PUT
	@Operation(summary = "Reschedule a job from failed to scheduled", responses = {
			@ApiResponse(description = "The status of the rescheduling", content = @Content(schema = @Schema(implementation = SchedulerEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response rescheduleJob(
			@Parameter(description = "ID of the job to reschedule", required = true) @PathParam("id") String jobId,
			String body) throws NumberFormatException, SchedulerException {

		schedulerService.rescheduleJob(Long.valueOf((jobId)));

		return Response.ok().type(MediaType.APPLICATION_JSON).build();
	}

	/**
	 * Returns the status of the reschedule
	 *
	 * @param jobId The job id to reschedule
	 * @return status of the rescheduling as {@link Response}.
	 * @throws SchedulerException
	 * @throws NumberFormatException
	 */
	@POST
	@Operation(summary = "Reschedule all failed jobs of the last days", responses = {
			@ApiResponse(description = "The status of the rescheduling", content = @Content(schema = @Schema(implementation = SchedulerEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/reschedule/{days}")
	public Response rescheduleJobOfDays(
			@Parameter(description = "ID of the job to reschedule", required = true) @PathParam("days") String days,
			String body) throws NumberFormatException, SchedulerException {

		schedulerService.rescheduleJobOfLastDays(Long.valueOf((days)));

		return Response.ok().type(MediaType.APPLICATION_JSON).build();

	}

	/**
	 * Returns the status of the deletion
	 *
	 * @param jobId The job id to delete
	 * @return status of the deletion as {@link Response}.
	 * @throws SchedulerException
	 * @throws NumberFormatException
	 */
	@DELETE
	@Operation(summary = "Delete a job from the list of jobs", responses = {
			@ApiResponse(description = "The status of the deletion", content = @Content(schema = @Schema(implementation = SchedulerEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response deleteJob(
			@Parameter(description = "ID of the job to delete", required = true) @PathParam("id") String jobId)
			throws NumberFormatException, SchedulerException {

		schedulerService.deleteJob(Long.valueOf((jobId)));

		return Response.ok().type(MediaType.APPLICATION_JSON).build();
	}
}
