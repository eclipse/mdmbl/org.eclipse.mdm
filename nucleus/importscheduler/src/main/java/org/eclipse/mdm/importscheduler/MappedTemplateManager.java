/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.importscheduler;

import java.util.Map;

import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.apicopy.control.ApiCopyException;
import org.eclipse.mdm.apicopy.control.DefaultTemplateManager;
import org.eclipse.mdm.apicopy.control.TemplateManager;

/**
 * This Implementation of {@link TemplateManager} will map the test/teststep
 * template name from the source datasource to the name of the target datasource
 * 
 * @author akn
 *
 */
public class MappedTemplateManager extends DefaultTemplateManager {

	private final Map<String, String> testTemplateMapping;
	private final Map<String, String> testStepTemplateMapping;

	/**
	 * 
	 * @param testTemplateMapping
	 * @param testStepTemplateMapping
	 */
	public MappedTemplateManager(Map<String, String> testTemplateMapping, Map<String, String> testStepTemplateMapping) {
		this.testTemplateMapping = testTemplateMapping;
		this.testStepTemplateMapping = testStepTemplateMapping;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.mdm.apicopy.control.DefaultTemplateManager#getTemplateTestName(
	 * org.eclipse.mdm.api.base.model.Test)
	 */
	@Override
	public String getTemplateTestName(Test test) throws ApiCopyException {

		String defaultName = super.getTemplateTestName(test);
		return testTemplateMapping.getOrDefault(defaultName, defaultName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.mdm.apicopy.control.DefaultTemplateManager#
	 * getTemplateTestStepName(org.eclipse.mdm.api.base.model.TestStep)
	 */
	@Override
	public String getTemplateTestStepName(TestStep testStep) throws ApiCopyException {

		String defaultName = super.getTemplateTestStepName(testStep);
		return testStepTemplateMapping.getOrDefault(defaultName, defaultName);
	}

}
