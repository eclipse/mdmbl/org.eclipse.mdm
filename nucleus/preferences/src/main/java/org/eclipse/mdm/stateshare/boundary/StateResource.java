/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.stateshare.boundary;

import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.mdm.stateshare.controller.StateService;
import org.eclipse.mdm.stateshare.entity.StateShare;
import org.eclipse.mdm.stateshare.entity.StateShareDTO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "State")
@Path("/states")
@Produces(MediaType.APPLICATION_JSON)
public class StateResource {

	@EJB
	private StateService stateService;

	@GET
	@Path("/{stateId}")
	@Operation(summary = "Loads an existing component state by ID.", description = "Loads an existing component state by ID.", responses = {
			@ApiResponse(description = "The component state", content = @Content(schema = @Schema(implementation = StateShare.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	public StateShareDTO getStateById(
			@Parameter(description = "ID of the component state to load", required = true) @PathParam("stateId") long stateId) {
		return convert(this.stateService.getStateById(stateId));
	}

	@POST
	@Operation(summary = "Creates a new component state for the current user.", description = "Creates a new component state for the current user.", responses = {
			@ApiResponse(description = "The created component state.") })
	public StateShareDTO createComponentState(StateShareDTO state) {
		return convert(this.stateService.createState(state.getName(), state.getState()));
	}

	@DELETE
	@Path("/{stateId}")
	@Operation(summary = "Deletes an exsiting component state for the current user.", description = "Deletes an exsiting component state for the current user.", responses = {
			@ApiResponse(description = "The deleted component state") })
	public StateShareDTO deleteComponentState(
			@Parameter(description = "ID of the component state to load", required = true) @PathParam("stateId") long stateId) {
		return convert(this.stateService.removeState(stateId));
	}

	private StateShareDTO convert(StateShare state) {
		return new StateShareDTO(state.getId(), state.getName(), state.getState());
	}

}
