/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apitoken.entity;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.google.common.base.MoreObjects;

@Entity
@Table(name = "API_TOKEN", uniqueConstraints = { @UniqueConstraint(columnNames = { "USERNAME", "TOKEN_NAME" }),
		@UniqueConstraint(columnNames = { "TOKEN_USER" }) })
@Cacheable(false)
public class UserApiToken {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "API_TOKEN_ID")
	private long id;

	@Column(name = "USERNAME")
	private String username;

	@Column(name = "TOKEN_NAME")
	private String name;

	@Column(name = "TOKEN_USER")
	private String tokenUser;

	@Column(name = "HASHED_TOKEN")
	private String hashedToken;

	@Column(name = "CREATED_AT")
	private Date created;

	@ElementCollection
	@CollectionTable(name = "API_TOKEN_GROUP", joinColumns = @JoinColumn(name = "TOKEN_USER", referencedColumnName = "TOKEN_USER"))
	@Column(name = "GROUP_NAME")
	private Set<String> groups;

	@SuppressWarnings("unused")
	private UserApiToken() {

	}

	public UserApiToken(String name, String hashedToken, String username, Set<String> groups) {
		super();
		this.name = name;
		this.tokenUser = username + "." + name;
		this.hashedToken = hashedToken;
		this.username = username;
		this.created = new Date();
		this.groups = groups;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the tokenUser
	 */
	public String getTokenUser() {
		return tokenUser;
	}

	/**
	 * @return the apiToken
	 */
	public String getHashedToken() {
		return hashedToken;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @return the groups
	 */
	public Set<String> getGroups() {
		return groups;
	}

	@Override
	public boolean equals(Object obj) {

		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		final UserApiToken other = (UserApiToken) obj;
		return Objects.equals(this.id, other.id) && Objects.equals(this.username, other.username)
				&& Objects.equals(this.name, other.name) && Objects.equals(this.tokenUser, other.tokenUser)
				&& Objects.equals(this.hashedToken, other.hashedToken) && Objects.equals(this.created, other.created)
				&& Objects.equals(this.groups, other.groups);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, username, name, tokenUser, hashedToken, created, groups);
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(UserApiToken.class).add("id", id).add("username", username).add("name", name)
				.add("tokenUser", tokenUser).add("tokenUser", tokenUser).add("hashedToken", hashedToken)
				.add("created", created).add("groups", groups).toString();
	}
}
