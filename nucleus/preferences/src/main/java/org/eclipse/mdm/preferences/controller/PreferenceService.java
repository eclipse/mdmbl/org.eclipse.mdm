/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.preferences.controller;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.security.DeclareRoles;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import org.eclipse.mdm.preferences.entity.Preference;
import org.eclipse.mdm.preferences.entity.PreferenceMessage;
import org.eclipse.mdm.preferences.entity.PreferenceMessage.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * 
 * @author Johannes Stamm, Peak Solution GmbH
 *
 */
@Stateless
@DeclareRoles(value = { "Admin", "DescriptiveDataAuthor", "Guest" })
public class PreferenceService {
	private static final Logger LOG = LoggerFactory.getLogger(PreferenceService.class);

	private static final String ADMIN_ROLE = "Admin";

	@PersistenceContext(unitName = "openMDM")
	private EntityManager em;

	@Context
	private SecurityContext securityContext;

	public PreferenceService() {
		// public no-arg constructor
	}

	/**
	 * 
	 * @param em             EntityManager to use
	 * @param sessionContext sessionManager to use
	 */
	public PreferenceService(EntityManager em, SecurityContext securityContext) {
		this.em = em;
		this.securityContext = securityContext;
	}

	@Deprecated
	public List<PreferenceMessage> getPreferences(String scope, String key) {

		return getPreferences(scope, key, null);
	}

	public List<PreferenceMessage> getPreferences(String scope, String key, String user) {

		TypedQuery<Preference> query;
		if (scope == null || scope.trim().isEmpty()) {
			query = em.createQuery(
					"select p from Preference p where (p.user is null or p.user = :user) and LOWER(p.key) like :key",
					Preference.class).setParameter("user", securityContext.getUserPrincipal().getName())
					.setParameter("key", key.toLowerCase() + "%");
		} else {
			String queryForUser = getQueryForUser(user);

			query = em.createQuery(buildQuery(scope, key, queryForUser), Preference.class);

			if (key != null && !key.trim().isEmpty()) {
				query.setParameter("key", key.toLowerCase() + "%");
			}
			if (Scope.USER.name().equalsIgnoreCase(scope) && queryForUser != null && !queryForUser.trim().isEmpty()) {
				query.setParameter("user", queryForUser.toLowerCase());
			}
		}

		return query.getResultList().stream().map(this::convert).collect(Collectors.toList());
	}

	private String getQueryForUser(String user) {
		if (user == null || !securityContext.isUserInRole(ADMIN_ROLE)) {
			return securityContext.getUserPrincipal().getName();
		} else {
			return user;
		}
	}

	public List<PreferenceMessage> getPreferencesBySource(String source, String key) {
		TypedQuery<Preference> query;

		if (key == null || key.trim().isEmpty()) {
			query = em.createQuery("select p from Preference p where p.source = :source", Preference.class)
					.setParameter("source", Strings.emptyToNull(source));

		} else {
			query = em
					.createQuery("select p from Preference p where p.source = :source and p.key = :key",
							Preference.class)
					.setParameter("source", Strings.emptyToNull(source)).setParameter("key", Strings.emptyToNull(key));
		}
		return query.getResultList().stream().map(this::convert).collect(Collectors.toList());
	}

	public PreferenceMessage save(PreferenceMessage preference) {
		Principal principal = securityContext.getUserPrincipal();
		List<Preference> existingPrefs = getExistingPreference(preference, principal);

		Preference pe;

		if (existingPrefs.isEmpty()) {
			pe = convert(preference);
		} else {
			if (existingPrefs.size() > 1) {
				LOG.warn(
						"Found multiple entries for preference with scope={}, source={}, user={}, key={} where one entry was expected!",
						preference.getScope(), preference.getSource(), preference.getUser(), preference.getKey());
			}
			pe = existingPrefs.get(0);
			pe.setValue(preference.getValue());
		}
		if (isAllowed(pe)) {
			em.persist(pe);
			em.flush();
			return convert(pe);
		} else {
			throw new PreferenceException("Only users with role " + ADMIN_ROLE
					+ " are allowed to save Preferences outside of the USER scope!");
		}

	}

	public PreferenceMessage deletePreference(Long id) {

		Preference preference = em.find(Preference.class, id);

		if (isAllowed(preference)) {
			em.remove(preference);
			em.flush();
			return convert(preference);
		} else {
			throw new PreferenceException("Only users with role " + ADMIN_ROLE + " are allowed to delete Preference!");
		}
	}

	private boolean isAllowed(Preference preference) {
		return securityContext.isUserInRole(ADMIN_ROLE) || (preference.getUser() != null
				&& preference.getUser().equalsIgnoreCase(securityContext.getUserPrincipal().getName()));
	}

	private PreferenceMessage convert(Preference pe) {
		PreferenceMessage p = new PreferenceMessage();
		p.setKey(pe.getKey());
		p.setValue(pe.getValue());
		p.setId(pe.getId());

		if (pe.getUser() == null && pe.getSource() == null) {
			p.setSource(pe.getSource());
			p.setScope(Scope.SYSTEM);
		} else if (pe.getUser() != null) {
			p.setUser(pe.getUser());
			p.setScope(Scope.USER);
		} else if (pe.getSource() != null) {
			p.setSource(pe.getSource());
			p.setScope(Scope.SOURCE);
		}

		return p;
	}

	private Preference convert(PreferenceMessage p) {
		Principal principal = securityContext.getUserPrincipal();

		Preference pe = new Preference();
		pe.setKey(p.getKey());
		pe.setValue(p.getValue());
		pe.setId(p.getId());

		switch (p.getScope()) {
		case SOURCE:
			pe.setSource(p.getSource());
			break;
		case USER:
			pe.setUser(principal.getName());
			break;
		case SYSTEM:
		default:
			break;
		}
		return pe;
	}

	private String buildQuery(String scope, String key, String user) {
		String query = "select p from Preference p";
		String whereOrAnd = " where";
		if (scope != null && scope.trim().length() > 0) {
			switch (scope.toLowerCase()) {
			case "system":
				query = query.concat(whereOrAnd).concat(" p.source is null and p.user is null");
				break;
			case "source":
				query = query.concat(whereOrAnd).concat(" p.source is not null and p.user is null");
				break;
			case "user":
				if (user != null && !user.trim().isEmpty()) {
					query = query.concat(whereOrAnd).concat(" p.source is null and LOWER(p.user) = :user");
				} else {
					query = query.concat(whereOrAnd).concat(" p.source is null and p.user is not null");
				}
				break;
			default:
			}
			whereOrAnd = " and";
		}
		if (key != null && key.trim().length() > 0) {
			query = query.concat(whereOrAnd).concat(" LOWER(p.key) LIKE :key");
		}
		return query;
	}

	private List<Preference> getExistingPreference(PreferenceMessage preference, Principal principal) {
		Preconditions.checkNotNull(preference.getScope(), "Scope cannot be null!");

		if (preference.getId() == null) {
			switch (preference.getScope()) {
			case USER:
				return em
						.createQuery(
								"select p from Preference p where p.source is null and p.user = :user and p.key = :key",
								Preference.class)
						.setParameter("user", Strings.emptyToNull(principal.getName()))
						.setParameter("key", preference.getKey()).getResultList();
			case SOURCE:
				return em.createQuery(
						"select p from Preference p where p.source = :source and p.user is null and p.key = :key",
						Preference.class).setParameter("source", Strings.emptyToNull(preference.getSource()))
						.setParameter("key", preference.getKey()).getResultList();
			case SYSTEM:
				return em.createQuery(
						"select p from Preference p where p.source is null and p.user is null and p.key = :key",
						Preference.class).setParameter("key", preference.getKey()).getResultList();
			default:
				throw new IllegalArgumentException("Unknown Scope!");
			}
		} else {
			return Arrays.asList(em.find(Preference.class, preference.getId()));
		}
	}
}
