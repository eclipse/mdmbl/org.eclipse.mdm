/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.apicopy.control;

/**
 * DTO Object which holds the pair of values and the corresponding flags
 * 
 * @author Alexander Knoblauch
 *
 */
public class MeasuredValuesDTO {

	private final Object values;
	private final short[] flags;

	/**
	 * @param values
	 * @param flags
	 */
	public MeasuredValuesDTO(Object values, short[] flags) {
		super();
		this.values = values;
		this.flags = flags;
	}

	/**
	 * @return the values
	 */
	public Object getValues() {
		return values;
	}

	/**
	 * @return the flags
	 */
	public short[] getFlags() {
		return flags;
	}

}
