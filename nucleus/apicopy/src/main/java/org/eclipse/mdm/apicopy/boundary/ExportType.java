/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.apicopy.boundary;

import javax.ws.rs.core.MediaType;

import org.eclipse.mdm.apicopy.control.ApiCopyException;

/*
 * Enums defining parameter for file export.
 * 
 * @author jz
 */
public enum ExportType {
	ATFX("atfx", "atfxfile", "org.eclipse.mdm.api.atfxadapter.ATFXContextFactory", ".atfx", "true", "true",
			MediaType.APPLICATION_XML),
	CSV("csv", "csvFile", "org.eclipse.mdm.csvadapter.CSVApplicationContextFactory", ".csv", "false", "true",
			MediaType.TEXT_PLAIN),
	EXCEL("xlsx", "excelFile", "org.eclipse.mdm.csvadapter.ExcelApplicationContextFactory", ".xlsx", "false", "true",
			"application/xlsx");

	private final String fileType;
	private final String key;
	private final String contextFactoryClassname;
	private final String suffix;
	private final String includeCatalog;
	private final String writeExternalComponents;
	private final String mediaType;

	private ExportType(String fileType, String key, String contextFactoryClassname, String suffix,
			String includeCatalog, String writeExternalComponents, String mediaType) {
		this.fileType = fileType;
		this.key = key;
		this.contextFactoryClassname = contextFactoryClassname;
		this.suffix = suffix;
		this.includeCatalog = includeCatalog;
		this.writeExternalComponents = writeExternalComponents;
		this.mediaType = mediaType;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return the contextFactoryClassname
	 */
	public String getContextFactoryClassname() {
		return contextFactoryClassname;
	}

	/**
	 * @return the suffix
	 */
	public String getSuffix() {
		return suffix;
	}

	public String getFileType() {
		return fileType;
	}

	public String getIncludeCatalog() {
		return includeCatalog;
	}

	public String getWriteExternalComponents() {
		return writeExternalComponents;
	}

	public String getMediaType() {
		return mediaType;
	}

	/**
	 * @param pKuerzel
	 * @return the enum for the specified fileType
	 */
	public static ExportType findByFileType(final String fileType) {
		if (fileType != null) {
			for (final ExportType element : ExportType.values()) {
				if (element.fileType.equalsIgnoreCase(fileType)) {
					return element;
				}
			}
		}
		throw new ApiCopyException("File type " + fileType + " is not supported");
	}
}
