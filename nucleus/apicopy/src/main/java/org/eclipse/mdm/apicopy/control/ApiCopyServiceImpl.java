/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.apicopy.control;

import javax.ejb.Stateless;

import org.eclipse.mdm.api.dflt.ApplicationContext;

@Stateless
public class ApiCopyServiceImpl implements ApiCopyService {

	@Override
	public ApiCopyTask newApiCopyTask(ApplicationContext src, ApplicationContext dst) {
		return newApiCopyTask(src, dst, new DefaultTemplateManager());
	}

	@Override
	public ApiCopyTask newApiCopyTask(ApplicationContext src, ApplicationContext dst, TemplateManager templateManager) {
		return new ImportTask(src, dst, templateManager);
	}

}
