/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.apicopy.control;

import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.ApplicationContext;

/**
 * Resolves the test template and test step template name, which will be used for creating the test an test step
 *
 */
public interface TemplateManager {

	/**
	 * 
	 * @param testSrc the source test
	 * @return the name of the template for the test
	 * @throws ApiCopyException
	 */
	String getTemplateTestName(Test testSrc) throws ApiCopyException;

	/**
	 * 
	 * @param testStepSrc the source test step
	 * @return the name of the template of the teststep
	 * @throws ApiCopyException
	 */
	String getTemplateTestStepName(TestStep testStepSrc) throws ApiCopyException;

	/**
	 * 
	 * @param source the {@link ApplicationContext} of the apicopy source
	 */
	void setSourceContext(ApplicationContext source) throws ApiCopyException;
}
