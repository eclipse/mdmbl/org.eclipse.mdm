/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.boundary;

import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.MEDIATYPE_APPLICATION_PROTOBUF;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Singleton;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.businessobjects.utils.JsonMessageBodyProvider;
import org.eclipse.mdm.businessobjects.utils.ProtobufMessageBodyProvider;
import org.eclipse.mdm.nodeprovider.entity.NodeProvider;
import org.eclipse.mdm.nodeprovider.entity.NodeProviderUIWrapper;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.protobuf.Mdm.Node;
import org.eclipse.mdm.protobuf.Mdm.NodeProviderResponse;
import org.glassfish.hk2.api.Injectee;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.ServiceHandle;
import org.glassfish.hk2.api.TypeLiteral;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

// TODO mkoller, 13.02.2024: JerseyTest does not support JUnit5, https://github.com/hanleyt/jersey-junit might be an alternative
public class NodeProviderResourceTest extends JerseyTest {
	private static NodeProviderService nodeProviderService = Mockito.mock(NodeProviderService.class);
	private static NodeProvider nodeProvider = Mockito.mock(NodeProvider.class);
	private Node rootNode;
	private Node childNode;

	public static class NodeProviderRepositoryInjectionResolver implements InjectionResolver<NodeProviderService> {
		@Override
		public Object resolve(Injectee injectee, ServiceHandle<?> root) {
			return nodeProviderService;
		}

		@Override
		public boolean isConstructorParameterIndicator() {
			return true;
		}

		@Override
		public boolean isMethodParameterIndicator() {
			return true;
		}
	}

	@Override
	protected void configureClient(ClientConfig config) {
		config.register(JacksonFeature.class);
		config.register(JsonMessageBodyProvider.class);
		config.register(ProtobufMessageBodyProvider.class);
		super.configureClient(config);
	}

	@Override
	public Application configure() {
		ResourceConfig config = new ResourceConfig();

		config.register(JacksonFeature.class);
		config.register(new AbstractBinder() {
			@Override
			protected void configure() {
				bind(NodeProviderRepositoryInjectionResolver.class).to(new TypeLiteral<InjectionResolver<EJB>>() {
				}).in(Singleton.class);
			}
		});

		config.register(NodeProviderResource.class);
		config.register(JsonMessageBodyProvider.class);
		config.register(ProtobufMessageBodyProvider.class);
		return config;
	}

	@Before
	public void init() {
		when(nodeProviderService.getNodeProviderIDs()).thenReturn(Collections.emptyList());
		when(nodeProviderService.getNodeProvider("default")).thenReturn(nodeProvider);

		rootNode = SerializationUtil.createNode("MDM", "Project", "1", "Id", Filter.and(), "Project1");
		childNode = SerializationUtil.createNode("MDM", "Pool", "2", "Id", Filter.and(), "Pool1");

		when(nodeProvider.getRoots()).thenReturn(Arrays.asList(rootNode));
		when(nodeProvider.getChildren(Mockito.any())).thenReturn(Arrays.asList(childNode));
		when(nodeProvider.getTreePath(Mockito.any())).thenReturn(Arrays.asList(rootNode, childNode));
	}

	@Test
	public void testGetNodeProviders() {
		when(nodeProviderService.getNodeProvidersSorted())
				.thenReturn(Arrays.asList(new NodeProviderUIWrapper(0, "default", "default"),
						new NodeProviderUIWrapper(1, "generic", "generic")));
		List<NodeProviderUIWrapper> nodeProviderNames = target("nodeprovider").request()
				.get(new GenericType<List<NodeProviderUIWrapper>>() {
				});
		assertThat(nodeProviderNames).containsExactly(new NodeProviderUIWrapper(0, "default", "default"),
				new NodeProviderUIWrapper(1, "generic", "generic"));
	}

	@Test
	public void testGetRoots() {
		NodeProviderResponse nodes = target("nodeprovider/default/").request(MEDIATYPE_APPLICATION_PROTOBUF)
				.get(NodeProviderResponse.class);
		assertThat(nodes.getDataList()).hasSize(1).contains(rootNode);
	}

	@Test
	public void testGetChildren() {
		NodeProviderResponse nodes = target("nodeprovider/default/" + rootNode.getSerial())
				.request(MEDIATYPE_APPLICATION_PROTOBUF).get(NodeProviderResponse.class);
		assertThat(nodes.getDataList()).contains(childNode);
	}

	@Test
	public void testGetTreePath() {
		Node node = SerializationUtil.createNode("MDM", "Project", "1", "Id", Filter.and(), "Project1");

		NodeProviderResponse nodes = target("nodeprovider/default/").request(MEDIATYPE_APPLICATION_PROTOBUF)
				.post(Entity.entity(node, MediaType.APPLICATION_JSON_TYPE), NodeProviderResponse.class);

		assertThat(nodes.getDataList()).containsExactly(rootNode, childNode);
	}
}
