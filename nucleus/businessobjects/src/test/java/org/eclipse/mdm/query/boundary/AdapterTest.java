package org.eclipse.mdm.query.boundary;

public class AdapterTest {

//	@ClassRule
//	public static AdapterTestRes resource = new AdapterTestRes();
//
//	private static Client client;
//	private static WebTarget endPoint;
//
//	@BeforeClass
//	public static void init() {
//		connect();
//	}
//
//	@AfterClass
//	public static void end() {
//	}
//
//	@Test
//	public void runComponentQueryEqualData() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - gleiche Daten' and Testunit.string eq 'value1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name", "Testunit.Id", "Testunit.Name", "Testunit.string", "Testunit.file_relation",
//				"Testunit.file_link", "Testunit.file_link_array"));
//		query.setResultType("TestStep");
//		query.setContextCompResultType("Testunit");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//
//		query.setResultType("Measurement");
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//	}
//
//	@Test
//	public void runComponentQueryMultipleResult() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - gleiche Daten' and TestStep.Name eq 'TestStep1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name", "Testunit.Id", "Testunit.Name", "Testunit.string", "Testunit.file_relation",
//				"Testunit.file_link", "Testunit.file_link_array"));
//		query.setResultType("TestStep");
//		query.setContextCompResultType("Testunit");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(2);
//
//		query.setResultType("Measurement");
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(2);
//	}
//
//	@Test
//	public void runComponentQueryTestStepData() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - Kontext an TestStep' and Testunit.string eq 'value1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name", "Testunit.Id", "Testunit.Name", "Testunit.string", "Testunit.file_relation",
//				"Testunit.file_link", "Testunit.file_link_array"));
//		query.setResultType("TestStep");
//		query.setContextCompResultType("Testunit");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - Kontext an TestStep' and Testunit.long eq '2'");
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(4);
//	}
//
//	@Test
//	public void runComponentQueryMeasurementData() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - Kontext an Messung' and Testunit.string eq 'value1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name", "Testunit.Id", "Testunit.Name", "Testunit.string", "Testunit.file_relation",
//				"Testunit.file_link", "Testunit.file_link_array"));
//		query.setResultType("Measurement");
//		query.setContextCompResultType("Testunit");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name"));
//		query.setContextCompResultType("Testunit");
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//	}
//
//	@Test
//	public void runComponentQueryDiffData() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - unterschiedlieche Daten' and Testunit.string eq 'value1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name", "Testunit.Id", "Testunit.Name", "Testunit.string", "Testunit.file_relation",
//				"Testunit.file_link", "Testunit.file_link_array"));
//		query.setResultType("TestStep");
//		query.setContextCompResultType("Testunit");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//		assertThat(res.getRows().get(0).getColumns().stream()
//				.filter(c -> c.getType().equalsIgnoreCase("Testunit") && c.getAttribute().equalsIgnoreCase("string"))
//				.map(c -> (ContextColumn) c).findFirst().get().getOrderedValue()).isEqualTo("value1");
//
//		query.setResultType("Measurement");
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(0);
//
//		filter.setFilter("Test.Name eq 'Test - unterschiedlieche Daten' and Testunit.string eq 'value_M1'");
//		query.setFilters(Arrays.asList(filter));
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(2);
//		assertThat(res.getRows().get(0).getColumns().stream()
//				.filter(c -> c.getType().equalsIgnoreCase("Testunit") && c.getAttribute().equalsIgnoreCase("string"))
//				.findFirst().get().getValue()).isEqualTo("value_M1");
//	}
//
//	@Test
//	public void runTestStepQueryDiffDataFilterMatchInResult() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - unterschiedlieche Daten' and Testunit.string eq 'value_M1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name", "Testunit.Id", "Testunit.Name", "Testunit.string", "Testunit.file_relation",
//				"Testunit.file_link", "Testunit.file_link_array"));
//		query.setResultType("TestStep");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(2);
//	}
//
//	@Test
//	public void runTestStepQueryDiffDataFilterMatchInOrder() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - unterschiedlieche Daten' and Testunit.string eq 'value1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name", "Testunit.Id", "Testunit.Name", "Testunit.string", "Testunit.file_relation",
//				"Testunit.file_link", "Testunit.file_link_array"));
//		query.setResultType("TestStep");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//	}
//
//	@Test
//	public void runTestStepQueryFilterId() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Pool.Name eq 'simple_pool'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name", "Testunit.Id", "Testunit.Name", "Testunit.string", "Testunit.file_relation",
//				"Testunit.file_link", "Testunit.file_link_array"));
//		query.setResultType("TestStep");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(8);
//
//		List<String> ids = res.getRows().stream().map(r -> r.getId()).collect(Collectors.toList());
//
//		ids.sort(new Comparator<String>() {
//
//			@Override
//			public int compare(String o1, String o2) {
//
//				return Long.valueOf(o1).compareTo(Long.valueOf(o2));
//			}
//		});
//
//		String id = ids.get(4);
//
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Pool.Name eq 'simple_pool' and TestStep.Id lt " + id);
//		query.setFilters(Arrays.asList(filter));
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(4);
//
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Pool.Name eq 'simple_pool' and TestStep.Id gt " + id);
//		query.setFilters(Arrays.asList(filter));
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(3);
//
//	}
//
//	@Test
//	public void runComponentQueryFilterIdOrder() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Pool.Name eq 'simple_pool'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name", "Testunit.Id", "Testunit.Name", "Testunit.string", "Testunit.file_relation",
//				"Testunit.file_link", "Testunit.file_link_array"));
//		query.setResultType("TestStep");
//		query.setContextCompResultType("Testunit");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(16);
//
//		List<String> ids = res.getRows().stream().map(r -> r.getId()).collect(Collectors.toList());
//
//		ids.sort(new Comparator<String>() {
//
//			@Override
//			public int compare(String o1, String o2) {
//
//				return Long.valueOf(o1).compareTo(Long.valueOf(o2));
//			}
//		});
//
//		String id = ids.get(6);
//
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Pool.Name eq 'simple_pool' and Testunit.Id lt " + id);
//		query.setFilters(Arrays.asList(filter));
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(6);
//
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Pool.Name eq 'simple_pool' and Testunit.Id gt " + id);
//		query.setFilters(Arrays.asList(filter));
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(9);
//
//	}
//
//	@Test
//	public void runComponentQueryFilterIdMeasurement() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Pool.Name eq 'simple_pool'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Project.Id", "Project.Name", "TestStep.Id", "TestStep.Name", "Measurement.Id",
//				"Measurement.Name", "Testunit.Id", "Testunit.Name", "Testunit.string", "Testunit.file_relation",
//				"Testunit.file_link", "Testunit.file_link_array"));
//		query.setResultType("Measurement");
//		query.setContextCompResultType("Testunit");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(16);
//
//		List<String> ids = res.getRows().stream().map(r -> r.getId()).collect(Collectors.toList());
//
//		ids.sort(new Comparator<String>() {
//
//			@Override
//			public int compare(String o1, String o2) {
//
//				return Long.valueOf(o1).compareTo(Long.valueOf(o2));
//			}
//		});
//
//		String id = ids.get(6);
//
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Pool.Name eq 'simple_pool' and Testunit.Id lt " + id);
//		query.setFilters(Arrays.asList(filter));
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(6);
//
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Pool.Name eq 'simple_pool' and Testunit.Id gt " + id);
//		query.setFilters(Arrays.asList(filter));
//		response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(9);
//
//	}
//
//	@Test
//	public void runComponentQueryFileRelationResult() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - gleiche Daten' and Testunit.string eq 'value1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Testunit.file_relation"));
//		query.setResultType("TestStep");
//		query.setContextCompResultType("Testunit");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//
//		Column column = res.getRows().get(0).getColumns().get(0);
//		assertThat(column).isInstanceOf(ContextColumn.class);
//		List<MDMFile> files = (List<MDMFile>) ((ContextColumn) column).getOrderedValue();
//		assertEquals(1, files.size());
//	}
//
//	@Test
//	public void runTestStepQueryFileRelationResult() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - gleiche Daten' and TestStep.Name eq 'TestStep1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Testunit.file_relation"));
//		query.setResultType("TestStep");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//	}
//
//	@Test
//	public void runTestStepQueryFileRelationResultPoolFilter() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter(
//				"Pool.Name eq 'simple_pool' and Test.Name eq 'Test - gleiche Daten' and TestStep.Name eq 'TestStep1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Testunit.file_relation"));
//		query.setResultType("TestStep");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//	}
//
//	@Test
//	public void runTestStepQueryFileRelationResultProjectFilter() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter(
//				"Project.Name eq 'simple_project' and Test.Name eq 'Test - gleiche Daten' and TestStep.Name eq 'TestStep1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Testunit.file_relation"));
//		query.setResultType("TestStep");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//	}
//
//	@Test
//	public void runTestStepQuerySimpleColumn() {
//		SourceFilter filter = new SourceFilter();
//		filter.setSourceName(System.getProperty("service"));
//		filter.setFilter("Test.Name eq 'Test - gleiche Daten' and Testunit.string eq 'value1'");
//		QueryRequest query = new QueryRequest();
//		query.setFilters(Arrays.asList(filter));
//		query.setColumns(Arrays.asList("Testunit.string"));
//		query.setResultType("TestStep");
//		query.setContextCompResultType("TestUnit");
//		Response response = endPoint.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
//				.post(javax.ws.rs.client.Entity.json(query));
//		QueryResult res = response.readEntity(QueryResult.class);
//		assertThat(res.getTotalRecords().get(filter.getSourceName()).longValue()).isEqualTo(1);
//
////		res.getRows().get(0).getColumns().get(0).getOrderedValue()
//	}
//
//	private static void connect() {
//		Form form = new Form();
//		form.param("j_username", "sa");
//		form.param("j_password", "sa");
//		client = ClientBuilder.newBuilder().register(JsonMessageBodyProvider.class).build();
//		client.register(new CookieFilterTest(client));
//		endPoint = client.target("http://localhost:8080/org.eclipse.mdm.nucleus");
//		endPoint.path("j_security_check").request().post(javax.ws.rs.client.Entity.form(form));
//	}
}
