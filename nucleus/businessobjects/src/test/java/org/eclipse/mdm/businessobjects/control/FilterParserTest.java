/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.control;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.query.boundary.QueryServiceTest;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class FilterParserTest {

	@Test
	public void testParseFilterString() throws Exception {
		EntityType test = QueryServiceTest.mockEntity("env2", "Test");

		when(test.getAttribute("Name").createValue(eq(""), any())).thenAnswer(new Answer<Value>() {

			@Override
			public Value answer(InvocationOnMock invocation) throws Throwable {
				return ValueType.STRING.create("env2", invocation.getArgument(0), true, invocation.getArgument(1));
			}
		});

		List<EntityType> possibleEntityTypes = Arrays.asList(test);

		Filter filter = FilterParser.parseFilterString(possibleEntityTypes, "Test.Name lk \"With\\\"quote\\\"\"");
		assertThat(filter).isEqualTo(Filter.and().name(test, "With\"quote\""));

		Filter filter2 = FilterParser.parseFilterString(possibleEntityTypes, "Test.Name lk 'With\"quote\"'");
		assertThat(filter2).isEqualTo(Filter.and().name(test, "With\"quote\""));

		Filter filter3 = FilterParser.parseFilterString(possibleEntityTypes, "Test.Name lk 'With\\'quote\\''");
		assertThat(filter3).isEqualTo(Filter.and().name(test, "With'quote'"));

		Filter filter4 = FilterParser.parseFilterString(possibleEntityTypes, "Test.Name lk \"With'quote'\"");
		assertThat(filter4).isEqualTo(Filter.and().name(test, "With'quote'"));
	}

	@Test
	public void testParseFilterStringWithMappedEntityTypes() throws Exception {
		EntityType test = QueryServiceTest.mockEntity("env2", "Test");
		EntityType pool = QueryServiceTest.mockEntity("env2", "StructureLevel");
		EntityType measurement = QueryServiceTest.mockEntity("env2", "MeaResult");
		EntityType channelGroup = QueryServiceTest.mockEntity("env2", "SubMatrix");
		EntityType channel = QueryServiceTest.mockEntity("env2", "MeaQuantity");

		when(test.getAttribute("Name").createValue(eq(""), any())).thenAnswer(new Answer<Value>() {

			@Override
			public Value answer(InvocationOnMock invocation) throws Throwable {
				return ValueType.STRING.create("env2", invocation.getArgument(0), true, invocation.getArgument(1));
			}
		});

		List<EntityType> possibleEntityTypes = Arrays.asList(test, pool, measurement, channelGroup, channel);

		Filter poolFilter = FilterParser.parseFilterString(possibleEntityTypes, "Pool.Name lk \"With'quote'\"");
		assertThat(poolFilter).isEqualTo(Filter.and().name(pool, "With'quote'"));

		Filter measurementFilter = FilterParser.parseFilterString(possibleEntityTypes,
				"Measurement.Name lk \"With'quote'\"");
		assertThat(measurementFilter).isEqualTo(Filter.and().name(measurement, "With'quote'"));

		Filter channelGroupFilter = FilterParser.parseFilterString(possibleEntityTypes,
				"ChannelGroup.Name lk \"With'quote'\"");
		assertThat(channelGroupFilter).isEqualTo(Filter.and().name(channelGroup, "With'quote'"));

		Filter channelFilter = FilterParser.parseFilterString(possibleEntityTypes, "Channel.Name lk \"With'quote'\"");
		assertThat(channelFilter).isEqualTo(Filter.and().name(channel, "With'quote'"));
	}

	@Test
	public void testToString() {
		EntityType entityType = mockEntityType();

		assertThat(FilterParser.toString(Filter.idOnly(entityType, "123"))).isEqualTo("Test.Id eq \"123\"");

		assertThat(FilterParser
				.toString(Filter.and().merge(Filter.idOnly(entityType, "123")).merge(Filter.idOnly(entityType, "125"))))
						.isEqualTo("Test.Id eq \"123\" and Test.Id eq \"125\"");
	}

	private EntityType mockEntityType() {

		Attribute attribute = Mockito.mock(Attribute.class);
		Mockito.when(attribute.getName()).thenReturn("Id");
		Mockito.when(attribute.getValueType()).thenReturn(ValueType.STRING);
		Mockito.when(attribute.createValue(Mockito.any(), Mockito.anyString())).thenAnswer(new Answer<Value>() {
			@Override
			public Value answer(InvocationOnMock invocation) {
				return ValueType.STRING.create("Id", "", true, invocation.getArgument(1));
			}
		});

		EntityType entityType = Mockito.mock(EntityType.class);
		Mockito.when(entityType.getIDAttribute()).thenReturn(attribute);
		Mockito.when(entityType.getAttribute(Mockito.eq("Id"))).thenReturn(attribute);
		Mockito.when(entityType.getName()).thenReturn("Test");
		Mockito.when(attribute.getEntityType()).thenReturn(entityType);

		return entityType;
	}
}
