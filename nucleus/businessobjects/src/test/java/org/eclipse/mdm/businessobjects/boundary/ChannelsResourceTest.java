/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.businessobjects.entity.MDMAttribute;
import org.eclipse.mdm.businessobjects.entity.MDMEntity;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.utils.ProtobufMessageBodyProvider;
import org.eclipse.mdm.query.entity.QueryRequest;
import org.eclipse.mdm.query.entity.QueryResult;
import org.eclipse.mdm.query.entity.SourceFilter;
import org.eclipse.mdm.testutils.GlassfishExtension;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
class ChannelsResourceTest {

	private static final Log LOG = LogFactory.getLog(ChannelsResourceTest.class);

	@Container
	static OdsServerContainer odsServer = OdsServerContainer.create();

	@RegisterExtension
	static GlassfishExtension glassfish = new GlassfishExtension(odsServer);

	static Quantity quantity;
	static TestStep testStep;
	static Unit unitKg;
	static Unit unitG;

	Measurement measurement;
	ChannelGroup channelGroup;

	@BeforeAll
	static void setup() throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		unitKg = em.loadAll(Unit.class, "kg").get(0);
		unitG = em.loadAll(Unit.class, "g").get(0);

		quantity = ef.createQuantity("MyQuantity");

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool);
		testStep = ef.createTestStep("MyTestStep", test);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(quantity));
			t.create(Arrays.asList(project));
			t.commit();
		}
		context.close();
	}

	@BeforeEach
	void createMeasurement() throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		measurement = ef.createMeasurement("MyMeasurement" + System.currentTimeMillis(), testStep);
		channelGroup = ef.createChannelGroup("MyChannelGroup", 3, measurement);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.commit();
		}
		context.close();
	}

	@org.junit.jupiter.api.Test
	void createChannelsInBatchMode(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String createChannels = "[\n" + "    {\n" + "        \"Name\": \"Ch01\",\n" + "        \"Measurement\": \""
				+ measurement.getID() + "\",\n" + "        \"Quantity\": \"" + quantity.getID() + "\",\n"
				+ "        \"Unit\": \"" + unitG.getID() + "\"\n" + "    },\n" + "    {\n"
				+ "        \"Name\": \"Ch02\",\n" + "        \"Measurement\": \"" + measurement.getID() + "\",\n"
				+ "        \"Quantity\": \"" + quantity.getID() + "\",\n" + "        \"Unit\": \"" + unitKg.getID()
				+ "\"\n" + "    }\n" + "]";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/channels").request().cookie(sessionCookie)
				.post(Entity.entity(createChannels, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("Channel");
		assertThat(response.getData()).hasSize(2);
	}

	@org.junit.jupiter.api.Test
	void createChannelsInBatchModeAndUpdate(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		LOG.info("Create two channels");
		final String createChannels = "[\n" + "    {\n" + "        \"Name\": \"Ch03\",\n"
				+ "        \"Measurement\": \"" + measurement.getID() + "\",\n" + "        \"Quantity\": \""
				+ quantity.getID() + "\",\n" + "        \"Unit\": \"" + unitG.getID() + "\"\n" + "    },\n" + "    {\n"
				+ "        \"Name\": \"Ch04\",\n" + "        \"Measurement\": \"" + measurement.getID() + "\",\n"
				+ "        \"Quantity\": \"" + quantity.getID() + "\",\n" + "        \"Unit\": \"" + unitKg.getID()
				+ "\"\n" + "    }\n" + "]";

		final MDMEntityResponse createResponse = target.path("mdm/environments/MDM/channels").request()
				.cookie(sessionCookie)
				.post(Entity.entity(createChannels, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(createResponse.getType()).isEqualTo("Channel");
		assertThat(createResponse.getData()).hasSize(2);

		final String updateChannels = "{\n" + "    \"" + createResponse.getData().get(0).getId() + "\": {\n"
				+ "        \"Description\": \"My first channel\"\n" + "    },\n" + "    \""
				+ createResponse.getData().get(1).getId() + "\": {\n"
				+ "        \"Description\": \"My second channel\"\n" + "    }\n" + "}";

		final MDMEntityResponse updateResponse = target.path("mdm/environments/MDM/channels").request()
				.cookie(sessionCookie)
				.put(Entity.entity(updateChannels, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		{
			LOG.info("Check returned attributes");
			assertThat(updateResponse.getType()).isEqualTo("Channel");
			assertThat(updateResponse.getData()).hasSize(2);
			{
				LOG.info("Check first channel");
				Optional<MDMEntity> channel = updateResponse.getData().stream()
						.filter(i -> i.getId().equals(createResponse.getData().get(0).getId())).findAny();
				assertThat(channel).isPresent();
				Optional<? extends MDMAttribute> descriptionAttribute = channel.get().getAttributes().stream()
						.filter(a -> a.getName().equals("Description")).findAny();
				assertThat(descriptionAttribute).isPresent();
				assertThat(descriptionAttribute.get().getValue()).isEqualTo("My first channel");
			}
			{
				LOG.info("Check second channel");
				Optional<MDMEntity> channel = updateResponse.getData().stream()
						.filter(i -> i.getId().equals(createResponse.getData().get(1).getId())).findAny();
				assertThat(channel).isPresent();
				Optional<? extends MDMAttribute> descriptionAttribute = channel.get().getAttributes().stream()
						.filter(a -> a.getName().equals("Description")).findAny();
				assertThat(descriptionAttribute).isPresent();
				assertThat(descriptionAttribute.get().getValue()).isEqualTo("My second channel");
			}
		}

		{
			LOG.info("Retrieve updated channels and check");
			{
				LOG.info("Check first channel");
				final MDMEntityResponse channelResponse = target
						.path("mdm/environments/MDM/channels/" + createResponse.getData().get(0).getId()).request()
						.cookie(sessionCookie).get(MDMEntityResponse.class);
				assertThat(channelResponse.getType()).isEqualTo("Channel");
				assertThat(channelResponse.getData()).hasSize(1);
				Optional<? extends MDMAttribute> descriptionAttribute = channelResponse.getData().get(0).getAttributes()
						.stream().filter(a -> a.getName().equals("Description")).findAny();
				assertThat(descriptionAttribute).isPresent();
				assertThat(descriptionAttribute.get().getValue()).isEqualTo("My first channel");
			}
			{
				LOG.info("Check second channel");
				final MDMEntityResponse channelResponse = target
						.path("mdm/environments/MDM/channels/" + createResponse.getData().get(1).getId()).request()
						.cookie(sessionCookie).get(MDMEntityResponse.class);
				assertThat(channelResponse.getType()).isEqualTo("Channel");
				assertThat(channelResponse.getData()).hasSize(1);
				Optional<? extends MDMAttribute> descriptionAttribute = channelResponse.getData().get(0).getAttributes()
						.stream().filter(a -> a.getName().equals("Description")).findAny();
				assertThat(descriptionAttribute).isPresent();
				assertThat(descriptionAttribute.get().getValue()).isEqualTo("My second channel");
			}
		}
	}

	/**
	 * Tests if query endpoint returns more than 1000 Channels if the result limit
	 * is increased.
	 * 
	 * @throws ConnectionException
	 */
	@org.junit.jupiter.api.Test
	void testQueryChannels(NewCookie sessionCookie) throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		List<Channel> channels = new ArrayList<>();
		for (int i = 0; i < 1487; i++) {
			channels.add(ef.createChannel("MyChannel" + i, measurement, quantity));
		}

		try (Transaction t = em.startTransaction()) {
			t.create(channels);
			t.commit();
		}

		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		QueryRequest request = new QueryRequest();
		SourceFilter filter = new SourceFilter();
		filter.setSourceName("MDM");
		filter.setFilter("Channel.Id gt 0");
		request.setFilters(Arrays.asList(filter));
		request.setColumns(Arrays.asList("Channel.Id", "Channel.Name"));
		request.setResultLimit(1500);
		request.setResultType("Channel");

		final QueryResult channelsResult = target.path("mdm/query").request(MediaType.APPLICATION_JSON_TYPE)
				.cookie(sessionCookie).post(Entity.entity(request, MediaType.APPLICATION_JSON_TYPE), QueryResult.class);

		assertThat(channelsResult.getTotalRecords()).containsEntry("MDM", 1487L);
		assertThat(channelsResult.getRows()).hasSize(1487);
	}
}
