/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.businessobjects.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.EnumRegistry;
import org.eclipse.mdm.api.base.model.Enumeration;
import org.eclipse.mdm.api.base.model.EnumerationValue;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.Interpolation;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.ScalarType;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;
import org.eclipse.mdm.api.base.model.ValueType;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

public class SerializerTest {

	@Test
	public void testApplyValueBoolean() throws Exception {

		assertThat(Serializer.deserializeValue(ValueType.BOOLEAN, "1")).isEqualTo(Boolean.TRUE);
		assertThat(Serializer.deserializeValue(ValueType.BOOLEAN, 1)).isEqualTo(Boolean.TRUE);
		assertThat(Serializer.deserializeValue(ValueType.BOOLEAN, "0")).isEqualTo(Boolean.FALSE);
		assertThat(Serializer.deserializeValue(ValueType.BOOLEAN, 0)).isEqualTo(Boolean.FALSE);
		assertThat(Serializer.deserializeValue(ValueType.BOOLEAN, "2")).isEqualTo(Boolean.FALSE);
		assertThat(Serializer.deserializeValue(ValueType.BOOLEAN, 2)).isEqualTo(Boolean.FALSE);
		assertThat(Serializer.deserializeValue(ValueType.BOOLEAN, "true")).isEqualTo(Boolean.TRUE);
		assertThat(Serializer.deserializeValue(ValueType.BOOLEAN, "false")).isEqualTo(Boolean.FALSE);
	}

	@Test
	public void testApplyValueByte() throws Exception {

		assertThat(Serializer.deserializeValue(ValueType.BYTE, "42")).isEqualTo((byte) 42);
		assertThat(Serializer.deserializeValue(ValueType.BYTE, 42)).isEqualTo((byte) 42);
		assertThat(Serializer.deserializeValue(ValueType.BYTE, (byte) 42)).isEqualTo((byte) 42);
	}

	@Test
	public void testApplyValueShort() throws Exception {

		assertThat(Serializer.deserializeValue(ValueType.SHORT, "42")).isEqualTo((short) 42);
		assertThat(Serializer.deserializeValue(ValueType.SHORT, 42)).isEqualTo((short) 42);
		assertThat(Serializer.deserializeValue(ValueType.SHORT, (short) 42)).isEqualTo((short) 42);
	}

	@Test
	public void testApplyValueInt() throws Exception {

		assertThat(Serializer.deserializeValue(ValueType.INTEGER, "42")).isEqualTo(42);
		assertThat(Serializer.deserializeValue(ValueType.INTEGER, 42)).isEqualTo(42);
		assertThat(Serializer.deserializeValue(ValueType.INTEGER, 42)).isEqualTo(42);
	}

	@Test
	public void testApplyValueLong() throws Exception {

		assertThat(Serializer.deserializeValue(ValueType.LONG, "42")).isEqualTo(42L);
		assertThat(Serializer.deserializeValue(ValueType.LONG, 42)).isEqualTo(42L);
		assertThat(Serializer.deserializeValue(ValueType.LONG, 42L)).isEqualTo(42L);
	}

	@Test
	public void testApplyValueFloat() throws Exception {

		assertThat(Serializer.deserializeValue(ValueType.FLOAT, "42.1")).isEqualTo(42.1f);
		assertThat(Serializer.deserializeValue(ValueType.FLOAT, 42.1)).isEqualTo(42.1f);
		assertThat(Serializer.deserializeValue(ValueType.FLOAT, 42)).isEqualTo(42.0f);
	}

	@Test
	public void testApplyValueDouble() throws Exception {

		assertThat(Serializer.deserializeValue(ValueType.DOUBLE, "42.1")).isEqualTo(42.1);
		assertThat(Serializer.deserializeValue(ValueType.DOUBLE, 42.1)).isEqualTo(42.1);
		assertThat(Serializer.deserializeValue(ValueType.DOUBLE, 42)).isEqualTo(42.0);
	}

	@Test
	public void testApplyValueDate() throws Exception {
		assertThat(Serializer.deserializeValue(ValueType.DATE, "2018-01-02T12:13:14Z"))
				.isEqualTo(Instant.parse("2018-01-02T12:13:14Z"));
		assertThat(Serializer.deserializeValue(ValueType.DATE, 123456789L))
				.isEqualTo(Instant.ofEpochSecond(123456789L));

		// with milliseconds
		assertThat(Serializer.deserializeValue(ValueType.DATE, "2018-01-02T12:13:14.123Z"))
				.isEqualTo(Instant.parse("2018-01-02T12:13:14.123000000Z"));

		// with microseconds
		assertThat(Serializer.deserializeValue(ValueType.DATE, "2018-01-02T12:13:14.123456Z"))
				.isEqualTo(Instant.parse("2018-01-02T12:13:14.123456000Z"));

		// with nanoseconds
		assertThat(Serializer.deserializeValue(ValueType.DATE, "2018-01-02T12:13:14.123456789Z"))
				.isEqualTo(Instant.parse("2018-01-02T12:13:14.123456789Z"));
	}

	@Test
	public void testDeserializeFileLink() throws Exception {

		FileLink link1 = FileLink.newRemote("root/fileLink.pdf", new MimeType("application/pdf"), "desc", -1L, null,
				FileServiceType.EXTREF);

		Map<String, String> map = ImmutableMap.of("remotePath", "root/fileLink.pdf", "mimeType", "application/pdf",
				"description", "desc", "fileServiceType", "EXTREF");

		assertThat(Serializer.deserializeValue(ValueType.FILE_LINK, map)).isEqualTo(link1);
	}

	@Test
	public void testDeserializeFileLinkSequence() throws Exception {

		FileLink link1 = FileLink.newRemote("root/fileLink1.pdf", new MimeType("application/pdf"), "desc1", -1L, null,
				FileServiceType.EXTREF);
		FileLink link2 = FileLink.newRemote("root/image.jpeg", new MimeType("application/jpeg"), "desc2", -1L, null,
				FileServiceType.EXTREF);

		Map<String, String> map1 = ImmutableMap.of("remotePath", "root/fileLink1.pdf", "mimeType", "application/pdf",
				"description", "desc1", "fileServiceType", "EXTREF");

		Map<String, String> map2 = ImmutableMap.of("remotePath", "root/image.jpeg", "mimeType", "application/jpeg",
				"description", "desc2", "fileServiceType", "EXTREF");

		assertThat((FileLink[]) Serializer.deserializeValue(ValueType.FILE_LINK_SEQUENCE, Arrays.asList(map1, map2)))
				.containsExactly(link1, link2);
	}

	public static class JunitEnum extends EnumerationValue {

		protected JunitEnum(String name, Integer ordinal) {
			super(name, ordinal);
		}
	}

	@Test
	public void testDeserializeCustomEnumerationFromMap() throws Exception {

		JunitEnum one = new JunitEnum("one", 1);
		JunitEnum two = new JunitEnum("two", 2);

		Enumeration<JunitEnum> myCustomJunitEnum = new Enumeration<>("myMDM", JunitEnum.class, "MyCustomJunitEnum");
		myCustomJunitEnum.addValue(one);
		myCustomJunitEnum.addValue(two);

		EnumRegistry.getInstance().add(myCustomJunitEnum);

		Map<String, String> struct = ImmutableMap.of("value", "one");
		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, struct, "myMDM", "MyCustomJunitEnum"))
				.isEqualTo(one);

		assertThatThrownBy(() -> Serializer.deserializeValue(ValueType.ENUMERATION, struct, "wrongSourceName",
				"MyCustomJunitEnum")).isInstanceOf(IllegalArgumentException.class);

		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, "one", "myMDM", "MyCustomJunitEnum"))
				.isEqualTo(one);

		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, 2, "myMDM", "MyCustomJunitEnum")).isEqualTo(two);
	}

	@Test
	public void testDeserializeCustomEnumerationSequenceFromMap() throws Exception {
		JunitEnum one = new JunitEnum("one", 1);
		JunitEnum two = new JunitEnum("two", 2);

		Enumeration<JunitEnum> myCustomJunitEnum = new Enumeration<>("myMDM", JunitEnum.class, "MyCustomJunitSeqEnum");
		myCustomJunitEnum.addValue(one);
		myCustomJunitEnum.addValue(two);

		EnumRegistry.getInstance().add(myCustomJunitEnum);

		List<Map<String, Object>> list = Arrays.asList(ImmutableMap.of("value", "one", "ordinal", 1),
				ImmutableMap.of("value", "two", "ordinal", 2));

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE, list, "myMDM",
				"MyCustomJunitSeqEnum")).containsExactly(one, two);

		assertThatThrownBy(() -> Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE,
				Arrays.asList("one", "two"), "wrongSourceName", "MyCustomJunitSeqEnum"))
						.isInstanceOf(IllegalArgumentException.class);

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE, Arrays.asList(2, 1),
				"myMDM", "MyCustomJunitSeqEnum")).containsExactly(two, one);
	}

	@Test
	public void testDeserializeEnumerationFromMap() throws Exception {

		EnumRegistry.getInstance().list("MDM");

		Map<String, String> struct = ImmutableMap.of("value", "X_AXIS");
		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, struct, "MDM", "AxisType"))
				.isEqualTo(AxisType.X_AXIS);

		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, ImmutableMap.of("ordinal", 1), "MDM", "AxisType"))
				.isEqualTo(AxisType.Y_AXIS);

		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, "XY_AXIS", "MDM", "AxisType"))
				.isEqualTo(AxisType.XY_AXIS);

		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, 1, "MDM", "AxisType")).isEqualTo(AxisType.Y_AXIS);

		struct = ImmutableMap.of("value", "STRING");
		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, struct, "MDM", "ScalarType"))
				.isEqualTo(ScalarType.STRING);

		struct = ImmutableMap.of("value", "NONE");
		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, struct, "MDM", "Interpolation"))
				.isEqualTo(Interpolation.NONE);

		struct = ImmutableMap.of("value", "EXPLICIT");
		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, struct, "MDM", "SequenceRepresentation"))
				.isEqualTo(SequenceRepresentation.EXPLICIT);
	}

	@Test
	public void testDeserializeEnumerationFromEnumerationValue() throws Exception {

		EnumRegistry.getInstance().list("MDM");

		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, AxisType.X_AXIS)).isEqualTo(AxisType.X_AXIS);
		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, ScalarType.STRING)).isEqualTo(ScalarType.STRING);
		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, Interpolation.NONE))
				.isEqualTo(Interpolation.NONE);
		assertThat(Serializer.deserializeValue(ValueType.ENUMERATION, SequenceRepresentation.EXPLICIT))
				.isEqualTo(SequenceRepresentation.EXPLICIT);
	}

	@Test
	public void testDeserializeEnumerationSequenceFromEnumerationValue() throws Exception {

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE,
				new EnumerationValue[0])).isEmpty();

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE,
				new EnumerationValue[] { ScalarType.STRING })).containsExactly(ScalarType.STRING);

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE,
				new EnumerationValue[] { AxisType.X_AXIS, AxisType.Y_AXIS })).containsExactly(AxisType.X_AXIS,
						AxisType.Y_AXIS);
	}

	@Test
	public void testDeserializeEnumerationSequenceFromMap() throws Exception {

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE,
				Collections.emptyList(), "MDM", "AxisType")).isEmpty();

		List<Map<String, Object>> list = Arrays
				.asList(ImmutableMap.of("EnumerationValue", "STRING", "Enumeration", "ScalarType"));

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE,
				new EnumerationValue[] { ScalarType.STRING })).containsExactly(ScalarType.STRING);

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE,
				Arrays.asList("XY_AXIS"), "MDM", "AxisType")).containsExactly(AxisType.XY_AXIS);

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE, Arrays.asList(1),
				"MDM", "AxisType")).containsExactly(AxisType.Y_AXIS);

		list = Arrays.asList(ImmutableMap.of("value", "X_AXIS"), ImmutableMap.of("value", "Y_AXIS"));

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE, list, "MDM",
				"AxisType")).containsExactly(AxisType.X_AXIS, AxisType.Y_AXIS);

		list = Arrays.asList(ImmutableMap.of("ordinal", 1), ImmutableMap.of("ordinal", 2));
		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE, list, "MDM",
				"AxisType")).containsExactly(AxisType.Y_AXIS, AxisType.XY_AXIS);

		assertThat((EnumerationValue[]) Serializer.deserializeValue(ValueType.ENUMERATION_SEQUENCE,
				Arrays.asList("X_AXIS", "Y_AXIS"), "MDM", "AxisType")).containsExactly(AxisType.X_AXIS,
						AxisType.Y_AXIS);
	}

	@Test
	public void testSerializeEnumeration() {

		JunitEnum one = new JunitEnum("one", 1);
		JunitEnum two = new JunitEnum("two", 2);

		Enumeration<JunitEnum> myCustomJunitEnum = new Enumeration<>("myMDM", JunitEnum.class, "MyCustomJunitEnum");
		myCustomJunitEnum.addValue(one);
		myCustomJunitEnum.addValue(two);

		assertThat(Serializer.serializeValue(ValueType.ENUMERATION.create("EnumAttr", "", true, one, "")))
				.isEqualTo("one");
	}

	@Test
	public void testSerializeEmptyEnumeration() {

		assertThat(Serializer.serializeValue(ValueType.ENUMERATION.create("EnumAttr", "", false, null, ""))).isNull();
	}

	@Test
	public void testSerializeEnumerationSequence() {

		JunitEnum one = new JunitEnum("one", 1);
		JunitEnum two = new JunitEnum("two", 2);

		Enumeration<JunitEnum> myCustomJunitEnum = new Enumeration<>("myMDM", JunitEnum.class, "MyCustomJunitEnum");
		myCustomJunitEnum.addValue(one);
		myCustomJunitEnum.addValue(two);

		assertThat((String[]) Serializer.serializeValue(ValueType.ENUMERATION_SEQUENCE.create("EnumSeqAttr", "", true,
				new EnumerationValue[] { two, one }, ""))).containsExactly("two", "one");
	}

	@Test
	public void testSerializeEmptyEnumerationSequence() {

		assertThat((String[]) Serializer.serializeValue(
				ValueType.ENUMERATION_SEQUENCE.create("EnumSeqAttr", "", true, new EnumerationValue[0], ""))).isEmpty();
	}

	@Test
	public void testSerializeDate() {
		Instant.parse("2017-01-02T01:02:03Z");

		assertThat(Serializer.serializeValue(ValueType.DATE.create("DateAttr", Instant.parse("2018-01-02T12:13:14Z"))))
				.isEqualTo("2018-01-02T12:13:14Z");

		// with milliseconds
		assertThat(Serializer
				.serializeValue(ValueType.DATE.create("DateAttr", Instant.parse("2018-01-02T12:13:14.123000000Z"))))
						.isEqualTo("2018-01-02T12:13:14.123Z");

		// with microseconds
		assertThat(Serializer
				.serializeValue(ValueType.DATE.create("DateAttr", Instant.parse("2018-01-02T12:13:14.123456000Z"))))
						.isEqualTo("2018-01-02T12:13:14.123456Z");

		// with nanoseconds
		assertThat(Serializer
				.serializeValue(ValueType.DATE.create("DateAttr", Instant.parse("2018-01-02T12:13:14.123456789Z"))))
						.isEqualTo("2018-01-02T12:13:14.123456789Z");

	}

}
