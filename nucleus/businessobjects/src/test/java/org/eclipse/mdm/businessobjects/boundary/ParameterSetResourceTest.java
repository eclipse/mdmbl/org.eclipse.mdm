/********************************************************************************
 * Copyright (c) 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;

import org.eclipse.mdm.api.base.ConnectionException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.ParameterSet;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.api.odsadapter.ODSHttpContextFactory;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.utils.ProtobufMessageBodyProvider;
import org.eclipse.mdm.testutils.GlassfishExtension;
import org.eclipse.mdm.testutils.OdsServerContainer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers(disabledWithoutDocker = true)
public class ParameterSetResourceTest {

	@Container
	static OdsServerContainer odsServer = OdsServerContainer.create();

	@RegisterExtension
	static GlassfishExtension glassfish = new GlassfishExtension(odsServer);

	static Quantity quantity;
	static TestStep testStep;
	Measurement measurement;
	Channel channel;

	@BeforeAll
	static void setup() throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		quantity = ef.createQuantity("MyQuantity");

		Project project = ef.createProject("MyProject");
		Pool pool = ef.createPool("MyPool", project);
		Test test = ef.createTest("MyTest", pool);
		testStep = ef.createTestStep("MyTestStep", test);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(quantity));
			t.create(Arrays.asList(project));
			t.commit();
		}
		context.close();
	}

	@BeforeEach
	void initData() throws ConnectionException {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		measurement = ef.createMeasurement("MyMeasurement" + System.currentTimeMillis(), testStep);
		channel = ef.createChannel("MyChannel" + System.currentTimeMillis(), measurement, quantity);

		try (Transaction t = em.startTransaction()) {
			t.create(Arrays.asList(measurement));
			t.commit();
		}
		context.close();
	}

	@org.junit.jupiter.api.Test
	void createParameterSetAtMeasurement(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String createParameterSet = "{ \"Name\": \"MyParameterSet1\", \"Measurement\": \"" + measurement.getID()
				+ "\" }";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/parametersets").request()
				.cookie(sessionCookie)
				.post(Entity.entity(createParameterSet, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("ParameterSet");
		assertThat(response.getData().size()).isEqualTo(1);
		assertThat(response.getData().get(0).getRelations()).allSatisfy(rel -> {
			assertThat(rel.getEntityType()).isEqualTo("Measurement");
			assertThat(rel.getIds()).containsExactly(measurement.getID());
		});
	}

	@org.junit.jupiter.api.Test
	void createParameterSetsAtMeasurementInBatchMode(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String createParameterSets = "[\n";
		createParameterSets += "{ \"Name\": \"MyParameterSet1\", \"Measurement\": \"" + measurement.getID() + "\" },\n";
		createParameterSets += "{ \"Name\": \"MyParameterSet2\", \"Measurement\": \"" + measurement.getID() + "\" }\n";
		createParameterSets += "]";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/parametersets").request()
				.cookie(sessionCookie)
				.post(Entity.entity(createParameterSets, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("ParameterSet");
		assertThat(response.getData().size()).isEqualTo(2);
		assertThat(response.getData().get(0).getRelations()).allSatisfy(rel -> {
			assertThat(rel.getEntityType()).isEqualTo("Measurement");
			assertThat(rel.getIds()).containsExactly(measurement.getID());
		});
	}

	@org.junit.jupiter.api.Test
	void createParameterSetAtChannel(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String createParameterSet = "{ \"Name\": \"MyParameterSet1\", \"Channel\": \"" + channel.getID() + "\" }";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/parametersets").request()
				.cookie(sessionCookie)
				.post(Entity.entity(createParameterSet, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("ParameterSet");
		assertThat(response.getData().size()).isEqualTo(1);
		assertThat(response.getData().get(0).getRelations()).allSatisfy(rel -> {
			assertThat(rel.getEntityType()).isEqualTo("Channel");
			assertThat(rel.getIds()).containsExactly(channel.getID());
		});
	}

	@org.junit.jupiter.api.Test
	void createParameterSetsAtChannelInBatchMode(NewCookie sessionCookie) {
		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String createParameterSets = "[\n";
		createParameterSets += "{ \"Name\": \"MyParameterSet1\", \"Channel\": \"" + channel.getID() + "\" },\n";
		createParameterSets += "{ \"Name\": \"MyParameterSet2\", \"Channel\": \"" + channel.getID() + "\" }\n";
		createParameterSets += "]";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/parametersets").request()
				.cookie(sessionCookie)
				.post(Entity.entity(createParameterSets, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("ParameterSet");
		assertThat(response.getData().size()).isEqualTo(2);
		assertThat(response.getData().get(0).getRelations()).allSatisfy(rel -> {
			assertThat(rel.getEntityType()).isEqualTo("Channel");
			assertThat(rel.getIds()).containsExactly(channel.getID());
		});
	}

	@org.junit.jupiter.api.Test
	void updateParameterSet(NewCookie sessionCookie) throws Exception {
		ParameterSet parameterSet = createTestParameterSet().get(0);

		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String updateParameterSet = "{\"Name\": \"test123\"}";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/parametersets/" + parameterSet.getID())
				.request().cookie(sessionCookie)
				.put(Entity.entity(updateParameterSet, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("ParameterSet");
		assertThat(response.getData().size()).isEqualTo(1);
		assertThat(response.getData().get(0).getName()).isEqualTo("test123");
		assertThat(response.getData().get(0).getRelations()).allSatisfy(rel -> {
			assertThat(rel.getEntityType()).isEqualTo("Measurement");
			assertThat(rel.getIds()).containsExactly(measurement.getID());
		});
	}

	@org.junit.jupiter.api.Test
	void updateParameterSetInBatchMode(NewCookie sessionCookie) throws Exception {
		List<ParameterSet> parameterSets = createTestParameterSet();

		WebTarget target = glassfish.getRoot().register(ProtobufMessageBodyProvider.class);

		String updateParameterSets = "{\n";
		updateParameterSets += "\"" + parameterSets.get(0).getID() + "\": {\"Name\": \"ParamSet1\"},\n";
		updateParameterSets += "\"" + parameterSets.get(1).getID() + "\": {\"Name\": \"ParamSet2\"}\n";
		updateParameterSets += "}";

		final MDMEntityResponse response = target.path("mdm/environments/MDM/parametersets").request()
				.cookie(sessionCookie)
				.put(Entity.entity(updateParameterSets, MediaType.APPLICATION_JSON_TYPE), MDMEntityResponse.class);

		assertThat(response.getType()).isEqualTo("ParameterSet");
		assertThat(response.getData()).satisfiesOnlyOnce(e -> {
			assertThat(e.getName()).isEqualTo("ParamSet1");
			assertThat(e.getRelations()).allSatisfy(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("Measurement");
				assertThat(rel.getIds()).containsExactly(measurement.getID());
			});
		}).satisfiesOnlyOnce(e -> {
			assertThat(e.getName()).isEqualTo("ParamSet2");
			assertThat(e.getRelations()).allSatisfy(rel -> {
				assertThat(rel.getEntityType()).isEqualTo("Channel");
				assertThat(rel.getIds()).containsExactly(channel.getID());
			});
		});
	}

	private List<ParameterSet> createTestParameterSet() throws Exception {
		ApplicationContext context = new ODSHttpContextFactory().connect("MDM", odsServer.getConnectionParameters());

		EntityFactory ef = context.getEntityFactory().get();
		EntityManager em = context.getEntityManager().get();

		List<ParameterSet> list = new ArrayList<>();
		list.add(ef.createParameterSet("MyParameterSet1" + System.currentTimeMillis(), measurement));
		list.add(ef.createParameterSet("MyParameterSet2" + System.currentTimeMillis(), channel));

		try (Transaction t = em.startTransaction()) {
			t.create(list);
			t.commit();
		}
		context.close();

		return list;
	}
}