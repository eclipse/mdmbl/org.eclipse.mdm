package org.eclipse.mdm.query.entity;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

public class CookieFilterTest implements ClientRequestFilter, ClientResponseFilter {

	private Map<String, NewCookie> cookies = new HashMap<>();

	private Client client;

	public CookieFilterTest(Client client) {
		this.client = client;
	}

	@Override
	public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
		if (responseContext.getStatus() == Response.Status.UNAUTHORIZED.getStatusCode()) {

		} else if (cookies.isEmpty()) {
			cookies.putAll(responseContext.getCookies());
			// the cookie name is in parameter resolvedUri for the response
			for (Method m : responseContext.getClass().getMethods()) {
				if (m.getName().equalsIgnoreCase("getResolvedRequestUri")) {
					try {
						Object obj = m.invoke(responseContext);
						String cookieStr = obj.toString().substring(1 + obj.toString().indexOf(";"));
						cookies.put(cookieStr.substring(0, cookieStr.indexOf("=")).toUpperCase(),
								new NewCookie(cookieStr.substring(0, cookieStr.indexOf("=")).toUpperCase(),
										cookieStr.substring(cookieStr.indexOf("=") + 1)));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		if (cookies != null && !cookies.isEmpty()) {
			String cookie = cookies.values().stream().map(c -> c.getName() + "=" + c.getValue())
					.collect(Collectors.joining("; "));
			requestContext.getHeaders().add(HttpHeaders.COOKIE, cookie);
		}
	}

}
