INSERT INTO openmdm.preference (keycol,"source",username,valuecol) VALUES 
('nodeprovider.generic_measured',NULL,NULL,'{
	"id" : "generic_measured",
	"name" : "Generic Measured",
	"contexts" : {
	    "*" : {
		    "type" : "Environment",
		    "filterAttributes" : ["Id"],
		    "labelAttributes" : ["Name"],
		    "virtual" : false,
		    "child" : {
		        "type" : "Project",
		        "filterAttributes" : ["Id"],
		        "labelAttributes" : ["Name", "Id"],
		        "virtual" : false,
		        "child" : {        
			        "type" : "vehicle",
			        "filterAttributes" : ["model"],
			        "labelAttributes" : ["model"],
		            "labelExpression" : "Model: ${model}",
			        "virtual" : true,
			        "contextState": "MEASURED",
			        "child" : {
						"type" : "Measurement",
						"filterAttributes" : ["DateCreated"],
						"labelAttributes" : ["DateCreated"],
						"labelExpression" : "${fn:formatDate(DateCreated, \"YYYY 'KW' ww\")}",
						"valuePrecision" : "DAY",
						"virtual" : true,
						"contextState": "MEASURED",
						"child" : {
							"type" : "Test",
							"filterAttributes" : ["Id"],
							"labelAttributes" : ["Name"],
							"virtual" : false,
							"child" : {
								"type" : "TestStep",
								"filterAttributes" : ["Id"],
								"labelAttributes" : ["Name"],
								"virtual" : false,
								"child" : {
									"type" : "Measurement",
									"filterAttributes" : ["Id"],
									"labelAttributes" : ["Name"],
									"virtual" : false,
									"child" : {
										"type" : "ChannelGroup",
										"filterAttributes" : ["Id"],
										"labelAttributes" : ["Name"],
										"virtual" : false,
										"child" : {
											"type" : "Channel",
											"filterAttributes" : ["Id"],
											"labelAttributes" : ["Name"],
											"virtual" : false
										}
									}
								}
							}
						}
					}
		        }
		    }
	    }
	}
}
')
;
