/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import com.google.common.base.MoreObjects;

/**
 * @author jz
 *
 */
public class MDMEnumValue {

	private final String value;
	private final Integer ordinal;

	public MDMEnumValue(String value, Integer ordinal) {
		this.value = value;
		this.ordinal = ordinal;
	}

	public String getValue() {
		return value;
	}

	public Integer getOrdinal() {
		return ordinal;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(MDMEnumValue.class).add("name", value).add("ordinal", ordinal).toString();
	}
}
