/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.ENTITYATTRIBUTE_NAME;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.L;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.dflt.model.ProjectDomain;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * {@link ProjectDomain} resource handling REST requests
 * 
 * @author Alexander Knoblauch, Peak Solution GmbH
 *
 */
@Tag(name = "Status")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/projectdomains")
public class ProjectDomainResource {

	@EJB
	private EntityService entityService;

	@Parameter(description = "Name of the MDM datasource", required = true)
	@PathParam(REQUESTPARAM_SOURCENAME)
	private String sourceName;

	/**
	 * Returns the found {@link ProjectDomain}.
	 * 
	 * @param id id of the {@link ProjectDomain}
	 * @return the found {@link ProjectDomain} as {@link Response}
	 */
	@GET
	@Operation(summary = "Find a ProjectDomain by ID", description = "Returns ProjectDomain based on ID", responses = {
			@ApiResponse(description = "The ProjectDomain", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response find(
			@Parameter(description = "ID of the ProjectDomain", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), ProjectDomain.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the (filtered) {@link ProjectDomain}s.
	 * 
	 * @param filter filter string to filter the {@link ProjectDomain} result
	 * @return the (filtered) {@link ValueList}s as {@link Response}
	 */
	@GET
	@Operation(summary = "Find ProjectDomain by filter", description = "Get list of ProjectDomains", responses = {
			@ApiResponse(description = "The ProjectDomains", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response findAll(@Parameter(description = "Filter expression", required = false) String filter) {
		return entityService.findAll(V(sourceName), ProjectDomain.class, filter)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the created {@link ProjectDomain}.
	 * 
	 * @param body The {@link ProjectDomain} to create.
	 * @return the created {@link ProjectDomain} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new ProjectDomain", responses = {
			@ApiResponse(description = "The created ProjectDomain", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response create(String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.create(V(sourceName), ProjectDomain.class, L(requestBody.getStringValueSupplier(ENTITYATTRIBUTE_NAME)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Updates the {@link ProjectDomain} with all parameters set in the given JSON
	 * body of the request.
	 * 
	 * @param id   the identifier of the {@link ProjectDomain} to update.
	 * @param body the body of the request containing the attributes to update
	 * @return the updated {@link ProjectDomain}
	 */
	@PUT
	@Operation(summary = "Update an existing ProjectDomain", description = "Updates the ProjectDomain with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated ProjectDomain", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response update(
			@Parameter(description = "ID of the ProjectDomain", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), ProjectDomain.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Deletes and returns the deleted {@link ProjectDomain}.
	 * 
	 * @param id The identifier of the {@link ProjectDomain} to delete.
	 * @return the deleted {@link ProjectDomain }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing ProjectDomain", responses = {
			@ApiResponse(description = "The deleted ProjectDomain", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response delete(
			@Parameter(description = "ID of the ProjectDomain", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), ProjectDomain.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}
}