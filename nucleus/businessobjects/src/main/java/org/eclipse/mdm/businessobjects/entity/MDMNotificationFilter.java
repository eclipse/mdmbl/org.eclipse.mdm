/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.notification.NotificationFilter.ModificationType;

/**
 * A filter for required notifications events. Events can be filtered by types
 * (Create, modify, delete, ...) and entity type (Test, TestStep, Measurement).
 * 
 * @since 1.0.0
 * @author Matthias Koller, Peak Solution GmbH
 */
public class MDMNotificationFilter {

	private EnumSet<ModificationType> types = EnumSet.allOf(ModificationType.class);

	private Set<String> entityTypes = Collections.emptySet();

	/**
	 * Returns the {@link ModificationType}s of this filter.
	 * 
	 * @return Set with {@link ModificationType} is returned.
	 */
	public Set<ModificationType> getTypes() {
		return types;
	}

	/**
	 * Sets the {@link ModificationType}s used to filter the notifications.
	 * 
	 * @param types Set with {@link ModificationType}
	 */
	public void setTypes(Set<ModificationType> types) {
		this.types = EnumSet.copyOf(types);
	}

	/**
	 * Returns the names of {@link EntityType}s of this filter.
	 * 
	 * @return Set with name {@link EntityType} is returned.
	 */
	public Set<String> getEntityTypes() {
		return entityTypes;
	}

	/**
	 * Sets the names of {@link EntityType}s used to filter the notifications.
	 * 
	 * @param entityTypes Set with name of {@link EntityType}.
	 */
	public void setEntityTypes(Set<String> entityTypes) {
		this.entityTypes = new HashSet<>(entityTypes);
	}
}
