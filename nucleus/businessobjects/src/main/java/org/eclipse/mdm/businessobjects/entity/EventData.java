/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.User;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.common.base.MoreObjects;

/**
 * Event data class which is returned as json in the event streaming api
 *
 * @author Juergen Kleck, Peak Solution GmbH
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(Include.NON_NULL)
public class EventData implements Serializable {

	@JsonFormat(shape = Shape.STRING, timezone = "UTC")
	private Instant received;
	private String type;
	private MDMEntityResponse mdmEntity;
	private User user;
	private EntityType entityType;
	private List<String> ids;

	public EventData() {
		received = Instant.now();
	}

	public EventData(String type, MDMEntityResponse mdmEntity, User user, EntityType entityType, List<String> ids) {
		this();
		this.type = type;
		this.mdmEntity = mdmEntity;
		this.user = user;
		this.entityType = entityType;
		this.ids = ids;
	}

	public Instant getReceived() {
		return received;
	}

	public void setReceived(Instant received) {
		this.received = received;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public MDMEntityResponse getMdmEntity() {
		return mdmEntity;
	}

	public void setMdmEntity(MDMEntityResponse mdmEntity) {
		this.mdmEntity = mdmEntity;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public List<String> getIds() {
		return ids;
	}

	public void setIds(List<String> ids) {
		this.ids = ids;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("received", received).add("type", type).add("mdmEntity", mdmEntity)
				.add("user", user).add("entityType", entityType).add("ids", ids).toString();
	}

}
