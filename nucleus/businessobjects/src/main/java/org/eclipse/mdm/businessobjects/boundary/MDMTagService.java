/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Tagable;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.ExtSystem;
import org.eclipse.mdm.api.dflt.model.MDMTag;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.connector.boundary.ConnectorService;

/**
 * MDMTagService Bean implementation with available operations
 * 
 * @author Alexander Knoblauch, Peak Solution GmbH
 *
 */
@Stateless
public class MDMTagService {

	@Inject
	private ConnectorService connectorService;

	/**
	 * returns the matching {@link MDMTags}s using the given filter or all
	 * {@link ExtSystem}s if no filter is available
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the found {@link ExtSystem}s
	 */
	public List<MDMTag> getMDMTags(String sourceName) {
		List<MDMTag> list = new ArrayList<>();
		try {
			Optional<List<MDMTag>> values = this.connectorService.getContextByName(sourceName).getEntityManager()
					.map(em -> em.loadAll(MDMTag.class));

			if (values.isPresent()) {
				list = values.get();
			}
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * returns a {@link MDMTag} identified by the given id.
	 * 
	 * @param sourceName  name of the source (MDM {@link Environment} name)
	 * @param extSystemId id of the {@link MDMTag}
	 * @return the matching {@link MDMTag}
	 */
	public MDMTag getMDMTag(String sourceName, String mdmTagId) {
		try {
			EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
			return em.load(MDMTag.class, mdmTagId);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * 
	 * @param sourceName
	 * @param mdmTagId
	 * @return
	 */
	public List<Tagable> getTagedEntities(String sourceName, String mdmTagId) {
		MDMTag mdmTag = getMDMTag(sourceName, mdmTagId);

		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

		List<Tagable> tagedItems = new ArrayList<>();
		tagedItems.addAll(em.loadRelatedEntities(mdmTag, "Test", Test.class));
		tagedItems.addAll(em.loadRelatedEntities(mdmTag, "TestStep", TestStep.class));
		tagedItems.addAll(em.loadRelatedEntities(mdmTag, "MeaResult", Measurement.class));
		return tagedItems;
	}

	public List<MDMTag> getMDMTags(String sourceName, String id, Class<? extends Tagable> sourceClass) {
		Tagable tagable = null;

		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

		if (Test.class.equals(sourceClass)) {
			tagable = em.load(Test.class, id);
		} else if (TestStep.class.equals(sourceClass)) {
			tagable = em.load(TestStep.class, id);
		} else if (Measurement.class.equals(sourceClass)) {
			tagable = em.load(Measurement.class, id);
		} else {
			throw new MDMEntityAccessException("Not supported tagable class: " + sourceClass);
		}

		return em.loadRelatedEntities(tagable, "MDMTag", MDMTag.class);
	}

	public List<Tagable> tagEntity(String sourceName, String id, Tagable tagable) {
		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		Transaction t = em.startTransaction();
		MDMTag mdmTag = getMDMTag(sourceName, id);

		mdmTag.tagItem(tagable);
		t.update(Collections.singletonList(mdmTag));
		t.commit();

		return getTagedEntities(sourceName, id);
	}

	public List<Tagable> removeEntity(String sourceName, String id, Tagable tagable) {
		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		Transaction t = em.startTransaction();
		MDMTag mdmTag = getMDMTag(sourceName, id);

		mdmTag.removeFromTag(tagable);
		t.update(Collections.singletonList(mdmTag));
		t.commit();

		return getTagedEntities(sourceName, id);
	}
}
