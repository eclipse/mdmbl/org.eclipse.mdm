/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import org.eclipse.mdm.api.base.model.FileLink;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * MDMFileLinkExt (Entity for a {@link FileLink})
 * 
 */
@Schema(description = "Extended representation of an MDM FileLink")
public class MDMFileLinkExt {

	private final String identifier;

	private final String mimeType;

	private final String description;
	
	private final String fileName;

	/**
	 * Constructor.
	 * 
	 * @param identifier
	 * @param mimeType
	 * @param description
	 * @param fileName
	 */
	public MDMFileLinkExt(String identifier, String mimeType, String description, String fileName) {
		this.identifier = identifier;
		this.mimeType = mimeType;
		this.description = description;
		this.fileName = fileName;
	}

	/**
	 * @return the identifier
	 */
	@Schema(description = "The identifier of the FileLink")
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @return the mimeType
	 */
	@Schema(description = "The mimetype of the FileLink")
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @return the description
	 */
	@Schema(description = "The description of the FileLink")
	public String getDescription() {
		return description;
	}
	
	/**
	 * @return the origFileName
	 */
	@Schema(description = "The file name of the FileLink")
	public String getFileName() {
		return fileName;
	}
}
