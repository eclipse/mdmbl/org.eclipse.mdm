/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.io.IOException;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.ParameterSet;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.entity.SearchAttributeResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.collection.List;
import io.vavr.control.Try;

/**
 * {@link ParameterSet} resource
 * 
 * @author Alexander Knoblauch, Peak Solution GmbH
 *
 */
@Tag(name = "ParameterSet")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/parametersets")
public class ParameterSetResource {

	@EJB
	private ConnectorServiceProxy connectorService;

	@EJB
	private ParameterSetService parameterSetService;

	@EJB
	private EntityService entityService;

	@Context
	javax.ws.rs.ext.Providers providers;

	/**
	 * delegates the request to the {@link ParameterSetService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link ParameterSet} result
	 * @return the result of the delegated request as {@link Response}
	 */

	@GET
	@Operation(summary = "Find Par by filter", description = "Get list of pools", responses = {
			@ApiResponse(description = "The parametersets", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getParameterSets(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "Filter expression", required = false) @QueryParam("filter") String filter) {

		return Try.of(() -> parameterSetService.getParameterSets(sourceName, filter)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, ParameterSet.class)).get();
	}

	/**
	 * delegates the request to the {@link ParameterSetService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the search attributes", description = "Get list of search attributes", responses = {
			@ApiResponse(description = "The search attributes", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchattributes")
	public Response getSearchAttributes(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		return Try.of(() -> parameterSetService.getSearchAttributes(sourceName))
				.map(attrs -> ServiceUtils.toResponse(new SearchAttributeResponse(attrs), Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link ParameterSetService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link Pool}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find a ParameterSet by ID", description = "Returns ParameterSet based on ID", responses = {
			@ApiResponse(description = "The ParameterSet", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response findParameterSet(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Pool", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), ParameterSet.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the created {@link ParameterSet}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link ParameterSet} to create.
	 * @return the created {@link ParameterSet} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new ParameterSet / multiple ParameterSets", responses = {
			@ApiResponse(description = "The created ParameterSet(s)", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
					@ExampleObject(name = "Create a single ParameterSet", value = "{ \"Name\": \"MyParameterSet\", \"Measurement\": \"2000\" }"),
					@ExampleObject(name = "Create multiple ParameterSets", value = "[\n{ \"Name\": \"MyParameterSet1\", \"Measurement\": \"2000\" }, "
							+ "{ \"Name\": \"MyParameterSet2\", \"Measurement\": \"2001\" }\n]") })) String body) {

		if (body != null && body.trim().startsWith("[")) {
			try {
				ContextResolver<ObjectMapper> resolver = providers.getContextResolver(ObjectMapper.class,
						MediaType.APPLICATION_JSON_TYPE);
				ObjectMapper mapper = resolver.getContext(ObjectMapper.class);

				List<ParameterSet> list = List.ofAll(parameterSetService.createParameterSets(sourceName,
						mapper.readValue(body, new TypeReference<java.util.List<CreateParameterSet>>() {
						})));

				return ServiceUtils.buildEntityResponse(list, Status.CREATED);
			} catch (IOException e) {
				throw new DataAccessException("Could not create multiple Parameters.", e);
			}
		} else {
			return entityService
					.create(V(sourceName), ParameterSet.class,
							entityService.extractRequestBody(body, sourceName,
									io.vavr.collection.List.of(Measurement.class, Channel.class)))
					.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
		}

	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class CreateParameterSet {
		@JsonProperty("Name")
		public String name;
		@JsonProperty("Channel")
		public String channel;
		@JsonProperty("Measurement")
		public String measurement;
	}

	/**
	 * Updates the {@link ParameterSet} with all parameters set in the given JSON
	 * body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link ParameterSet} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link ParameterSet}
	 */
	@PUT
	@Operation(summary = "Update an existing ParameterSet", description = "Updates the ParameterSet with all parameters set in the given body of the request.", responses = {
			@ApiResponse(description = "The updated ParameterSet", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response update(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ParameterSet", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		ParameterSet ps = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class))
				.loadWithoutChildren(ParameterSet.class, id);

		return entityService.update(V(sourceName), Try.of(() -> ps), requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Updates the {@link ParameterSet} with all parameters set in the given JSON
	 * body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link ParameterSet} to update.
	 * @param body       the body of the request containing the attributes to
	 *                   update. Example: { "4711": { "Description": "My first
	 *                   ParameterSet" }, "4712": { "Description": "My second
	 *                   ParameterSet" } }
	 * @return the updated {@link ParameterSet}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Operation(summary = "Updates the ParameterSets with all values set in the body of the request", responses = {
			@ApiResponse(responseCode = "200", description = "Successfully updated ParameterSets", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response updateParameterSets(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
					@ExampleObject(name = "Update the names of two ParameterSets", value = "{\n" + "	\"4711\": {\n"
							+ "		\"Name\": \"My first ParameterSet\"\n" + "	},\n" + "	\"4712\": {\n"
							+ "		\"Name\": \"My second ParameterSet\"\n" + "	}\n" + "}") })) String body) {
		RequestBody requestBody = RequestBody.create(body);

		final io.vavr.collection.Map<String, Object> entityIdToAttributesMap = requestBody.getValueMapSupplier().get();
		final List<ParameterSet> updatedEntities = List.ofAll(
				entityService.updateEntitiesAndPersist(ParameterSet.class, entityIdToAttributesMap, V(sourceName)));
		return ServiceUtils.buildEntityResponse(updatedEntities, Status.OK);
	}

	/**
	 * Deletes and returns the deleted {@link ParameterSet}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link Pool} to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing ParameterSet", responses = {
			@ApiResponse(description = "The deleted ParameterSet", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response delete(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ParameterSet", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), ParameterSet.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}
}
