/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import org.eclipse.mdm.api.base.model.*;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.ExtSystem;
import org.eclipse.mdm.api.dflt.model.ExtSystemAttribute;
import org.eclipse.mdm.businessobjects.control.*;
import org.eclipse.mdm.connector.boundary.ConnectorService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.*;

/**
 * ExtSystemService Bean implementation with available
 * operations
 * 
 * @author Juergen Kleck, Peak Solution GmbH
 *
 */
@Stateless
public class ExtSystemService {

	@Inject
	private ConnectorService connectorService;

	/**
	 * returns the matching {@link ExtSystem}s using the given filter or all
	 * {@link ExtSystem}s if no filter is available
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the found {@link ExtSystem}s
	 */
	public List<ExtSystem> getExtSystems(String sourceName) {
		List<ExtSystem> list = new ArrayList<>();
		try {
			Optional<List<ExtSystem>> values = this.connectorService.getContextByName(sourceName).getEntityManager()
					.map(em -> em.loadAll(ExtSystem.class));

			if(values.isPresent()) {
				list = values.get();
			}
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * returns a {@link ExtSystem} identified by the given id.
	 * 
	 * @param sourceName    name of the source (MDM {@link Environment} name)
	 * @param extSystemId id of the {@link ExtSystem}
	 * @return the matching {@link ExtSystem}
	 */
	public ExtSystem getExtSystem(String sourceName, String extSystemId) {
		try {
			EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
			return em.load(ExtSystem.class, extSystemId);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * returns the matching {@link ExtSystemAttribute}s using the given filter or all
	 * {@link ExtSystemAttribute}s if no filter is available
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the id of the external system
	 * @return the found {@link ExtSystemAttribute}s
	 */
	public List<ExtSystemAttribute> getExtSystemAttributes(String sourceName, String id) {
		List<ExtSystemAttribute> list = new ArrayList<>();
		try {

			Optional<List<ExtSystemAttribute>> values = this.connectorService.getContextByName(sourceName).getEntityManager()
					.map(em -> em.loadChildren(em.load(ExtSystem.class, id), ExtSystemAttribute.class));

			if(values.isPresent()) {
				list = values.get();
			}
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
		return list;
	}

}
