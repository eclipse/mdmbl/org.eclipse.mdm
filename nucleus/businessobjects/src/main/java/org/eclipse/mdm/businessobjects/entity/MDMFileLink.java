/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import org.eclipse.mdm.api.base.model.FileLink;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * MDMFileLink (Entity for a {@link FileLink})
 * 
 */
@Schema(description = "Representation of a MDM FileLink")
public class MDMFileLink {

	private final String remotePath;

	private final String mimeType;

	private final String description;

	/**
	 * Constructor.
	 * 
	 * @param remotePath
	 * @param mimeType
	 * @param description
	 */
	public MDMFileLink(String remotePath, String mimeType, String description) {
		this.remotePath = remotePath;
		this.mimeType = mimeType;
		this.description = description;
	}

	/**
	 * @return the remotePath
	 */
	@Schema(description = "The remote path of the FileLink")
	public String getRemotePath() {
		return remotePath;
	}

	/**
	 * @return the mimeType
	 */
	@Schema(description = "The mimetype of the FileLink")
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @return the description
	 */
	@Schema(description = "The description of the FileLink")
	public String getDescription() {
		return description;
	}
}
