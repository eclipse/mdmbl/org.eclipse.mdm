/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.businessobjects.service.DescribableContexts;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MultipleContextResponse {

	/** transferable data content */
	private List<MultipleContextCollection> data;

	/**
	 * Constructor
	 * 
	 * @param context {@link DescribableContexts} holding the context data (ordered
	 *                and measured)
	 */
	public MultipleContextResponse(List<DescribableContexts> contexts) {
		this.data = new ArrayList<>();

		for (DescribableContexts context : contexts) {
			MultipleContextCollection contextData = new MultipleContextCollection();

			if (context.getTestStep() != null) {
				contextData.setContext(context.getOrderedContext());
				contextData.setEntity(new MDMEntityResponse(TestStep.class, context.getTestStep()));
			} else {
				contextData.setContext(context.getMeasuredContext());
				if (context.getAnyMeasurement().isPresent()) {
					contextData.setEntity(new MDMEntityResponse(Measurement.class, context.getAnyMeasurement().get()));
				}
			}

			this.data.add(contextData);
		}
	}

	/**
	 * returns the context data
	 * 
	 * @return the context data
	 */
	public List<MultipleContextCollection> getData() {
		return Collections.unmodifiableList(this.data);
	}
}
