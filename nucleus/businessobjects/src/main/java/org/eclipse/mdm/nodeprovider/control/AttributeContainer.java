/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.control;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.search.ContextState;

public class AttributeContainer {

	private ContextState contextState;
	private Attribute attribute;
	private boolean valid;
	private Object value;

	public AttributeContainer(final ContextState contextState, final Attribute attribute, final boolean valid,
			final Object value) {
		this.contextState = contextState;
		this.attribute = attribute;
		this.valid = valid;
		this.value = value;
	}

	public ContextState getContextState() {
		return contextState;
	}

	public Attribute getAttribute() {
		return attribute;
	}

	public boolean isValid() {
		return valid;
	}

	public Object getValue() {
		return value;
	}
}
