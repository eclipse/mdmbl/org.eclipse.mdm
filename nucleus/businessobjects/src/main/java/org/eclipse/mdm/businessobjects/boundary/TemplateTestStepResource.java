/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.ENTITYATTRIBUTE_NAME;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTTYPE;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID2;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.L;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.dflt.model.TemplateMeasurement;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.entity.SearchAttribute;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.collection.List;
import io.vavr.control.Try;

/**
 * {@link TemplateTestStep} resource handling REST requests
 * 
 * @author Alexander Nehmer, science+computing AG Tuebingen (Atos SE)
 *
 */
@Tag(name = "Template")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/tplteststeps")
public class TemplateTestStepResource {

	@EJB
	private EntityService entityService;
	
	@EJB
	private TemplateMeasurementService tplMeaResultService;

	/**
	 * Returns the found {@link TemplateTestStep}.
	 * 
	 * @param sourceName  name of the source (MDM {@link Environment} name)
	 * @param contextType {@link ContextType} of the {@link TemplateTestStep} to
	 *                    load
	 * @param id          id of the {@link TemplateTestStep}
	 * @return the found {@link TemplateTestStep} as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response find(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), TemplateTestStep.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the (filtered) {@link TemplateTestStep}s.
	 * 
	 * @param sourceName  name of the source (MDM {@link Environment} name)
	 * @param contextType {@link ContextType} of the {@link TemplateTestStep} to
	 *                    load
	 * @param filter      filter string to filter the {@link TemplateTestStep}
	 *                    result
	 * @return the (filtered) {@link TemplateTestStep}s as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_CONTEXTTYPE) String contextTypeParam, @QueryParam("filter") String filter) {
		return entityService.findAll(V(sourceName), TemplateTestStep.class, filter)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}
	
	/**
	 * Returns the {@link TemplateMeasurement}s linked to a {@link TemplateTestStep}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link TemplateTestStep}
	 * @return the {@link TemplateMeasurement}s as {@link Response}
	 */
	@GET
	@Operation(summary = "Gets the TemplateMeasurements linked to a TemplateTestStep", description = "Returns the TemplateMeasurements linked to a TemplateTestStep.", responses = {
			@ApiResponse(description = "The TemplateMeasurements", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/tplmearesult")
	public Response getTplMeaResults(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the TemplateTestStep", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return Try
				.of(() -> tplMeaResultService.getTemplateMeasurements(V(sourceName), V(id),
						TemplateTestStep.class))
				.map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, TemplateMeasurement.class)).get();
	}

	/**
	 * Returns the created {@link TemplateTestStepValue}.
	 * 
	 * @param body The {@link TemplateTestStep} to create.
	 * @return the created {@link TemplateTestStep} as {@link Response}.
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_CONTEXTTYPE) String contextTypeParam, String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.create(V(sourceName), TemplateTestStep.class,
						L(requestBody.getStringValueSupplier(ENTITYATTRIBUTE_NAME)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Updates the {@link TemplateTestStep} with all parameters set in the given
	 * JSON body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link TemplateTestStep} to delete.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link TemplateTestStep}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response update(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName, @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), TemplateTestStep.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Deletes and returns the deleted {@link TemplateTestStep}.
	 * 
	 * @param id The identifier of the {@link TemplateTestStep} to delete.
	 * @return the deleted {@link TemplateTestStep }s as {@link Response}
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response delete(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_CONTEXTTYPE) String contextTypeParam, @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), TemplateTestStep.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}
	
	/**
	 * Adds a {@link TemplateMeasurement} specified in the JSON body of the request to the {@link TemplateTestStep}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link TemplateTestStep} to update.
	 * @param body       the body of the request containing the ID of the {@link TemplateMeasurement}
	 * @return the updated {@link TemplateTestStep}
	 */
	@PUT
	@Operation(summary = "Adds a TemplateMeasurement to a TemplateTestStep", description = "Updates the relation of TemplateTestStep to TemplateMeasurement.", responses = {
			@ApiResponse(description = "The updated TemplateTestStep", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/tplmearesult")
	public Response addMeaResult(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the TemplateTestStep", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return Try
				.of(() -> tplMeaResultService.addTplMeaResult(V(sourceName),
						V(requestBody.getStringValueSupplier("TemplateMeasurement").get()),
						entityService.find(V(sourceName), TemplateTestStep.class, V(id)).get()))
				.map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, TemplateMeasurement.class)).get();
	}
	
	/**
	 * Removes a {@link TemplateMeasurement} from the {@link TemplateTestStep}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link TemplateTestStep} to update.
	 * @param tplMeaResultId         the identifier of the {@link TemplateMeasurement} to remove.
	 * @return the updated {@link TemplateTestStep}
	 */
	@DELETE
	@Operation(summary = "Removes a TemplateMeasurement from a TemplateTestStep", description = "Updates the relation of TemplateTestStep to TemplateMeasurement.", responses = {
			@ApiResponse(description = "The updated TemplateTestStep", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/tplmearesult/{" + REQUESTPARAM_ID2 + "}")
	public Response removeMeaResult(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the TemplateTestStep", required = true) @PathParam(REQUESTPARAM_ID) String id,
			@Parameter(description = "ID of the TemplateMeasurement", required = true) @PathParam(REQUESTPARAM_ID2) String tplMeaResultId) {

		return Try
				.of(() -> tplMeaResultService.removeTplMeaResult(V(sourceName),
						V(tplMeaResultId),
						entityService.find(V(sourceName), TemplateTestStep.class, V(id)).get()))
				.map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, TemplateMeasurement.class)).get();
	}

	/**
	 * Returns the search attributes for the {@link TemplateTestStep} type.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the {@link SearchAttribute}s as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchattributes")
	public Response getSearchAttributes(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {
		return ServiceUtils.buildSearchAttributesResponse(V(sourceName), TemplateTestStep.class, entityService);
	}

	/**
	 * Returns a map of localization for the entity type and the attributes.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the I18N as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/localizations")
	@Deprecated
	public Response localize(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {
		return ServiceUtils.buildLocalizationResponse(V(sourceName), TemplateTestStep.class, entityService);
	}
}