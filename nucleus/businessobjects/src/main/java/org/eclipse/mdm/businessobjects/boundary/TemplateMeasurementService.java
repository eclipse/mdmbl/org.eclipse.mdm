/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.TemplateMeasurement;
import org.eclipse.mdm.api.dflt.model.TemplateTestStep;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.connector.boundary.ConnectorService;

import io.vavr.Value;

/**
 * {@link TemplateMeasurement} service
 * 
 * @author Martin Fleischer, Peak Solution GmbH
 *
 */
@Stateless
public class TemplateMeasurementService {

	@Inject
	private ConnectorService connectorService;

	public List<TemplateMeasurement> getTemplateMeasurements(Value<String> sourceName, Value<String> id, Class<?> sourceClass) {
		Entity entity = null;
		EntityManager em = this.connectorService.getContextByName(sourceName.get()).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		if (TemplateTestStep.class.equals(sourceClass)) {
			entity = em.load(TemplateTestStep.class, id.get());
		}

		return em.loadRelatedEntities(entity, "TplMeaResult", TemplateMeasurement.class);
	}

	/**
	 * returns a {@link TemplateMeasurement} identified by the given id.
	 * 
	 * @param sourceName  name of the source (MDM {@link Environment} name)
	 * @param extSystemId id of the {@link TemplateMeasurement}
	 * @return the matching {@link TemplateMeasurement}
	 */
	public TemplateMeasurement getTplMeaResult(Value<String> sourceName, Value<String> tplMeaResultId) {
		try {
			EntityManager em = this.connectorService.getContextByName(sourceName.get()).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
			return em.load(TemplateMeasurement.class, tplMeaResultId.get());
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	public List<TemplateMeasurement> addTplMeaResult(Value<String> sourceName, Value<String> id, TemplateTestStep tplTestStep) {
		EntityManager em = this.connectorService.getContextByName(sourceName.get()).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		Transaction t = em.startTransaction();
		TemplateMeasurement tplMeaResult = getTplMeaResult(sourceName, id);

		tplTestStep.addItem(tplMeaResult);
		t.update(Collections.singletonList(tplTestStep));
		t.commit();

		return getTemplateMeasurements(sourceName, V(tplTestStep.getID()), tplTestStep.getClass());
	}
	
	public List<TemplateMeasurement> removeTplMeaResult(Value<String> sourceName, Value<String> id, TemplateTestStep tplTestStep) {
		EntityManager em = this.connectorService.getContextByName(sourceName.get()).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		Transaction t = em.startTransaction();
		TemplateMeasurement tplMeaResult = getTplMeaResult(sourceName, id);

		tplTestStep.removeItem(tplMeaResult);
		t.update(Collections.singletonList(tplTestStep));
		t.commit();

		return getTemplateMeasurements(sourceName, V(tplTestStep.getID()), tplTestStep.getClass());
	}
}
