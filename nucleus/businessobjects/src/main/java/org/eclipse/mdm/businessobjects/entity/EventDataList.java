package org.eclipse.mdm.businessobjects.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.base.MoreObjects;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EventDataList {

	public List<EventData> data;

	public EventDataList() {

	}

	public List<EventData> getData() {
		return data;
	}

	public void setData(List<EventData> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("data", data).toString();
	}
}
