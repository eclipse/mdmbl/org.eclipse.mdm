/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.templatequery.entity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.mdm.api.base.model.Value;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.type.CollectionType;

/**
 * Implementation for converting JSON to structured DTOs.
 *
 * @since 5.2.0
 * @author JZ, Peak Solution GmbH
 */
public class TemplateEntityDTODeserializer extends JsonDeserializer<TemplateEntityDTO> {

	@Override
	public TemplateEntityDTO deserialize(JsonParser pars, DeserializationContext ctx)
			throws IOException, JsonProcessingException {
		JsonNode node = pars.getCodec().readTree(pars);
		String sourceName = node.get("sourceName").asText();
		ctx.setAttribute("sourceName", sourceName);

		TemplateEntityDTO dto;
		if ("TemplateRoot".equals(node.get("type").asText())) {
			dto = new TemplateEntityTplRootDTO();
		} else if ("TemplateComponent".equals(node.get("type").asText())) {
			dto = new TemplateEntityTplCompDTO();
		} else if ("TemplateAttribute".equals(node.get("type").asText())) {
			dto = new TemplateEntityTplAttrDTO();
		} else if ("TemplateSensor".equals(node.get("type").asText())) {
			dto = new TemplateEntityTplSensorDTO();
		} else if ("TemplateSensorAttribute".equals(node.get("type").asText())) {
			dto = new TemplateEntityTplSensorAttrDTO();
		} else if ("CatalogComponent".equals(node.get("type").asText())) {
			dto = new TemplateEntityCatCompDTO();
		} else if ("CatalogAttribute".equals(node.get("type").asText())) {
			dto = new TemplateEntityCatAttrDTO();
		} else if ("CatalogSensor".equals(node.get("type").asText())) {
			dto = new TemplateEntityCatSensorDTO();
		} else if ("CatalogSensorAttribute".equals(node.get("type").asText())) {
			dto = new TemplateEntityCatSensorAttrDTO();
		} else {
			throw new RuntimeException("Cannot deserialize type " + node.get("type"));
		}

		dto.setName(node.get("name").asText());
		dto.setId(node.get("id").asText());
		dto.setType(node.get("type").asText());
		dto.setSourceType(node.get("sourceType").asText());
		dto.setSourceName(node.get("sourceName").asText());

		if (node.get("attributes") != null) {
			JsonParser attributesParser = node.get("attributes").traverse(pars.getCodec());
			attributesParser.nextToken();

			List<Value> list = new MDMValueListDeserializer().deserialize(attributesParser, ctx);

			List<String> ignoredAttributes = Arrays.asList("TplCompParent", "TplUnitUnderTestComp",
					"TplTestSequenceComp", "TplTestEquipmentComp", "TplUnitUnderTestRoot", "TplTestSequenceRoot",
					"TplTestEquipmentRoot", "CatUnitUnderTestComp", "CatTestSequenceComp", "CatTestEquipmentComp");
			dto.setAttributes(
					list.stream().filter(v -> !ignoredAttributes.contains(v.getName())).collect(Collectors.toList()));
		}

		List<TemplateEntityDTO> list = new ArrayList<>();
		for (String n : Arrays.asList("catComp", "catSensor")) {
			if (node.get(n) != null) {
				JsonParser subEntitiesParser = node.get(n).traverse(pars.getCodec());
				subEntitiesParser.nextToken();

				list.add(ctx.readValue(subEntitiesParser, TemplateEntityDTO.class));
			}
			dto.setSubentities(list);
		}

		for (String n : Arrays.asList("tplRoots", "tplComps", "tplAttrs", "tplSensors", "tplSensorAttrs", "catAttrs",
				"catSensorAttrs")) {
			if (node.get(n) != null) {
				JsonParser subEntitiesParser = node.get(n).traverse(pars.getCodec());
				subEntitiesParser.nextToken();

				CollectionType javaType = ctx.getTypeFactory().constructCollectionType(List.class,
						TemplateEntityDTO.class);

				list.addAll(ctx.readValue(subEntitiesParser, javaType));
			}
			dto.setSubentities(list);
		}

		return dto;
	}
}
