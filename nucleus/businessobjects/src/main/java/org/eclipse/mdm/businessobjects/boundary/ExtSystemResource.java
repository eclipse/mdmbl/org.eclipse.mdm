/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.dflt.model.ExtSystem;
import org.eclipse.mdm.api.dflt.model.ExtSystemAttribute;
import org.eclipse.mdm.api.dflt.model.MDMAttribute;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.control.FileLinkActivity;
import org.eclipse.mdm.businessobjects.entity.MDMEntity;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.service.ContextService;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.collection.List;
import io.vavr.control.Try;

/**
 * {@link MDMEntity} REST interface for external systems
 * 
 * @author Juergen Kleck, Peak Solution GmbH
 *
 */
@Tag(name = "ExtSystem")
@Path("/administration/{" + REQUESTPARAM_SOURCENAME + "}/externalsystems")
public class ExtSystemResource {

	@EJB
	private ExtSystemService extSystemService;

	@EJB
	private EntityService entityService;

	@EJB
	private ContextService contextService;

	@EJB
	private FileLinkActivity fileLinkActivity;

	/**
	 * delegates the request to the {@link ExtSystemService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the ExtSystem result
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find ExtSystems by filter", description = "Get list of ExtSystems", responses = {
			@ApiResponse(description = "The ExtSystems", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getExtSystems(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		return Try.of(() -> extSystemService.getExtSystems(sourceName)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, ExtSystem.class)).get();
	}

	/**
	 * delegates the request to the {@link ExtSystemService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the ExtSystem result
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find ExtSystems by filter", description = "Get list of ExtSystems", responses = {
			@ApiResponse(description = "The ExtSystems", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/attributes/{" + REQUESTPARAM_ID + "}")
	public Response getExtSystemAttributes(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ExtSystem", required = true) @PathParam(REQUESTPARAM_ID) String id) {

		java.util.List all = new java.util.ArrayList<>();
		java.util.List extSystems = extSystemService.getExtSystemAttributes(sourceName, id);
		// the ext. system attributes
		all.addAll(extSystems);
		// the mdm attributes
		extSystems.stream().forEach(e -> all.addAll(((ExtSystemAttribute) e).getAttributes()));

		return ServiceUtils.buildEntityResponse(List.ofAll(all), Status.OK, ExtSystemAttribute.class);
	}

	/**
	 * delegates the request to the {@link ExtSystemService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link MDMEntity}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find a ExtSystem by ID", description = "Returns ExtSystem based on ID", responses = {
			@ApiResponse(description = "The Project", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response findExtSystem(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ExtSystem", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), ExtSystem.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the created {@link MDMEntity}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link MDMEntity} to create.
	 * @return the created {@link MDMEntity} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new ExtSystem", responses = {
			@ApiResponse(description = "The created ExtSystem", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createExtSystem(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {

		return entityService
				.create(V(sourceName), ExtSystem.class,
						entityService.extractRequestBody(body, sourceName, List.of(ExtSystem.class)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Returns the created {@link MDMEntity}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link MDMEntity} to create.
	 * @return the created {@link MDMEntity} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new ExtSystem", responses = {
			@ApiResponse(description = "The created ExtSystem", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/attribute")
	public Response createExtSystemAttribute(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {

		return entityService
				.create(V(sourceName), ExtSystemAttribute.class,
						entityService.extractRequestBody(body, sourceName,
								List.of(ExtSystem.class, ExtSystemAttribute.class)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Returns the created {@link MDMEntity}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link MDMEntity} to create.
	 * @return the created {@link MDMEntity} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new ExtSystem", responses = {
			@ApiResponse(description = "The created ExtSystem", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/mdmattribute")
	public Response createMDMAttribute(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {

		return entityService
				.create(V(sourceName), MDMAttribute.class,
						entityService.extractRequestBody(body, sourceName,
								List.of(ExtSystemAttribute.class, MDMAttribute.class)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Updates the {@link MDMEntity} with all parameters set in the given JSON body
	 * of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MDMEntity} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link MDMEntity}
	 */
	@PUT
	@Operation(summary = "Update an existing ExtSystem", description = "Updates the ExtSystem with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated ExtSystem", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response updateExtSystem(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ExtSystem", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), ExtSystem.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Updates the {@link MDMEntity} with all parameters set in the given JSON body
	 * of the request.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MDMEntity} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link MDMEntity}
	 */
	@PUT
	@Operation(summary = "Update an existing ExtSystem", description = "Updates the ExtSystem with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated ExtSystem", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/attribute/{" + REQUESTPARAM_ID + "}")
	public Response updateExtSystemAttribute(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ExtSystem", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), ExtSystemAttribute.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Updates the {@link MDMEntity} with all parameters set in the given JSON body
	 * of the request.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MDMEntity} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link MDMEntity}
	 */
	@PUT
	@Operation(summary = "Update an existing ExtSystem", description = "Updates the ExtSystem with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated ExtSystem", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/mdmattribute/{" + REQUESTPARAM_ID + "}")
	public Response updateMDMAttribute(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ExtSystem", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), MDMAttribute.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Deletes and returns the deleted {@link MDMEntity}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link MDMEntity} to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing ExtSystem", responses = {
			@ApiResponse(description = "The deleted ExtSystem", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response deleteExtSystem(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ExtSystem", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), ExtSystem.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Deletes and returns the deleted {@link MDMEntity}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link MDMEntity} to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing ExtSystem", responses = {
			@ApiResponse(description = "The deleted ExtSystem", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/attribute/{" + REQUESTPARAM_ID + "}")
	public Response deleteExtSystemAttribute(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ExtSystem", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), ExtSystemAttribute.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Deletes and returns the deleted {@link MDMEntity}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link MDMEntity} to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing ExtSystem", responses = {
			@ApiResponse(description = "The deleted ExtSystem", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/mdmattribute/{" + REQUESTPARAM_ID + "}")
	public Response deleteMDMAttribute(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ExtSystem", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), MDMAttribute.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

}
