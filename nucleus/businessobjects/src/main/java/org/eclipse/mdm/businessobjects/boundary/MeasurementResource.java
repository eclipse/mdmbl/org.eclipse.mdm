/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ATTRIBUTENAME;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTCOMPONENTNAME;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTGROUP;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_CONTEXTTYPE;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.MeaResultFile;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.MDMTag;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.control.FileLinkActivity;
import org.eclipse.mdm.businessobjects.entity.ContextResponse;
import org.eclipse.mdm.businessobjects.entity.ContextSensorResponse;
import org.eclipse.mdm.businessobjects.entity.I18NResponse;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.entity.SearchAttributeResponse;
import org.eclipse.mdm.businessobjects.service.ContextService;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.Value;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Try;

/**
 * {@link Measurement} resource
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@Tag(name = "Measurement")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/measurements")
public class MeasurementResource {

	@EJB
	private MeasurementService measurementService;

	@EJB
	private EntityService entityService;

	@EJB
	private ContextService contextService;

	@EJB
	private FileLinkActivity fileLinkActivity;

	@EJB
	private MDMTagService mdmTagService;

	@Context
	private ResourceContext resourceContext;

	@EJB
	private ConnectorServiceProxy connectorService;

	/**
	 * delegates the request to the {@link MeasurementService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the search attributes", description = "Get a list of search attributes", responses = {
			@ApiResponse(description = "The search attributes", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchattributes")
	public Response getSearchAttributes(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		return Try.of(() -> measurementService.getSearchAttributes(sourceName))
				.map(attrs -> ServiceUtils.toResponse(new SearchAttributeResponse(attrs), Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link MeasurementService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the Measurement result
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find Measurements by filter", description = "Get list of Measurements", responses = {
			@ApiResponse(description = "The Measurements", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMeasurements(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "Filter expression", required = false) @QueryParam("filter") String filter) {

		return Try.of(() -> measurementService.getMeasurements(sourceName, filter)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, Measurement.class)).get();
	}

	/**
	 * delegates the request to the {@link MeasurementService}
	 * 
	 * @param sourceName    name of the source (MDM {@link Environment} name)
	 * @param MeasurementId id of the {@link Measurement}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find a Measurement by ID", description = "Returns Measurement based on ID", responses = {
			@ApiResponse(description = "The Project", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response findMeasurement(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Measurement", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), Measurement.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link MeasurementService}
	 * 
	 * @param sourceName    name of the source (MDM {@link Environment} name)
	 * @param MeasurementId id of the {@link Measurement}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the complete context data for a Mesurement", description = "Returns the complete context", responses = {
			@ApiResponse(responseCode = "200", description = "The Measurement context data", content = @Content(schema = @Schema(implementation = ContextResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts")
	public Response findContext(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id) {

		return contextService.getMeasurementContext(V(sourceName), V(id)).map(ServiceUtils::contextMapToJava)
				.map(ContextResponse::new).map(contextResponse -> ServiceUtils.toResponse(contextResponse, Status.OK))
				.get();
	}

	/**
	 * Updates the context of {@link Measurement} with all parameters set in the
	 * given JSON body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MeasurementValue} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the context map of the updated {@link Measurement}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts")
	public Response updateContext(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id, String body) {

		return entityService.find(V(sourceName), Measurement.class, V(id))
				.map(measurement -> contextService.updateContext(body, measurement)).map(ContextResponse::new)
				.map(contextResponse -> ServiceUtils.toResponse(contextResponse, Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link MeasurementService}
	 * 
	 * @param sourceName    name of the source (MDM {@link Environment} name)
	 * @param MeasurementId id of the {@link Measurement}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the UnitUnderTest context data for a Measurment", description = "Returns the complete context", responses = {
			@ApiResponse(responseCode = "200", description = "The UnitUnderTest context data", content = @Content(schema = @Schema(implementation = ContextResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts/unitundertest")
	public Response findContextUUT(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id) {

		return contextService.getMeasurementContext(V(sourceName), V(id), ContextType.UNITUNDERTEST)
				.map(ServiceUtils::contextMapToJava).map(ContextResponse::new)
				.map(contextResponse -> ServiceUtils.toResponse(contextResponse, Status.OK)).get();
	}

	/**
	 * Updates the context of {@link ContextType} UNITUNDERTEST of
	 * {@link Measurement} with all parameters set in the given JSON body of the
	 * request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MeasurementValue} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the context map of {@link ContextType} UNITUNDERTEST of the updated
	 *         {@link TestStep}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts/unitundertest")
	public Response updateContextUUT(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id, String body) {

		return entityService.find(V(sourceName), Measurement.class, V(id))
				.map(measurement -> contextService.updateContext(body, measurement, ContextType.UNITUNDERTEST))
				.map(ContextResponse::new).map(contextResponse -> ServiceUtils.toResponse(contextResponse, Status.OK))
				.get();
	}

	/**
	 * delegates the request to the {@link MeasurementService}
	 * 
	 * @param sourceName    name of the source (MDM {@link Environment} name)
	 * @param MeasurementId id of the {@link Measurement}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the TestSequence context data for a Measurement", description = "Returns the TestSequence context data", responses = {
			@ApiResponse(responseCode = "200", description = "The TestSequence context data", content = @Content(schema = @Schema(implementation = ContextResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts/testsequence")
	public Response findContextTSQ(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id) {

		return contextService.getMeasurementContext(V(sourceName), V(id), ContextType.TESTSEQUENCE)
				.map(ServiceUtils::contextMapToJava).map(ContextResponse::new)
				.map(contextResponse -> ServiceUtils.toResponse(contextResponse, Status.OK)).get();
	}

	/**
	 * Updates the context of {@link ContextType} TESTSEQUENCE of
	 * {@link Measurement} with all parameters set in the given JSON body of the
	 * request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MeasurementValue} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the context map of {@link ContextType} TESTSEQUENCE of the updated
	 *         {@link TestStep}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts/testsequence")
	public Response updateContextTSQ(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id, String body) {

		return entityService.find(V(sourceName), Measurement.class, V(id))
				.map(measurement -> contextService.updateContext(body, measurement, ContextType.TESTSEQUENCE))
				.map(ContextResponse::new).map(contextResponse -> ServiceUtils.toResponse(contextResponse, Status.OK))
				.get();
	}

	/**
	 * delegates the request to the {@link MeasurementService}
	 * 
	 * @param sourceName    name of the source (MDM {@link Environment} name)
	 * @param MeasurementId id of the {@link Measurement}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the TestEquipment context data for a Measurement", description = "Returns the TestEquipment context data", responses = {
			@ApiResponse(responseCode = "200", description = "The TestEquipment context data", content = @Content(schema = @Schema(implementation = ContextResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts/testequipment")
	public Response findContextTEQ(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id) {

		return contextService.getMeasurementContext(V(sourceName), V(id), ContextType.TESTEQUIPMENT)
				.map(ServiceUtils::contextMapToJava).map(ContextResponse::new)
				.map(contextResponse -> ServiceUtils.toResponse(contextResponse, Status.OK)).get();
	}

	/**
	 * Updates the context of {@link ContextType} TESTEQUIPMENT of
	 * {@link Measurement} with all parameters set in the given JSON body of the
	 * request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MeasurementValue} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the context map of {@link ContextType} TESTEQUIPMENT of the updated
	 *         {@link TestStep}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts/testequipment")
	public Response updateContextTEQ(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id, String body) {

		return entityService.find(V(sourceName), Measurement.class, V(id))
				.map(measurement -> contextService.updateContext(body, measurement, ContextType.TESTEQUIPMENT))
				.map(ContextResponse::new).map(contextResponse -> ServiceUtils.toResponse(contextResponse, Status.OK))
				.get();
	}

	/**
	 * delegates the request to the {@link MeasurementService}
	 * 
	 * @param sourceName    name of the source (MDM {@link Environment} name)
	 * @param MeasurementId id of the {@link Measurement}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the Sensor context data", description = "Returns the Sensor context data of TestEquipments", responses = {
			@ApiResponse(responseCode = "200", description = "The Sensor context data", content = @Content(schema = @Schema(implementation = ContextResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/contexts/testequipment/sensors")
	public Response getContextTEQSensors(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_ID) String id) {

		return Try.of(() -> measurementService.getSensors(sourceName, id))
				.map(sensorMap -> ServiceUtils.toResponse(new ContextSensorResponse(sensorMap), Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link MeasurementService}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the Measurement localizations", description = "Returns Measurement localizations", responses = {
			@ApiResponse(description = "The Measurement localizations", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/localizations")
	@Deprecated
	public Response localize(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		return Try
				.of(() -> new I18NResponse(measurementService.localizeType(sourceName),
						measurementService.localizeAttributes(sourceName)))
				.map(resp -> ServiceUtils.toResponse(resp, Status.OK)).get();
	}

	/**
	 * Returns the created {@link Measurement}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link Measurement} to create.
	 * @return the created {@link Measurement} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new Measurement", responses = {
			@ApiResponse(description = "The created Measurement", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {
		Seq<Value<?>> args = entityService.extractRequestBody(body, sourceName,
				io.vavr.collection.List.of(TestStep.class));
		return entityService.create(V(sourceName), Measurement.class, args)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Updates the {@link Measurement} with all parameters set in the given JSON
	 * body of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MeasurementValue} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link Measurement}
	 */
	@PUT
	@Operation(summary = "Update an existing Measurement", description = "Updates the Measurement with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated Measurement", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response update(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Measurement", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);
		Measurement mea = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class))
				.loadWithoutChildren(Measurement.class, id);

		return entityService.update(V(sourceName), Try.of(() -> mea), requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Deletes and returns the deleted {@link Measurement}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link Measurement} to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing Measurement", responses = {
			@ApiResponse(description = "The deleted Measurement", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response delete(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Measurement", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), Measurement.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	@Path("/{" + REQUESTPARAM_ID + "}/contexts/{" + REQUESTPARAM_CONTEXTTYPE + "}/{" + REQUESTPARAM_CONTEXTGROUP + "}/{"
			+ REQUESTPARAM_CONTEXTCOMPONENTNAME + "}/{" + REQUESTPARAM_ATTRIBUTENAME + "}/files")
	public ContextFilesSubresource<DescriptiveFile> getContextFilesSubresource() {
		ContextFilesSubresource<DescriptiveFile> resource = resourceContext.getResource(ContextFilesSubresource.class);
		resource.setEntityClass(Measurement.class);
		return resource;
	}

	@Path("/{" + REQUESTPARAM_ID + "}/files/")
	public FilesAttachableSubresource<MeaResultFile> getFilesAttachableSubresource() {
		FilesAttachableSubresource<MeaResultFile> resource = resourceContext
				.getResource(FilesAttachableSubresource.class);
		resource.setEntityClass(Measurement.class);
		return resource;
	}

	/**
	 * delegates the request to the {@link TestStepService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link Test}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get MDMTags of a Measurement by ID", description = "Returns MDMTags  based on ID of Measurement", responses = {
			@ApiResponse(description = "The MDMTags", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/mdmtags")
	public Response getMDMTags(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Test", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return Try.of(() -> mdmTagService.getMDMTags(sourceName, id, Measurement.class)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, MDMTag.class)).get();
	}
}
