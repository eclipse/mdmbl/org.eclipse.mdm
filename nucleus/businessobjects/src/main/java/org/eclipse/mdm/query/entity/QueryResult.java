/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.query.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Matthias Koller, Peak Solution GmbH
 *
 */
public class QueryResult {
	private List<Row> rows;
	private Map<String, Long> totalRecords = new HashMap<>();

	public QueryResult() {
		this.rows = new ArrayList<>();
	}

	public QueryResult(List<Row> rows) {
		this.rows = rows;
	}

	public List<Row> getRows() {
		return rows;
	}

	public void setRows(List<Row> rows) {
		this.rows = rows;
	}

	public Map<String, Long> getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(String source, long totalRecords) {
		this.totalRecords.put(source, totalRecords);
	}
}
