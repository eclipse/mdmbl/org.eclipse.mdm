/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.List;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.mdm.businessobjects.entity.Profile;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Profile")
@Path("profile")
@PermitAll
public class ProfileResource {

	@EJB
	private UserService userService;

	@GET
	@Path("current")
	@Operation(summary = "Read authenticated user information", description = "Read the current user profile. The profile holds user details (incl. roles) for each connected source and also from server security context.", responses = {
			@ApiResponse(description = "The authenticated user.", content = @Content(schema = @Schema(implementation = Profile.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response currentUser(
			@Parameter(description = "List of roles to check. (Only effective for details from servers security contex). Not querries roles will be ignored. "
					+ " Necessary sincet there is no reliable way of accessing all (possibly mapped) roles of a user. ", required = true) @QueryParam("roles") List<String> roles) {
		return Response.ok(this.userService.readCurrentProfile(roles)).build();
	}
}
