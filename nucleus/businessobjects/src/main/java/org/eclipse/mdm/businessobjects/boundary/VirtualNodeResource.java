/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.protobuf.Mdm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import java.util.ArrayList;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

/**
 * Virtual node resource
 *
 * @author Juergen Kleck, Peak Solution GmbH
 *
 */
@Tag(name = "virtual")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/virtual")
public class VirtualNodeResource {

	private static final Logger LOG = LoggerFactory.getLogger(VirtualNodeResource.class);

	@GET
	@Path("{NODEPROVIDER}/{NODE_ID}")
	@Operation(summary = "Load virtual node", description = "Get virtual node of given node for a nodeprovider", responses = {
			@ApiResponse(description = "The requested virtual nodes", content = @Content(schema = @Schema(implementation = Mdm.NodeProviderResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	public Response getVirtualNode(
			@Parameter(description = "ID of the nodeprovider", required = true) @PathParam("NODEPROVIDER") String nodeProviderId,
			@Parameter(description = "serial of the virtual node", required = true) @PathParam("NODE_ID") String nodeId) {
		try {
			java.util.List<Mdm.Node> nodes = new ArrayList<>();
			// since virtual nodes are not persisted we simply transform it back from the serialized data
			nodes.add(SerializationUtil.deserializeNode(nodeId));
			// return the transformed virtual node
			return Response.status(Status.OK).entity(Mdm.NodeProviderResponse.newBuilder().addAllData(nodes).build())
					.build();

		} catch (RuntimeException e) {
			LOG.error(e.getMessage(), e);
			throw new WebApplicationException(e.getMessage(), e, Status.INTERNAL_SERVER_ERROR);
		}
	}
}
