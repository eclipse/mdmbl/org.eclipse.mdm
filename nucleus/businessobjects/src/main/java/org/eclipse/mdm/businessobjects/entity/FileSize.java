/*******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
package org.eclipse.mdm.businessobjects.entity;

import java.io.Serializable;

public class FileSize implements Serializable {

	private static final long serialVersionUID = 1228535240780890498L;
	
	private String identifier;
	private String size;
	
	public FileSize() {
		// intentially empty
	}
	
	/**
	 * 
	 * @return
	 */
	public String getIdentifier() {
		return identifier;
	}
	
	/**
	 * 
	 * @param identifier
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getSize() {
		return size;
	}
	
	/**
	 * 
	 * @param size
	 */
	public void setSize(String size) {
		this.size = size;
	}

}
