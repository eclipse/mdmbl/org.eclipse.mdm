/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.entity;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Condition;
import org.eclipse.mdm.api.base.search.ContextState;

public enum ValuePrecision {

	EXACT {
		@Override
		public void applyOn(Value value, ZoneId zoneId) {
			// Intentionally empty
		}

		@Override
		public Condition getCondition(ContextState contextState, Attribute attribute, Object value, ZoneId zoneId) {
			return ComparisonOperator.EQUAL.create(contextState, attribute, value);
		}
	},
	YEAR {
		@Override
		public void applyOn(Value value, ZoneId zoneId) {
			ValuePrecision.applyDate(value, YEAR, zoneId);
		}

		@Override
		public Condition getCondition(ContextState contextState, Attribute attribute, Object value, ZoneId zoneId) {
			return getDateCondition(contextState, attribute, value, YEAR, zoneId);
		}
	},
	MONTH {
		@Override
		public void applyOn(Value value, ZoneId zoneId) {
			ValuePrecision.applyDate(value, MONTH, zoneId);
		}

		@Override
		public Condition getCondition(ContextState contextState, Attribute attribute, Object value, ZoneId zoneId) {
			return getDateCondition(contextState, attribute, value, MONTH, zoneId);
		}
	},
	WEEK {
		@Override
		public void applyOn(Value value, ZoneId zoneId) {
			ValuePrecision.applyDate(value, WEEK, zoneId);
		}

		@Override
		public Condition getCondition(ContextState contextState, Attribute attribute, Object value, ZoneId zoneId) {
			return getDateCondition(contextState, attribute, value, WEEK, zoneId);
		}
	},
	DAY {
		@Override
		public void applyOn(Value value, ZoneId zoneId) {
			ValuePrecision.applyDate(value, DAY, zoneId);
		}

		@Override
		public Condition getCondition(ContextState contextState, Attribute attribute, Object value, ZoneId zoneId) {
			return getDateCondition(contextState, attribute, value, DAY, zoneId);
		}
	},
	HOUR {
		@Override
		public void applyOn(Value value, ZoneId zoneId) {
			ValuePrecision.applyDate(value, HOUR, zoneId);
		}

		@Override
		public Condition getCondition(ContextState contextState, Attribute attribute, Object value, ZoneId zoneId) {
			return getDateCondition(contextState, attribute, value, HOUR, zoneId);
		}
	},
	MINUTE {
		@Override
		public void applyOn(Value value, ZoneId zoneId) {
			ValuePrecision.applyDate(value, MINUTE, zoneId);
		}

		@Override
		public Condition getCondition(ContextState contextState, Attribute attribute, Object value, ZoneId zoneId) {
			return getDateCondition(contextState, attribute, value, MINUTE, zoneId);
		}
	};

	/**
	 * Applies this {@link ValuePrecision} to given {@link Value}s value, if
	 * acceptable for related {@link ValueType}.
	 * 
	 * @param value  the value
	 * @param zoneId
	 */
	abstract public void applyOn(Value value, ZoneId zoneId);

	abstract public Condition getCondition(ContextState contextState, Attribute attribute, Object value, ZoneId zoneId);

	/**
	 * Applies this {@link ValuePrecision} to given {@link Value}s value, for
	 * {@link ValueType.DATE}.
	 *
	 * @param value  the value
	 * @param zoneId
	 */
	private static void applyDate(Value value, ValuePrecision precision, ZoneId zoneId) {
		if (value.getValueType() == ValueType.DATE) {
			if (value.isValid()) {
				Instant dateTime = value.extract();
				if (dateTime != null) {
					switch (precision) {
					case WEEK:
						dateTime = dateTime.atZone(zoneId).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY))
								.toInstant().truncatedTo(ChronoUnit.DAYS);
						break;
					case YEAR:
						dateTime = dateTime.atZone(zoneId).with(TemporalAdjusters.firstDayOfYear()).toInstant();
						// no break intentionally;
					case MONTH:
						dateTime = dateTime.atZone(zoneId).with(TemporalAdjusters.firstDayOfMonth()).toInstant();
						// no break intentionally;
					case DAY:
						dateTime = dateTime.atZone(zoneId).with(ChronoField.HOUR_OF_DAY, 0).toInstant();
						// no break intentionally;
					case HOUR:
						dateTime = dateTime.atZone(zoneId).with(ChronoField.MINUTE_OF_HOUR, 0).toInstant();
						// no break intentionally;
					case MINUTE:
						dateTime = dateTime.atZone(zoneId).with(ChronoField.SECOND_OF_MINUTE, 0).toInstant();
						break;
					default:
						// Intentionally empty.
					}
					value.set(dateTime);
				}
			} else {
				System.out.println();
			}
		}
	}

	private static Condition getDateCondition(ContextState contextState, Attribute attribute, Object value,
			ValuePrecision precision, ZoneId zoneId) {
		if (attribute.getValueType() == ValueType.DATE) {
			Instant start = (Instant) value;
			Instant end;
			switch (precision) {
			case YEAR:
				end = start.atZone(zoneId).plus(1, ChronoUnit.YEARS).toInstant();
				break;
			case MONTH:
				end = start.atZone(zoneId).plus(1, ChronoUnit.MONTHS).toInstant();
				break;
			case WEEK:
				end = start.atZone(zoneId).plus(1, ChronoUnit.WEEKS).toInstant();
				break;
			case DAY:
				end = start.atZone(zoneId).plus(1, ChronoUnit.DAYS).toInstant();
				break;
			case HOUR:
				end = start.atZone(zoneId).plus(1, ChronoUnit.HOURS).toInstant();
				break;
			case MINUTE:
				end = start.atZone(zoneId).plus(1, ChronoUnit.MINUTES).toInstant();
				break;
			default:
				end = start.atZone(zoneId).plus(1, ChronoUnit.NANOS).toInstant();
			}
			return ComparisonOperator.BETWEEN.create(contextState, attribute,
					new Instant[] { start, end.atZone(zoneId).minusNanos(1).toInstant() });
		} else {
			return ComparisonOperator.EQUAL.create(contextState, attribute, value);
		}
	}
}
