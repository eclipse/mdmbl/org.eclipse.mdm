/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/
package org.eclipse.mdm.webclient.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_FILTER;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.query.Condition;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.nodeprovider.utils.SerializationUtil;
import org.eclipse.mdm.protobuf.Mdm;
import org.eclipse.mdm.webclient.entity.ConditionResponse;

import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * 
 * @author jst
 *
 */
@Tag(name = "Conditions")
@Path("environments/{" + REQUESTPARAM_SOURCENAME + "}/conditions/")
public class ConditionResource {

	@EJB
	private ConditionService service;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("filter/{" + REQUESTPARAM_FILTER + "}")
	public Response convertFilter(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_FILTER) String filterString) {
		try {
			List<Condition> conditions = service.toCondition(sourceName, filterString);
			return ServiceUtils.toResponse(new ConditionResponse(conditions), Status.OK);
		} catch (RuntimeException e) {
			throw new WebApplicationException(e.getMessage(), e, Status.INTERNAL_SERVER_ERROR);
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("serial/{" + REQUESTPARAM_FILTER + "}")
	public Response convertSerial(@PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@PathParam(REQUESTPARAM_FILTER) String serialString) {
		try {
			Mdm.Node node = SerializationUtil.deserializeNode(serialString);
			List<Condition> conditions = service.toCondition(sourceName, node.getFilter());
			return ServiceUtils.toResponse(new ConditionResponse(conditions), Status.OK);
		} catch (RuntimeException e) {
			throw new WebApplicationException(e.getMessage(), e, Status.INTERNAL_SERVER_ERROR);
		}
	}
}
