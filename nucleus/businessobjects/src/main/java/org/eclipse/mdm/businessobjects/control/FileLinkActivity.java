/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.control;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.adapter.RelationType;
import org.eclipse.mdm.api.base.file.DescriptiveFileManager;
import org.eclipse.mdm.api.base.file.FileService;
import org.eclipse.mdm.api.base.file.FileService.FileServiceType;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextDescribable;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.DescriptiveFile;
import org.eclipse.mdm.api.base.model.DescriptiveFilesAttachable;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.FileLink;
import org.eclipse.mdm.api.base.model.FileLink.Format;
import org.eclipse.mdm.api.base.model.FilesAttachable;
import org.eclipse.mdm.api.base.model.MDMFile;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.MimeType;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.TemplateAttribute;
import org.eclipse.mdm.businessobjects.entity.FileSize;
import org.eclipse.mdm.businessobjects.entity.MDMFileLinkExt;
import org.eclipse.mdm.businessobjects.service.ContextService;
import org.eclipse.mdm.businessobjects.service.DescribableContexts;
import org.eclipse.mdm.businessobjects.utils.Serializer;
import org.eclipse.mdm.connector.boundary.ConnectorService;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.io.ByteStreams;

import io.vavr.Tuple;
import io.vavr.Tuple2;

@Stateless
public class FileLinkActivity<T extends MDMFile> {

	@Inject
	private ConnectorService connectorService;

	@EJB
	private ContextService contextService;

	@EJB
	private ContextActivity contextActivity;

	/**
	 * Loads size of File with given remotePath.
	 * 
	 * @param sourceName      name of the source (MDM {@link Environment})
	 * @param filesAttachable the {@link FilesAttachable} {@link Entity} the file is
	 *                        attached to
	 * @param remotePath      the remotePath
	 * @return the {@link FileSize}
	 */
	public FileSize loadFileSize(String sourceName, Entity entity, FileLink fileLink) {
		if (fileLink.isLink()) {
			FileSize fileSize = new FileSize();
			fileSize.setIdentifier(Serializer.getFileLinkIdentifier(fileLink));
			fileSize.setSize("");
			return fileSize;
		}
		try (Transaction transaction = getEntityManager(sourceName).startTransaction()) {
			FileService fileService = transaction.getFileService(fileLink.getFileServiceType());
			return loadSize(entity, fileLink, fileService, transaction);
		}
	}

	/**
	 * Loads sizes for all files attached to the given entity.
	 * 
	 * @param sourceName      name of the source (MDM {@link Environment})
	 * @param filesAttachable the {@link FilesAttachable} {@link Entity} the files
	 *                        are attached to
	 * @return the {@link FileSize}s as {@link List}
	 */
	public List<FileSize> loadFileSizes(String sourceName, FilesAttachable<T> filesAttachable) {
		try (Transaction transaction = getEntityManager(sourceName).startTransaction()) {
			return getAllFileLinks(sourceName, filesAttachable).stream()
					.map(fileLink -> loadSize(filesAttachable, fileLink,
							transaction.getFileService(fileLink.getFileServiceType()), transaction))
					.collect(Collectors.toList());
		}
	}

	/**
	 * Helper function to trigger the actual file size loading process via given
	 * {@link FileService}
	 * 
	 * @param filesAttachable the {@link FilesAttachable} {@link Entity} the file is
	 *                        attached to
	 * @param fileLink        the {@link FileLink}
	 * @param fileService     the {@link FileService}
	 * @return the {@link FileSize}
	 */
	private FileSize loadSize(Entity entity, FileLink fileLink, FileService fileService, Transaction transaction) {
		FileSize fileSize = new FileSize();
		try {
			fileService.loadSize(entity, fileLink, transaction);
			fileSize.setIdentifier(Serializer.getFileLinkIdentifier(fileLink));
			fileSize.setSize(fileLink.getSize(Format.BINARY));
		} catch (IOException e) {
			throw new MDMFileAccessException(e.getMessage(), e);
		}
		return fileSize;
	}

	/**
	 * Persists file at file server. Creates a new {@link FileLink} (pointing at the
	 * new file) and adds it to the {@link FilesAttachable}
	 * 
	 * @param sourceName      name of the source (MDM {@link Environment})
	 * @param filesAttachable the {@link FilesAttachable} {@link Entity} the file is
	 *                        attached to
	 * @param fileName        the name of the file
	 * @param fis             the {@code InputStream} with the file data
	 * @param description     the file description. Default value is the file name.
	 * @param mimeType        the mimeType of the file.
	 * @return the {@link FileLink} to the created File
	 */
	public FileLink createFile(String sourceName, FilesAttachable<T> filesAttachable, String fileName,
			Instant fileModificationDate, InputStream fis, String description, MimeType mimeType) {

		FileLink fileLink = null;
		Transaction transaction = getEntityManager(sourceName).startTransaction();
		
		try {

			Relation relMDMFile = null;
			try {
				relMDMFile = getFileRelations(sourceName, filesAttachable).findFirst().orElseGet(() -> null);
			} catch (IllegalArgumentException exc) {
				// relation not found
			}

			FileServiceType fileServiceType = (relMDMFile != null ? FileServiceType.AOFILE : FileServiceType.EXTREF);

			// Create new local FileLink
			fileLink = newLocalFileLink(fileName, fis, description, mimeType, fileServiceType);

			

			switch (fileServiceType) {
			case AOFILE:

				MDMFile mdmFile;
				if (filesAttachable instanceof DescriptiveFilesAttachable) {
					// Create DescriptiveFile for TemplateAttribute
					mdmFile = getEntityFactory(sourceName)
							.createDescriptiveFile((DescriptiveFilesAttachable) filesAttachable, relMDMFile);
				} else {
					// Create MDMFile for fileAttachable:
					mdmFile = getEntityFactory(sourceName).createMDMFile(filesAttachable);
				}
				mdmFile.setName(Strings.isNullOrEmpty(fileName) ? mdmFile.getTypeName() : fileName);
				mdmFile.setDescription(description);
				mdmFile.setFileMimeType(mimeType.toString());
				mdmFile.setOriginalFileName(fileName);
				if (fileModificationDate != null) {
					mdmFile.setOriginalFileDate(fileModificationDate);
				}
				createEntity(sourceName, mdmFile, transaction);
				mdmFile.setFileLink(fileLink);
				break;
			case EXTREF:
				// Add file link to fileAttachable:
				filesAttachable.addFileLink(fileLink);
				break;
			default:
				break;
			}

			// Upload fileLink. Upload will create remote path.

			uploadFile(sourceName, filesAttachable, fileLink, transaction);			
			// Persist changes
			persistEntity(sourceName, filesAttachable, transaction);
			transaction.commit();
			
		} catch (Exception exc) {
			if (fileLink != null) {
				deleteFile(sourceName, filesAttachable, fileLink, transaction);
			}
			transaction.abort();
			
			throw new MDMFileAccessException(
					String.format("File (%s, %s, %s) could not be created.", fileName, description, mimeType), exc);
		}

		return fileLink;
	}

	/**
	 * Persists file at file server. Creates a new {@link FileLink} (pointing to the
	 * new file) and adds it to the specified context attribute
	 * 
	 * @param sourceName           name of the source (MDM {@link Environment})
	 * @param contextDescribable   the {@link ContextDescribable}
	 * @param fileName             the name of the file
	 * @param fis                  the {@code InputStream} with the file data
	 * @param description          the file description. Default value is the file
	 *                             name.
	 * @param mimeType             the mimeType of the file.
	 * @param contextType          the {@link ContextType} holding the component
	 * @param contextComponentName the name of the component holding the file link
	 *                             attribute
	 * @param attributeName        the name of the attribute holding the file link
	 *                             or the name of the relation to the DecriptiveFile
	 * @return
	 */
	public FileLink createFile(String sourceName, ContextDescribable contextDescribable, String fileName,
			Instant fileModificationDate, InputStream fis, String description, MimeType mimeType,
			ContextType contextType, String contextGroup, String contextComponentName, String attributeName) {

		Map<ContextType, ContextRoot> contextMap = getContextMap(sourceName, contextDescribable, contextType,
				contextGroup);
		ContextComponent contextComponent = findContextComponent(contextType, contextComponentName, contextMap)
				.orElseThrow(() -> new MDMEntityAccessException("ContextComponent with name " + contextComponentName
						+ " not found in context of type " + contextType));

		FileLink fileLink = null;

		
		Transaction transaction = getEntityManager(sourceName).startTransaction();
		try {
			
			ModelManager modelManager = getModelManager(sourceName);

			EntityFactory entityFactory = getEntityFactory(sourceName);

			Relation relDescriptiveFile = null;

			DescriptiveFileManager descriptiveFilesManager = new DescriptiveFileManager(modelManager);
			if (descriptiveFilesManager.getDescriptiveFileRelationNames(contextComponent).contains(attributeName)) {
				relDescriptiveFile = descriptiveFilesManager.getDescriptiveFileRelation(contextComponent,
						attributeName);
			}

			FileServiceType fileServiceType = (relDescriptiveFile != null ? FileServiceType.AOFILE
					: FileServiceType.EXTREF);

			// Create new local FileLink
			fileLink = newLocalFileLink(fileName, fis, description, mimeType, fileServiceType);

			switch (fileServiceType) {
			case AOFILE:
				// Create DescriptiveFile for DescriptiveFileAttachable:
				DescriptiveFile descriptiveFile = entityFactory.createDescriptiveFile(contextComponent,
						relDescriptiveFile);
				descriptiveFile.setName(Strings.isNullOrEmpty(fileName) ? descriptiveFile.getTypeName() : fileName);
				descriptiveFile.setDescription(description);
				descriptiveFile.setFileMimeType(mimeType.toString());
				descriptiveFile.setOriginalFileName(fileName);
				if (fileModificationDate != null) {
					descriptiveFile.setOriginalFileDate(fileModificationDate);
				}
				createEntity(sourceName, descriptiveFile, transaction);
				descriptiveFile.setFileLink(fileLink);
				break;
			case EXTREF:
				Value value = contextComponent.getValue(attributeName);

				// Since attributes of type FILE_LINK can hold a maximum of one file, the old
				// file has to be deleted from fileServer
				if (ValueType.FILE_LINK.equals(value.getValueType())) {
					deleteFile(sourceName, contextDescribable, value.extract(ValueType.FILE_LINK), null);
				}

				if (fileLink.isLink()) {
					fileLink.setRemotePath(fileLink.getDescription());
				}
				// Adds file link to the context attribute
				setOrAddFileLinkToValue(value, fileLink);
				break;
			default:
				break;
			}

			if (!fileLink.isLink()) {
				// Upload file to file server. Upload creates and sets remote path in fileLink.
				uploadFile(sourceName, contextDescribable, fileLink, transaction);
			}

			// Persist changes
			DescribableContexts dc = createDescribableContext(contextDescribable, contextGroup, contextMap);
			persist(Arrays.asList(dc), transaction);
			transaction.commit();
		} catch (Exception e) {
			if (fileLink != null) {
				deleteFile(sourceName, contextDescribable, fileLink, transaction);
			}
			transaction.abort();
			
			throw new MDMFileAccessException(
					String.format("File (%s, %s, %s) could not be created.", fileName, description, mimeType), e);
		} 
		return fileLink;
	}

	private void persist(List<DescribableContexts> ecList, Transaction t) {
		if (!ecList.isEmpty()) {

			for (DescribableContexts ec : ecList) {
				java.util.Map<Boolean, java.util.List<Entity>> map = ec.getEntities().stream()
						.collect(Collectors.groupingBy(this::isPersisted));

				t.create(map.getOrDefault(Boolean.FALSE, Collections.emptyList()));
				t.update(map.getOrDefault(Boolean.TRUE, Collections.emptyList()));
			}

		}
	}

	private boolean isPersisted(Entity e) {
		return !Strings.isNullOrEmpty(e.getID()) && !"0".equals(e.getID());
	}

	/**
	 * Persists file at file server. Creates a new {@link FileLink} (pointing to the
	 * new file) and adds it to the specified context attribute
	 * 
	 * @param sourceName           name of the source (MDM {@link Environment})
	 * @param contextDescribable   the {@link ContextDescribable}
	 * @param fileName             the name of the file
	 * @param fis                  the {@code InputStream} with the file data
	 * @param description          the file description. Default value is the file
	 *                             name.
	 * @param mimeType             the mimeType of the file.
	 * @param contextType          the {@link ContextType} holding the component
	 * @param contextComponentName the name of the component holding the file link
	 *                             attribute
	 * @param attributeName        the name of the attribute holding the file link
	 *                             or the name of the relation to the DecriptiveFile
	 * @return
	 */
	public FileLink createFile(String sourceName, ContextComponent contextComponent, String fileName,
			Instant fileModificationDate, InputStream fis, String description, MimeType mimeType,
			String attributeName) {

		FileLink fileLink = null;
		
		Transaction transaction = getEntityManager(sourceName).startTransaction();
		try {
			
			ModelManager modelManager = getModelManager(sourceName);

			EntityFactory entityFactory = getEntityFactory(sourceName);

			Relation relDescriptiveFile = null;

			DescriptiveFileManager descriptiveFilesManager = new DescriptiveFileManager(modelManager);
			if (descriptiveFilesManager.getDescriptiveFileRelationNames(contextComponent).contains(attributeName)) {
				relDescriptiveFile = descriptiveFilesManager.getDescriptiveFileRelation(contextComponent,
						attributeName);
			}

			FileServiceType fileServiceType = (relDescriptiveFile != null ? FileServiceType.AOFILE
					: FileServiceType.EXTREF);

			// Create new local FileLink
			fileLink = newLocalFileLink(fileName, fis, description, mimeType, fileServiceType);

			switch (fileServiceType) {
			case AOFILE:
				// Create DescriptiveFile for DescriptiveFileAttachable:
				DescriptiveFile descriptiveFile = entityFactory.createDescriptiveFile(contextComponent,
						relDescriptiveFile);
				descriptiveFile.setName(Strings.isNullOrEmpty(fileName) ? descriptiveFile.getTypeName() : fileName);
				descriptiveFile.setDescription(description);
				descriptiveFile.setFileMimeType(mimeType.toString());
				descriptiveFile.setOriginalFileName(fileName);
				if (fileModificationDate != null) {
					descriptiveFile.setOriginalFileDate(fileModificationDate);
				}
				createEntity(sourceName, descriptiveFile, transaction);
				descriptiveFile.setFileLink(fileLink);
				break;
			case EXTREF:
				Value value = contextComponent.getValue(attributeName);

				// Since attributes of type FILE_LINK can hold a maximum of one file, the old
				// file has to be deleted from fileServer
				if (ValueType.FILE_LINK.equals(value.getValueType())) {
					deleteFile(sourceName, contextComponent, value.extract(ValueType.FILE_LINK), null);
				}

				if (fileLink.isLink()) {
					fileLink.setRemotePath(fileLink.getDescription());
				}
				// Adds file link to the context attribute
				setOrAddFileLinkToValue(value, fileLink);
				break;
			default:
				break;
			}

			if (!fileLink.isLink()) {
				// Upload file to file server. Upload creates and sets remote path in fileLink.
				uploadFile(sourceName, contextComponent, fileLink, transaction);
			}
			transaction.commit();
		} catch (Exception e) {
			if (fileLink != null) {
				deleteFile(sourceName, contextComponent, fileLink, transaction);
			}
			transaction.abort();
			throw new MDMFileAccessException(
					String.format("File (%s, %s, %s) could not be created.", fileName, description, mimeType), e);
		} 
		
		return fileLink;
	}

	/**
	 * Deletes file from file server and removes the related {@link FileLink} from
	 * the {@link FilesAttachable}
	 * 
	 * @param sourceName      name of the source (MDM {@link Environment})
	 * @param filesAttachable the {@link FilesAttachable} {@link Entity} the file is
	 *                        attached to
	 * @param identifier      the identifier
	 * @return the {@link FileLink} to the deleted File
	 */
	public FileLink deleteFileLink(String sourceName, FilesAttachable<T> filesAttachable, String identifier) {

		// Find file link
		FileLink fileLink = findFileLinkAtFileAttachable(sourceName, identifier, filesAttachable);

		
		Transaction transaction = getEntityManager(sourceName).startTransaction();
		try {
			
			switch (fileLink.getFileServiceType()) {
			case EXTREF: {
				// Remove FileLink from FileAttachable
				filesAttachable.removeFileLink(fileLink);

				// Persist changes
				persistEntity(sourceName, filesAttachable, transaction);
			}
				break;
			case AOFILE: {
				@SuppressWarnings("unchecked")
				T mdmFile = (T) fileLink.getRemoteObject();

				// Disconnect MDMFile from FilesAttachable
				filesAttachable.removeMDMFile(mdmFile);

				// Delete MDMFile
				transaction.delete(Arrays.asList(mdmFile));
			}
				break;
			}

			transaction.commit();
		} catch (Exception exc) {
			transaction.abort();
			throw new MDMFileAccessException("Could not delete " + fileLink + " " + identifier, exc);
		} 

		return fileLink;
	}

	public FileLink deleteFileLink(String sourceName, TemplateAttribute templateAttribute, String identifier) {
		// Find file link
		@SuppressWarnings("unchecked")
		FileLink fileLink = findFileLinkAtFileAttachable(sourceName, identifier,
				(FilesAttachable<T>) templateAttribute);

		Transaction transaction = getEntityManager(sourceName).startTransaction();
		
		try {
			
			switch (fileLink.getFileServiceType()) {
			case EXTREF: {
				// Remove FileLink from FileAttachable
				templateAttribute.removeFileLink(fileLink);

				// Persist changes
				persistEntity(sourceName, templateAttribute, transaction);
			}
				break;
			case AOFILE: {
				EntityManager entityManager = getEntityManager(sourceName);
				DescriptiveFileManager descriptiveFileManager = new DescriptiveFileManager(getModelManager(sourceName));
				Relation relation = descriptiveFileManager.getDescriptiveFileRelation(templateAttribute,
						TemplateAttribute.ATTR_DEFAULT_FILES);

				DescriptiveFile descriptiveFile = templateAttribute.getDescriptiveFiles(relation, entityManager)
						.stream()
						.filter(df -> Serializer.getFileLinkIdentifier(df.getFileLink()).equalsIgnoreCase(identifier))
						.findFirst().orElseThrow(() -> new MDMEntityAccessException(
								"FileLink with identifier " + identifier + " not found!"));

				boolean hasAdditionalRelatedInstances = descriptiveFileManager
						.hasAdditionalRelatedInstances(entityManager, descriptiveFile, relation);

				// Disconnect MDMFile from TemplateAttribute:
				templateAttribute.removeDescriptiveFile(relation, descriptiveFile);

				fileLink = descriptiveFile.getFileLink();
				transaction.update(Arrays.asList(templateAttribute));
				if (!hasAdditionalRelatedInstances) {
					transaction.delete(Arrays.asList(descriptiveFile));
				}

				transaction.commit();
			}
				break;
			}
		} catch (Exception e) {
			transaction.abort();
			throw new MDMFileAccessException(String.format(
					"FileLink (%s) of TemplateAttribute (%s) could not be deleted.", identifier, templateAttribute), e);
		}

		return fileLink;
	}

	/**
	 * Deletes file from file server and removes the related {@link FileLink} from
	 * the specified context attribute.
	 * 
	 * @param sourceName           name of the source (MDM {@link Environment})
	 * @param contextDescribable   the context describable
	 * @param contextType          the {@link ContextType} holding the component
	 * @param contextComponentName the name of the component holding the file link
	 *                             attribute
	 * @param attributeName        the name of the attribute holding the file link
	 * @param identifier           the remotePath
	 * @return
	 */
	public FileLink deleteFileLink(String sourceName, ContextDescribable contextDescribable, ContextType contextType,
			String contextGroup, String contextComponentName, String attributeName, String identifier) {
		Pair<String, String> fileLinkIdentifierInfo = getFileLinkIdentifierInfo(identifier);
		if (fileLinkIdentifierInfo != null) {
			Transaction transaction = getEntityManager(sourceName).startTransaction();
			
			try {
				
				String serviceid = fileLinkIdentifierInfo.getLeft();

				ModelManager modelManager = getModelManager(sourceName);

				Map<ContextType, ContextRoot> contextMap = getContextMap(sourceName, contextDescribable, contextType,
						contextGroup);
				ContextComponent contextComponent = findContextComponent(contextType, contextComponentName, contextMap)
						.orElseThrow(() -> new MDMEntityAccessException("ContextComponent with name "
								+ contextComponentName + " not found in context of type " + contextType));

				FileLink fileLink = null;

				if (FileServiceType.EXTREF.name().equalsIgnoreCase(serviceid)) {
					// Find link attribute in context
					Value value = contextComponent.getValue(attributeName);
					// Value value = findValueInContext(contextType, contextComponentName,
					// contextMap, attributeName);
					fileLink = extractFileLink(fileLinkIdentifierInfo.getRight(), value);

					// Delete the file from file Server
					if (!fileLink.isLink()) {
						deleteFile(sourceName, contextDescribable, fileLink, transaction);
					}

					// Removes file link from the context attribute
					removeFileLinkfromValue(value, fileLink);
				} else if (FileServiceType.AOFILE.name().equalsIgnoreCase(serviceid)) {
					EntityManager entityManager = getEntityManager(sourceName);
					DescriptiveFileManager descriptiveFileManager = new DescriptiveFileManager(modelManager);
					Relation relation = descriptiveFileManager.getDescriptiveFileRelation(contextComponent,
							attributeName);

					DescriptiveFile descriptiveFile = contextComponent.getDescriptiveFiles(relation, entityManager)
							.stream()
							.filter(df -> Serializer.getFileLinkIdentifier(df.getFileLink())
									.equalsIgnoreCase(identifier))
							.findFirst().orElseThrow(() -> new MDMEntityAccessException(
									"FileLink with identifier " + identifier + " not found!"));

					boolean hasAdditionalRelatedInstances = descriptiveFileManager
							.hasAdditionalRelatedInstances(entityManager, descriptiveFile, relation);

					// Disconnect MDMFile from ContextComponent:
					contextComponent.removeDescriptiveFile(relation, descriptiveFile);

					fileLink = descriptiveFile.getFileLink();

					if (!hasAdditionalRelatedInstances) {
						deleteMDMFile(sourceName, descriptiveFile);
					}
				}

				// Persist changes
				DescribableContexts dc = createDescribableContext(contextDescribable, contextGroup, contextMap);
				contextService.persist(Arrays.asList(dc));
				transaction.commit();
				return fileLink;

			} catch (Exception exc) {
				transaction.abort();
				throw new MDMFileAccessException(
						"Could not delete " + fileLinkIdentifierInfo.getRight() + " at " + contextDescribable, exc);
			} 
		}

		return null;
	}

	/**
	 * Helper function to find the {@link FileLink} with the specified remotePath
	 * attached to the given entity.
	 * 
	 * @param identifier      the identifier
	 * @param filesAttachable the {@link ContextDescribable} the file is attached to
	 * @return the specified {@link FileLink}
	 */
	public FileLink findFileLinkInContext(String sourceName, String identifier, ContextDescribable contextDescribable,
			ContextType contextType, String contextGroup, String contextComponentName, String attributeName) {

		Pair<String, String> fileLinkIdentifierInfo = getFileLinkIdentifierInfo(identifier);
		if (fileLinkIdentifierInfo != null) {
			String serviceid = fileLinkIdentifierInfo.getLeft();
			if (FileServiceType.EXTREF.name().equalsIgnoreCase(serviceid)) {
				for (FileLink l : findAllFileLinksInContext(sourceName, contextDescribable, contextType, contextGroup,
						contextComponentName, attributeName)) {
					if (l.isRemote() && l.getFileServiceType() == FileServiceType.EXTREF
							&& l.getRemotePath().equals(fileLinkIdentifierInfo.getRight())) {
						return l;
					}
				}
			} else if (FileServiceType.AOFILE.name().equalsIgnoreCase(serviceid)) {
				for (FileLink l : findAllFileLinksInContext(sourceName, contextDescribable, contextType, contextGroup,
						contextComponentName, attributeName)) {
					if (/*
						 * l.isRemote() &&
						 */l.getFileServiceType() == FileServiceType.AOFILE
							&& Serializer.getFileLinkIdentifier(l).equalsIgnoreCase(identifier)) {
						return l;
					}
				}
			}
		}
		throw new MDMEntityAccessException("FileLink with identifier " + identifier + " not found!");
	}

	public List<MDMFileLinkExt> findFileLinks(String sourceName, FilesAttachable<T> filesAttachable) {
		Optional<EntityType> descriptiveFile = findDescriptiveFileEntityType(sourceName);

		if (descriptiveFile.isPresent()) {
			return filesAttachable.getMDMFiles(getEntityManager(sourceName)).stream().map(file -> file.getFileLink())
					.map(fl -> Serializer.serializeFileLink(fl)).collect(Collectors.toList());
		} else {
			return Stream.of(filesAttachable.getFileLinks()).map(fl -> Serializer.serializeFileLink(fl))
					.collect(Collectors.toList());
		}
	}

	public List<FileLink> findAllFileLinksInContext(String sourceName, ContextDescribable contextDescribable,
			ContextType contextType, String contextGroup, String contextComponentName, String attributeName) {
		ModelManager modelManager = getModelManager(sourceName);

		Map<ContextType, ContextRoot> contextMap = getContextMap(sourceName, contextDescribable, contextType,
				contextGroup);
		ContextComponent contextComponent = findContextComponent(contextType, contextComponentName, contextMap)
				.orElse(null);

		if (contextComponent != null) {
			EntityType etCC = modelManager.getEntityType(contextComponent.getTypeName());

			Relation rel = null;
			Attribute attr = null;
			Optional<EntityType> etDf = findDescriptiveFileEntityType(sourceName);
			if (etDf.isPresent()) {
				Optional<Relation> optRel = etCC.getRelations().stream().filter(r -> r.getName().equals(attributeName)
						&& r.isNtoM() && r.getTarget().getName().equals(etDf.get().getName())).findFirst();

				if (optRel.isPresent()) {
					rel = optRel.get();
				} else {
					// Relation not found, try to find attribute:
					try {
						attr = etCC.getAttribute(attributeName);
					} catch (IllegalArgumentException exc) {
						// Attribute not found
					}
				}
			} else {
				try {
					attr = etCC.getAttribute(attributeName);
				} catch (IllegalArgumentException exc) {
					// Attribute not found
				}
			}

			if (rel != null) {
				return contextComponent.getDescriptiveFiles(rel, getEntityManager(sourceName)).stream()
						.map(mdmFile -> mdmFile.getFileLink()).collect(Collectors.toList());
			} else if (attr != null) {
				return extractFileLinks(contextComponent.getValue(attributeName));
			}
		}

		return Collections.emptyList();
	}

	/**
	 * Helper function to find the {@link FileLink} with the specified identifier
	 * attached to the given entity.
	 * 
	 * @param identifier      the identifier
	 * @param filesAttachable the {@link FilesAttachable} the file is attached to
	 * @return the specified {@link FileLink}
	 */
	public FileLink findFileLinkAtFileAttachable(String sourceName, String identifier, FilesAttachable<T> entity) {
		Pair<String, String> fileLinkIdentifierInfo = getFileLinkIdentifierInfo(identifier);
		if (fileLinkIdentifierInfo != null) {
			String serviceid = fileLinkIdentifierInfo.getLeft();
			if (FileServiceType.EXTREF.name().equalsIgnoreCase(serviceid)) {
				for (FileLink l : getAllFileLinks(sourceName, entity)) {
					if (l.isRemote() && l.getFileServiceType() == FileServiceType.EXTREF
							&& l.getRemotePath().equals(fileLinkIdentifierInfo.getRight())) {
						return l;
					}
				}
			} else if (FileServiceType.AOFILE.name().equalsIgnoreCase(serviceid)) {
				for (FileLink l : getAllFileLinks(sourceName, entity)) {
					if (/*
						 * l.isRemote() &&
						 */l.getFileServiceType() == FileServiceType.AOFILE
							&& Serializer.getFileLinkIdentifier(l).equalsIgnoreCase(identifier)) {
						return l;
					}
				}
			}
		}
		throw new MDMEntityAccessException("FileLink with identifier " + identifier + " not found!");
	}

	public Pair<String, String> getFileLinkIdentifierInfo(String identifier) {
		if (identifier != null && identifier.contains(":") && identifier.indexOf(':') < identifier.length() - 1) {
			String serviceid = identifier.split(":")[0].trim();
			String idstring = identifier.substring(identifier.indexOf(':') + 1).trim();

			return new ImmutablePair<String, String>(serviceid, idstring);
		}

		return null;
	}

	public StreamingOutput toStreamingOutput(String sourceName, Entity entity, FileLink fileLink) {
		return new StreamingOutput() {
			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException {
				try (Transaction transaction = getEntityManager(sourceName).startTransaction()) {
					streamFileLink(sourceName, entity, fileLink, output, transaction);
				}
			}
		};
	}

	public StreamingOutput toStreamingOutput(String sourceName, List<FileLink> fileLinks) {
		return new StreamingOutput() {
			@Override
			public void write(OutputStream output) throws IOException, WebApplicationException {
				ZipOutputStream zipfile = new ZipOutputStream(output);
				ZipEntry zipentry = null;

				try (Transaction transaction = getEntityManager(sourceName).startTransaction()) {
					for (FileLink fileLink : fileLinks) {
						zipentry = new ZipEntry(Serializer.getFileLinkIdentifier(fileLink).replaceAll(":", "_") + "/"
								+ ((MDMFile) fileLink.getRemoteObject()).getOriginalFileName());
						zipfile.putNextEntry(zipentry);
						streamFileLink(sourceName, (MDMFile) fileLink.getRemoteObject(), fileLink, zipfile,
								transaction);
						zipfile.closeEntry();
					}
				} finally {
					zipfile.flush();
					zipfile.close();
				}
			}
		};
	}

	/**
	 * Creates {@link StreamingOutput} for file. Guesses mime-type to ensure proper
	 * display/download functionality at client side.
	 * 
	 * @TODO Mime-type guess works only for limited types, but CorbaFileServer does
	 *       not offer methods to properly load mime-type. Find a solution!
	 * 
	 * @param sourceName
	 * @param entity
	 * @param fileLink
	 * @return
	 */
	public Tuple2<StreamingOutput, String> toStreamingOutputGuessMimeType(String sourceName, Entity entity,
			FileLink fileLink, Transaction transaction) {
		String m = null;
		StreamingOutput o = null;
		try {
			InputStream in = transaction.getFileService(fileLink.getFileServiceType()).openStream(entity, fileLink,
					transaction);
			m = URLConnection.guessContentTypeFromStream(in);
			o = new StreamingOutput() {
				public void write(OutputStream output) throws IOException, WebApplicationException {
					try {
						ByteStreams.copy(in, output);
					} finally {
						in.close();
					}
				}
			};
		} catch (IOException e) {
			throw new MDMFileAccessException(e.getMessage(), e);
		}
		return Tuple.of(o, m);
	}

	private Stream<Relation> getFileRelations(String sourceName, FilesAttachable<T> filesAttachable) {
		ModelManager modelManager = getModelManager(sourceName);

		return Stream.concat(
				modelManager.getEntityType(filesAttachable).getRelations(RelationType.FATHER_CHILD).stream()
						.filter(r -> "FileChildren".equals(r.getName())),
				modelManager.getEntityType(filesAttachable).getRelations().stream()
						.filter(r -> "DescriptiveFile".equals(r.getTarget().getName())));
	}

	/**
	 * Helping function to physically delete file from file server
	 * 
	 * @param sourceName name of the source (MDM {@link Environment})
	 * @param entity     the {@link Entity} the file is attached to or holding the
	 *                   context with the file attribute
	 * @param fileLink   the {@link FileLink} related to the file
	 */
	private void deleteFile(String sourceName, Entity entity, FileLink fileLink, Transaction transaction) {
		FileService fileService = transaction.getFileService(fileLink.getFileServiceType());
		fileService.delete(entity, fileLink, transaction);
	}

	/**
	 * Helping function to physically upload file to file server
	 * 
	 * @param sourceName name of the source (MDM {@link Environment})
	 * @param entity     the {@link Entity} the file is attached to or holding the
	 *                   context with the file attribute
	 * @param fileLink   the {@link FileLink} related to the file
	 */
	private void uploadFile(String sourceName, Entity entity, FileLink fileLink, Transaction transaction)
			throws IOException {
		FileService fileService = transaction.getFileService(fileLink.getFileServiceType());
		List<FileLink> fileLinks = new ArrayList<>();
		fileLinks.add(fileLink);
		fileService.uploadSequential(entity, fileLinks, transaction, null);

	}

	/**
	 * Helper function to persist an {@link Entity}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment})
	 * @param entity     the {@link Entity} the file is attached to or holding the
	 *                   context with the file attribute
	 */
	private void persistEntity(String sourceName, Entity entity, Transaction t) {
		List<Entity> entities = new ArrayList<>();
		entities.add(entity);
		t.update(entities);
	}

//	/**
//	 * Helper function to create an {@link Entity}
//	 * 
//	 * @param sourceName name of the source (MDM {@link Environment})
//	 * @param entity     the {@link Entity} the file is attached to or holding the
//	 *                   context with the file attribute
//	 */
//	private void createEntity(String sourceName, Entity entity) {
//		List<Entity> entities = new ArrayList<>();
//		entities.add(entity);
//
//		EntityManager em = getEntityManager(sourceName);
//		try (Transaction t = em.startTransaction()) {
//			t.create(entities);
//			t.commit();
//		}
//	}

	private void createEntity(String sourceName, Entity entity, Transaction t) {
		List<Entity> entities = new ArrayList<>();
		entities.add(entity);
		t.create(entities);

	}

	/**
	 * Helper function to delete an {@link Entity}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment})
	 * @param entity     the {@link Entity} the file is attached to or holding the
	 *                   context with the file attribute
	 */
	private void deleteMDMFile(String sourceName, MDMFile mdmFile) {
		EntityManager em = getEntityManager(sourceName);
		try (Transaction t = em.startTransaction()) {
			t.delete(Arrays.asList(mdmFile));
			t.commit();
		}
	}

	/**
	 * Helper function to create new local {@FileLink}. Sets filename as default
	 * description, if no description is provided.
	 * 
	 * @param fileName    the file name
	 * @param fis         the file {@link InputStream}
	 * @param description the file description
	 * @param mimeType    the files {@link MimeType}
	 * @return the new {@FileLink}
	 * @throws IOException
	 */
	private FileLink newLocalFileLink(String fileName, InputStream fis, String description, MimeType mimeType,
			FileServiceType fileServiceType) throws IOException {
		String desc = (description == null || description.isEmpty()) ? fileName : description;
		return FileLink.newLocal(fis, fileName, -1, mimeType, desc, null, fileServiceType);
	}

	/**
	 * Helper function to load context for {@link ContextDescribable}s.
	 * 
	 * Notice: For {@link TestStep}s only the ordered context is loaded. For
	 * {@link Measurement}s only the measured context is loaded.
	 * 
	 * @param sourceName         name of the source (MDM {@link Environment})
	 * @param contextDescribable the {@link ContextDescribable}
	 * @param contextType        the {@link ContextType} holding the component
	 * @return
	 */
	private Map<ContextType, ContextRoot> getContextMap(String sourceName, ContextDescribable contextDescribable,
			ContextType contextType, String contextGroup) {
		String cg = Strings.nullToEmpty(contextGroup).trim().toLowerCase();

		switch (cg) {
		case ContextActivity.CONTEXT_GROUP_ORDERED:
		case ContextActivity.CONTEXT_GROUP_MEASURED:
			break;
		default:
			throw new MDMEntityAccessException("Cannot use context group " + contextGroup + " here.");
		}

		if (contextDescribable instanceof TestStep) {
			return contextActivity.getTestStepContext(sourceName, contextDescribable.getID(), contextType).get(cg);
		} else if (contextDescribable instanceof Measurement) {
			return contextActivity.getMeasurementContext(sourceName, contextDescribable.getID(), contextType).get(cg);
		}
		throw new MDMEntityAccessException(
				"No context of type " + contextType + " found for " + contextDescribable.toString());
	}

	/**
	 * Helper function to add {@link FileLink} to a {@link Value}
	 * 
	 * @param value    the {@link Value}
	 * @param fileLink the {@link FileLink}
	 */
	private void setOrAddFileLinkToValue(Value value, FileLink fileLink) {
		if (ValueType.FILE_LINK.equals(value.getValueType())) {
			value.set(fileLink);
		} else if (ValueType.FILE_LINK_SEQUENCE.equals(value.getValueType())) {
			// delete if exists or create if not exists.
			FileLink[] old = value.extract(ValueType.FILE_LINK_SEQUENCE);
			FileLink[] current = Arrays.copyOf(old, old.length + 1);
			current[old.length] = fileLink;
			value.set(current);
		}
	}

	/**
	 * Helper function to remove {@link FileLink} from a {@link Value}
	 * 
	 * @param value    the {@link Value}
	 * @param fileLink the {@link FileLink}
	 */
	private void removeFileLinkfromValue(Value value, FileLink fileLink) {
		if (ValueType.FILE_LINK.equals(value.getValueType())) {
			value.set(null);
		} else if (ValueType.FILE_LINK_SEQUENCE.equals(value.getValueType())) {
			FileLink[] links = Stream.of(value.extract(ValueType.FILE_LINK_SEQUENCE))
					.filter(link -> !link.equals(fileLink)).toArray(FileLink[]::new);
			value.set(links);
		}
	}

	/**
	 * Helping function to create {@link DescribableContexts}
	 * 
	 * @param contextDescribable the {@link ContextDescribable}
	 * @param contextMap         the context
	 * @return
	 */
	private DescribableContexts createDescribableContext(ContextDescribable contextDescribable, String contextGroup,
			Map<ContextType, ContextRoot> contextMap) {
		String cg = Strings.nullToEmpty(contextGroup).trim().toLowerCase();

		DescribableContexts dc = new DescribableContexts();
		dc.setTestStep(contextService.getTestStep(contextDescribable));
		if (ContextActivity.CONTEXT_GROUP_ORDERED.equals(cg)) {
			dc.setOrdered(contextMap);
		} else if (ContextActivity.CONTEXT_GROUP_MEASURED.equals(cg)) {
			dc.setMeasured(contextMap);
		}

		if (contextDescribable instanceof Measurement) {
			List<Measurement> list = new ArrayList<>();
			list.add((Measurement) contextDescribable);
			dc.setMeasurements(list);
		}
		return dc;
	}

	/**
	 * 
	 * Helper function to find a ContextComponent
	 * 
	 * @param contextType
	 * @param componentName
	 * @param typeRootMap
	 * @return
	 */
	private Optional<ContextComponent> findContextComponent(ContextType contextType, String componentName,
			Map<ContextType, ContextRoot> typeRootMap) {

		ContextRoot contextRoot = typeRootMap.get(contextType);
		if (contextRoot != null) {
			List<ContextComponent> components = contextRoot.getContextComponents().stream()
					.filter(cc -> cc.getName().equals(componentName)).collect(Collectors.toList());
			if (components != null && !components.isEmpty()) {
				return Optional.ofNullable(components.get(0));
			}
		}

		return Optional.empty();
	}

	/**
	 * 
	 * @param remotePath
	 * @param value
	 * @return
	 */
	private FileLink extractFileLink(String remotePath, Value value) {
		return extractFileLinks(value).stream().filter(link -> link.getRemotePath().equals(remotePath))
				.collect(Collectors.toList()).get(0);
	}

	private List<FileLink> extractFileLinks(Value value) {
		if (ValueType.FILE_LINK.equals(value.getValueType())) {
			FileLink fileLink = value.extract(ValueType.FILE_LINK);
			if (fileLink.isLocal() || fileLink.isRemote()) {
				return Lists.newArrayList(fileLink);
			}
		} else if (ValueType.FILE_LINK_SEQUENCE.equals(value.getValueType())) {
			return Stream.of(value.extract(ValueType.FILE_LINK_SEQUENCE)).collect(Collectors.toList());
		}

		return Collections.emptyList();
	}

	/**
	 * Opens an {@code InputStream} for given {@link FileLink} and copies it to
	 * {@param outputStream}.
	 * 
	 * @param sourceName      name of the source (MDM {@link Environment})
	 * @param filesAttachable the {@link FilesAttachable} {@link Entity} the file is
	 *                        attached to
	 * @param fileLink        the {@link FileLink}
	 * @param outputStream    the {link OutputStream}
	 * @throws IOException Thrown if unable to provide as stream
	 */
	private void streamFileLink(String sourceName, Entity entity, FileLink fileLink, OutputStream outputStream,
			Transaction transaction) throws IOException {

		try (InputStream in = transaction.getFileService(fileLink.getFileServiceType()).openStream(entity, fileLink,
				transaction)) {
			ByteStreams.copy(in, outputStream);
		}
	}

	private Optional<EntityType> findDescriptiveFileEntityType(String sourceName) {
		return getModelManager(sourceName).listEntityTypes().stream()
				.filter(et -> DescriptiveFile.TYPE_NAME.equals(et.getName())).findFirst();
	}

	/**
	 * Helper function to load a {@link ModelManager}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment})
	 * @return the {@link ModelManager}
	 */
	private ModelManager getModelManager(String sourceName) {
		ApplicationContext context = this.connectorService.getContextByName(sourceName);
		return context.getModelManager()
				.orElseThrow(() -> new MDMEntityAccessException("Model manager not present in '" + sourceName + "'."));
	}

	/**
	 * Helper function to load an {@link EntityManager}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment})
	 * @return the {@link EntityManager}
	 */
	public EntityManager getEntityManager(String sourceName) {
		ApplicationContext context = this.connectorService.getContextByName(sourceName);
		return context.getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present in '" + sourceName + "'."));
	}

	/**
	 * Helper function to load an {@link EntityFactory}
	 * 
	 * @param sourceName name of the source (MDM {@link Environment})
	 * @return the {@link EntityFactory}
	 */
	private EntityFactory getEntityFactory(String sourceName) {
		ApplicationContext context = this.connectorService.getContextByName(sourceName);
		return context.getEntityFactory()
				.orElseThrow(() -> new MDMEntityAccessException("Entity factory not present in '" + sourceName + "'."));
	}

	/**
	 * Helper function returning all FileLinks (irrespective of
	 * {@link FileServiceType}) of a FilesAttachable
	 * 
	 * @param sourceName      name of the source (MDM {@link Environment})
	 * @param filesAttachable the {@link FilesAttachable} {@link Entity} the
	 *                        FileLinks are attached to
	 * @return The {@link FileLink}s attached to filesAttachable
	 */
	public List<FileLink> getAllFileLinks(String sourceName, FilesAttachable<T> filesAttachable) {
		List<FileLink> listFileLinks = new ArrayList<>();
		listFileLinks.addAll(Arrays.asList(filesAttachable.getFileLinks()));
		listFileLinks.addAll(filesAttachable.getMDMFiles(getEntityManager(sourceName)).stream()
				.map(mdmFile -> mdmFile.getFileLink()).collect(Collectors.toList()));

		return listFileLinks;
	}

	/**
	 * Returns the {@link MediaType} for a FileLink. In case of error
	 * {@link MediaType#APPLICATION_OCTET_STREAM_TYPE} is returned
	 * 
	 * @param fileLink
	 * @return MediaType corresponding to the FileLink's mimetype
	 */
	public MediaType toMediaType(FileLink fileLink) {
		String mimeType = fileLink.getMimeType().toString();

		if (mimeType == null || mimeType.isEmpty()) {
			return MediaType.APPLICATION_OCTET_STREAM_TYPE;
		}

		try {
			return MediaType.valueOf(mimeType);
		} catch (IllegalArgumentException e) {
			// if the mimeType is not parsable to a MediaType, just application/octet-stream
			// is returned
			return MediaType.APPLICATION_OCTET_STREAM_TYPE;
		}

	}
}
