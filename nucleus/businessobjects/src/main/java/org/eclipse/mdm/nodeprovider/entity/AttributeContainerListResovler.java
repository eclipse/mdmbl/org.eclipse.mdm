/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.entity;

import java.beans.FeatureDescriptor;
import java.util.Iterator;
import java.util.Optional;

import javax.el.ELContext;
import javax.el.ELResolver;

import org.eclipse.mdm.nodeprovider.control.AttributeContainer;

public class AttributeContainerListResovler extends ELResolver {

	@Override
	public void setValue(ELContext context, Object base, Object property, Object value) {
		// Intentionally empty.
	}

	@Override
	public boolean isReadOnly(ELContext context, Object base, Object property) {
		return true;
	}

	@Override
	public Object getValue(ELContext context, Object base, Object property) {
		Optional<AttributeContainer> resolved = getValue(context, property);
		if (resolved.isPresent()) {
			AttributeContainer ac = resolved.get();
			if (ac.isValid()) {
				context.setPropertyResolved(base, property);
				return ac.getValue();
			} else {
				// resolved to null
				context.setPropertyResolved(base, property);
				return null;
			}
		} else {
			// unresolved
			return null;
		}
	}

	@Override
	public Class<?> getType(ELContext context, Object base, Object property) {
		Optional<AttributeContainer> resolved = getValue(context, property);
		if (resolved.isPresent()) {
			AttributeContainer ac = resolved.get();
			context.setPropertyResolved(base, property);
			return ac.getValue().getClass();
		} else {
			// unresolved
			return null;
		}
	}

	private Optional<AttributeContainer> getValue(ELContext context, Object property) {
		AttributeContainerList labelAttrContainers = (AttributeContainerList) context
				.getContext(AttributeContainerList.class);

		return labelAttrContainers.stream()
				.filter(attr -> attr.getAttribute().getName().equalsIgnoreCase(String.valueOf(property))).findFirst();
	}

	@Override
	public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext context, Object base) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<?> getCommonPropertyType(ELContext context, Object base) {
		// TODO Auto-generated method stub
		return null;
	}
}
