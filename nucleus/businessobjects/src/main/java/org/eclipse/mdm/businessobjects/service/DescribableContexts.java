/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.service;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.TestStep;

/**
 * DescribableContexts hold Context information for a TestStep and child
 * Measurements.
 *
 */
public class DescribableContexts {

	private TestStep testStep;
	private List<Measurement> measurements = new ArrayList<>();

	private Map<ContextType, ContextRoot> orderedContext = new EnumMap<>(ContextType.class);
	private Map<ContextType, ContextRoot> measuredContext = new EnumMap<>(ContextType.class);

	public TestStep getTestStep() {
		return testStep;
	}

	public void setTestStep(TestStep testStep) {
		this.testStep = testStep;
	}

	public List<Measurement> getMeasurements() {
		return measurements;
	}

	public void setMeasurements(List<Measurement> measurements) {
		this.measurements = measurements;
	}

	public void setOrdered(Map<ContextType, ContextRoot> orderedContext) {
		this.orderedContext = orderedContext;
	}

	public void setMeasured(Map<ContextType, ContextRoot> measuredContext) {
		this.measuredContext = measuredContext;
	}

	public Map<ContextType, ContextRoot> getMeasuredContext() {
		return measuredContext;
	}

	public Map<ContextType, ContextRoot> getOrderedContext() {
		return orderedContext;
	}

	/**
	 * @return a list with all entities (TestStep, Measurements, ContextRoots)
	 */
	public List<Entity> getEntities() {
		List<Entity> entities = new ArrayList<>();

		entities.addAll(orderedContext.values());
		entities.addAll(measuredContext.values());

		if (getTestStep() != null) {
			entities.add(getTestStep());
		}
		entities.addAll(getMeasurements());

		return entities;
	}

	/**
	 * @return optional Measurement if existing
	 */
	public Optional<Measurement> getAnyMeasurement() {
		if (measurements.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of(measurements.get(0));
		}
	}
}
