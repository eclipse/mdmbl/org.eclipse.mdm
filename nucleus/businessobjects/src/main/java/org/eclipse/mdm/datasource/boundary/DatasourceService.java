/*******************************************************************************
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

package org.eclipse.mdm.datasource.boundary;

import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.connector.control.ServiceConfigurationActivity;

/**
 * Bean implementation with available Services
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
@Stateless
public class DatasourceService {

	@Inject
	private ConnectorService connectorService;

	@Inject
	ServiceConfigurationActivity serviceConfigurationActivity;

	/**
	 * Default no-arg constructor for EJB
	 */
	public DatasourceService() {
	}

	public List<String> readServiceNames() {
		return serviceConfigurationActivity.readServiceConfigurations().stream().map(s -> s.getName())
				.collect(Collectors.toList());
	}

	public void setActiveServices(List<String> activeServices) {
		this.connectorService.setActiveSourceNames(activeServices);
	}
}
