/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.Collection;

import javax.ejb.Stateless;

import org.eclipse.mdm.api.base.model.EnumRegistry;
import org.eclipse.mdm.api.base.model.Enumeration;

/**
 * @author jz
 *
 */
@Stateless
public class EnumService {

	public EnumService() {
		// Default no-arg constructor for EJB
	}

	public Collection<Enumeration<?>> allEnums(String sourceName) {
		return EnumRegistry.getInstance().list(sourceName);
	}

	public Enumeration<?> findEnum(String sourceName, String enumName) {
		return EnumRegistry.getInstance().get(sourceName, enumName);
	}
}
