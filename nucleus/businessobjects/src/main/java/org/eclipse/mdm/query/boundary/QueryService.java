/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.query.boundary;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.adapter.Relation;
import org.eclipse.mdm.api.base.adapter.RelationType;
import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Value;
import org.eclipse.mdm.api.base.model.ValueType;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.base.search.SearchService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.businessobjects.control.FilterParser;
import org.eclipse.mdm.businessobjects.utils.Serializer;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.property.GlobalProperty;
import org.eclipse.mdm.query.entity.QueryRequest;
import org.eclipse.mdm.query.entity.QueryResult;
import org.eclipse.mdm.query.entity.Row;
import org.eclipse.mdm.query.entity.SourceFilter;
import org.eclipse.mdm.query.entity.SuggestionRequest;
import org.eclipse.mdm.query.util.ConversionUtil;
import org.eclipse.mdm.query.util.DescriptiveFileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Shorts;

/**
 * 
 * @author Matthias Koller, Peak Solution GmbH
 *
 */
@Stateless
public class QueryService {

	private static final Logger LOG = LoggerFactory.getLogger(QueryService.class);

	@Inject
	@GlobalProperty(value = "businessobjects.query.maxresultspersource")
	private String maxResultsPerSource;

	@Inject
	@GlobalProperty(value = "businessobjects.query.suggestionsforsequenceattributes")
	private String suggestionsforsequenceattributes;

	@Inject
	ConnectorService connectorService;

	/**
	 * Supported ValueTypes for loading suggestion values. Enums are handled
	 * separately. The corresponding *_SEQUENCE types are also supported.
	 */
	private static final Set<ValueType<?>> SUPPORTED_VALUE_TYPES = ImmutableSet.of(ValueType.STRING, ValueType.DATE,
			ValueType.LONG, ValueType.INTEGER, ValueType.SHORT, ValueType.BYTE);

	public QueryService() {

	}

	QueryService(ConnectorService connectorService) {
		this.connectorService = connectorService;
	}

	public QueryResult queryRows(QueryRequest request) {
		QueryResult result = new QueryResult(new ArrayList<>());

		for (SourceFilter filter : request.getFilters()) {
			try {
				ApplicationContext context = this.connectorService.getContextByName(filter.getSourceName());

				List<Row> rows = queryRowsForSource(context, request.getResultType(), request.getColumns(),
						request.getResultOffset(), getResultLimit(request), filter.getFilter(),
						filter.getSearchString(), request.getContextCompResultType());

				result.getRows().addAll(rows);
				result.setTotalRecords(filter.getSourceName(), rows.size());
			} catch (Exception e) {
				String messsage = "Could not retrieve query results for environment '" + filter.getSourceName() + "': "
						+ e.getMessage();
				throw new DataAccessException(messsage, e);
			}
		}
		return result;
	}

	public List<String> getSuggestions(SuggestionRequest suggestionRequest) {

		List<String> suggestions = new ArrayList<>();

		for (String envName : suggestionRequest.getSourceNames()) {

			ApplicationContext context = this.connectorService.getContextByName(envName);
			Optional<ModelManager> mm = context.getModelManager();
			Optional<org.eclipse.mdm.api.base.query.QueryService> qs = context.getQueryService();

			if (mm.isPresent() && qs.isPresent()) {

				try {
					// ==================================================================
					// Redirect Search-Column "Status" for Test and TestStep
					// ==================================================================
					if ((suggestionRequest.getType().equals("Test") || suggestionRequest.getType().equals("TestStep"))
							&& suggestionRequest.getAttrName().equals("Status")) {
						suggestionRequest.setType("Status");
						suggestionRequest.setAttrName("Name");
					}

					EntityType entityType = mm.get()
							.getEntityType(ServiceUtils.invertMapping(suggestionRequest.getType()));

					Attribute attr = entityType.getAttribute(suggestionRequest.getAttrName());

					Filter prefixFilter = Filter.and();
					if (!Strings.isNullOrEmpty(suggestionRequest.getPrefix()) && attr.getValueType().isStringType()) {
						// Prefix Filter only supported for String type attributes
						if (attr.getValueType().isStringSequence()) {
							prefixFilter.add(ComparisonOperator.CASE_INSENSITIVE_LIKE.create(attr,
									new String[] { suggestionRequest.getPrefix() + "*" }));
						} else {
							prefixFilter.add(ComparisonOperator.CASE_INSENSITIVE_LIKE.create(attr,
									suggestionRequest.getPrefix() + "*"));
						}
					}

					if (attr.getValueType().isEnumerationType()) {
						attr.getEnumObj().getValues().values().stream().map(e -> e.name()).sorted().distinct()
								.forEach(suggestions::add);
					} else if (attr.getValueType().isSequence()
							&& SUPPORTED_VALUE_TYPES.contains(attr.getValueType().toSingleType())
							&& isSuggestionsForSequenceAttributes()) {
						// GroupBy cannot be used on sequence attributes, thus loading all values and
						// use distinct and limit on fetched values
						Stream<String> stream = qs.get().createQuery().select(attr).limit(getMaxResultsPerSource())
								.fetch(prefixFilter).stream().flatMap(r -> flatten(r.getValue(attr))).distinct();

						if (!Strings.isNullOrEmpty(suggestionRequest.getPrefix())) {
							stream = stream.filter(s -> s != null)
									.filter(s -> s.startsWith(suggestionRequest.getPrefix()));
						}

						stream.limit(suggestionRequest.getMaxResults()).forEach(suggestions::add);
					} else if (SUPPORTED_VALUE_TYPES.contains(attr.getValueType())) {
						Stream<String> stream = qs.get().createQuery().select(attr)
								.limit(suggestionRequest.getMaxResults()).group(attr).fetch(prefixFilter).stream()
								.map(r -> r.getValue(attr)).filter(v -> v.isValid()).map(Serializer::serializeValue)
								.map(Objects::toString);

						if (!Strings.isNullOrEmpty(suggestionRequest.getPrefix())) {
							stream = stream.filter(s -> s != null)
									.filter(s -> s.startsWith(suggestionRequest.getPrefix()));
						}

						stream.forEach(suggestions::add);
					} else {
						LOG.debug("Suggestions for valuetype {} (Attribute: {}.{}) are not supported!",
								attr.getValueType(), entityType.getName(), attr.getName());
					}

				} catch (DataAccessException | IllegalArgumentException e) {
					LOG.warn("Cannot retreive suggestions " + suggestionRequest + " for Environment + " + envName + "!",
							e);
				}
			}
		}
		return suggestions;
	}

	/**
	 * If value is a String, Byte, Short, Int or Long sequence, the values of all
	 * sequences are flattened a returned as a stream of strings.
	 * 
	 * @param value
	 * @return flattened values as string stream
	 */
	private Stream<String> flatten(Value value) {
		if (value.getValueType().isStringSequence()) {
			return Stream.of(value.extract(ValueType.STRING_SEQUENCE));
		} else if (value.getValueType().isDateSequence()) {
			return Stream.of(value.extract(ValueType.DATE_SEQUENCE)).map(Serializer::formatDate);
		} else if (value.getValueType().isByteSequence()) {
			return Bytes.asList(value.extract(ValueType.BYTE_SEQUENCE)).stream().map(b -> Byte.toString(b));
		} else if (value.getValueType().isShortSequence()) {
			return Shorts.asList(value.extract(ValueType.SHORT_SEQUENCE)).stream().map(s -> Short.toString(s));
		} else if (value.getValueType().isIntegerSequence()) {
			return IntStream.of(value.extract(ValueType.INTEGER_SEQUENCE)).mapToObj(Integer::toString);
		} else if (value.getValueType().isLongSequence()) {
			return LongStream.of(value.extract(ValueType.LONG_SEQUENCE)).mapToObj(Long::toString);
		} else {
			return Stream.empty();
		}
	}

	private List<Row> queryRowsForSource(ApplicationContext context, String resultEntity, List<String> columns,
			int resultOffset, Integer resultLimit, String filterString, String searchString,
			String contextCompResultType) throws DataAccessException {

		ModelManager modelManager = context.getModelManager()
				.orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));

		SearchService searchService = context.getSearchService()
				.orElseThrow(() -> new IllegalStateException("neccessary SearchService is not available"));

		Class<? extends Entity> resultType = getEntityClassByNameType(searchService, resultEntity);

		List<EntityType> searchableTypes = searchService.listEntityTypes(resultType);

		List<Attribute> attributes = columns.stream().map(c -> getAttribute(searchableTypes, c))
				.filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList());

		Filter filter = FilterParser.parseFilterString(searchableTypes, filterString);

		EntityType resultContextCompType = getResultContextComponentType(contextCompResultType, searchableTypes);

		List<Result> result = searchService.fetchResults(resultType, attributes, filter, searchString, resultOffset,
				resultLimit, resultContextCompType);

		List<Attribute> requestedAttributes = new DescriptiveFileUtil(context).addDescriptiveFilesToResult(columns,
				searchableTypes, result);

		ConversionUtil queryUtil = new ConversionUtil(modelManager);

		if (resultContextCompType == null) {
			return queryUtil.convertResultList(result, resultType, requestedAttributes,
					modelManager.getEntityType(resultType));
		} else {
			return queryUtil.convertResultList(result, ContextComponent.class, requestedAttributes,
					resultContextCompType);
		}
	}

	private EntityType getResultContextComponentType(String contextCompResultType, List<EntityType> searchableTypes) {
		if (contextCompResultType != null) {
			for (EntityType type : searchableTypes) {
				if (contextCompResultType.equalsIgnoreCase(type.getName())) {
					return type;
				}
			}
		}
		return null;
	}

	private Optional<Attribute> getAttribute(List<EntityType> searchableTypes, String c) {
		String[] parts = c.split("\\.");

		if (parts.length != 2) {
			throw new IllegalArgumentException("Cannot parse attribute " + c + "!");
		}

		String type = parts[0];
		String attributeName = parts[1];

		Optional<EntityType> entityType = searchableTypes.stream()
				.filter(e -> ServiceUtils.workaroundForTypeMapping(e).equalsIgnoreCase(type)).findFirst();

		if (entityType.isPresent()) {
			Optional<Attribute> attr = entityType.get().getAttributes().stream()
					.filter(a -> a.getName().equalsIgnoreCase(attributeName)).findFirst();
			if (!attr.isPresent() && entityType.get().getRelations().stream()
					.filter(r -> r.getName().equalsIgnoreCase(attributeName)).findFirst().isPresent()) {
				attr = this.getParentIdAttribute(entityType.get().getParentRelations());
			}
			return attr;
		} else {
			return Optional.empty();
		}
	}

	private Optional<Attribute> getParentIdAttribute(List<Relation> relations) {
		Optional<Relation> rel = relations.stream().filter(r -> "TestStep".indexOf(r.getTarget().getName()) >= 0)
				.findFirst();
		if (rel.isPresent()) {
			return rel.get().getTarget().getAttributes().stream().filter(a -> a.getName().equalsIgnoreCase("Id"))
					.findFirst();
		} else {
			for (Relation relation : relations) {
				return this.getParentIdAttribute(relation.getTarget().getRelations(RelationType.INFO));
			}
		}
		return Optional.empty();
	}

	private Class<? extends Entity> getEntityClassByNameType(SearchService s, String name) {

		for (Class<? extends Entity> entityClass : s.listSearchableTypes()) {
			if (entityClass.getSimpleName().equalsIgnoreCase(name)) {
				return entityClass;
			}
		}
		throw new IllegalArgumentException("Invalid Entity '" + name + "'. Allowed values are: "
				+ s.listSearchableTypes().stream().map(Class::getSimpleName).collect(Collectors.joining(", ")));
	}

	private int getResultLimit(QueryRequest request) {
		if (request.getResultLimit() == null) {
			return getMaxResultsPerSource();
		} else {
			return request.getResultLimit();
		}
	}

	private int getMaxResultsPerSource() {
		try {
			return Integer.parseInt(maxResultsPerSource);
		} catch (NumberFormatException e) {
			return 1001;
		}
	}

	private boolean isSuggestionsForSequenceAttributes() {
		return Boolean.parseBoolean(suggestionsforsequenceattributes);
	}

}
