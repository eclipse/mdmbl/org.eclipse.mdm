/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import io.vavr.control.Try;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.entity.I18NResponse;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.entity.SearchAttributeResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.collection.List;

/**
 * {@link ChannelGroup} resource
 *
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@Tag(name = "ChannelGroup")
@Path("/environments/{SOURCENAME}/channelgroups")
public class ChannelGroupResource {

	@EJB
	private ChannelGroupService channelGroupService;

	@EJB
	private EntityService entityService;

	/**
	 * delegates the request to the {@link ChannelGroupService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link ChannelGroup} result
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Operation(summary = "Find ChannelGroups by filter", description = "Get a list of ChannelGroups matched by an optional filter", responses = {
			@ApiResponse(description = "The ChannelGroup", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response getChannelGroups(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@QueryParam("filter") String filter) {

		java.util.List<ChannelGroup> channelGroups = this.channelGroupService.getChannelGroups(sourceName, filter);
		return ServiceUtils.toResponse(new MDMEntityResponse(ChannelGroup.class, channelGroups), Status.OK);

	}

	/**
	 * delegates the request to the {@link ChannelGroupService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the search attributes", description = "Get a list of search attributes", responses = {
			@ApiResponse(description = "The search attributes", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchattributes")
	public Response getSearchAttributes(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		return Try.of(() -> channelGroupService.getSearchAttributes(sourceName))
				.map(attrs -> ServiceUtils.toResponse(new SearchAttributeResponse(attrs), Status.OK)).get();
	}

	/**
	 * returns a {@link ChannelGroup} identified by the given id.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link ChannelGroup}
	 * @return the matching {@link ChannelGroup}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{ID}")
	@Operation(summary = "Find a ChannelGroup by ID", description = "Returns ChannelGroup based on ID", responses = {
			@ApiResponse(description = "The ChannelGroup", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response getChannelGroup(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ChannelGroup", required = true) @PathParam(REQUESTPARAM_ID) String id) {

		ChannelGroup channelGroup = this.channelGroupService.getChannelGroup(sourceName, id);
		return ServiceUtils.toResponse(new MDMEntityResponse(ChannelGroup.class, channelGroup), Status.OK);

	}

	/**
	 * delegates the request to the {@link ChannelGroupService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/localizations")
	@Operation(summary = "Get the ChannelGroup localizations", description = "Returns ChannelGroup localizations", responses = {
			@ApiResponse(description = "The ChannelGroup localizations", content = @Content(schema = @Schema(implementation = I18NResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Deprecated
	public Response localize(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		Map<Attribute, String> localizedAttributeMap = this.channelGroupService.localizeAttributes(sourceName);
		Map<EntityType, String> localizedEntityTypeMap = this.channelGroupService.localizeType(sourceName);
		return ServiceUtils.toResponse(new I18NResponse(localizedEntityTypeMap, localizedAttributeMap), Status.OK);
	}

	/**
	 * Returns the created {@link ChannelGroup}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link ChannelGroup} to create.
	 * @return the created {@link ChannelGroup} as {@link Response}.
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Operation(summary = "Create a new ChannelGroup", responses = {
			@ApiResponse(description = "The ChannelGroup", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response create(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {

		return entityService
				.create(V(sourceName), ChannelGroup.class,
						entityService.extractRequestBody(body, sourceName, List.of(Measurement.class)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Updates the {@link ChannelGroup} with all parameters set in the given JSON
	 * body of the request.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link ChannelGroup} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link ChannelGroup}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	@Operation(summary = "Updates the ChannelGroup with all parameters set in the body of the request", responses = {
			@ApiResponse(description = "The ChannelGroup", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response update(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Uni", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), ChannelGroup.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Deletes and returns the deleted {@link ChannelGroup}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link ChannelGroup} to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	@Operation(summary = "Delete an existing ChannelGroup", responses = {
			@ApiResponse(description = "The deleted ChannelGroup", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response delete(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Uni", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), ChannelGroup.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}
}
