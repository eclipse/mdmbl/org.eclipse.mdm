/*******************************************************************************
 * Copyright (c) 2021 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

package org.eclipse.mdm.datasource.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * {@link Environment} resource
 * 
 * @author Joachim Zeyn, Peak Solution GmbH
 *
 */
@Tag(name = "Datasource")
@Path("/datasources")
public class DatasourceResource {

	@EJB
	private DatasourceService datasourceService;

	@GET
	@Operation(summary = "Get the available datasources", description = "Get the available services/datasources", responses = {
			@ApiResponse(description = "The datasources", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDatasources() {
		return ServiceUtils.toResponse(datasourceService.readServiceNames(), Status.OK);
	}

	@PUT
	@Operation(summary = "Set the selected datasources", description = "Set the active services/datasources", responses = {
			@ApiResponse(description = "List of service names currently active.", content = @Content(schema = @Schema(implementation = List.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response setDatasources(
			@Parameter(description = "Names of the MDM datasources", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			List<String> activeServices) {

		this.datasourceService.setActiveServices(activeServices);
		return ServiceUtils.toResponse(datasourceService.readServiceNames(), Status.OK);
	}
}
