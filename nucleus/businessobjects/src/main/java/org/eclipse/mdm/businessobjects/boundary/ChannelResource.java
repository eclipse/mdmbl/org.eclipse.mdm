/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import java.io.IOException;
import java.util.Map;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Quantity;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.entity.I18NResponse;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.entity.SearchAttributeResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.collection.List;
import io.vavr.control.Try;

/**
 * {@link Channel} resource
 *
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@Tag(name = "Channel")
@Path("/environments/{SOURCENAME}/channels")
public class ChannelResource {

	@EJB
	private ChannelService channelService;

	@EJB
	private EntityService entityService;

	@Context
	javax.ws.rs.ext.Providers providers;

	/**
	 * delegates the request to the {@link ChannelService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link Channel} result
	 * @param limit      optional limit the results
	 * @param offset     optional offset where the results begin
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find Channels by filter", description = "Get list of Channels", responses = {
			@ApiResponse(description = "The Channels", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getChannels(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam("SOURCENAME") String sourceName,
			@Parameter(description = "Filter expression", required = false) @QueryParam("filter") String filter,
			@Parameter(description = "Channel limit", required = false) @QueryParam("limit") String limit,
			@Parameter(description = "Channel offset", required = false) @QueryParam("offset") String offset) {

		java.util.List<Channel> channels = this.channelService.getChannels(sourceName, filter, limit, offset);
		return ServiceUtils.toResponse(new MDMEntityResponse(Channel.class, channels), Status.OK);

	}

	/**
	 * delegates the request to the {@link ChannelService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link Test}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{ID}")
	@Operation(summary = "Find a Channel by ID", description = "Returns Channel based on ID", responses = {
			@ApiResponse(description = "The Channel", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Invalid ID supplied") })
	public Response getChannel(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam("SOURCENAME") String sourceName,
			@Parameter(description = "ID of the Channel", required = true) @PathParam("ID") String id) {

		Channel channel = this.channelService.getChannel(sourceName, id);
		return ServiceUtils.toResponse(new MDMEntityResponse(Channel.class, channel), Status.OK);

	}

	/**
	 * delegates the request to the {@link ChannelService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the search attributes", description = "Get a list of search attributes", responses = {
			@ApiResponse(description = "The search attributes", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchattributes")
	public Response getSearchAttributes(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		return Try.of(() -> channelService.getSearchAttributes(sourceName))
				.map(attrs -> ServiceUtils.toResponse(new SearchAttributeResponse(attrs), Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link ChannelService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Get the Channel localizations", description = "Returns Channel localizations", responses = {
			@ApiResponse(description = "The Channel localizations", content = @Content(schema = @Schema(implementation = I18NResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/localizations")
	@Deprecated
	public Response localize(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam("SOURCENAME") String sourceName) {

		Map<Attribute, String> localizedAttributeMap = this.channelService.localizeAttributes(sourceName);
		Map<EntityType, String> localizedEntityTypeMap = this.channelService.localizeType(sourceName);
		return ServiceUtils.toResponse(new I18NResponse(localizedEntityTypeMap, localizedAttributeMap), Status.OK);
	}

	/**
	 * Returns the created {@link Channel}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link Channel} to create.
	 * @return the created {@link Channel} as {@link Response}.
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Operation(summary = "Create a new Channel / multiple Channels", responses = {
			@ApiResponse(description = "The created Channel(s)", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response create(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam("SOURCENAME") String sourceName,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
					@ExampleObject(name = "Create a single Channel", value = "{\"Name\": \"velocity\", \"Measurement\": \"123\", \"Quantity\": \"9\", \"Unit\": \"1\"}"),
					@ExampleObject(name = "Create multiple Channels", value = "[\n{\"Name\": \"velocity\", \"Measurement\": \"123\", \"Quantity\": \"9\", \"Unit\": \"1\"}, "
							+ "{\"Name\": \"temperature\", \"Measurement\": \"123\", \"Quantity\": \"10\", \"Unit\": \"2\"}\n]") })) String body) {

		if (body != null && body.trim().startsWith("[")) {
			try {
				ContextResolver<ObjectMapper> resolver = providers.getContextResolver(ObjectMapper.class,
						MediaType.APPLICATION_JSON_TYPE);
				ObjectMapper mapper = resolver.getContext(ObjectMapper.class);

				List<Channel> list = List.ofAll(channelService.createChannels(sourceName,
						mapper.readValue(body, new TypeReference<java.util.List<CreateChannel>>() {
						})));

				return ServiceUtils.buildEntityResponse(list, Status.CREATED);
			} catch (IOException e) {
				throw new DataAccessException("Could not create multiple Channels.", e);
			}
		} else {
			return entityService
					.create(V(sourceName), Channel.class,
							entityService.extractRequestBody(body, sourceName,
									List.of(Measurement.class, Quantity.class, Unit.class)))
					.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class CreateChannel {
		@JsonProperty("Name")
		public String name;
		@JsonProperty("Measurement")
		public String measurement;
		@JsonProperty("Quantity")
		public String quantity;
		@JsonProperty("Unit")
		public String unit;
	}

	/**
	 * Updates the {@link Channel} with all parameters set in the given JSON body of
	 * the request.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link Channel} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link Channel}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	@Operation(summary = "Updates the Channel with all parameters set in the body of the request", responses = {
			@ApiResponse(description = "The Channel", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response update(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Channel", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), Channel.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Updates the {@link Channel} with all parameters set in the given JSON body of
	 * the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link Channel} to update.
	 * @param body       the body of the request containing the attributes to
	 *                   update. Example: { "4711": { "Description": "My first
	 *                   channel" }, "4712": { "Description": "My second channel" }
	 *                   }
	 * @return the updated {@link Channel}
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Operation(summary = "Updates the Channels with all parameters set in the body of the request", responses = {
			@ApiResponse(responseCode = "200", description = "Successfully updated Channels", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response updateChannels(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@io.swagger.v3.oas.annotations.parameters.RequestBody(content = @Content(examples = {
					@ExampleObject(name = "Update the description of two elements", value = "{\n" + "	\"4711\": {\n"
							+ "		\"Description\": \"My first channel\"\n" + "	},\n" + "	\"4712\": {\n"
							+ "		\"Description\": \"My second channel\"\n" + "	}\n" + "}") })) String body) {
		RequestBody requestBody = RequestBody.create(body);

		final io.vavr.collection.Map<String, Object> entityIdToAttributesMap = requestBody.getValueMapSupplier().get();
		final List<Channel> updatedEntities = List
				.ofAll(entityService.updateEntitiesAndPersist(Channel.class, entityIdToAttributesMap, V(sourceName)));
		return ServiceUtils.buildEntityResponse(updatedEntities, Status.OK);
	}

	/**
	 * Deletes and returns the deleted {@link Channel}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link Channel} to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	@Operation(summary = "Delete an existing Channel", responses = {
			@ApiResponse(description = "The deleted Channel", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	public Response delete(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the Channel", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), Channel.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}
}
