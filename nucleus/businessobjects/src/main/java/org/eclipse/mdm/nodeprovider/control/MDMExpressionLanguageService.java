/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.nodeprovider.control;

import java.lang.reflect.Method;

import javax.el.ELException;
import javax.el.ELManager;
import javax.el.ELProcessor;
import javax.el.ExpressionFactory;
import javax.el.StandardELContext;
import javax.el.ValueExpression;

import org.eclipse.mdm.nodeprovider.entity.AttributeContainerListResovler;
import org.eclipse.mdm.nodeprovider.utils.ExpressionLanguageMethodProvider;
import org.eclipse.mdm.nodeprovider.utils.LegacyExpressionLanguageMethodProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MDMExpressionLanguageService {

	private static final Logger LOG = LoggerFactory.getLogger(MDMExpressionLanguageService.class);

	private final ELProcessor processor;
	private final ExpressionFactory factory;

	/**
	 * Intentionally empty
	 */
	public MDMExpressionLanguageService() {
		processor = new ELProcessor();
		factory = ELManager.getExpressionFactory();
		init();
	}

	/**
	 * Initialize functionMapper and resolvers for expression language
	 * interpretation.
	 */
	public void init() {
		ELManager manager = processor.getELManager();
		manager.addELResolver(new AttributeContainerListResovler());

		for (Method method : ExpressionLanguageMethodProvider.class.getDeclaredMethods()) {
			try {
				processor.defineFunction("mdm", "", method);
			} catch (NoSuchMethodException e) {
				LOG.warn("Could not add method: {}", method);
			}
		}

		for (Method method : LegacyExpressionLanguageMethodProvider.class.getDeclaredMethods()) {
			try {
				processor.defineFunction("fn", "", method);
			} catch (NoSuchMethodException e) {
				LOG.warn("Could not add method: {}", method);
			}
		}
	}

	/**
	 * Parses {@link String} to a {@link ValueExpression}.
	 *
	 * @param expressionString the value expression
	 * @return the value expression, or null if none present
	 */
	public ValueExpression parseValueExpression(String expressionString, Class<?> expectedType) {
		StandardELContext elContext = processor.getELManager().getELContext();
		ValueExpression valueExpression = null;
		if (expressionString != null) {
			valueExpression = factory.createValueExpression(elContext, expressionString, expectedType);
		}
		return valueExpression;
	}

	/**
	 * Evaluates the {@link ValueExpression} with respect to given properties.
	 * 
	 * @param valueExpression the value expression
	 * @param properties      the properties to be inserted
	 * @return
	 */
	public Object evaluateValueExpression(ValueExpression valueExpression, Object... properties) {
		StandardELContext context = processor.getELManager().getELContext();
		for (Object prop : properties) {
			context.putContext(prop.getClass(), prop);
		}
		try {
			return valueExpression.getValue(context);
		} catch (NullPointerException | ELException exception) {
			LOG.error("Could not retrieve value from given EL context", exception);
			return null;
		}
	}
}
