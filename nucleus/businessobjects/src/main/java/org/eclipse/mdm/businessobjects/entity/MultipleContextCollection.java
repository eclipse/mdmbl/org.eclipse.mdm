/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mdm.api.base.model.ContextComponent;
import org.eclipse.mdm.api.base.model.ContextRoot;
import org.eclipse.mdm.api.base.model.ContextType;

/**
 * MultopleContextCollection (Entity for single context data)
 * 
 * @author Alexander Knoblauch, peakSolution GmbH
 *
 */
public class MultipleContextCollection {

	public MDMEntityResponse entity;

	public Map<ContextType, List<MDMContextEntity>> context = new HashMap<>();

	/**
	 * sets the context data map
	 * 
	 * @param contextMap the context data map
	 */
	public void setContext(Map<ContextType, ContextRoot> contextMap) {

		for (java.util.Map.Entry<ContextType, ContextRoot> setEntry : contextMap.entrySet()) {

			ContextType contextType = setEntry.getKey();
			ContextRoot contextRoot = setEntry.getValue();

			this.context.put(contextType, new ArrayList<>());

			for (ContextComponent contextComponent : contextRoot.getContextComponents()) {
				MDMContextEntity entity = new MDMContextEntity(contextComponent);
				this.context.get(contextType).add(entity);
			}

			// sort by SortIndex
			Collections.sort(this.context.get(contextType), Comparator.comparing(MDMContextEntity::getSortIndex,
					Comparator.nullsFirst(Comparator.naturalOrder())));
		}
	}

	/**
	 * @param entity the entity to set
	 */
	public void setEntity(MDMEntityResponse entity) {
		this.entity = entity;
	}

}
