/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.massdata.AnyTypeValuesBuilder;
import org.eclipse.mdm.api.base.massdata.ChannelValues;
import org.eclipse.mdm.api.base.massdata.ComplexNumericalValuesBuilder;
import org.eclipse.mdm.api.base.massdata.IndependentBuilder;
import org.eclipse.mdm.api.base.massdata.ReadRequest;
import org.eclipse.mdm.api.base.massdata.UnitBuilder;
import org.eclipse.mdm.api.base.massdata.UnitIndependentBuilder;
import org.eclipse.mdm.api.base.massdata.UpdateRequest;
import org.eclipse.mdm.api.base.massdata.ValuesFilter;
import org.eclipse.mdm.api.base.massdata.WriteRequest;
import org.eclipse.mdm.api.base.massdata.WriteRequestBuilder;
import org.eclipse.mdm.api.base.massdata.WriteRequestFinalizer;
import org.eclipse.mdm.api.base.model.AxisType;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.DoubleComplex;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.FloatComplex;
import org.eclipse.mdm.api.base.model.MeasuredValues;
import org.eclipse.mdm.api.base.model.SequenceRepresentation;
import org.eclipse.mdm.api.base.model.Unit;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.search.SearchService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.businessobjects.control.FilterParser;
import org.eclipse.mdm.businessobjects.control.ValuesFilterParser;
import org.eclipse.mdm.businessobjects.entity.FlagMode;
import org.eclipse.mdm.businessobjects.utils.PreviewHelper;
import org.eclipse.mdm.businessobjects.utils.ProtobufConverter;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.protobuf.Mdm;
import org.eclipse.mdm.protobuf.Mdm.ChannelValuesList;
import org.eclipse.mdm.protobuf.Mdm.PreviewRequest;
import org.eclipse.mdm.protobuf.Mdm.PreviewValuesList;
import org.eclipse.mdm.protobuf.Mdm.ReadChannelValuesRequest;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList.WriteRequest.ExplicitData;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList.WriteRequest.ImplicitConstant;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList.WriteRequest.ImplicitLinear;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList.WriteRequest.ImplicitSaw;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList.WriteRequest.RawLinear;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList.WriteRequest.RawLinearCalibrated;
import org.eclipse.mdm.protobuf.Mdm.WriteRequestList.WriteRequest.RawPolynomial;
import org.eclipse.mdm.query.boundary.QueryService;
import org.eclipse.mdm.query.entity.Column;
import org.eclipse.mdm.query.entity.QueryRequest;
import org.eclipse.mdm.query.entity.QueryResult;
import org.eclipse.mdm.query.entity.SourceFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.primitives.Booleans;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Floats;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import com.google.common.primitives.Shorts;

@Stateless
public class ValuesService {

	private static final Logger LOG = LoggerFactory.getLogger(ValuesService.class);

	@Inject
	private QueryService querService;

	@Inject
	private ConnectorService connectorService;

	/**
	 * Loads measured values for multiple Channels specified by
	 * {@link ReadChannelValuesRequest}.
	 * 
	 * @param sourceName       name of the source (MDM {@link Environment} name)
	 * @param protoReadRequest Protobuf {@link ReadChannelValuesRequest}.
	 * @param flagMode         Enum describing the kind of flags requested
	 * @return the loaded ChannelValuesList
	 */
	public ChannelValuesList loadChannelValues(String sourceName, Mdm.ReadChannelValuesRequest protoReadRequest,
			FlagMode flagMode) {
		ApplicationContext context = connectorService.getContextByName(sourceName);

		ModelManager mm = context.getModelManager()
				.orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));

		SearchService search = context.getSearchService()
				.orElseThrow(() -> new ServiceNotProvidedException(SearchService.class));

		Filter filter = FilterParser.parseFilterString(mm.listEntityTypes(), protoReadRequest.getFilter());
		ValuesFilter valuesFilter = ValuesFilterParser.parseFilterString(protoReadRequest.getValuesFilter());

		Map<String, Collection<ChannelValues>> result = search.fetchValueResults(filter, "", valuesFilter, 0,
				protoReadRequest.getResultLimit());

		ChannelValuesList.Builder builder = ChannelValuesList.newBuilder();

		result.values().forEach(e -> builder.addAllValues(ProtobufConverter.convert(e, flagMode)));

		return builder.build();
	}

	/**
	 * Loads multiple MeasuredValues as specified in ReadRequest.
	 * 
	 * @param sourceName       name of the source (MDM {@link Environment} name)
	 * @param protoReadRequest Protobuf ReadRequest
	 * @param flagMode         Enum describing the kind of flags requested
	 * @return the loaded MeasuredValuesList
	 */
	public Mdm.MeasuredValuesList load(String sourceName, Mdm.ReadRequest protoReadRequest, FlagMode flagMode) {

		ApplicationContext context = connectorService.getContextByName(sourceName);

		ReadRequest readRequest = ProtobufConverter.convert(context, protoReadRequest);

		List<MeasuredValues> measuredValues = context.getEntityManager().get().readMeasuredValues(readRequest);

		return ProtobufConverter.convert(measuredValues, flagMode);
	}

	/**
	 * Loads preview values for multiple channels as specified in
	 * {@link PreviewRequest}. The preview consists of minimum (min), maximum (max)
	 * and average (avg) values for each chunk of values. The number of chunks must
	 * be given in {@link PreviewRequest}.
	 * 
	 * @param sourceName       name of the source (MDM {@link Environment} name)
	 * @param protoReadRequest Protobuf {@link PreviewRequest}
	 * @return the loaded PreviewValuesList
	 */
	public PreviewValuesList preview(String sourceName, Mdm.PreviewRequest previewRequest) {
		ApplicationContext context = connectorService.getContextByName(sourceName);

		ReadRequest readRequest = ProtobufConverter.convert(context, previewRequest.getReadRequest());

		List<Mdm.MeasuredValues> measuredValues = ProtobufConverter
				.convert(context.getEntityManager().get().readMeasuredValues(readRequest), FlagMode.BOOLEAN)
				.getValuesList();

		// Calculate Preview
		return new PreviewHelper().calculatePreview(measuredValues, previewRequest.getNumberOfChunks());
	}

	/**
	 * Writes values for multiple Channels.
	 * 
	 * @param sourceName            name of the source (MDM {@link Environment}
	 *                              name)
	 * @param protoWriteRequestList Protobuf {@link WriteRequest}
	 */
	public void write(String sourceName, Mdm.WriteRequestList protoWriteRequestList) {
		ApplicationContext context = connectorService.getContextByName(sourceName);
		EntityManager em = context.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));

		Supplier<ZoneId> zoneId = Suppliers
				.memoize(() -> getServerZoneId(context.getEntityManager().get().loadEnvironment()));

		List<String> channelGroupIds = protoWriteRequestList.getValuesList().stream().map(v -> v.getChannelGroupId())
				.distinct().collect(Collectors.toList());
		Map<String, List<ChannelGroup>> channelGroups = em.load(ChannelGroup.class, channelGroupIds).stream()
				.collect(Collectors.groupingBy(m -> m.getID()));

		List<String> channelIds = protoWriteRequestList.getValuesList().stream().map(v -> v.getChannelId()).distinct()
				.collect(Collectors.toList());
		Map<String, List<Channel>> channels = em.load(Channel.class, channelIds).stream()
				.collect(Collectors.groupingBy(m -> m.getID()));

		List<WriteRequest> writeRequests = new ArrayList<>();
		for (Mdm.WriteRequestList.WriteRequest r : protoWriteRequestList.getValuesList()) {
			writeRequests.add(convert(context, r, zoneId, channelGroups, channels));
		}

		Transaction t = context.getEntityManager().get().startTransaction();
		t.writeMeasuredValues(writeRequests);
		t.commit();
	}

	/**
	 * Converts a Protobuf to a api.base WritRequest.
	 * 
	 * @param context           {@link ApplicationContext} to write to.
	 * @param protoWriteRequest Data to write.
	 * @param zoneId            A supplier for a {@link ZoneId} which will be used,
	 *                          if Data with DT_DATE is written.
	 * @param channels
	 * @param channelGroups
	 * @return converted {@link WriteRequest}
	 */
	private WriteRequest convert(ApplicationContext context, Mdm.WriteRequestList.WriteRequest protoWriteRequest,
			Supplier<ZoneId> zoneId, Map<String, List<ChannelGroup>> channelGroups,
			Map<String, List<Channel>> channels) {

		List<ChannelGroup> channelGroup = channelGroups.get(protoWriteRequest.getChannelGroupId());
		if (channelGroup == null || channelGroup.isEmpty()) {
			throw new DataAccessException("No ChannelGroup with ID " + protoWriteRequest.getChannelGroupId()
					+ " found in source " + context.getSourceName());
		}

		List<Channel> channel = channels.get(protoWriteRequest.getChannelId());
		if (channel == null || channel.isEmpty()) {
			throw new DataAccessException("No Channel with ID " + protoWriteRequest.getChannelId() + " found in source "
					+ context.getSourceName());
		}

		AxisType axisType = ProtobufConverter.convert(protoWriteRequest.getAxisType());

		WriteRequestBuilder rb = WriteRequest.create(channelGroup.get(0), channel.get(0), axisType);

		if (!Strings.isNullOrEmpty(protoWriteRequest.getMimeType())) {
			rb = rb.setMimeType(protoWriteRequest.getMimeType());
		}

		WriteRequestFinalizer wrf;
		switch (protoWriteRequest.getDataCase()) {
		case EXPLICIT:
			wrf = setValues(rb.explicit(), protoWriteRequest.getExplicit(), zoneId);
			break;
		case IMPLICIT_CONSTANT:
			ImplicitConstant ic = protoWriteRequest.getImplicitConstant();
			wrf = rb.implicitConstant(ProtobufConverter.convert(ic.getScalarType()), ic.getOffset());
			break;
		case IMPLICIT_LINEAR:
			ImplicitLinear il = protoWriteRequest.getImplicitLinear();
			wrf = rb.implicitLinear(ProtobufConverter.convert(il.getScalarType()), il.getStart(), il.getIncrement());
			break;
		case IMPLICIT_SAW:
			ImplicitSaw is = protoWriteRequest.getImplicitSaw();
			wrf = rb.implicitSaw(ProtobufConverter.convert(is.getScalarType()), is.getStart(), is.getIncrement(),
					is.getStop());
			break;
		case RAW_LINEAR:
			RawLinear rl = protoWriteRequest.getRawLinear();
			wrf = setValues(rb.rawLinear(rl.getOffset(), rl.getFactor()), rl.getValues(), zoneId);
			break;
		case RAW_LINEAR_CALIBRATED:
			RawLinearCalibrated rlc = protoWriteRequest.getRawLinearCalibrated();
			wrf = setValues(rb.rawLinearCalibrated(rlc.getOffset(), rlc.getFactor(), rlc.getCalibration()),
					rlc.getValues(), zoneId);
			break;
		case RAW_POLYNOMIAL:
			RawPolynomial rp = protoWriteRequest.getRawPolynomial();
			wrf = setValues(rb.rawPolynomial(Doubles.toArray(rp.getCoefficientsList())), rp.getValues(), zoneId);
			break;
		case DATA_NOT_SET:
		default:
			throw new RuntimeException("Not supported yet: " + protoWriteRequest.getDataCase());

		}

		if (!Strings.isNullOrEmpty(protoWriteRequest.getSourceUnitId())) {

			Unit unit = context.getEntityManager().get().load(Unit.class, protoWriteRequest.getSourceUnitId());

			if (wrf instanceof UnitBuilder) {
				wrf = ((UnitBuilder) wrf).sourceUnit(unit);
			} else if (wrf instanceof UnitIndependentBuilder) {
				wrf = ((UnitIndependentBuilder) wrf).sourceUnit(unit);
			} else {
				throw new DataAccessException("Cannot set source unit on " + wrf.getClass().getName() + "!");
			}
		}

		if (wrf instanceof IndependentBuilder) {
			wrf = ((IndependentBuilder) wrf).independent(protoWriteRequest.getIndependent());
		}
		return wrf.build();

	}

	/**
	 * Helper function to set values for the appropriate datatype in an
	 * {@link AnyTypeValuesBuilder}
	 * 
	 * @param builder
	 * @param explicitData
	 * @param zoneId
	 * @return {@link WriteRequestFinalizer}
	 */
	private WriteRequestFinalizer setValues(AnyTypeValuesBuilder builder, ExplicitData explicitData,
			Supplier<ZoneId> zoneId) {
		short[] flags = getFlags(Shorts.toArray(explicitData.getFlagsFullList()),
				Booleans.toArray(explicitData.getFlagsList()));
		switch (explicitData.getValuesCase()) {
		case STRING_ARRAY:
			String[] strings = ProtobufConverter.convertStrings(explicitData.getStringArray());
			return (flags.length > 0) ? builder.stringValues(strings, flags) : builder.stringValues(strings);
		case DATE_ARRAY:
			Instant[] dates = ProtobufConverter.convertDates(explicitData.getDateArray(), zoneId.get());
			return (flags.length > 0) ? builder.dateValues(dates, flags) : builder.dateValues(dates);
		case BOOLEAN_ARRAY:
			boolean[] booleans = Booleans.toArray(explicitData.getBooleanArray().getValuesList());
			return (flags.length > 0) ? builder.booleanValues(booleans, flags) : builder.booleanValues(booleans);
		case BYTE_STREAM_ARRAY:
			byte[][] byteStreams = ProtobufConverter
					.convertByteStreams(explicitData.getByteStreamArray().getValuesList());
			return (flags.length > 0) ? builder.byteStreamValues(byteStreams, flags)
					: builder.byteStreamValues(byteStreams);
		case BYTE_ARRAY:
		case SHORT_ARRAY:
		case INTEGER_ARRAY:
		case LONG_ARRAY:
		case FLOAT_ARRAY:
		case DOUBLE_ARRAY:
		case FLOAT_COMPLEX_ARRAY:
		case DOUBLE_COMPLEX_ARRAY:
			return setValues((ComplexNumericalValuesBuilder) builder, explicitData, zoneId);
		case VALUES_NOT_SET:
		default:
			throw new RuntimeException("No explicit data set!");
		}
	}

	/**
	 * Helper function to set values for the appropriate datatype in a
	 * {@link ComplexNumericalValuesBuilder}.
	 * 
	 * @param builder
	 * @param explicitData
	 * @param zoneId
	 * @return {@link WriteRequestFinalizer}
	 */
	private WriteRequestFinalizer setValues(ComplexNumericalValuesBuilder builder, ExplicitData explicitData,
			Supplier<ZoneId> zoneId) {
		short[] flags = getFlags(Shorts.toArray(explicitData.getFlagsFullList()),
				Booleans.toArray(explicitData.getFlagsList()));
		switch (explicitData.getValuesCase()) {
		case STRING_ARRAY:
		case DATE_ARRAY:
		case BOOLEAN_ARRAY:
		case BYTE_STREAM_ARRAY:
			throw new RuntimeException(
					explicitData.getValuesCase() + " not supported by ComplexNumericalValuesBuilder!");
		case BYTE_ARRAY:
			byte[] bytes = explicitData.getByteArray().getValues().toByteArray();
			return (flags.length > 0) ? builder.byteValues(bytes, flags) : builder.byteValues(bytes);
		case SHORT_ARRAY:
			short[] shorts = Shorts.toArray(explicitData.getShortArray().getValuesList());
			return (flags.length > 0) ? builder.shortValues(shorts, flags) : builder.shortValues(shorts);
		case INTEGER_ARRAY:
			int[] ints = Ints.toArray(explicitData.getIntegerArray().getValuesList());
			return (flags.length > 0) ? builder.integerValues(ints, flags) : builder.integerValues(ints);
		case LONG_ARRAY:
			long[] longs = Longs.toArray(explicitData.getLongArray().getValuesList());
			return (flags.length > 0) ? builder.longValues(longs, flags) : builder.longValues(longs);
		case FLOAT_ARRAY:
			float[] floats = Floats.toArray(explicitData.getFloatArray().getValuesList());
			return (flags.length > 0) ? builder.floatValues(floats, flags) : builder.floatValues(floats);
		case DOUBLE_ARRAY:
			double[] doubles = Doubles.toArray(explicitData.getDoubleArray().getValuesList());
			return (flags.length > 0) ? builder.doubleValues(doubles, flags) : builder.doubleValues(doubles);
		case FLOAT_COMPLEX_ARRAY:
			FloatComplex[] floatComplexes = ProtobufConverter
					.convertFloatComplex(explicitData.getFloatComplexArray().getValuesList());
			return (flags.length > 0) ? builder.floatComplexValues(floatComplexes, flags)
					: builder.floatComplexValues(floatComplexes);
		case DOUBLE_COMPLEX_ARRAY:
			DoubleComplex[] doubleComplexes = ProtobufConverter
					.convertDoubleComplex(explicitData.getDoubleComplexArray().getValuesList());
			return (flags.length > 0) ? builder.doubleComplexValues(doubleComplexes, flags)
					: builder.doubleComplexValues(doubleComplexes);
		case VALUES_NOT_SET:
		default:
			throw new RuntimeException("No explicit data set!");
		}
	}

	/**
	 * Returns the ZonId configured in the {@link Environment}
	 * 
	 * @param env {@link Environment} for extracting the ZoneId
	 * @return ZonId configured in the {@link Environment}
	 */
	private ZoneId getServerZoneId(Environment env) {
		String timezone = env.getTimezone();
		try {
			return ZoneId.of(timezone);
		} catch (Exception e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Timezone '" + timezone + "' of Environment '" + env.getName() + "' is invalid!", e);
			} else {
				LOG.warn("Timezone '" + timezone + "' of Environment '" + env.getName() + "' is invalid!");
			}
			return ZoneId.systemDefault();
		}
	}

	/**
	 * Append values for multiple Channels.
	 * 
	 * @param sourceName            name of the source (MDM {@link Environment}
	 *                              name)
	 * @param protoWriteRequestList Protobuf {@link WriteRequest}
	 */
	public void append(String sourceName, WriteRequestList protoWriteRequestList) {
		ApplicationContext context = connectorService.getContextByName(sourceName);
		EntityManager em = context.getEntityManager()
				.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
		Supplier<ZoneId> zoneId = Suppliers
				.memoize(() -> getServerZoneId(context.getEntityManager().get().loadEnvironment()));

		List<String> channelGroupIds = protoWriteRequestList.getValuesList().stream().map(v -> v.getChannelGroupId())
				.distinct().collect(Collectors.toList());
		Map<String, List<ChannelGroup>> channelGroups = em.load(ChannelGroup.class, channelGroupIds).stream()
				.collect(Collectors.groupingBy(m -> m.getID()));

		List<String> channelIds = protoWriteRequestList.getValuesList().stream().map(v -> v.getChannelId()).distinct()
				.collect(Collectors.toList());
		Map<String, List<Channel>> channels = em.load(Channel.class, channelIds).stream()
				.collect(Collectors.groupingBy(m -> m.getID()));

		List<WriteRequest> writeRequests = new ArrayList<>();
		for (Mdm.WriteRequestList.WriteRequest r : protoWriteRequestList.getValuesList()) {
			WriteRequest writeRequest = convert(context, r, zoneId, channelGroups, channels);

			if (!SequenceRepresentation.EXPLICIT.equals(writeRequest.getSequenceRepresentation())) {
				throw new DataAccessException("Appending only with SequenceRepresentation explicitit possible!");
			}
			writeRequests.add(writeRequest);
		}

		Transaction t = context.getEntityManager().get().startTransaction();
		t.appendMeasuredValues(writeRequests);
		t.commit();
	}

	/**
	 * Update values for Channel.
	 * 
	 * @param sourceName         name of the source (MDM {@link Environment} name)
	 * @param protoUpdateRequest Protobuf {@link UpdateRequest}
	 */
	public void update(String sourceName, Mdm.UpdateRequest protoUpdateRequest) {
		ApplicationContext context = connectorService.getContextByName(sourceName);

		Transaction t = context.getEntityManager().get().startTransaction();
		t.updateMeasuredValues(Arrays.asList(convert(context, protoUpdateRequest)));
		t.commit();
	}

	/**
	 * Converts a Protobuf to a api.base WritRequest.
	 * 
	 * @param context           {@link ApplicationContext} to write to.
	 * @param protoWriteRequest Data to write.
	 * @param zoneId            A supplier for a {@link ZoneId} which will be used,
	 *                          if Data with DT_DATE is written.
	 * @return converted {@link WriteRequest}
	 */
	private UpdateRequest convert(ApplicationContext context, Mdm.UpdateRequest protoUpdateRequest) {
		EntityManager em = context.getEntityManager().get();

		ChannelGroup channelGroup = em.load(ChannelGroup.class, protoUpdateRequest.getChannelGroupId());
		Channel channel = em.load(Channel.class, protoUpdateRequest.getChannelId());
		AxisType axisType = ProtobufConverter.convert(protoUpdateRequest.getAxisType());

		Map<String, Object> result = this.getIdAndNoRows(context.getSourceName(), channelGroup.getID(),
				channel.getID());

		int numberOfRows = getNumber(result, "ChannelGroup.SubMatrixNoRows").intValue();

		short[] flags = getFlags(Shorts.toArray(protoUpdateRequest.getFlagsFullList()),
				Booleans.toArray(protoUpdateRequest.getFlagsList()));
		if (flags != null && flags.length > 0 && flags.length != numberOfRows) {
			throw new RuntimeException("The size of the flag list must be equal to the number of rows! Was: "
					+ flags.length + " expected:" + numberOfRows);
		}

		UpdateRequest updateRequest = null;
		if (!result.isEmpty()) {
			updateRequest = new UpdateRequest(channelGroup, channel, Objects.toString(result.get("LocalColumn.Id")));
			updateRequest.setAxisType(axisType);
			updateRequest.setFlags(flags);
			updateRequest.setGlobalFlag(protoUpdateRequest.getGlobalFlag() ? (short) 15 : (short) 0);
			updateRequest.setIndependent(protoUpdateRequest.getIndependent());
			updateRequest.setMimeType(protoUpdateRequest.getMimeType());
		}
		return updateRequest;
	}

	private Number getNumber(Map<String, Object> result, String name) {
		Object valueObj = result.get(name);
		if (!(valueObj instanceof Number)) {
			throw new DataAccessException("Value for " + name + " is not a number. Received: "
					+ (valueObj == null ? "null" : valueObj.getClass()));
		}
		return ((Number) result.get(name)).longValue();
	}

	private Map<String, Object> getIdAndNoRows(String sourceName, String channelGroupId, String channelId) {

		SourceFilter filter = new SourceFilter();
		filter.setSourceName(sourceName);
		filter.setFilter("(ChannelGroup.Id eq " + channelGroupId + " and Channel.Id eq " + channelId + ")");
		filter.setSearchString("");

		QueryRequest query = new QueryRequest();
		query.setFilters(Arrays.asList(filter));
		query.setColumns(
				Arrays.asList("ChannelGroup.Id", "ChannelGroup.SubMatrixNoRows", "Channel.Id", "LocalColumn.Id"));
		query.setResultType("Channel");
		query.setResultOffset(0);
		query.setResultLimit(0);
		QueryResult result = this.querService.queryRows(query);

		Map<String, Object> map = new HashMap<>();
		if (result.getRows().size() == 1) {
			for (Column column : result.getRows().get(0).getColumns()) {
				map.put(column.getType() + "." + column.getAttribute(), column.getValue());
			}
		}
		return map;
	}

	private short[] getFlags(short[] flagsShort, boolean[] flagsBoolean) {

		short[] flagsRet = flagsShort;
		if ((flagsShort == null || flagsShort.length == 0) && (flagsBoolean != null && flagsBoolean.length > 0)) {
			final short[] flagsNew = new short[flagsBoolean.length];
			IntStream.range(0, flagsBoolean.length).forEach(i -> flagsNew[i] = (short) (flagsBoolean[i] ? 15 : 0));
			flagsRet = flagsNew;
		}

		return flagsRet;
	}
}
