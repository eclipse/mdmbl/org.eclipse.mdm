/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_ID;
import static org.eclipse.mdm.businessobjects.boundary.ResourceConstants.REQUESTPARAM_SOURCENAME;
import static org.eclipse.mdm.businessobjects.service.EntityService.V;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Tagable;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.dflt.model.MDMTag;
import org.eclipse.mdm.api.dflt.model.ValueList;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.entity.MDMEntity;
import org.eclipse.mdm.businessobjects.entity.MDMEntityResponse;
import org.eclipse.mdm.businessobjects.service.EntityService;
import org.eclipse.mdm.businessobjects.utils.RequestBody;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.vavr.Value;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Try;

/**
 * {@link MDMEntity} REST interface for MDMTags
 * 
 * @author Alexander Knoblauch, Peak Solution GmbH
 *
 */
@Tag(name = "MDMTag")
@Path("/environments/{" + REQUESTPARAM_SOURCENAME + "}/mdmtags")
public class MDMTagResource {

	@EJB
	private MDMTagService mdmTagService;

	@EJB
	private EntityService entityService;

	/**
	 * delegates the request to the {@link MDMTagService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the ExtSystem result
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find MDMTags by filter", description = "Get list of MDMTags", responses = {
			@ApiResponse(description = "The MDMTag", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMDMTags(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName) {

		return Try.of(() -> mdmTagService.getMDMTags(sourceName)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, MDMTag.class)).get();
	}

	/**
	 * delegates the request to the {@link ExtSystemService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         id of the {@link MDMEntity}
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find a MDMTag by ID", description = "Returns MDMTag based on ID", responses = {
			@ApiResponse(description = "The MDMTag", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response findMDMTag(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the MDMTag", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.find(V(sourceName), MDMTag.class, V(id))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Returns the created {@link MDMEntity}.
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param body       The {@link MDMEntity} to create.
	 * @return the created {@link MDMEntity} as {@link Response}.
	 */
	@POST
	@Operation(summary = "Create a new MDMTag", responses = {
			@ApiResponse(description = "The created MDMTag", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "500", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createMDMTag(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			String body) {

		return entityService.create(V(sourceName), MDMTag.class, entityService.extractRequestBody(body, sourceName))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.CREATED)).get();
	}

	/**
	 * Updates the {@link MDMEntity} with all parameters set in the given JSON body
	 * of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MDMEntity} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link MDMEntity}
	 */
	@PUT
	@Operation(summary = "Add an entity to the tag", description = "Updates the MDMTag with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated MDMTag", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/tag")
	public Response tagEntity(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the MDMTag", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {

		Seq<Value<?>> extractRequestBody = entityService.extractRequestBody(body, sourceName,
				io.vavr.collection.List.of(Test.class, TestStep.class, Measurement.class));

		Tagable tagable = getTagableEntity(extractRequestBody);

		return Try.of(() -> mdmTagService.tagEntity(sourceName, id, tagable)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, Tagable.class)).get();
	}

	/**
	 * Updates the {@link MDMEntity} with all parameters set in the given JSON body
	 * of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MDMEntity} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link MDMEntity}
	 */
	@PUT
	@Operation(summary = "Remove an entity of the tag", description = "Updates the MDMTag with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated MDMTag", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/remove")
	public Response removeTagEntity(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the MDMTag", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {

		Seq<Value<?>> extractRequestBody = entityService.extractRequestBody(body, sourceName,
				io.vavr.collection.List.of(Test.class, TestStep.class, Measurement.class));

		Tagable tagable = getTagableEntity(extractRequestBody);

		return Try.of(() -> mdmTagService.removeEntity(sourceName, id, tagable)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, Tagable.class)).get();
	}

	private Tagable getTagableEntity(Seq<Value<?>> extractRequestBody) {
		Tagable returnVal = null;

		for (Value<?> v : extractRequestBody) {
			if (!v.isEmpty()) {
				Object o = v.getOrNull();

				if (o != null && o instanceof Test) {
					returnVal = (Test) o;
				} else if (o != null && o instanceof TestStep) {
					returnVal = (TestStep) o;
				} else if (o != null && o instanceof Measurement) {
					returnVal = (Measurement) o;
				}
			}
		}

		if (returnVal == null) {
			throw new MDMEntityAccessException("No tagable entity found in Request!");
		}

		return returnVal;
	}

	/**
	 * Updates the {@link MDMEntity} with all parameters set in the given JSON body
	 * of the request.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         the identifier of the {@link MDMEntity} to update.
	 * @param body       the body of the request containing the attributes to update
	 * @return the updated {@link MDMEntity}
	 */
	@PUT
	@Operation(summary = "Update an existing MDMTag", description = "Updates the MDMTag with all parameters set in the body of the request.", responses = {
			@ApiResponse(description = "The updated MDMTag", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response updateMDMTag(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the MDMTag", required = true) @PathParam(REQUESTPARAM_ID) String id,
			String body) {
		RequestBody requestBody = RequestBody.create(body);

		return entityService
				.update(V(sourceName), entityService.find(V(sourceName), MDMTag.class, V(id)),
						requestBody.getValueMapSupplier())
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * Deletes and returns the deleted {@link MDMEntity}.
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param id         The identifier of the {@link MDMEntity} to delete.
	 * @return the deleted {@link ValueList }s as {@link Response}
	 */
	@DELETE
	@Operation(summary = "Delete an existing MDMTag", responses = {
			@ApiResponse(description = "The deleted MDMTag", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Invalid ID supplied") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}")
	public Response deleteMDMTag(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the ExtSystem", required = true) @PathParam(REQUESTPARAM_ID) String id) {
		return entityService.delete(V(sourceName), entityService.find(V(sourceName), MDMTag.class, V(id)))
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK)).get();
	}

	/**
	 * delegates the request to the {@link MDMTagService}
	 *
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the ExtSystem result
	 * @return the result of the delegated request as {@link Response}
	 */
	@GET
	@Operation(summary = "Find Taged Items", description = "Get list of Taged Entities", responses = {
			@ApiResponse(description = "The Taged Items", content = @Content(schema = @Schema(implementation = MDMEntityResponse.class))),
			@ApiResponse(responseCode = "400", description = "Error") })
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{" + REQUESTPARAM_ID + "}/taged")
	public Response getTagedEntities(
			@Parameter(description = "Name of the MDM datasource", required = true) @PathParam(REQUESTPARAM_SOURCENAME) String sourceName,
			@Parameter(description = "ID of the MDMTag", required = true) @PathParam(REQUESTPARAM_ID) String id) {

		return Try.of(() -> mdmTagService.getTagedEntities(sourceName, id)).map(List::ofAll)
				.map(e -> ServiceUtils.buildEntityResponse(e, Status.OK, Tagable.class)).get();
	}

}
