/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.businessobjects.boundary;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.Transaction;
import org.eclipse.mdm.api.base.adapter.Attribute;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Environment;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.Condition;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.query.QueryService;
import org.eclipse.mdm.api.base.query.Record;
import org.eclipse.mdm.api.base.query.Result;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Classification;
import org.eclipse.mdm.api.dflt.model.Domain;
import org.eclipse.mdm.api.dflt.model.EntityFactory;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.ProjectDomain;
import org.eclipse.mdm.api.dflt.model.Status;
import org.eclipse.mdm.businessobjects.control.I18NActivity;
import org.eclipse.mdm.businessobjects.control.MDMEntityAccessException;
import org.eclipse.mdm.businessobjects.control.NavigationActivity;
import org.eclipse.mdm.businessobjects.control.SearchActivity;
import org.eclipse.mdm.businessobjects.entity.SearchAttribute;
import org.eclipse.mdm.businessobjects.utils.ServiceUtils;
import org.eclipse.mdm.connector.boundary.ConnectorService;

import com.google.common.collect.Lists;

/**
 * TestService Bean implementation with available {@link Test} operations
 * 
 * @author Sebastian Dirsch, Gigatronik Ingolstadt GmbH
 *
 */
@Stateless
public class TestService {

	@Inject
	private ConnectorService connectorService;
	@EJB
	private I18NActivity i18nActivity;
	@EJB
	private SearchActivity searchActivity;
	@EJB
	private NavigationActivity navigationActivity;

	/**
	 * returns the matching {@link Test}s using the given filter or all
	 * {@link Test}s if no filter is available
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param filter     filter string to filter the {@link Test} result
	 * @return the found {@link Test}s
	 */
	public List<Test> getTests(String sourceName, String filter) {

		try {
			ApplicationContext context = this.connectorService.getContextByName(sourceName);
			EntityManager em = context.getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));

			if (filter == null || filter.trim().length() <= 0) {
				return em.loadAll(Test.class);
			}

			if (ServiceUtils.isParentFilter(context, filter, Pool.class)) {
				String id = ServiceUtils.extactIdFromParentFilter(context, filter, Pool.class);
				return this.navigationActivity.getTests(sourceName, id);
			}

			return this.searchActivity.search(context, Test.class, filter);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * Returns the {@link SearchAttribute} for the entity type Test in the given
	 * data source.
	 * 
	 * @param sourceName The name of the data source.
	 * @return the found {@link SearchAttribute}s
	 */
	public List<SearchAttribute> getSearchAttributes(String sourceName) {
		return this.searchActivity.listAvailableAttributes(this.connectorService.getContextByName(sourceName),
				Test.class);
	}

	/**
	 * returns a {@link Test} identified by the given id.
	 * 
	 * @param testId     id of the {@link Test}
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @param testStepId id of the {@link Test}
	 * @return the matching {@link Test}
	 */
	public Test getTest(String sourceName, String testId) {
		try {
			EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
					.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
			return em.load(Test.class, testId);
		} catch (DataAccessException e) {
			throw new MDMEntityAccessException(e.getMessage(), e);
		}
	}

	/**
	 * returns localized {@link Test} attributes
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the localized {@link Test} attributes
	 */
	@Deprecated
	public Map<Attribute, String> localizeAttributes(String sourceName) {
		return this.i18nActivity.localizeAttributes(sourceName, Test.class);
	}

	/**
	 * returns the localized {@link Test} type name
	 * 
	 * @param sourceName name of the source (MDM {@link Environment} name)
	 * @return the localized {@link Test} type name
	 */
	@Deprecated
	public Map<EntityType, String> localizeType(String sourceName) {
		return this.i18nActivity.localizeType(sourceName, Test.class);
	}
	
	public Classification getClassification(String sourceName, Status status, ProjectDomain projectDomain, Domain domain) {
		Optional<QueryService> queryService = connectorService.getContextByName(sourceName).getQueryService();
		Optional<ModelManager> modelManager = connectorService.getContextByName(sourceName).getModelManager();
		
		EntityType entityType = modelManager.get().getEntityType(Classification.class);
		Condition c1 = ComparisonOperator.CASE_INSENSITIVE_EQUAL.create(entityType.getAttribute("ProjectDomain"), projectDomain.getID());
		Condition c2 = ComparisonOperator.CASE_INSENSITIVE_EQUAL.create(entityType.getAttribute("Domain"), domain.getID());
		Condition c3 = ComparisonOperator.CASE_INSENSITIVE_EQUAL.create(entityType.getAttribute("Status"), status.getID());
		
		java.util.List<Result> fetch = queryService.get().createQuery().selectID(entityType).fetch(Filter.and().addAll(c1, c2, c3));
		
		java.util.List<String> collect = fetch.stream()
		.map(r -> r.getRecord(entityType)).map(Record::getID).collect(Collectors.toList());
		
		EntityManager em = this.connectorService.getContextByName(sourceName).getEntityManager()
				.orElseThrow(() -> new MDMEntityAccessException("Entity manager not present!"));
		
		Classification classification = null;
		if (!collect.isEmpty()) {
			
			classification = em.load(Classification.class, collect.get(0));
		} else {
			StringBuilder className = new StringBuilder();
			className.append("ProjDomainId_");
			className.append(projectDomain.getID());
			className.append(".DomainId_");
			className.append(domain.getID());
			className.append(".StatusId_");
			className.append(status.getID());
			
			boolean classificationCreated = false;
			Transaction transaction = em.startTransaction();
			
			try {
				
				EntityFactory ef = this.connectorService.getContextByName(sourceName).getEntityFactory()
						.orElseThrow(() -> new MDMEntityAccessException("Entity factory not present!"));
				classification = ef.createClassification(className.toString(), status, projectDomain, domain);
				transaction.create(Lists.newArrayList(classification));
				transaction.commit();
				classificationCreated = true;
			} finally {
				if (!classificationCreated) {
					transaction.abort();
					throw new MDMEntityAccessException("Failed to create classification!");
				}
			}
			
		}
		
		
		return classification;
	}
}
