package org.eclipse.mdm.businessobjects.entity;

import java.util.Map;

public class Profile {
	private final String name;
	private final UserDetails applicationUserDetails;
	private final Map<String, UserDetails> dataSourceUserDetails;
	
	
	public Profile(final String name, final UserDetails applicationUserDetails, final Map<String, UserDetails> dataSourceUserDetails) {
		this.name = name;
		this.dataSourceUserDetails = dataSourceUserDetails;
		this.applicationUserDetails = applicationUserDetails;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the applicationUserDetails
	 */
	public UserDetails getApplicationUserDetails() {
		return applicationUserDetails;
	}

	/**
	 * @return the dataSourceUserDetails
	 */
	public Map<String, UserDetails> getDataSourceUserDetails() {
		return dataSourceUserDetails;
	}

}
