/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.freetextindexer.boundary;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.inject.Inject;

import org.eclipse.mdm.api.base.ServiceNotProvidedException;
import org.eclipse.mdm.api.base.adapter.EntityType;
import org.eclipse.mdm.api.base.adapter.ModelManager;
import org.eclipse.mdm.api.base.model.Channel;
import org.eclipse.mdm.api.base.model.ChannelGroup;
import org.eclipse.mdm.api.base.model.Entity;
import org.eclipse.mdm.api.base.model.Measurement;
import org.eclipse.mdm.api.base.model.Test;
import org.eclipse.mdm.api.base.model.TestStep;
import org.eclipse.mdm.api.base.model.User;
import org.eclipse.mdm.api.base.notification.NotificationException;
import org.eclipse.mdm.api.base.notification.NotificationFilter;
import org.eclipse.mdm.api.base.notification.NotificationFilter.ModificationType;
import org.eclipse.mdm.api.base.notification.NotificationListener;
import org.eclipse.mdm.api.base.notification.NotificationService;
import org.eclipse.mdm.api.base.query.ComparisonOperator;
import org.eclipse.mdm.api.base.query.DataAccessException;
import org.eclipse.mdm.api.base.query.Filter;
import org.eclipse.mdm.api.base.search.SearchService;
import org.eclipse.mdm.api.dflt.ApplicationContext;
import org.eclipse.mdm.api.dflt.EntityManager;
import org.eclipse.mdm.api.dflt.model.Pool;
import org.eclipse.mdm.api.dflt.model.Project;
import org.eclipse.mdm.connector.boundary.ConnectorService;
import org.eclipse.mdm.connector.boundary.GlobalConnectorService;
import org.eclipse.mdm.freetextindexer.control.DatabaseLockHandler;
import org.eclipse.mdm.freetextindexer.control.LockListener;
import org.eclipse.mdm.freetextindexer.control.UpdateIndex;
import org.eclipse.mdm.freetextindexer.entities.MDMEntityResponse;
import org.eclipse.mdm.property.GlobalProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This boundary is a back-end Boundary to the openMDM Api. It uses the Seach
 * Server to build up MDDocuments.
 *
 * @author CWE
 *
 */
@TransactionAttribute(value = TransactionAttributeType.NOT_SUPPORTED)
@Startup
@Singleton
public class MdmApiBoundary {

	private static final Logger LOGGER = LoggerFactory.getLogger(MdmApiBoundary.class);

	private static final String FREETEXT_NOTIFICATION_NAME = "freetext.notificationName";
	private static final String FREETEXT_SESSION_CHECK_INTERVAL = "freetext.sessionCheckInterval";

	public class FreeTextNotificationListener implements NotificationListener {
		private ApplicationContext context;
		private EntityManager entityManager;

		public FreeTextNotificationListener(ApplicationContext context, EntityManager entityManager) {
			this.context = context;
			this.entityManager = entityManager;
		}

		@Override
		public void instanceCreated(List<? extends Entity> entities, User arg1) {
			LOGGER.debug("{} entities created: {}", entities.size(), entities);
			entities.forEach(e -> update.change(MDMEntityResponse.build(e.getClass(), e, entityManager)));
		}

		@Override
		public void instanceDeleted(EntityType entityType, List<String> ids, User user) {
			LOGGER.debug("{} entities deleted: {}", ids.size(), ids);
			ids.forEach(id -> update.delete(context.getSourceName(), workaroundForTypeMapping(entityType), id));
		}

		@Override
		public void instanceModified(List<? extends Entity> entities, User arg1) {
			LOGGER.debug("{} entities modified: {}", entities.size(), entities);
			entities.forEach(e -> update.change(MDMEntityResponse.build(e.getClass(), e, entityManager)));
		}

		@Override
		public void modelModified(EntityType arg0, User arg1) {
			// not needed
		}

		@Override
		public void securityModified(EntityType entityType, List<String> ids, User user) {
			// not needed
		}
	}

	private static final List<Class<? extends Entity>> INDEXED_ENTITIES = Arrays.asList(Project.class, Pool.class,
			Test.class, TestStep.class, Measurement.class, ChannelGroup.class, Channel.class);

	@Inject
	UpdateIndex update;

	@Resource
	ManagedScheduledExecutorService scheduler;

	@Inject
	@GlobalProperty(value = "freetext.active")
	private String active = "false";

	ConnectorService connectorService;

	@Inject
	@GlobalProperty
	private Map<String, String> globalProperties = Collections.emptyMap();

	private ScheduledFuture<?> scheduledFuture;

	@EJB
	DatabaseLockHandler databaseLockHandler;

	@EJB
	private GlobalConnectorService globalConnectorService;

	@PostConstruct
	public void initalize() {
		if (!isActive()) {
			return;
		}

		databaseLockHandler.init("free-text-indexer", new LockListener() {

			@Override
			public void onLockAcquired() {
				LOGGER.info("Acquired database lock, starting fretextindexer.");

				connectorService = globalConnectorService.getConnectorService();
				connectorService.getContexts().forEach(MdmApiBoundary.this::initializeContext);

				long intervalSeconds = 60L;
				try {
					intervalSeconds = Long
							.parseLong(globalProperties.getOrDefault(FREETEXT_SESSION_CHECK_INTERVAL, "60"));
				} catch (NumberFormatException e) {
					LOGGER.warn("Could not parse value for parameter '" + FREETEXT_SESSION_CHECK_INTERVAL
							+ "'. Using default value '60'.");
				}

				scheduledFuture = MdmApiBoundary.this.scheduler.scheduleAtFixedRate(MdmApiBoundary.this::sessionCheck,
						intervalSeconds, intervalSeconds, TimeUnit.SECONDS);

				LOGGER.info("Initialized session tick timer with interval {}s.", intervalSeconds);
			}

			@Override
			public void onLockLost() {
				deregister();
			}
		});
		databaseLockHandler.requestLock();
	}

	/**
	 * Tries in intervals of one minute to acquire the database lock. If the lock
	 * could be acquired, the onLockAcquired method on the registered listener is
	 * invoked.
	 */
	@Schedule(hour = "*", minute = "*/1", persistent = false)
	public void checkLock() {
		if (isActive()) {
			databaseLockHandler.requestLock();
		} else {
			LOGGER.trace("freetext.active=false. No lock needs to be requested.");
		}
	}

	private void initializeContext(ApplicationContext context) {
		try {
			EntityManager entityManager = context.getEntityManager()
					.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));
			ModelManager modelManager = context.getModelManager()
					.orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));

			Optional<NotificationService> manager = context.getNotificationService();

			if (manager.isPresent()) {
				String notificationName = context.getParameters().getOrDefault(FREETEXT_NOTIFICATION_NAME, "mdm5");
				NotificationFilter notificationFilter = new NotificationFilter();
				notificationFilter.setTypes(EnumSet.of(ModificationType.INSTANCE_CREATED,
						ModificationType.INSTANCE_MODIFIED, ModificationType.INSTANCE_DELETED));
				notificationFilter.setEntityTypes(
						INDEXED_ENTITIES.stream().map(modelManager::getEntityType).collect(Collectors.toSet()));

				LOGGER.debug("Registering with name '{}' and filter '{}' at source '{}'", notificationName,
						notificationFilter, context.getSourceName());

				manager.get().register(notificationName, notificationFilter,
						new FreeTextNotificationListener(context, entityManager));
				LOGGER.info("Successfully registered for new notifications with name '{}' at source '{}!",
						notificationName, context.getSourceName());
			} else {
				LOGGER.debug("No NotificationManager found, cannot register at source '{}'", context.getSourceName());
			}
		} catch (NotificationException e) {
			throw new IllegalArgumentException(
					"The ODS Server and/or the Notification Service cannot be accessed for source '"
							+ context.getSourceName() + "'!",
					e);
		}
	}

	public void sessionCheck() {
		try {
			LOGGER.debug("Session check on {} context(s).", connectorService.getContexts().size());

			for (ApplicationContext context : connectorService.getContexts()) {
				try {
					Optional<EntityManager> em = context.getEntityManager();
					if (em.isPresent()) {
						em.get().loadLoggedOnUser();
					}
				} catch (Exception e) {
					LOGGER.warn("ApplicationContext seems to be closed. Trying to reconnect.");

					try {
						Optional<ApplicationContext> c = connectorService.reconnect(context);
						initializeContext(c.get());
						LOGGER.info("Freetextindexer ApplicationContext reconnected.");
					} catch (Exception ex) {
						LOGGER.info("Could not reconnect Freetextindexer ApplicationContext!", ex);
					}
				}
			}
		} catch (Throwable throwable) {
			LOGGER.error("Unexpected error occurred at MdmApiBoundary.sessionCheck()", throwable);
		}
	}

	@PreDestroy
	public void deregister() {
		if (isActive()) {
			scheduledFuture.cancel(true);

			for (ApplicationContext context : getContexts().values()) {
				context.getNotificationService().ifPresent(n -> {
					try {
						n.close(false);
					} catch (NotificationException e) {
						throw new IllegalStateException(
								"The NotificationManager could not be deregistered. In rare cases, this leads to a missed notification. This means the index might not be up-to-date.");
					}
				});
			}

			connectorService.disconnect();
		}
		if (databaseLockHandler.hasDatabaseLock()) {
			databaseLockHandler.releaseDatabaseLock();
		}
	}

	public void doForAllEntities(Class<? extends Entity> entityClass, ApplicationContext context,
			Consumer<? super MDMEntityResponse> executor) {
		if (isActive()) {
			try {
				EntityManager entityManager = context.getEntityManager()
						.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));

				entityManager.loadAll(entityClass).stream().map(e -> this.buildEntity(e, entityManager))
						.forEach(executor);

			} catch (DataAccessException e) {
				throw new IllegalStateException("MDM cannot be querried for new elements. Please check the MDM runtime",
						e);
			}
		}
	}

	public Map<String, ApplicationContext> getContexts() {
		if (connectorService == null) {
			return Collections.emptyMap();
		}
		return connectorService.getContexts().stream()
				.collect(Collectors.toMap(ApplicationContext::getSourceName, Function.identity()));
	}

	/**
	 * Load entites of the provided class within a certain DateCreated timeframe
	 * 
	 * @param entityClass    the entity class to load
	 * @param timeStampFrom  the upper time limit
	 * @param timeStampUntil the lower time limit
	 * @return List of entities that match all criteria
	 */
	public List<MDMEntityResponse> loadEntitiesOfTypeWithinTimeFrame(Class<? extends Entity> entityClass,
			Date timeStampFrom, Date timeStampUntil) {
		List<MDMEntityResponse> list = new ArrayList<>();
		for (ApplicationContext context : getContexts().values()) {
			try {
				EntityManager entityManager = context.getEntityManager()
						.orElseThrow(() -> new ServiceNotProvidedException(EntityManager.class));

				SearchService searchService = context.getSearchService()
						.orElseThrow(() -> new ServiceNotProvidedException(SearchService.class));
				ModelManager modelManager = context.getModelManager()
						.orElseThrow(() -> new ServiceNotProvidedException(ModelManager.class));
				EntityType entityType = modelManager.getEntityType(entityClass);

				Instant ldtFrom = timeStampFrom.toInstant();

				Filter filter = Filter.and().add(
						ComparisonOperator.LESS_THAN_OR_EQUAL.create(entityType.getAttribute("DateCreated"), ldtFrom));

				if (timeStampUntil != null) {
					Instant ldtUntil = timeStampUntil.toInstant();
					filter.add(ComparisonOperator.GREATER_THAN_OR_EQUAL.create(entityType.getAttribute("DateCreated"),
							ldtUntil));
				}

				List<? extends Entity> results = searchService.fetch(entityClass, filter).stream()
						.sorted(Comparator.comparing(entity -> entity.getValue("DateCreated").extract()))
						.collect(Collectors.toList());

				for (Entity result : results) {
					MDMEntityResponse response = this.buildEntity(result, entityManager);
					list.add(response);
				}

			} catch (DataAccessException e) {
				throw new IllegalStateException("MDM cannot be queried for new elements. Please check the MDM runtime",
						e);
			}
		}
		return list;
	}

	private boolean isActive() {
		return Boolean.parseBoolean(active);
	}

	private MDMEntityResponse buildEntity(Entity e, EntityManager entityManager) {
		return MDMEntityResponse.build(e.getClass(), e, entityManager);
	}

	/**
	 * Simple workaround for naming mismatch between Adapter and Business object
	 * names.
	 *
	 * @param entityType entity type
	 * @return MDM business object name
	 */
	private static String workaroundForTypeMapping(EntityType entityType) {
		switch (entityType.getName()) {
		case "StructureLevel":
			return "Pool";
		case "MeaResult":
			return "Measurement";
		case "SubMatrix":
			return "ChannelGroup";
		case "MeaQuantity":
			return "Channel";
		default:
			return entityType.getName();
		}
	}

}