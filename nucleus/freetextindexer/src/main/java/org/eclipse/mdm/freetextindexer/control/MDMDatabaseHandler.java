/********************************************************************************
 * Copyright (c) 2015, 2023 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

package org.eclipse.mdm.freetextindexer.control;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.eclipse.mdm.freetextindexer.entities.SystemProcess;

/**
 * MDM Dateabase handler to access preferences and system process tables
 */
@Stateful
public class MDMDatabaseHandler {

	@PersistenceContext(unitName = "openMDM")
	private EntityManager em;

	/**
	 * Load the preferences with a specific keycol value and no source or user
	 * 
	 * @param preferenceKey The preference key column
	 * @return Map of key-value pairs
	 */
	public Map<String, String> getPreferences(String preferenceKey) {
		Map<String, String> map = new HashMap<>();

		@SuppressWarnings("unchecked")
		List<Object[]> results = em
				.createNativeQuery("SELECT p.keycol, p.valuecol FROM preference p WHERE p.source IS NULL "
						+ "AND p.username IS NULL AND p.keycol LIKE '" + preferenceKey + "%'")
				.getResultList();

		if (results != null && results.size() > 0) {
			for (Object[] result : results) {
				map.put((String) result[0], (String) result[1]);
			}
		}

		return map;
	}

	/**
	 * Set the last runtime in the system process table
	 * 
	 * @param processKey The process key to use
	 * @param latestId   the last runtime with optional millisecond value to
	 *                   continue an open batch
	 */
	public void setLastRuntime(String processKey, String latestId) {
		List<SystemProcess> results = em
				.createQuery("SELECT sp FROM SystemProcess sp WHERE sp.processKey LIKE '" + processKey + "%'",
						SystemProcess.class)
				.getResultList();

		if (results != null && results.size() > 0) {
			for (SystemProcess result : results) {
				result.setProcessId(latestId);
				em.merge(result);
			}
		} else {
			SystemProcess systemProcess = new SystemProcess();
			systemProcess.setProcessId(latestId);
			systemProcess.setProcessKey(processKey);
			em.persist(systemProcess);
		}

	}

	/**
	 * Get the runtime values for a specific process key range
	 * 
	 * @param processKey The process key to load
	 * @return Map of key-value pairs
	 */
	public Map<String, String> getLastRuntimes(String processKey) {
		Map<String, String> map = new HashMap<>();
		List<SystemProcess> results = em
				.createQuery("SELECT sp FROM SystemProcess sp WHERE sp.processKey LIKE '" + processKey + "%'",
						SystemProcess.class)
				.getResultList();

		if (results != null && results.size() > 0) {
			for (SystemProcess result : results) {
				map.put(result.getProcessKey(), result.getProcessId());
			}
		}

		return map;
	}

}
